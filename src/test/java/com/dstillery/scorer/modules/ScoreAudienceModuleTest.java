package com.dstillery.scorer.modules;

import static com.dstillery.common.util.Constants.OBJECT_MAPPER;

import java.time.Instant;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.dstillery.common.audience.AudienceRow;
import com.dstillery.common.audience.Domain;
import com.dstillery.common.audience.models.CompoundAudience;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.feature.Feature;
import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.interest.Interest;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.featuredevice.FeatureHistory;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.util.StringUtil;
import com.dstillery.scorer.modules.compoundaudience.AudienceGroupQualifier;
import com.dstillery.scorer.modules.compoundaudience.AudienceRowQualifier;
import com.dstillery.scorer.modules.compoundaudience.matchers.DeviceClassMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.DeviceTypeMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.DomainMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.EntityMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.EnvironmentTypeMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.FirstPartySegmentMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.GpsCoordinatesMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.InterestMatcher;
import com.google.common.collect.ImmutableList;

@RunWith(MockitoJUnitRunner.class)
public class ScoreAudienceModuleTest {

    private static final Domain DOMAIN1 = new Domain("ski1.com");
    private static final Domain DOMAIN2 = new Domain("olympic.com");
    private static final Domain DOMAIN3 = new Domain("beauty.com");
    private ScoreCompoundAudiencesModule module;
    private MetricReporterService metricReporterService;

    @Mock
    private GpsCoordinatesMatcher gpsCoordinatesMatcher;

    @Mock
    private FirstPartySegmentMatcher firstPartySegmentMatcher;

    @Mock
    private DomainMatcher domainMatcher;

    @Mock
    private DeviceClassMatcher deviceClassMatcher;

    @Mock
    private DeviceTypeMatcher deviceTypeMatcher;

    @Mock
    private EnvironmentTypeMatcher environmentTypeMatcher;

    @Mock
    private InterestMatcher interestMatcher;

    @Mock
    private CacheService<AudienceRow> audienceCacheService;

    @Mock
    private CacheService<Interest> interestCacheService;

    @Mock
    private SegmentInsertionService segmentInsertionService;

    @Before
    public void setup() {

        ConcurrentHashMap<Long, Interest> interestMap = new ConcurrentHashMap<>();
        Interest interest3 = getInterest(110L, DOMAIN3);
        Interest interest1 = getInterest(135L, DOMAIN1);
        Interest interest2 = getInterest(334L, DOMAIN2);
        interestMap.put(interest1.getId(), interest1);
        interestMap.put(interest2.getId(), interest2);
        interestMap.put(interest3.getId(), interest3);
//        when(interestCacheService.getAllEntitiesMap()).thenReturn(interestMap);

        AudienceRow audienceRow = new AudienceRow();
        audienceRow.setCompoundAudience(getSampleCompoundAudience());
        audienceRow.setId(13L);
        //LOGGER.info("obj read : {}", obj.getAudiences());

//        when(audienceCacheService.getAllEntities())
//                .thenReturn(ImmutableList.of(audienceRow));

        EntityMatcher entityMatcher = new EntityMatcher(
                deviceClassMatcher,
                deviceTypeMatcher,
                environmentTypeMatcher,
                interestMatcher,
                gpsCoordinatesMatcher,
                firstPartySegmentMatcher,
                metricReporterService
        );
        AudienceRowQualifier audienceRowQualifier =
                new AudienceRowQualifier(metricReporterService,
                        new AudienceGroupQualifier(entityMatcher));

        module = new ScoreCompoundAudiencesModule(
                metricReporterService,
                audienceRowQualifier,
                audienceCacheService,
                segmentInsertionService);
    }

    @Test
    public void testParsingAudiencesMain() {
//
//        givenCompoundAudienceWithTwoGroups
//        module.process(context);
    }

    private Interest getInterest(Long id, Domain domain) {
        Interest interest = new Interest();
        interest.setId(id);
        interest.setInterestType(Interest.InterestType.CRAFTED_AUDIENCE);
        interest.setDomains(ImmutableList.of(domain));
        return interest;
    }

    private DeviceFeaturePair createDeviceFeatureHistory(String deviceId, String featureName) {
        Feature feature = getRandomFeature();
        feature.setName(featureName);
        DeviceFeaturePair deviceFeatureHistory =
                new DeviceFeaturePair(
                        new NomadiniDevice(
                                deviceId,
                                DeviceTypeValue.DISPLAY,
                                DeviceClassValue.DESKTOP)
                );

        deviceFeatureHistory.setFeatureHistory(
                new FeatureHistory(
                        feature,
                        Instant.now().toEpochMilli()
                ));
        return deviceFeatureHistory;
    }

    private CompoundAudience getSampleCompoundAudience() {
        String data =
                "{\"audiences\":[{\"title\":\"Audience Group 1\",\"groupOperand\":\"AND\",\"data\":[{\"id\":\"{\\\"type\\\": \\\"interest\\\", \\\"value\\\": \\\"135\\\"}\",\"text\":\"Ski and Snowboard Enthusiasts\"},{\"id\":\"{\\\"type\\\": \\\"interest\\\", \\\"value\\\": \\\"334\\\"}\",\"text\":\"Olympic Snowboarding and Freeskiing Fans\"},{\"id\":\"{\\\"type\\\": \\\"Device_Type\\\", \\\"value\\\": 200003}\",\"text\":\"IDFA\"},{\"text\":\"reza1.com\",\"id\":\"{\\\"type\\\": \\\"Domain\\\", \\\"value\\\": \\\"reza1.com\\\"}\"}]},{\"title\":\"Audience Group 2\",\"groupOperand\":\"OR\",\"data\":[{\"id\":\"{\\\"type\\\": \\\"interest\\\", \\\"value\\\": \\\"110\\\"}\",\"text\":\"Beauty & Skincare Buyers\"},{\"id\":\"{\\\"type\\\": \\\"Environment_Type\\\", \\\"value\\\": 300001}\",\"text\":\"App\"},{\"id\":\"{\\\"type\\\": \\\"Device_Type\\\", \\\"value\\\": 200002}\",\"text\":\"G_IDFA\"},{\"text\":\"ski1.com\",\"id\":\"{\\\"type\\\": \\\"Domain\\\", \\\"value\\\": \\\"ski1.com\\\"}\"}]}],\"outerOperand\":\"OR\"}";
        try {
            return OBJECT_MAPPER.readValue(
                    data, CompoundAudience.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Feature getRandomFeature() {
        return new Feature(FeatureType.GTLD,
                "feature" + StringUtil.randomString(6), Feature.FeatureStatus.ACTIVE);
    }
}

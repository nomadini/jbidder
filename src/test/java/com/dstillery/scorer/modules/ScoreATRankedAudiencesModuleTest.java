package com.dstillery.scorer.modules;

import static java.time.ZoneOffset.UTC;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.codahale.metrics.Counter;
import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.PlacesCachedResult;
import com.dstillery.common.TransAppMessage;
import com.dstillery.common.audience.Domain;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.eventlog.UniqueDeviceBidEventLog;
import com.dstillery.common.geo.places.Place;
import com.dstillery.common.geo.places.Tag;
import com.dstillery.common.interest.Interest;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentBuilder;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.segment.SegmentSourceType;
import com.dstillery.common.segment.SegmentSubType;
import com.dstillery.common.segment.SegmentType;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.scorer.main.ScorerContext;
import com.google.common.collect.ImmutableList;

@RunWith(MockitoJUnitRunner.class)
public class ScoreATRankedAudiencesModuleTest {

    private static final Double LAT1 = 1d;
    private static final Double LON1 = 1d;

    private static final String MGRS10_1 = "mgrs10_1";
    private static final long CREATED_SEGMENT_TIME = 1553284429000L;
    private static final String AT_1 = "AT_1";
    private static final Long PIXEL_ID_1 = 1L;
    private static final Long SEGMENT_ID = 2L;

    private ScorerContext context;

    @Mock
    private MetricReporterService metricReporterService;

    @Mock
    private SegmentInsertionService segmentInsertionService;

    @Mock
    private AeroCacheService<PlacesCachedResult> aerospikePlaceCache;

    @Mock
    private CassandraService<UniqueDeviceBidEventLog> deviceBidEventLogCassandraService;

    @Mock
    private DiscoveryService discoveryService;

    private ScoreATRankedAudiencesModule module;

    private Segment segment = new SegmentBuilder()
            .withName(AT_1)
            .withId(32L)
            .withDateCreatedInMillis(CREATED_SEGMENT_TIME)
            .withType(SegmentType.ACTION_TAKER)
            .withSubType(SegmentSubType.ONCE)
            .withSourceEntityId(PIXEL_ID_1)
            .withSourceType(SegmentSourceType.PIXEL)
            .withId(SEGMENT_ID).build();

    private final Integer numberOfTimesSeenInADayToBeQualified = 2;

    @Before
    public void setup() {

        SegmentCacheService segmentCacheService = new SegmentCacheService(
                discoveryService,
                "asd",
                null,
                "asd");
        segmentCacheService.getAllEntitiesMap().put(SEGMENT_ID, segment);

        module = new ScoreATRankedAudiencesModule(
                metricReporterService,
                segmentInsertionService,
                segmentCacheService,
                Clock.fixed(Instant.ofEpochMilli(CREATED_SEGMENT_TIME), UTC),
                numberOfTimesSeenInADayToBeQualified);
    }

    @Test
    public void testScoringATRankedSegments() {

        givenContextExists();
        givenEnoughATsegmentsExist();
        givenSegmentGetsCreatedInDb();

        whenModuleRuns();

        thenSegmentsCreatedAreAsExpected();
    }

    @Test
    public void testNotScoringATRankedSegments() {

        givenContextExists();
        givenNotEnoughATsegmentsExist();
        givenSegmentGetsCreatedInDb();

        whenModuleRuns();

        thenNotSegmentsAreCreated();
    }

    private void thenNotSegmentsAreCreated() {
        assertEquals(ImmutableList.of(), context.getAllSegmentsCreatedForDevice());
    }

    private void givenEnoughATsegmentsExist() {
        List<DeviceSegmentHistory> allSegmentsInThePastNDays = new ArrayList<>();
        for (int i = 0; i < numberOfTimesSeenInADayToBeQualified; i++) {
            allSegmentsInThePastNDays.add(getCreatedDeviceSegmentHistory());
        }

        context.setDeviceSegmentHistories(allSegmentsInThePastNDays);
    }

    private void givenNotEnoughATsegmentsExist() {
        List<DeviceSegmentHistory> allSegmentsInThePastNDays = new ArrayList<>();
        for (int i = 0; i < numberOfTimesSeenInADayToBeQualified - 1; i++) {
            allSegmentsInThePastNDays.add(getCreatedDeviceSegmentHistory());
        }

        context.setDeviceSegmentHistories(allSegmentsInThePastNDays);
    }

    private DeviceSegmentHistory getCreatedDeviceSegmentHistory() {
        DeviceSegmentHistory responseObject =
                new DeviceSegmentHistory(new NomadiniDevice("nomadini_1", DeviceTypeValue.DISPLAY, DeviceClassValue.DESKTOP));
        responseObject.setSegment(segment);
        return responseObject;
    }

    private void givenContextExists() {
        Map<Long, ModelRequest> allLoadedModels = givenModelsMap();

        givenScorerContext(allLoadedModels);
        givenDeviceFeatureHistories();
        context.getTransAppMessage().setMgrs10m(MGRS10_1);
        context.getTransAppMessage().setDeviceLat(LAT1);
        context.getTransAppMessage().setDeviceLon(LON1);

        when(metricReporterService.getCounter(any(), anyMap())).thenReturn(new Counter());
    }

    private void givenSegmentGetsCreatedInDb() {
        when(segmentInsertionService.writeAndGet(any())).thenAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            Segment segment = (Segment) args[0];
            segment.setId(SEGMENT_ID);
            return segment;
        });
    }

    private void thenSegmentsCreatedAreAsExpected() {
        Segment expectedSegment1 = createTopSegment(segment.getName(), PIXEL_ID_1);
        assertEquals(
                ImmutableList.of(expectedSegment1), context.getAllSegmentsCreatedForDevice());
    }

    private Segment createTopSegment(String segmentName, Long sourceEntityId) {
        return new SegmentBuilder()
                .withId(SEGMENT_ID)
                .withDateCreatedInMillis(CREATED_SEGMENT_TIME)
                .withName(segmentName + SegmentSubType.RANKED)
                .withType(SegmentType.ACTION_TAKER)
                .withSubType(SegmentSubType.RANKED)
                .withSourceEntityId(sourceEntityId)
                .withSourceType(SegmentSourceType.PIXEL).build();
    }

    private void addFirstTopPlace(
            Long placeId,
            Long tagId,
            String tagName,
            List<Place> allPlaces) {

        Place topPlace = new Place();
        topPlace.setId(placeId);
        Tag tag = new Tag();
        tag.setId(tagId);
        tag.setName(tagName);
        topPlace.setTags(ImmutableList.of(tag));
        allPlaces.add(topPlace);
    }

    private void whenModuleRuns() {
        module.process(context);
    }

    private void givenDeviceFeatureHistories() {
        List<DeviceFeaturePair> allFeatureHistories = new ArrayList<>();
        context.setDeviceFeatureHistory(allFeatureHistories);
    }

    private void givenScorerContext(Map<Long, ModelRequest> allLoadedModels) {
        TransAppMessage msg = new TransAppMessage();
        String deviceId = NomadiniDevice.createStandardDeviceId();
        msg.setDevice(new NomadiniDevice(deviceId, DeviceTypeValue.DISPLAY, DeviceClassValue.DESKTOP));
        context = new ScorerContext(msg, allLoadedModels);
    }

    private Map<Long, ModelRequest> givenModelsMap() {
        Map<Long, ModelRequest> allLoadedModels = new HashMap<>();
        ModelRequest modelRequest = new ModelRequest();
        modelRequest.setId(12L);
        allLoadedModels.put(modelRequest.getId(), modelRequest);
        return allLoadedModels;
    }

    private Interest getInterest(Long id, Domain domain) {
        Interest interest = new Interest();
        interest.setId(id);
        interest.setInterestType(Interest.InterestType.CRAFTED_AUDIENCE);
        interest.setDomains(ImmutableList.of(domain));
        return interest;
    }

}

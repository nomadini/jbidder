package com.dstillery.scorer.modules;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.scorer.modules.compoundaudience.matchers.DeviceClassMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.DeviceTypeMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.DomainMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.EntityMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.EnvironmentTypeMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.FirstPartySegmentMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.GpsCoordinatesMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.InterestMatcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

public class EntityMatcherTest {

    @Mock
    private GpsCoordinatesMatcher gpsCoordinatesMatcher;

    @Mock
    private FirstPartySegmentMatcher firstPartySegmentMatcher;


    @Mock
    private DeviceClassMatcher deviceClassMatcher;

    @Mock
    private DeviceTypeMatcher deviceTypeMatcher;

    @Mock
    private EnvironmentTypeMatcher environmentTypeMatcher;

    @Mock
    private InterestMatcher interestMatcher;
    private EntityMatcher entityMatcher;

    private MetricReporterService metricReporterService;

    @Before
    public void setup() {
        entityMatcher = new EntityMatcher(
                deviceClassMatcher,
                deviceTypeMatcher,
                environmentTypeMatcher,
                interestMatcher,
                gpsCoordinatesMatcher,
                firstPartySegmentMatcher,
                metricReporterService
        );
    }

    @Test
    public void testParsingAudiencesMain() {

    }
}

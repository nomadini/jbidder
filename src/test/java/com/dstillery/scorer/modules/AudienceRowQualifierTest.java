package com.dstillery.scorer.modules;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.scorer.modules.compoundaudience.AudienceGroupQualifier;
import com.dstillery.scorer.modules.compoundaudience.AudienceRowQualifier;
import com.dstillery.scorer.modules.compoundaudience.matchers.EntityMatcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

public class AudienceRowQualifierTest {

    private MetricReporterService metricReporterService = null;

    @Mock
    private EntityMatcher entityMatcher;

    private AudienceRowQualifier audienceRowQualifier;

    @Before
    public void setup() {
        audienceRowQualifier =
                new AudienceRowQualifier(metricReporterService,
                        new AudienceGroupQualifier(entityMatcher));

    }

    @Test
    public void testParsingAudiencesMain() {

    }
}

package com.dstillery.scorer.modules;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.codahale.metrics.Counter;
import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.PlacesCachedResult;
import com.dstillery.common.TransAppMessage;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.eventlog.UniqueDeviceBidEventLog;
import com.dstillery.common.geo.places.Place;
import com.dstillery.common.geo.places.PlaceFinderModule;
import com.dstillery.common.geo.places.Tag;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentBuilder;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.segment.SegmentNameCreator;
import com.dstillery.common.segment.SegmentSourceType;
import com.dstillery.common.segment.SegmentSubType;
import com.dstillery.common.segment.SegmentType;
import com.dstillery.scorer.main.ScorerContext;
import com.google.common.collect.ImmutableList;

@RunWith(MockitoJUnitRunner.class)
public class ScoreLocationPlaceTagHomeModuleTest {

    private static final Double LAT1 = 1d;
    private static final Double LON1 = 1d;
    private static final Long TOP_PLACE_ID = 13L;
    private static final Long TOP_PLACE_ID_1 = 14L;
    private static final Long TOP_PLACE_ID_2 = 15L;

    private static final Long TOP_TAG_ID = 12L;
    private static final Long TOP_TAG_ID_1 = 13L;
    private static final Long TOP_TAG_ID_2 = 14L;

    private static final String MGRS10_1 = "mgrs10_1";

    private static final String TOP_TAG_NAME = "TOP_TAG_NAME_1";
    private static final String TOP_TAG_NAME_1 = "TOP_TAG_NAME_2";
    private static final String TOP_TAG_NAME_2 = "TOP_TAG_NAME_3";

    private static final long CREATED_SEGMENT_TIME = 23;
    private Long SEGMENT_ID = 2L;
    private ScorerContext context;

    @Mock
    private MetricReporterService metricReporterService;

    @Mock
    private SegmentInsertionService segmentInsertionService;

    @Mock
    private AeroCacheService<PlacesCachedResult> aerospikePlaceCache;

    @Mock
    private PlaceFinderModule placeFinderModule;

    @Mock
    private CassandraService<UniqueDeviceBidEventLog> deviceBidEventLogCassandraService;

    private ScoreLocationPlaceTagHomeModule module;

    @Before
    public void setup() {
        Integer numberOfTopPlacesToPick = 2;
        module = new ScoreLocationPlaceTagHomeModule(
                metricReporterService,
                segmentInsertionService,
                aerospikePlaceCache,
                placeFinderModule,
                deviceBidEventLogCassandraService,
                Clock.fixed(Instant.ofEpochMilli(CREATED_SEGMENT_TIME), ZoneOffset.UTC),
                numberOfTopPlacesToPick);
    }

    @Test
    public void testScoringPlaceTagHomeSegments() {

        givenContextExists();
        givenEventLogsExistForDevice();
        givenPlacesAreFoundForLatLons();
        givenSegmentGetsCreatedInDb();

        whenModuleRuns();

        thenSegmentsCreatedAreAsExpected();
    }

    private void givenContextExists() {
        Map<Long, ModelRequest> allLoadedModels = givenModelsMap();

        givenScorerContext(allLoadedModels);
        givenDeviceFeatureHistories();
        context.getTransAppMessage().setMgrs10m(MGRS10_1);

        when(metricReporterService.getCounter(any(), anyMap())).thenReturn(new Counter());
    }

    private void givenEventLogsExistForDevice() {
        List<UniqueDeviceBidEventLog> eventLogs = new ArrayList<>();
        UniqueDeviceBidEventLog eventLog = new UniqueDeviceBidEventLog(new EventLog());
        eventLog.getEventLog().setDeviceLat(LAT1);
        eventLog.getEventLog().setDeviceLon(LON1);
        eventLog.getEventLog().setMgrs10m(MGRS10_1);
        eventLogs.add(eventLog);

        when(deviceBidEventLogCassandraService.readWithCustomQuery(anyString(), anyInt()))
                .thenReturn(eventLogs);
    }

    private void givenPlacesAreFoundForLatLons() {
        List<Place> allPlaces = new ArrayList<>();
        addFirstTopPlace(TOP_PLACE_ID, TOP_TAG_ID, TOP_TAG_NAME, allPlaces);
        addFirstTopPlace(TOP_PLACE_ID_1, TOP_TAG_ID_1, TOP_TAG_NAME_1, allPlaces);
        addFirstTopPlace(TOP_PLACE_ID_2, TOP_TAG_ID_2, TOP_TAG_NAME_2, allPlaces);

        for (long i = 0L; i < module.getMinNumberOfFrequencyToBePickedAsTop() - 2; i++) {
            Place notATopPlace = new Place();
            notATopPlace.setId(1L);
            Tag tag = new Tag();
            tag.setName("tag2");
            notATopPlace.setTags(ImmutableList.of(tag));
            allPlaces.add(notATopPlace);
        }
        when(placeFinderModule.findNearestFeatures(
                eq(LAT1),
                eq(LON1),
                anyInt(),
                anyDouble())
            ).thenReturn(allPlaces);
    }

    private void givenSegmentGetsCreatedInDb() {
        when(segmentInsertionService.writeAndGet(any())).thenAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            Segment segment = (Segment) args[0];
            segment.setId(SEGMENT_ID);
            return segment;
        });
    }

    private void thenSegmentsCreatedAreAsExpected() {
        Segment expectedSegment1 = createTopSegment(TOP_TAG_NAME, TOP_TAG_ID);
        Segment expectedSegment2 = createTopSegment(TOP_TAG_NAME_1, TOP_TAG_ID_1);
        assertEquals(
                ImmutableList.of(expectedSegment1, expectedSegment2), context.getAllSegmentsCreatedForDevice());
    }

    private Segment createTopSegment(String segmentName,
                                     Long topTagId) {
        return new SegmentBuilder()
                .withId(SEGMENT_ID)
                .withDateCreatedInMillis(CREATED_SEGMENT_TIME)
                .withName(
                        SegmentNameCreator.createName(
                                segmentName,
                                "",
                                SegmentType.LOCATION_POINT_OF_INTEREST))
                .withType(SegmentType.LOCATION_POINT_OF_INTEREST)
                .withSubType(SegmentSubType.HOMED) // we create a HOMED or once subtype
                .withSourceEntityId(topTagId)
                .withSourceType(SegmentSourceType.PLACE_TAG).build();
    }

    private void addFirstTopPlace(
            Long placeId,
            Long tagId,
            String tagName,
            List<Place> allPlaces) {

        for (long i = 0l; i < module.getMinNumberOfFrequencyToBePickedAsTop() + 2; i++) {
            Place topPlace = new Place();
            topPlace.setId(placeId);
            Tag tag = new Tag();
            tag.setId(tagId);
            tag.setName(tagName);
            topPlace.setTags(ImmutableList.of(tag));
            allPlaces.add(topPlace);
        }
    }

    private void whenModuleRuns() {
        module.process(context);
    }

    private void givenDeviceFeatureHistories() {
        List<DeviceFeaturePair> allFeatureHistories = new ArrayList<>();
        context.setDeviceFeatureHistory(allFeatureHistories);
    }

    private void givenScorerContext(Map<Long, ModelRequest> allLoadedModels) {
        TransAppMessage msg = new TransAppMessage();
        String deviceId = NomadiniDevice.createStandardDeviceId();
        msg.setDevice(new NomadiniDevice(deviceId, DeviceTypeValue.DISPLAY, DeviceClassValue.DESKTOP));
        context = new ScorerContext(msg, allLoadedModels);
    }

    private Map<Long, ModelRequest> givenModelsMap() {
        Map<Long, ModelRequest> allLoadedModels = new HashMap<>();
        ModelRequest modelRequest = new ModelRequest();
        modelRequest.setId(12L);
        allLoadedModels.put(modelRequest.getId(), modelRequest);
        return allLoadedModels;
    }

}

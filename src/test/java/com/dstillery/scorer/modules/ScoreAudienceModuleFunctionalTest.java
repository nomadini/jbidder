package com.dstillery.scorer.modules;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.dstillery.common.TransAppMessage;
import com.dstillery.common.audience.AudienceRow;
import com.dstillery.common.audience.Domain;
import com.dstillery.common.audience.models.CompoundAudience;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.feature.Feature;
import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.interest.Interest;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.featuredevice.FeatureHistory;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.util.StringUtil;
import com.dstillery.scorer.main.ScorerContext;
import com.dstillery.scorer.modules.compoundaudience.AudienceGroupQualifier;
import com.dstillery.scorer.modules.compoundaudience.AudienceRowQualifier;
import com.dstillery.scorer.modules.compoundaudience.matchers.DeviceClassMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.DeviceTypeMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.DomainMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.EntityMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.EnvironmentTypeMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.FirstPartySegmentMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.GpsCoordinatesMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.InterestMatcher;
import com.google.common.collect.ImmutableList;

@RunWith(MockitoJUnitRunner.class)
public class ScoreAudienceModuleFunctionalTest {

    private static final Domain DOMAIN1 = new Domain("ski1.com");
    private static final Domain DOMAIN2 = new Domain("olympic.com");
    private static final Domain DOMAIN3 = new Domain("beauty.com");
    private ScoreCompoundAudiencesModule module;
    private MetricReporterService metricReporterService;

    private DeviceClassMatcher deviceClassMatcher;
    private DeviceTypeMatcher deviceTypeMatcher;
    private EnvironmentTypeMatcher environmentTypeMatcher;
    private InterestMatcher interestMatcher;

    private GpsCoordinatesMatcher gpsCoordinatesMatcher;

    private FirstPartySegmentMatcher firstPartySegmentMatcher;


    @Mock
    private SegmentCacheService segmentCacheService;

    @Mock
    private CacheService<AudienceRow> audienceCacheService;

    @Mock
    private CacheService<Interest> interestCacheService;

    @Mock
    private SegmentInsertionService segmentInsertionService;

    @Before
    public void setup() {

        ConcurrentHashMap<Long, Interest> interestMap = new ConcurrentHashMap<>();
        Interest interest3 = getInterest(110L, DOMAIN3);
        Interest interest1 = getInterest(135L, DOMAIN1);
        Interest interest2 = getInterest(334L, DOMAIN2);
        interestMap.put(interest1.getId(), interest1);
        interestMap.put(interest2.getId(), interest2);
        interestMap.put(interest3.getId(), interest3);
        when(interestCacheService.getAllEntitiesMap()).thenReturn(interestMap);

        firstPartySegmentMatcher = new FirstPartySegmentMatcher(segmentCacheService);
        gpsCoordinatesMatcher = new GpsCoordinatesMatcher();
        deviceClassMatcher = new DeviceClassMatcher();
        deviceTypeMatcher = new DeviceTypeMatcher();
        environmentTypeMatcher = new EnvironmentTypeMatcher();
        interestMatcher = new InterestMatcher(interestCacheService, segmentCacheService);

        AudienceRow audienceRow = new AudienceRow();
        audienceRow.setCompoundAudience(getSampleCompoundAudience());
        audienceRow.setId(13L);
        //LOGGER.info("obj read : {}", obj.getAudiences());

        when(audienceCacheService.getAllEntities())
                .thenReturn(ImmutableList.of(audienceRow));

        EntityMatcher entityMatcher = new EntityMatcher(
                deviceClassMatcher,
                deviceTypeMatcher,
                environmentTypeMatcher,
                interestMatcher,
                gpsCoordinatesMatcher,
                firstPartySegmentMatcher,
                metricReporterService
        );
        AudienceRowQualifier audienceRowQualifier =
                new AudienceRowQualifier(metricReporterService,
                new AudienceGroupQualifier(entityMatcher));

        module = new ScoreCompoundAudiencesModule(
                metricReporterService,
                audienceRowQualifier,
                audienceCacheService,
                segmentInsertionService);
    }

    @Test
    public void testParsingAudiences() {
        try {
            Map<Long, ModelRequest> allLoadedModels = givenModelsMap();

            ScorerContext context = givenScorerContext(allLoadedModels);
            givenDeviceFeatureHistories(context);

            whenModuleRuns(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void whenModuleRuns(ScorerContext context) {
        module.process(context);
    }

    private void givenDeviceFeatureHistories(ScorerContext context) {
        List<DeviceFeaturePair> allFeatureHistories = new ArrayList<>();

        ImmutableList.of(DOMAIN1.getName(), DOMAIN2.getName(), DOMAIN3.getName())
                .forEach(featureName -> {
                    DeviceFeaturePair deviceFeatureHistory =
                            createDeviceFeatureHistory(
                                    context.getTransAppMessage().getDevice().getDeviceId(), featureName);
                    allFeatureHistories.add(deviceFeatureHistory);
                });

        context.setDeviceFeatureHistory(allFeatureHistories);
    }

    private ScorerContext givenScorerContext(Map<Long, ModelRequest> allLoadedModels) {
        TransAppMessage msg = new TransAppMessage();
        String deviceId = NomadiniDevice.createStandardDeviceId();
        msg.setDevice(new NomadiniDevice(deviceId, DeviceTypeValue.DISPLAY, DeviceClassValue.DESKTOP));
        return new ScorerContext(msg, allLoadedModels);
    }

    private Map<Long, ModelRequest> givenModelsMap() {
        Map<Long, ModelRequest> allLoadedModels = new HashMap<>();
        ModelRequest modelRequest = new ModelRequest();
        modelRequest.setId(12L);
        allLoadedModels.put(modelRequest.getId(), modelRequest);
        return allLoadedModels;
    }

    private Interest getInterest(Long id, Domain domain) {
        Interest interest = new Interest();
        interest.setId(id);
        interest.setInterestType(Interest.InterestType.CRAFTED_AUDIENCE);
        interest.setDomains(ImmutableList.of(domain));
        return interest;
    }

    private DeviceFeaturePair createDeviceFeatureHistory(String deviceId, String featureName) {
        Feature feature = getRandomFeature();
        feature.setName(featureName);
        DeviceFeaturePair deviceFeatureHistory =
                new DeviceFeaturePair(
                        new NomadiniDevice(
                                deviceId,
                                DeviceTypeValue.DISPLAY,
                                DeviceClassValue.DESKTOP)
                );

        deviceFeatureHistory.setFeatureHistory(
                new FeatureHistory(
                        feature,
                        Instant.now().toEpochMilli()
                ));
        return deviceFeatureHistory;
    }

    private CompoundAudience getSampleCompoundAudience() {
        String data =
                "{\"audiences\":[{\"title\":\"Audience Group 1\",\"groupOperand\":\"AND\",\"data\":[{\"id\":\"{\\\"type\\\": \\\"interest\\\", \\\"value\\\": \\\"135\\\"}\",\"text\":\"Ski and Snowboard Enthusiasts\"},{\"id\":\"{\\\"type\\\": \\\"interest\\\", \\\"value\\\": \\\"334\\\"}\",\"text\":\"Olympic Snowboarding and Freeskiing Fans\"},{\"id\":\"{\\\"type\\\": \\\"Device_Type\\\", \\\"value\\\": 200003}\",\"text\":\"IDFA\"},{\"text\":\"reza1.com\",\"id\":\"{\\\"type\\\": \\\"Domain\\\", \\\"value\\\": \\\"reza1.com\\\"}\"}]},{\"title\":\"Audience Group 2\",\"groupOperand\":\"OR\",\"data\":[{\"id\":\"{\\\"type\\\": \\\"interest\\\", \\\"value\\\": \\\"110\\\"}\",\"text\":\"Beauty & Skincare Buyers\"},{\"id\":\"{\\\"type\\\": \\\"Environment_Type\\\", \\\"value\\\": 300001}\",\"text\":\"App\"},{\"id\":\"{\\\"type\\\": \\\"Device_Type\\\", \\\"value\\\": 200002}\",\"text\":\"G_IDFA\"},{\"text\":\"ski1.com\",\"id\":\"{\\\"type\\\": \\\"Domain\\\", \\\"value\\\": \\\"ski1.com\\\"}\"}]}],\"outerOperand\":\"OR\"}";
        try {
            return OBJECT_MAPPER.readValue(
                    data, CompoundAudience.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Feature getRandomFeature() {
        return new Feature(FeatureType.GTLD,
                "feature" + StringUtil.randomString(6), Feature.FeatureStatus.ACTIVE);
    }
}

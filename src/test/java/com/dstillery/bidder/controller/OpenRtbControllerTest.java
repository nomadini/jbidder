package com.dstillery.bidder.controller;

import com.codahale.metrics.MetricRegistry;
import com.dstillery.openrtb.beans.BidRequest;
import com.dstillery.openrtb.beans.BidResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class OpenRtbControllerTest {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final String OPEN_RTB_BID_REQUEST_URL = "/openrtb";

    private MvcResult mvcResult;

    private OpenRtbController openRtbController;

    private MockMvc mockMvc;
    private BidResponse response;

    @Before
    public void setupController() {
        response = new BidResponse();
        this.openRtbController = new OpenRtbController(new MetricRegistry());
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.openRtbController)
                .setHandlerExceptionResolvers()
                .build();
    }

    @Test
    public void testGettingTwoWeekEstimate() throws Exception {
        whenRequestingTwoWeekEstimate();

        thenResponseIsAsExpected();
    }

    private void whenRequestingTwoWeekEstimate() throws Exception {
        this.mvcResult = this.mockMvc.perform(post(OPEN_RTB_BID_REQUEST_URL)
                .content(OBJECT_MAPPER.writeValueAsString(new BidRequest()))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
    }

    private void thenResponseIsAsExpected() throws Exception {
        String receivedJSON = mvcResult.getResponse().getContentAsString();
        String expectedJSON = OBJECT_MAPPER.writeValueAsString(response);

        assertThat(receivedJSON, equalTo(expectedJSON));
    }
}

package com.dstillery.modeler;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.feature.Feature;
import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.devicefeature.DeviceHistorySet;
import com.dstillery.common.modeling.featuredevice.FeatureHistory;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.common.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class ModelerTestUtil {

    public static ModelerContext createContext() {
        ModelRequest modelRequest = new ModelRequest();
        ModelerContext context = new ModelerContext(modelRequest);
        context.setPositiveDeviceHistories(createDeviceHistories(true));
        context.setNegativeDeviceHistories(createDeviceHistories(false));
        context.setAllDeviceHistories(context.combineDeviceHistories());
        return context;
    }

    public static List<DeviceHistorySet> createDeviceHistories(boolean positive) {

        List<DeviceHistorySet> positiveDeviceHistories =
                new ArrayList<>();
        for (int i = 0; i < 200; i++) {
            List<DeviceFeaturePair> positiveDevice = new ArrayList<>();
            for (int j = 0; j < 5; j++) {
                String featureName = StringUtil.randomStringPure(1);
                for (int numberOfFeaturesOccurence = 0;  numberOfFeaturesOccurence < 4; numberOfFeaturesOccurence ++) {
                    DeviceFeaturePair dfh = new DeviceFeaturePair(
                            new NomadiniDevice(
                                    "nomadini" + StringUtil.randomString(10),
                                    DeviceTypeValue.DISPLAY,
                                    DeviceClassValue.DESKTOP));

                    dfh.setFeatureHistory(new FeatureHistory(
                            new Feature(FeatureType.GTLD,
                                    featureName, Feature.FeatureStatus.ACTIVE),
                            DateTimeUtil.getNowInMilliSecond()
                    ));
                    if (positive) {
                        dfh.setPositiveHistoryValue(positive);
                    }
                    positiveDevice.add(dfh);
                }

            }
            positiveDeviceHistories.add(new DeviceHistorySet(positiveDevice));
        }

        return positiveDeviceHistories;
    }
}

package com.dstillery.modeler.engine;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.modeler.ModelerContext;
import org.junit.Test;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.dstillery.modeler.ModelerTestUtil.createContext;

public class ModelDataCleanerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelDataCleaner.class);

    @Mock
    private MetricReporterService metricReporterService;
    private ModelDataCleaner modelDataCleaner =
            new ModelDataCleaner(metricReporterService);
    @Test
    public void testCleaningUpData() {
        ModelerContext context = createContext();
        modelDataCleaner.process(context);
        LOGGER.debug("context.getPositiveDeviceHistories().size() : {}", context.getPositiveDeviceHistories().size());
        LOGGER.debug("context.getNegativeDeviceHistories().size() : {}", context.getNegativeDeviceHistories().size());
    }
}

package com.dstillery.modeler.engine;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.modeler.ModelerContext;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.dstillery.modeler.ModelerTestUtil.createContext;

public class FeatureBasedModelerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureBasedModeler.class);

    private MetricReporterService metricReporterService = null;
    private FeatureValidator featureValidator = new FeatureValidator();
    private FeatureWeightToModelConverter featureWeightToModelConverter = new FeatureWeightToModelConverter();
    private FeatureWeightCalculator featureWeightCalculator = new FeatureWeightCalculator(metricReporterService);

    private FeatureBasedModeler featureBasedModeler =
            new FeatureBasedModeler(
                    metricReporterService,
                    featureValidator,
                    featureWeightToModelConverter,
                    featureWeightCalculator
            );
    @Test
    public void testCreatingModel() {
        ModelerContext context = createContext();
        featureBasedModeler.process(context);
        LOGGER.debug("context.getPositiveDeviceHistories().size() : {}", context.getPositiveDeviceHistories().size());
        LOGGER.debug("context.getNegativeDeviceHistories().size() : {}", context.getNegativeDeviceHistories().size());
    }
}

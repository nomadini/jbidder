#!/bin/bash

# Unlike the build-time scripts, there is an argument defined. The sole argument to these scripts,
# is a number representing the number of instances of the package currently installed on the system,
# after the current package has been installed or erased.

if [ "$1" -eq 0 ]; then
    rm -r /opt/counts-api
fi
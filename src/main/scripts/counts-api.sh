#!/usr/bin/env bash

##############################################################################
##
##  counts api server start up script for UN*X
##
##############################################################################

# Add default JVM options here. You can also use JAVA_OPTS and COUNTS_API_OPTS to
# pass JVM options to this script.
DEFAULT_JVM_OPTS="-Dcom.sun.management.jmxremote \
                  -Dcom.sun.management.jmxremote.port=10703 \
                  -Dcom.sun.management.jmxremote.local.only=false \
                  -Dcom.sun.management.jmxremote.authenticate=false \
                  -Dcom.sun.management.jmxremote.ssl=false \
                  -Djava.library.path='/usr/lib:/usr/local/lib'
                  -Djsse.enableSNIExtension=false"

APP_NAME="Counts Api"
APP_BASE_NAME=`basename "$0"`
APP_JAR=${rpm.install.basedir}/${project.build.finalName}.jar
echo $APP_JAR
die ( ) {
    echo
    echo "$*"
    echo
    exit 1
}

# Attempt to set APP_HOME
# Resolve links: $0 may be a link
PRG="$0"
# Need this for relative symlinks.
while [ -h "$PRG" ] ; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`"/$link"
    fi
done
SAVED="`pwd`"
cd "`dirname \"$PRG\"`/.." >&-
APP_HOME="`pwd -P`"
cd "$SAVED" >&-

# Determine the Java command to use to start the JVM.
if [ -n "$JAVA_HOME" ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
        # IBM's JDK on AIX uses strange locations for the executables
        JAVACMD="$JAVA_HOME/jre/sh/java"
    else
        JAVACMD="$JAVA_HOME/bin/java"
    fi
    if [ ! -x "$JAVACMD" ] ; then
        die "ERROR: JAVA_HOME is set to an invalid directory: $JAVA_HOME

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
    fi
else
    JAVACMD="java"
    which java >/dev/null 2>&1 || die "ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
fi

# Split up the JVM_OPTS And COUNTS_API_OPTS values into an array, following the shell quoting and substitution rules
function splitJvmOpts() {
    JVM_OPTS=("$@")
}

eval splitJvmOpts $DEFAULT_JVM_OPTS $JAVA_OPTS $COUNTS_API_OPTS

exec "$JAVACMD" "${JVM_OPTS[@]}" -jar /opt/counts-api/counts-api.jar "$@" >> /var/log/counts-api/counts-api.log
package com.dstillery.bidder.pipeline;


import static com.dstillery.common.util.StringUtil.toStr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.modules.ActiveAndCurrentFilterModule;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.BiddingStatusFilterModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TargetGroupWithoutCreativeFilterModule;
import com.dstillery.bidder.modules.TargetGroupWithoutSegmentFilterModule;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.bidder.modules.budgetcontrol.capping.CampaignBudgetCappingFilterModule;
import com.dstillery.bidder.modules.budgetcontrol.capping.CampaignImpressionCappingFilterModule;
import com.dstillery.bidder.modules.budgetcontrol.capping.TargetGroupBudgetCappingFilterModule;
import com.dstillery.bidder.modules.budgetcontrol.capping.TargetGroupImpressionCappingFilterModule;
import com.dstillery.bidder.modules.budgetcontrol.pacing.TargetGroupPacingByBudgetModule;
import com.dstillery.bidder.modules.budgetcontrol.pacing.TargetGroupPacingByImpressionModule;
import com.dstillery.bidder.modules.filters.DayPartFilter;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.targetgroup_maps.tgCreative.TargetGroupCreativeCacheService;
import com.dstillery.common.targetgroup.targetgroup_maps.tgDaypart.TargetGroupDayPartCacheService;

/**
 Thsi pipeline will be called after we reload data
 */
public class DataReloadPipeline {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataReloadPipeline.class);

    private final MetricReporterService metricReporterService;
    private List<BidderModule> modules = new ArrayList<>();public DataReloadPipeline(EntityDeliveryInfoCacheService entityDeliveryInfoCacheService,
                              TargetGroupCreativeCacheService targetGroupCreativeCacheService,
                              CampaignCacheService campaignCacheService,
                              ActiveAndCurrentFilterModule activeAndCurrentFilterModule,
                              BiddingStatusFilterModule biddingStatusFilterModule,
                              TargetGroupWithoutSegmentFilterModule targetGroupWithoutSegmentFilterModule,
                              TargetGroupDayPartCacheService targetGroupDayPartCacheService,
                              TargetGroupPacingByBudgetModule targetGroupPacingByBudgetModule,
                              TargetGroupPacingByImpressionModule targetGroupPacingByImpressionModule,
                              DayPartFilter dayPartFilter,
                              CampaignBudgetCappingFilterModule campaignBudgetCappingFilterModule,
                              CampaignImpressionCappingFilterModule campaignImpressionCappingFilterModule,
                              TargetGroupWithoutCreativeFilterModule targetGroupWithoutCreativeFilterModule,
                              TargetGroupImpressionCappingFilterModule targetGroupImpressionCappingFilterModule,
                              TargetGroupBudgetCappingFilterModule targetGroupBudgetCappingFilterModule,
                              MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        addToModuleList (activeAndCurrentFilterModule);
        addToModuleList (biddingStatusFilterModule);
        addToModuleList (targetGroupPacingByBudgetModule);
        addToModuleList (targetGroupPacingByImpressionModule);
        addToModuleList (dayPartFilter);
        addToModuleList (campaignImpressionCappingFilterModule);
        addToModuleList (campaignBudgetCappingFilterModule);
        addToModuleList (targetGroupImpressionCappingFilterModule);
        addToModuleList (targetGroupBudgetCappingFilterModule);

        //this will filter out all target groups that don't have any assigned creatives
        addToModuleList (targetGroupWithoutCreativeFilterModule);
        addToModuleList (targetGroupWithoutSegmentFilterModule);
    }

    private void addToModuleList(BidderModule module) {
        this.modules.add(module);
    }

    public List<TargetGroup> process(List<TargetGroup>  tgCandidates) {
        //LOGGER.error("tgCandidates are null";
        OpportunityContext context = new OpportunityContext();
        for (TargetGroup tg : tgCandidates) {
            context.getAllAvailableTgs().add(tg);
        }

        metricReporterService.addStateModuleForEntity("BEGINNING_OF_PIPELINE:#OfAvailableTargetGroupsAtStart = "
                        + toStr(context.getSizeOfUnmarkedTargetGroups()),
                "DataReloadPipeline",
                "ALL");

        for (BidderModule module : modules) {
            int beforeTgSize = context.getSizeOfUnmarkedTargetGroups();

            try {
                module.process(context);
            } catch (IOException e) {
                LOGGER.warn("exception occurred", e);
            }

            List<TargetGroup> afterFilteringTgs = TgMarker.getPassingTargetGroups(context);

            LOGGER.info("DataReloadPipeline ran {}, left {} available targetgroups, passing tg size : {}, passing tgs : {}",
                    module.getClass().getSimpleName(),
                    context.getSizeOfUnmarkedTargetGroups(),
                    afterFilteringTgs.size(),
                    afterFilteringTgs.stream().map(targetGroup -> targetGroup.getId() + " : " + targetGroup.getName()).collect(Collectors.toList()));

            metricReporterService.addStateModuleForEntityWithValue(module.getClass().getSimpleName() + ":AfterTgSize",
                    context.getSizeOfUnmarkedTargetGroups(),
                    "DataReloadPipeline",
                    "ALL");

        }
        List<TargetGroup> tgsLeftForBidding = TgMarker.getPassingTargetGroups(context);
        List<TargetGroup> allTargetGroups = new ArrayList<>(tgsLeftForBidding);

        metricReporterService.addStateModuleForEntity("END_OF_PIPELINE:#OfAvailableTargetGroupsAtEnd = "
                        + toStr(allTargetGroups.size()),
                "DataReloadPipeline",
                "ALL");
        return allTargetGroups;
    }
}

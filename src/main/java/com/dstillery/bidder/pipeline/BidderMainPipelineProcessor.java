package com.dstillery.bidder.pipeline;




import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static com.dstillery.common.util.StringUtil.equalsIgnoreCase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Timer;
import com.dstillery.bidder.modules.AsyncPipelineFeederAndInvokerModule;
import com.dstillery.bidder.modules.BidResponseCreatorModule;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.BidderResponseModule;
import com.dstillery.bidder.modules.EventLogCreatorModule;
import com.dstillery.bidder.modules.GicapodsIdToExchangeIdMapModule;
import com.dstillery.bidder.modules.GlobalWhiteListModule;
import com.dstillery.bidder.modules.MGRSBuilderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TargetGroupFrequencyCapModule;
import com.dstillery.bidder.modules.TargetGroupSegmentFilter;
import com.dstillery.bidder.modules.TargetGroupSelectorModule;
import com.dstillery.bidder.modules.TopKeyWordsExtractorModule;
import com.dstillery.bidder.modules.budgetcontrol.capping.TargetingTypeLessTargetGroupFilter;
import com.dstillery.bidder.modules.filters.TargetGroupBudgetFilterChainModule;
import com.dstillery.bidder.modules.filters.TargetGroupFilterChainModule;
import com.dstillery.bidder.modules.frauddetection.BadDeviceByCountFilterModule;
import com.dstillery.bidder.modules.frauddetection.BadIpByCountFilterModule;
import com.dstillery.bidder.modules.frauddetection.LastXSecondBidOnDeviceModule;
import com.dstillery.bidder.modules.frauddetection.LastXSecondSeenIpVisitorModule;
import com.dstillery.bidder.modules.pricing.BidPriceCalculatorModule;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.ProcessingStatus;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.targetgroup.TargetGroup;

public class BidderMainPipelineProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidderMainPipelineProcessor.class);
    private final TargetGroupFilterChainModule targetGroupFilterChainModule;

    private int acceptableModuleLatencyInMillis;

    private MetricReporterService metricReporterService;
    private ConfigService configService;
    private List<BidderModule> initialEssentialModules = new ArrayList<>();
    private List<BidderModule> preBidModules = new ArrayList<>();

    //the modules that will run only after we make a bid
    private List<BidderModule> postBidModules = new ArrayList<>();

    //the modules that will run at the final stage
    private List<BidderModule> finalStageModules = new ArrayList<>();
    private AtomicLong numberProcessorCalls;
    private GicapodsIdToExchangeIdMapModule gicapodsIdToExchangeIdMapModule;
    private GlobalWhiteListModule globalWhiteListModule;
    private TargetGroupSegmentFilter targetGroupSegmentFilter;
    private TargetGroupFrequencyCapModule targetGroupFrequencyCapModule;
    private TopKeyWordsExtractorModule topKeyWordsExtractorModule;
    private static final String CONFIRMED_WINS_SERVLET_PATH = "/confirmedwins";

    public BidderMainPipelineProcessor(
            TargetGroupFilterChainModule targetGroupFilterChainModule,
            MetricReporterService metricReporterService,
            TopKeyWordsExtractorModule topKeyWordsExtractorModule,
            MGRSBuilderModule mgrsBuilderModule,
            LastXSecondBidOnDeviceModule lastXSecondBidOnDeviceModule,
            LastXSecondSeenIpVisitorModule lastXSecondSeenIpVisitorModule,
            BadIpByCountFilterModule badIpByCountFilterModule,
            BadDeviceByCountFilterModule badDeviceByCountFilterModule,
            TargetGroupBudgetFilterChainModule targetGroupBudgetFilterChainModule,
            BidPriceCalculatorModule bidPriceCalculatorModule,
            TargetingTypeLessTargetGroupFilter targetingTypeLessTargetGroupFilter,
            TargetGroupSelectorModule targetGroupSelectorModule,
            EventLogCreatorModule eventLogCreatorModule,
            AsyncPipelineFeederAndInvokerModule asyncPipelineFeederAndInvokerModule,
            GicapodsIdToExchangeIdMapModule gicapodsIdToExchangeIdMapModule,
            GlobalWhiteListModule globalWhiteListModule,
            TargetGroupSegmentFilter targetGroupSegmentFilter,
            TargetGroupFrequencyCapModule targetGroupFrequencyCapModule,
            BidderResponseModule bidderResponseModule,
            BidResponseCreatorModule bidResponseCreatorModule,
            int acceptableModuleLatencyInMillis,
            ConfigService configService,
            DiscoveryService discoveryService

    )  {
        this.numberProcessorCalls = new AtomicLong();
        this.acceptableModuleLatencyInMillis = acceptableModuleLatencyInMillis;
        this.targetGroupFilterChainModule = targetGroupFilterChainModule;
        this.metricReporterService = metricReporterService;
        this.configService = configService;

        this.gicapodsIdToExchangeIdMapModule = gicapodsIdToExchangeIdMapModule;
        this.globalWhiteListModule = globalWhiteListModule;
        this.targetGroupSegmentFilter = targetGroupSegmentFilter;
        this.targetGroupFrequencyCapModule = targetGroupFrequencyCapModule;

        addToInitialEssentialModules(new BidderModule() {
            // this is a tiny module that sets the required variables
            @Override
            public void process(OpportunityContext context) throws IOException {
                try {
                    context.setBidderCallbackServletUrl(discoveryService.discoverService("bidder") + CONFIRMED_WINS_SERVLET_PATH);
                } catch (Exception e) {
                    context.stopProcessing("bidder_win_link_setter");
                    //ignoring the exception but setting it to no bid
                }
            }
        });
        addToInitialEssentialModules(mgrsBuilderModule);
        addToInitialEssentialModules(topKeyWordsExtractorModule);

        // this checks if user has been seen in last n seconds, we don't bid on it
        addToPreBidModuleList(lastXSecondBidOnDeviceModule);
        addToPreBidModuleList(lastXSecondSeenIpVisitorModule);
        addToPreBidModuleList(badIpByCountFilterModule);
        addToPreBidModuleList(badDeviceByCountFilterModule);

        addTargetingModules();

        addToPreBidModuleList(targetGroupBudgetFilterChainModule);

        addToPreBidModuleList(bidPriceCalculatorModule);

        //this has to run before targetGroupSelectorModule always
        addToPreBidModuleList(targetingTypeLessTargetGroupFilter);
        addToPreBidModuleList(targetGroupSelectorModule);

        /*
           Post Bid modules, these modules run only if we are bidding
         */
        addToPostBidModuleList(eventLogCreatorModule);
        addToPostBidModuleList(bidderResponseModule);
        addToFinalStageModuleList(bidResponseCreatorModule);
        //async pipeline mighe need some data like places that we might get in the GeoFeatureFilter
        //so we run it as a finalStageModule, or some modules like BidEventRecorderModule that
        //need to record a 'bid' event sometimes

        addToFinalStageModuleList(asyncPipelineFeederAndInvokerModule);
    }

    private void addToInitialEssentialModules(BidderModule module) {
        this.initialEssentialModules.add(module);
    }

    private void addTargetingModules() {
        //some of the preBidModules here belong to bidValidation pipeline
        addToPreBidModuleList(gicapodsIdToExchangeIdMapModule);

        // makes sure we only bid on certain websites
        addToPreBidModuleList(globalWhiteListModule);

        addToPreBidModuleList(targetGroupSegmentFilter);

        addToPreBidModuleList(targetGroupFilterChainModule);

        addToPreBidModuleList(targetGroupFrequencyCapModule);
    }

    private void addToPreBidModuleList(BidderModule module) {

        String moduleName = module.getClass().getSimpleName();
        if (configService.contains(moduleName + "IsEnabled") &&
                equalsIgnoreCase(configService.get(moduleName + "IsEnabled"), "false")) {
            logExclusion(moduleName, metricReporterService);
        } else {
            this.preBidModules.add(module);
        }
    }

    public static void logExclusion(String moduleName,
                                    MetricReporterService metricReporterService) {
        LOGGER.error("excluding the {} module", moduleName);
        //this is a very important change, we need to add it to the exceptions
        //so we know whats functionality is being excluded
        metricReporterService.addStateModuleForEntity(
                "excluding_" + moduleName,
                "BidderMainPipelineProcessor",
                ALL_ENTITIES,
                MetricReporterService.MetricPriority.EXCEPTION
        );
    }

    private void addToPostBidModuleList(BidderModule module) {
        this.postBidModules.add(module);
    }

    private void addToFinalStageModuleList(BidderModule module) {

        String moduleName = module.getClass().getSimpleName();
        if (configService.contains(moduleName + "IsEnabled") &&
                equalsIgnoreCase(configService.get(moduleName + "IsEnabled"), "false")) {
            logExclusion(moduleName, metricReporterService);
        } else {
            this.finalStageModules.add(module);
        }
    }

    public void process(OpportunityContext context) {
        numberProcessorCalls.getAndIncrement();
        context.setAcceptableModuleLatencyInMillis(acceptableModuleLatencyInMillis);
        metricReporterService.addStateModuleForEntity("#OfProcessingBidRequests",
                "BidderMainPipelineProcessor",
                "ALL");


        //this is very important to know which targetgroups are being processed in the
        //main pipeline
        for (TargetGroup tg : context.getAllAvailableTgs()) {
            metricReporterService.addStateModuleForEntity(
                    String.valueOf(tg.getId()),
                    "BidderMainPipelineProcessor-tg-for-considerations",
                    "ALL");

        }

        for (BidderModule module : initialEssentialModules) {
            try {
                runModule(module, context, metricReporterService);
            } catch (Exception e) {

                LOGGER.warn("exception occurred", e);
                throw new RuntimeException("EXCEPTION_WHILE_RUNNING_MODULE", e);
            }
        }

        for (BidderModule module : preBidModules) {
            try {

                if (context.getStatus() == ProcessingStatus.STOP_PROCESSING
                    || context.getDevice() == null) {
                    //sometimes when building context...
                    //we dont get ip info or we dont get user timeZoneValue
                    // we dont want to process anything
                    //so we set status value STOP_PROCESSING in the beginning
                    registerLastModule(module);
                    break;
                }
                int beforeTgSize = context.getSizeOfUnmarkedTargetGroups();
                if (beforeTgSize > 0) {
                    runModule(module, context, metricReporterService);
                    recordFilteringPercentage(context, module, beforeTgSize);
                }

                if (context.getStatus() == ProcessingStatus.STOP_PROCESSING ||
                        context.getSizeOfUnmarkedTargetGroups() == 0) {
                    context.setLastModuleRunInPipeline(module.getClass().getSimpleName());
                    registerLastModule(module);
                    break;
                }

            } catch(Exception e) {
                registerLastModule(module);
                metricReporterService.addStateModuleForEntity(
                        "EXCEPTION_WHILE_RUNNING_MODULE",//don't include exception here, because it might lead to too many states
                        "BidderMainPipelineProcessor",
                        "ALL",
                        MetricReporterService.MetricPriority.EXCEPTION);
                //we stop running other modules in case of an exception
                LOGGER.warn("exception occurred", e);
                throw new RuntimeException("EXCEPTION_WHILE_RUNNING_MODULE", e);
            }
        }

        metricReporterService.addStateModuleForEntity("processResult:'" +
                        context.getBidDecisionResult() +"'",
                "BidderMainPipelineProcessor",
                "ALL");

        if (EventLog.BidEventType.BID.equals(context.getBidDecisionResult())) {
            assertAndThrow(context.getChosenTargetGroup() != null);
            assertAndThrow(context.getChosenCreative() != null);
            metricReporterService.addStateModuleForEntity(
                    "runPostBidModules",
                    "BidderMainPipelineProcessor",
                    "ALL");
            runPostBidModules(context);
        }

        runFinalStageModules(context);
    }

    private void registerLastModule(BidderModule module) {
        metricReporterService.addStateModuleForEntity(
                "LastModule-" + module.getClass().getSimpleName(),
                "BidderMainPipelineProcessor", "ALL");
    }

    private void recordFilteringPercentage(
            OpportunityContext context,
            BidderModule module,
            int beforeTgSize) {
        assertAndThrow(beforeTgSize > 0);
        int afterTgSize = context.getSizeOfUnmarkedTargetGroups();

        int filterPercentage =(beforeTgSize - afterTgSize) * 100 / beforeTgSize;
        metricReporterService.addStateModuleForEntityWithValue(module.getClass().getSimpleName(),
                filterPercentage,
                "BidderMainPipelineProcessor-PercentageOfFilterCalculator",
                "ALL");

        //LOG_EVERY_N(INFO, 10000), "percenteOfFilteringTg for module ", module.getClass().getSimpleName(), " is ", filterPercentage;

    }

    /**
     we only run the modules in postBidModules when we are bidding on a request
     */
    private void runPostBidModules(OpportunityContext context) {
        runGenericModules(postBidModules, context);
    }

    private void runFinalStageModules(OpportunityContext context) {
        runGenericModules(finalStageModules, context);
    }

    public static void runModule(
            BidderModule module,
            OpportunityContext context,
            MetricReporterService metricReporterService) throws IOException {
        final Timer.Context timerContext = metricReporterService.getTimerContext(module.getClass().getSimpleName());
        try {
            module.beforeProcessing(context);
            module.process(context);
            module.afterProcessing(context);

        } finally {
            timerContext.stop();
        }
    }

    private void runGenericModules(
            List<BidderModule> modules,
            OpportunityContext context) {

        for (BidderModule module : modules) {
            try {
                runModule(module, context, metricReporterService);
            } catch(IOException e) {
                LOGGER.warn("exception occurred", e);
            }
        }
    }
}


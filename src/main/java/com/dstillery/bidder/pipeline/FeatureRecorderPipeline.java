package com.dstillery.bidder.pipeline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.DeviceHistoryOfGeoFeatureUpdaterModuleWrapper;
import com.dstillery.bidder.modules.DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper;
import com.dstillery.bidder.modules.DeviceHistoryOfVisitFeatureUpdaterModuleWrapper;
import com.dstillery.bidder.modules.FeatureToFeatureHistoryUpdaterModuleWrapper;
import com.dstillery.bidder.modules.GeoFeatureHistoryOfDeviceUpdaterModuleWrapper;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.VisitFeatureHistoryOfDeviceUpdaterModuleWrapper;

/*
   This pipeline is a series of modules that record the feature data in mysql
   and cassandra for modeling

   This pipeline is run as part of async pipeline sampled list
 */
public class FeatureRecorderPipeline {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureRecorderPipeline.class);

    private List<BidderModule> modules = new ArrayList<>();

    public FeatureRecorderPipeline(
            DeviceHistoryOfVisitFeatureUpdaterModuleWrapper deviceHistoryOfVisitFeatureUpdaterModuleWrapper,
            DeviceHistoryOfGeoFeatureUpdaterModuleWrapper deviceHistoryOfGeoFeatureUpdaterModuleWrapper,
            VisitFeatureHistoryOfDeviceUpdaterModuleWrapper visitFeatureHistoryOfDeviceUpdaterModuleWrapper,
            GeoFeatureHistoryOfDeviceUpdaterModuleWrapper geoFeatureHistoryOfDeviceUpdaterModuleWrapper,
            GeoFeatureHistoryOfDeviceUpdaterModuleWrapper placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper,
            DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper deviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper,
            FeatureToFeatureHistoryUpdaterModuleWrapper domainFeatureToFeatureHistoryUpdaterModuleWrapper
    )  {
       //saves domains' device history
        this.modules.add(deviceHistoryOfVisitFeatureUpdaterModuleWrapper);

        //saves geoFeatures' device history
        this.modules.add(deviceHistoryOfGeoFeatureUpdaterModuleWrapper);

        //saves the device's domain history
        this.modules.add(visitFeatureHistoryOfDeviceUpdaterModuleWrapper);

        //saves the device's geo history
        this.modules.add(geoFeatureHistoryOfDeviceUpdaterModuleWrapper);

        //saves the device's placeTag history...we only save Tag not place
        this.modules.add(placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper);

        //saves placeTags' device history
        this.modules.add(deviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper);

        //saves domain features' feature history
        this.modules.add(domainFeatureToFeatureHistoryUpdaterModuleWrapper);
    }

    void process(OpportunityContext context) {
        for (BidderModule module : modules) {
            try {
                LOGGER.debug("running module : {}", module.getClass().getSimpleName());
                module.process(context);
            } catch (IOException e) {
                LOGGER.warn("exception occurred", e);
            }
        }
    }

}


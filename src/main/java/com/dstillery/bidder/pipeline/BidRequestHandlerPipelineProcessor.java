package com.dstillery.bidder.pipeline;




import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.ProcessorInvokerModule;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.google.common.collect.ImmutableList;

public class BidRequestHandlerPipelineProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidRequestHandlerPipelineProcessor.class);

    private int maxAllowedTimeToProcessARequestInMillis;
    private MetricReporterService metricReporterService;
    private ImmutableList<BidderModule> biddingModules;
    private ProcessorInvokerModule processorInvokerModule;

    public BidRequestHandlerPipelineProcessor(
            MetricReporterService metricReporterService,
            int maxAllowedTimeToProcessARequestInMillis,
            ImmutableList<BidderModule> biddingModules) {
        this.metricReporterService = metricReporterService;
        this.maxAllowedTimeToProcessARequestInMillis = maxAllowedTimeToProcessARequestInMillis;
        this.biddingModules = biddingModules;
    }

    public void process(OpportunityContext context) {

        metricReporterService.addStateModuleForEntity("#OfProcessingBidRequests",
                "BidRequestHandlerPipelineProcessor",
                "ALL",
                MetricReporterService.MetricPriority.IMPORTANT);

        Instant timeToProcessRequest = Instant.now();

        for (BidderModule it : biddingModules) {
            try {

                LOGGER.debug("running module :{} for device : {}", it.getClass().getSimpleName(),
                        context.getDevice());
                BidderMainPipelineProcessor.runModule(it,
                        context,
                        metricReporterService);
            } catch (Exception e) {
                LOGGER.warn("exception in biddingModules ", e);
                context.setBidResponseStatusCode(204);
            }

            if (Instant.now().toEpochMilli() - timeToProcessRequest.toEpochMilli() >
                    maxAllowedTimeToProcessARequestInMillis) {
                context.setLastModuleRunInPipeline(it.getClass().getSimpleName());
                metricReporterService.addStateModuleForEntity(
                        "Yanking_Request",
                        "BidRequestHandlerPipelineProcessor",
                        "ALL",
                        MetricReporterService.MetricPriority.IMPORTANT);

                break;
            }
        }

    }

    /*
    used by spring for setter-based injection to break circular dependency
    */
    public void setProcessorInvokerModule(ProcessorInvokerModule processorInvokerModule) {
        this.processorInvokerModule = processorInvokerModule;
    }
}


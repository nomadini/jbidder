package com.dstillery.bidder.pipeline;




import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.modules.BannedDevicesFilterModule;
import com.dstillery.bidder.modules.BidEventRecorderModule;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.BidderWiretapModule;
import com.dstillery.bidder.modules.CrossWalkUpdaterModuleWrapper;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.ScoringFeederModule;
import com.dstillery.bidder.modules.frauddetection.BadDeviceByCountPopulatorModule;
import com.dstillery.bidder.modules.frauddetection.BadIpByCountPopulatorModule;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.LoadPercentageBasedSampler;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.GUtil;

public class BidderAsyncPipelineProcessor implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidderAsyncPipelineProcessor.class);

    private AtomicLong numberProcessorCalls;
    private List<BidderModule> modulesList = new ArrayList<>();
    private List<BidderModule> sampledModulesList= new ArrayList<>();

    private MetricReporterService metricReporterService;
    private LoadPercentageBasedSampler sampledModuleSampler;
    private FeatureRecorderPipeline featureRecorderPipeline;

    private BlockingQueue<OpportunityContext> queueOfContexts;

    public BidderAsyncPipelineProcessor(FeatureRecorderPipeline featureRecorderPipeline,
                                        ScoringFeederModule scoringFeederModule,
                                        BannedDevicesFilterModule bannedDevicesFilterModule,
                                        BadIpByCountPopulatorModule badIpByCountPopulatorModule,
                                        BadDeviceByCountPopulatorModule badDeviceByCountPopulatorModule,
                                        BidEventRecorderModule bidEventRecorderModule,
                                        CrossWalkUpdaterModuleWrapper crossWalkUpdaterModuleWrapper,
                                        BidderWiretapModule bidderWiretapModule,
                                        MetricReporterService metricReporterService,
                                        ConfigService configService,
                                        BlockingQueue<OpportunityContext> queueOfContexts) {
        this.queueOfContexts = queueOfContexts;

        this.sampledModuleSampler = new LoadPercentageBasedSampler (
                configService.getAsInt("percentageOfBidRequestsToRunInAsyncPipelineProcessor"),
                "percentageOfBidRequestsToRunInAsyncPipelineProcessor"
        );

        this.metricReporterService = metricReporterService;
        numberProcessorCalls = new AtomicLong();

        this.modulesList.add(bidderWiretapModule);
        this.modulesList.add(scoringFeederModule);

        this.modulesList.add(bannedDevicesFilterModule);

        this.modulesList.add(badIpByCountPopulatorModule);

        this.modulesList.add(badDeviceByCountPopulatorModule);

        this.modulesList.add(bidEventRecorderModule);

        // we don't to save too much data , because of that, we run
        // these modules based on a sampler

        this.sampledModulesList.add(crossWalkUpdaterModuleWrapper);

        //this pipeline is responsible for adding data to our systems
        this.featureRecorderPipeline = featureRecorderPipeline;

    }

    public void run() {
        if (GUtil.allowedToCall(1000)) {
            LOGGER.info("queueOfContexts size : {}", queueOfContexts.size());
        }
        while (!queueOfContexts.isEmpty()) {
            try {

                OpportunityContext context = queueOfContexts.take();
                //we popped something off the queue
                metricReporterService.addStateModuleForEntity("process_msg",
                        "BidderAsyncPipelineProcessor",
                        "ALL");

                //we set this to true, to run async specific logics in some of the modules
                context.setAsyncPiplineExecuting(true);
                this.process(context);
                LOGGER.debug("finished processing async pipeline");
            } catch(Exception e){
                //LOG_EVERY_N(ERROR, 100), google.COUNTER,"th error in BidderAsyncPipelineProcessor";
                metricReporterService.addStateModuleForEntity("ERROR_IN_PIPELINE",
                        "BidderAsyncPipelineProcessor",
                        "ALL",
                        MetricReporterService.MetricPriority.EXCEPTION);

                LOGGER.warn("exception occurred", e);
            }
        }
    }

    private void process(OpportunityContext opt) {

        numberProcessorCalls.getAndIncrement();
        metricReporterService.addStateModuleForEntity
                ("#OfProcessingBidRequests",
                        "BidderAsyncPipelineProcessor",
                        "ALL");

        runModules(opt, modulesList);
        //feature modules have a sampler in them...so we don't run
        //the feature recorder pipeline within the sampler
        featureRecorderPipeline.process(opt);

        //we run the modules that are sampled
        if (sampledModuleSampler.isPartOfSample()) {
            metricReporterService.addStateModuleForEntity(
                    "part_of_sample",
                    "BidderAsyncPipelineProcessor",
                    "ALL");
            runModules(opt, sampledModulesList);
        } else {
            metricReporterService.addStateModuleForEntity(
                    "not_part_of_sample",
                    "BidderAsyncPipelineProcessor",
                    "ALL");
        }

    }

    private void runModules(OpportunityContext opt, List<BidderModule> modules) {
        for (BidderModule it : modules) {
            try {
                BidderMainPipelineProcessor.runModule(it, opt, metricReporterService);
            } catch (IOException e) {
                LOGGER.warn("exception occurred", e);
            }
        }
    }
}

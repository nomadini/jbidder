package com.dstillery.bidder.samplers;//
// Created by Mahmoud Taabodi on 11/24/16.
//

import java.util.concurrent.atomic.AtomicLong;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.util.RandomUtil;
import com.google.common.util.concurrent.AtomicDouble;

//checks if a user nomadiniDeviceId is part of sample rate given
public class UserBucketSampler extends Sampler {

    int range;
AtomicLong numberOfTotalCalls;
AtomicLong numberOfFoundInSample;
AtomicDouble percentageOfSampleRate;

RandomUtil userBucketRandomizer;

    UserBucketSampler(int range)  {
        this.range = range;
        numberOfTotalCalls = new AtomicLong();
        numberOfFoundInSample = new AtomicLong();
        percentageOfSampleRate = new AtomicDouble();

        userBucketRandomizer = new RandomUtil (range);
    }

    boolean isPartOfSample( OpportunityContext context) {
        long hasheValue = context.getDevice().getDeviceId().hashCode();
        numberOfTotalCalls.getAndIncrement();
        long randomNumber = userBucketRandomizer.randomPositiveNumberV2();
        if (hasheValue % randomNumber == 0) {
            numberOfFoundInSample.getAndIncrement();
            percentageOfSampleRate.set(
                    (numberOfFoundInSample.get() /
                            numberOfTotalCalls.get()) * 100);
            return true;
        }  else {
            return false;
        }

        ////LOG_EVERY_N(ERROR, 10000) , "percentageOfSampleRate : " , percentageOfSampleRate.getValue()
//                                  , " for range : " , range;

    }

}

 //GICAPODS_RANDOMUserBucketSampler_H

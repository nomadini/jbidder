package com.dstillery.bidder.delivery_info;

import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.HttpUtil.APPLICATION_JSON;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.util.HttpUtil;
import com.fasterxml.jackson.core.type.TypeReference;

public class MangoApiClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(MangoApiClient.class);

    private DiscoveryService discoveryService;

    public MangoApiClient(DiscoveryService discoveryService) {
        this.discoveryService = discoveryService;
    }

    public List<EntityRealTimeDeliveryInfo> getTgRealTimeDeliveryInfos(
            String entityName,
            Set<Long> entityIds) {
            String url = discoveryService.discoverService("mango-backend-services")
                            + "/api/druid/get-entity-imps-delivered";
            return getTgRealTimeDeliveryInfos(entityName, entityIds, url);
    }


    public List<EntityRealTimeDeliveryInfo> getTgRealTimeDeliveryInfos(
            String entityName,
            Set<Long> entityIds,
            String url) {

        Map<Long, EntityRealTimeDeliveryInfo> deliveryCacheMap = new ConcurrentHashMap<>();

        Stream.of(IntervalType.values()).forEach(intervalType -> {
            fetch(deliveryCacheMap,
                    intervalType,
                    entityName,
                    entityIds,
                    url);
        });

        return new ArrayList<>(deliveryCacheMap.values());
    }

    private void fetch(Map<Long, EntityRealTimeDeliveryInfo> deliveryCacheMap,
                       IntervalType intervalType,
                       String entityName,
                       Set<Long> entityIds,
                       String url) {
        try {
            EntityImpsDeliveredRequest request = new EntityImpsDeliveredRequest();
            request.setEntity(entityName);
            request.setEntityIds(entityIds);

            if (intervalType.equals(IntervalType.CURRENT_HOUR)) {
                request.setTimeInMillis(Instant.now().minus(LocalDateTime.now().getMinute(),
                        ChronoUnit.MINUTES).toEpochMilli());
            } else if (intervalType.equals(IntervalType.CURRENT_DAY)) {
                request.setTimeInMillis(Instant.now().minus(LocalDateTime.now().getHour(),
                        ChronoUnit.HOURS).toEpochMilli());
            } else if (intervalType.equals(IntervalType.TOTAL)) {
                request.setTimeInMillis(Instant.now().minus(365, ChronoUnit.DAYS).toEpochMilli());
            }

            String requestBody = OBJECT_MAPPER.writeValueAsString(request);

            LOGGER.debug("requestBody : {}", requestBody);

            String response = HttpUtil.sendPostRequest(url,
                    requestBody,
                    APPLICATION_JSON,
                    10000,
                    2);

            LOGGER.debug("response : {}", response);
            Map<Integer, EntityImpsDeliveredEvent> idsToImpsDelivered = OBJECT_MAPPER.readValue(response,
                    new TypeReference<Map<Integer, EntityImpsDeliveredEvent>>() {});

            LOGGER.info("idsToImpsDelivered : {}", idsToImpsDelivered);
            idsToImpsDelivered.values().forEach(
                    entityImpsDeliveredEvent -> {
                        deliveryCacheMap.computeIfAbsent(entityImpsDeliveredEvent.getEntityId(),
                                t -> new EntityRealTimeDeliveryInfo());
                        deliveryCacheMap.get(entityImpsDeliveredEvent.getEntityId()).getAdvertiserCost()
                                .put(intervalType, entityImpsDeliveredEvent.getAdvertiserCost());
                        deliveryCacheMap.get(entityImpsDeliveredEvent.getEntityId()).getExchangeCost()
                                .put(intervalType, entityImpsDeliveredEvent.getExchangeCost());

                        deliveryCacheMap.get(entityImpsDeliveredEvent.getEntityId()).getPlatformCost()
                                .put(intervalType, entityImpsDeliveredEvent.getPlatformCost());
                        deliveryCacheMap.get(entityImpsDeliveredEvent.getEntityId()).getImpressions()
                                .put(intervalType, entityImpsDeliveredEvent.getCount());
                    });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {

        Map<Long, EntityRealTimeDeliveryInfo> deliveryCacheMap =
                new ConcurrentHashMap<>();

        new MangoApiClient(null).getTgRealTimeDeliveryInfos(
                "targetGroup",
                LongStream.range(1, 50).boxed().collect(Collectors.toSet()),
                "http://localhost:9220/mango-backend-services/api/druid/get-entity-imps-delivered");
    }
}

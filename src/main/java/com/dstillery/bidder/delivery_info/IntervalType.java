package com.dstillery.bidder.delivery_info;

public enum IntervalType {
    CURRENT_HOUR, CURRENT_DAY, TOTAL
}

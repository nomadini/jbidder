package com.dstillery.bidder.delivery_info;



import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.StringUtil.equalsIgnoreCase;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.campaign.Campaign;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 this class is now responsible for hitting the pacing engine
 and populating all the tg realtime info in Cache Manager
 *
 */
public class EntityDeliveryInfoFetcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityDeliveryInfoFetcher.class);
    private EntityDeliveryInfoCacheService entityDeliveryInfoCacheService;
    private AtomicBoolean entityDeliveryInfoCacheServiceIsHealthy;
    private MangoApiClient mangoApiClient;

    private final MetricReporterService metricReporterService;
    /***
     This class is responsible for updating the EntityDeliveryInfoCacheService with the most up-to-date information
     */

    public EntityDeliveryInfoFetcher(EntityDeliveryInfoCacheService entityDeliveryInfoCacheService,
                                     AtomicBoolean entityDeliveryInfoCacheServiceIsHealthy,
                                     MangoApiClient mangoApiClient,
                                     MetricReporterService metricReporterService) {
        this.entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
        this.entityDeliveryInfoCacheServiceIsHealthy = entityDeliveryInfoCacheServiceIsHealthy;
        this.mangoApiClient = mangoApiClient;
        this.metricReporterService = metricReporterService;
    }


    public void getLatestRealTimeInfo(String entityName, Set<Long> entityIds) {

        try {
            refreshRealTimeInfos (entityName, entityIds);
            entityDeliveryInfoCacheServiceIsHealthy.set(true);
        } catch (Exception e) {
            metricReporterService.addStateModuleForEntity
                    ("EXCEPTION_WHILE_GETTING_LATEST_REAL_TIME_INFO",
                            getClass().getSimpleName(),
                            "ALL",
                            MetricReporterService.MetricPriority.EXCEPTION);
            entityDeliveryInfoCacheServiceIsHealthy.set(false);
            throw new RuntimeException(e);//this will cause the data to reload again
        }
    }

    private void refreshRealTimeInfos(String entityName, Set<Long> entityIds) throws JsonProcessingException {
        List<EntityRealTimeDeliveryInfo> allDeliveryInfos =
                mangoApiClient.
                        getTgRealTimeDeliveryInfos(entityName, entityIds);

        LOGGER.debug("entityName : {}, ids : {}, latest delivery Infos {}",
                entityName, entityIds, OBJECT_MAPPER.writeValueAsString(allDeliveryInfos));
        populateDeliveryInfoCache(entityName, allDeliveryInfos);
    }

    private void populateDeliveryInfoCache(
            String entityName,
            List<EntityRealTimeDeliveryInfo> allDeliveryInfos) {

        ConcurrentHashMap<Long, EntityRealTimeDeliveryInfo> deliveryCacheMap;
        if (equalsIgnoreCase(entityName, TargetGroup.class.getSimpleName())) {
            deliveryCacheMap = entityDeliveryInfoCacheService.getAllTgRealTimeDeliveryInfoMap();
            deliveryCacheMap.clear();
            for (EntityRealTimeDeliveryInfo deliveryInfo : allDeliveryInfos) {
                deliveryCacheMap.put(deliveryInfo.getEntityId(), deliveryInfo);
            }

        } else if (equalsIgnoreCase(entityName, Campaign.class.getSimpleName())) {
            deliveryCacheMap = entityDeliveryInfoCacheService.getAllCampaignRealTimeDeliveryInfoMap();
            deliveryCacheMap.clear();
            for (EntityRealTimeDeliveryInfo deliveryInfo : allDeliveryInfos) {
                deliveryCacheMap.put(deliveryInfo.getEntityId(), deliveryInfo);
            }
        }
    }
}

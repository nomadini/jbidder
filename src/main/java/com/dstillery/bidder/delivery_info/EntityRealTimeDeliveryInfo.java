package com.dstillery.bidder.delivery_info;

import java.time.Instant;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class EntityRealTimeDeliveryInfo {
    private Long entityId;
    private String entityName;
    private Instant timeReported;

    private Map<IntervalType, Long> impressions = new ConcurrentHashMap<>();
    private Map<IntervalType, Double> platformCost = new ConcurrentHashMap<>();
    private Map<IntervalType, Double> advertiserCost = new ConcurrentHashMap<>();
    private Map<IntervalType, Double> exchangeCost = new ConcurrentHashMap<>();

    public EntityRealTimeDeliveryInfo()   {
        timeReported = Instant.now();
        entityId = 0L;
        Arrays.asList(IntervalType.values()).forEach(intervalType -> {
            impressions.put(intervalType, 0L);
            platformCost.put(intervalType, 0D);
            advertiserCost.put(intervalType, 0D);
            exchangeCost.put(intervalType, 0D);
        });
    }

    public Map<IntervalType, Long> getImpressions() {
        return impressions;
    }

    public void setImpressions(Map<IntervalType, Long> impressions) {
        this.impressions = impressions;
    }

    public Map<IntervalType, Double> getPlatformCost() {
        return platformCost;
    }

    public void setPlatformCost(Map<IntervalType, Double> platformCost) {
        this.platformCost = platformCost;
    }

    public Map<IntervalType, Double> getAdvertiserCost() {
        return advertiserCost;
    }

    public void setAdvertiserCost(Map<IntervalType, Double> advertiserCost) {
        this.advertiserCost = advertiserCost;
    }

    public Map<IntervalType, Double> getExchangeCost() {
        return exchangeCost;
    }

    public void setExchangeCost(Map<IntervalType, Double> exchangeCost) {
        this.exchangeCost = exchangeCost;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Instant getTimeReported() {
        return timeReported;
    }

    public void setTimeReported(Instant timeReported) {
        this.timeReported = timeReported;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}


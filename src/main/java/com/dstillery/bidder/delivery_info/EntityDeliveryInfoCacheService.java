package com.dstillery.bidder.delivery_info;

import java.util.concurrent.ConcurrentHashMap;

public class EntityDeliveryInfoCacheService {

    public enum EntityType {
        CAMPAIGN, TARGETGROUP, CLIENT
    }

    private ConcurrentHashMap<Long, EntityRealTimeDeliveryInfo> allTgRealTimeDeliveryInfoMap;
    private ConcurrentHashMap<Long, EntityRealTimeDeliveryInfo> allCampaignRealTimeDeliveryInfoMap;

    public EntityDeliveryInfoCacheService() {
        this.allTgRealTimeDeliveryInfoMap = new ConcurrentHashMap<>();
        this.allCampaignRealTimeDeliveryInfoMap = new ConcurrentHashMap<>();
    }

    public EntityRealTimeDeliveryInfo getDeliveryInfo(
            EntityType entityType, Long id
    ) {
        if (entityType.equals(EntityType.TARGETGROUP)) {
            return allTgRealTimeDeliveryInfoMap.getOrDefault(id, new EntityRealTimeDeliveryInfo());
        } else if (entityType.equals(EntityType.CAMPAIGN)) {
            return allCampaignRealTimeDeliveryInfoMap.getOrDefault(id, new EntityRealTimeDeliveryInfo());
        }
        throw new RuntimeException("no delivery info available for id " + id + "entity type " + entityType);
    }

    public ConcurrentHashMap<Long, EntityRealTimeDeliveryInfo> getAllTgRealTimeDeliveryInfoMap() {
        return allTgRealTimeDeliveryInfoMap;
    }

    public ConcurrentHashMap<Long, EntityRealTimeDeliveryInfo> getAllCampaignRealTimeDeliveryInfoMap() {
        return allCampaignRealTimeDeliveryInfoMap;
    }
}

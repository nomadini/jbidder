package com.dstillery.bidder.delivery_info;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class EntityImpsDeliveredEvent {
    private Long entityId;
    private Long count = 0L;

    private Double advertiserCost = 0D;
    private Double exchangeCost = 0D;
    private Double platformCost = 0D;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Double getAdvertiserCost() {
        return advertiserCost;
    }

    public void setAdvertiserCost(Double advertiserCost) {
        this.advertiserCost = advertiserCost;
    }

    public Double getExchangeCost() {
        return exchangeCost;
    }

    public void setExchangeCost(Double exchangeCost) {
        this.exchangeCost = exchangeCost;
    }

    public Double getPlatformCost() {
        return platformCost;
    }

    public void setPlatformCost(Double platformCost) {
        this.platformCost = platformCost;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

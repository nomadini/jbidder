package com.dstillery.bidder.contexts;//
// Created by Mahmoud Taabodi on 5/7/16.
//

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.geo.State;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.AddressNotFoundException;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Country;
import com.maxmind.geoip2.record.Location;
import com.maxmind.geoip2.record.Postal;
import com.maxmind.geoip2.record.Subdivision;
public class IpInfoFetcherModule extends BiddingMonitor {
    private static final Logger LOGGER = LoggerFactory.getLogger(IpInfoFetcherModule.class);

    private final DatabaseReader reader;

    public IpInfoFetcherModule(
            ConfigService configService,
            int failureThresholdPercentageArg) throws IOException {
        super(failureThresholdPercentageArg,
                "IpInfoFetcherModuleMonitor");
        File database = new File(configService.get("maxmindDataPath") + "GeoIP2-City.mmdb");
        reader = new DatabaseReader.Builder(database).build();
    }

    public void getIpInfo(OpportunityContext context) {
        //TODO extract more info from maxmind https://maxmind.github.io/GeoIP2-java/

        try {
            InetAddress ipAddress = InetAddress.getByName(context.getDeviceIp());
// Replace "city" with the appropriate method for your database, e.g.,
// "country".
            CityResponse response = reader.city(ipAddress);

            Country country = response.getCountry();
//        System.out.println(country.getIsoCode());            // 'US'
//        System.out.println(country.getName());               // 'United States'
//        System.out.println(country.getNames().get("zh-CN")); // '美国'

            Subdivision subdivision = response.getMostSpecificSubdivision();
//        System.out.println(subdivision.getName());    // 'Minnesota'
//        System.out.println(subdivision.getIsoCode()); // 'MN'

            City city = response.getCity();
//        System.out.println(city.getName()); // 'Minneapolis'

            Postal postal = response.getPostal();
//        System.out.println(postal.getCode()); // '55455'

            Location location = response.getLocation();
//        System.out.println(location.getLatitude());  // 44.9733
//        System.out.println(location.getLongitude()); // -93.2323
            context.setDeviceCity(city.getName());
            if (country.getIsoCode() != null) {
                if(country.getIsoCode().toUpperCase().equalsIgnoreCase("US")) {
                    context.setDeviceCountry(com.dstillery.common.geo.Country.USA);
                } else if (country.getIsoCode().toUpperCase().equalsIgnoreCase("CA")) {
                    context.setDeviceCountry(com.dstillery.common.geo.Country.CANADA);
                } else if (country.getIsoCode().toUpperCase().equalsIgnoreCase("GB") ||
                        country.getIsoCode().toUpperCase().equalsIgnoreCase("UK")) {
                    context.setDeviceCountry(com.dstillery.common.geo.Country.UK);
                }
            }
            if (subdivision != null && subdivision.getIsoCode() != null) {
                try {
                    context.setDeviceState(State.valueOf(subdivision.getIsoCode()));
                } catch (RuntimeException e) {
                    //ignore
                }
            }
            context.setUserTimeZone(location.getTimeZone());
            //LOG_EVERY_N(INFO, 100000), google.COUNTER,"th responseFromMaxmindService : "
//                                 , responseFromMaxmindService;
        } catch (AddressNotFoundException e) {
            //ignore
        } catch (GeoIp2Exception | IOException e) {
            LOGGER.warn("bad ip : {}, error message : {}" + context.getDeviceIp(), e.getMessage());
        }
    }
}

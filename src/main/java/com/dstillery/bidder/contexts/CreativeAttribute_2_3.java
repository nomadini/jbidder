package com.dstillery.bidder.contexts;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CreativeAttribute_2_3 {
    public static Map<Integer, String> VALUE_TO_NAME = new HashMap();
    static {
        Arrays.asList(CreativeAttribute_2_3_enum.values())
                .forEach(val -> VALUE_TO_NAME.put(val.getValue(), val.name()));
    }

    public enum CreativeAttribute_2_3_enum {

        Audio_Ad_Auto_Play(1),
        Audio_Ad_User_Initiated(2),
        Expandable_Automatic(3),
        Expandable_User_Initiated_Click(4),
        Expandable_User_Initiated_Rollover(5),
        In_Banner_Video_Ad_Auto_Play(6),
        In_Banner_Video_Ad_User_Initiated(7),
        Pop(8),
        Provocative_Suggestive_Imagery(9),
        Distracting(10),
        Surveys(11),
        Text_Only(12),
        User_Interactive(13),
        Windows_Dialog_Alert_Style(14),
        Has_Audio_On_Off_Button(15),
        Can_be_Skipped(16);

        private int value;

        CreativeAttribute_2_3_enum(int value) {
            this.value = value;
            VALUE_TO_NAME.put(value, this.name());
        }

        public int getValue() {
            return value;
        }
    }

}
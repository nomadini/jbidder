package com.dstillery.bidder.contexts;

public class BannerAdTypeConverter {

    public static String convert(int adType, String openRtbVersion) {
        if (openRtbVersion.equalsIgnoreCase("2.3")) {
                /*
                    1 --> XHTML Text Ad (usually mobile)
                    2 --> XHTML Banner Ad. (usually mobile)
                    3 --> JavaScript Ad; must be valid XHTML (i.e., Script Tags Included)
                    4 --> iframe
                 */
            if (adType == 1) { return "XHTML_TEXT_AD"; }
            if (adType == 2) { return "XHTML_BANNER_AD"; }
            if (adType == 3) { return "JAVASCRIPT"; }
            if (adType == 4) { return "IFRAME"; }
        }
        throw new RuntimeException("open rtb version not supported " + openRtbVersion);
    }
}

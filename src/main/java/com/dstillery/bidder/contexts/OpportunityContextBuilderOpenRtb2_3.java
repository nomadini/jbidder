package com.dstillery.bidder.contexts;//
// Created by Mahmoud Taabodi on 5/7/16.
//


import static com.dstillery.common.creative.CreativeContentType.DISPLAY;
import static com.dstillery.common.geo.Country.UNKNOWN;
import static com.dstillery.common.geo.Country.USA;

import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static com.dstillery.common.util.StringUtil.replaceString;
import static com.dstillery.common.util.StringUtil.toStr;
import static nl.basjes.parse.useragent.UserAgent.OPERATING_SYSTEM_CLASS;
import static nl.basjes.parse.useragent.UserAgent.OPERATING_SYSTEM_NAME;

import java.io.IOException;
import java.time.Instant;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.DomainUtil;
import com.dstillery.common.device.DeviceEnvironmentType;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.device.OsType;
import com.dstillery.common.geo.Country;
import com.dstillery.common.geo.State;
import com.dstillery.common.metric.OpenRtbVersion;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.common.util.RandomUtil;
import com.dstillery.openrtb.beans.BidRequest;
import com.fasterxml.jackson.core.JsonProcessingException;

import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;

public class OpportunityContextBuilderOpenRtb2_3 {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpportunityContextBuilderOpenRtb2_3.class);

    private RandomUtil transactionIdRandomizer;
    private UserAgentAnalyzer uaParser = UserAgentAnalyzer
            .newBuilder()
            .hideMatcherLoadStats()
            .withCache(25000)
            .build();

    private ConcurrentHashMap<String, Integer> timeZoneToOffsetWithUtcMap;
    private final MetricReporterService metricReporterService;
    private CreativeAttribute_2_3 creativeAttributeClass;
    private IpInfoFetcherModule ipInfoFetcherModule;

    private BidRequestParser bidRequestParser;

    public OpportunityContextBuilderOpenRtb2_3(
            BidRequestParser bidRequestParser,
            IpInfoFetcherModule ipInfoFetcherModule,
            AtomicLong uniqueNumberOfContext,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        transactionIdRandomizer = new RandomUtil(100000000);
        this.ipInfoFetcherModule = ipInfoFetcherModule;
        this.bidRequestParser = bidRequestParser;
        timeZoneToOffsetWithUtcMap = new ConcurrentHashMap<>();
    }

    void configureContextFromBidRequest(
            OpportunityContext context,
            String requestBody) throws IOException {

        Instant parseBidRequestTime;
        BidRequest bidRequest =
                bidRequestParser.parseBidRequest(requestBody);

        context.setBidRequestBody(requestBody);

        metricReporterService.addStateModuleForEntity(
                "CONTEXT_BUILDING_ATTEMPTS",
                "OpportunityContextBuilderOpenRtb2_3",
                "ALL");

        setDeviceForContext(context, bidRequest);
        context.setExchangeUserId(bidRequest.getUser().getId());

        if ((context.getExchangeUserId() == null ||
                context.getExchangeUserId().isEmpty()) && context.getDevice() == null) {
            metricReporterService.addStateModuleForEntity("NO_KNOWN_USER_NO_EXCHANGE_ID", "OpportunityContextBuilderOpenRtb2_3", "ALL");
            context.stopProcessing(this.getClass().getSimpleName());
        }

        context.setCreativeAPI(CreativeAPIConverter.convertApiToName(
                bidRequest.getImp().get(0).getBanner().getApi(), "2.3"));
        context.setImpressionType("Banner"); //video, app or banner

        context.setAdPosition(AdPositionConverter.convert(
                bidRequest.getImp().get(0).getBanner().getPos(),
                OpenRtbVersion.V2_3));

        context.setDeviceUserAgent(bidRequest.getDevice().getUa());
        parseUserAgent(context);
        context.setDeviceIp(bidRequest.getDevice().getIp());
        context.setDeviceLat(bidRequest.getDevice().getGeo().getLat());
        context.setDeviceLon(bidRequest.getDevice().getGeo().getLon());

        context.setCreativeContentType(DISPLAY);
        context.setEnvironmentType(DeviceEnvironmentType.WEB); //versus MOBILE  for mobile bid requests
        context.setExchangeId("exchangeA"); // TODO : separate bid parsing logics for different exchanges later and different enviornments
        setGeoLocationInfoToContext(bidRequest, context);

        context.setDeviceZipcode(bidRequest.getDevice().getGeo().getZip());
        assertAndThrow(!context.getDeviceZipcode().isEmpty());
        context.setProtocolVersion("OpenRtb2.3");

        for (String url : bidRequest.getBadv()) {
            try {
                String badv = DomainUtil.getDomainName(url);
                context.addBlockedAdvertiser(badv);
            } catch (Exception e) {
                LOGGER.warn("exception in getting blocked adv from : {}", url, e);
            }
        }

        //LOGGER.debug("context.getBlockedAdvertiserMap() size ",context.getBlockedAdvertiserMap().size();

        for (String cat : bidRequest.getBcat()) {
            context.addBlockedIabCategory(cat);
        }

        //LOGGER.debug("context.getBlockedIabCategoriesMap() size ",context.getBlockedIabCategoriesMap().size();

        for (int attr : bidRequest.getImp().get(0).getBanner().getBattr()) {
            String name = CreativeAttribute_2_3.VALUE_TO_NAME.get(attr);
            if (name == null) {
                throw new RuntimeException("creative attribute was not found in map : " + attr);
            }
            context.getBlockedAttributes().add(name);
        }
//        //LOGGER.debug("context.getBlockedBannerTypesMap() size ",context.getBlockedBannerTypesMap().size();
//
        StringBuilder allBlockedBannerTypesAsStr = new StringBuilder();
        for (int type : bidRequest.getImp().get(0).getBanner().getBtype()) {
            String bannerAdType = BannerAdTypeConverter.convert(type, "2.3");
            allBlockedBannerTypesAsStr.append(bannerAdType).append("_");
            context.addBlockedBannerType(bannerAdType);
        }

        context.setAllBlockedBannerTypesAsStr(allBlockedBannerTypesAsStr.toString());

        context.getSiteCategory().add(bidRequest.getSite().getCat().get(0));
        context.setSiteDomain(bidRequest.getSite().getDomain());
        if (context.getSiteDomain() == null || context.getSiteDomain().isEmpty()) {
           throw new RuntimeException("unable to get siteDomain from " +
                   context.getBidRequestBody() + " parsed request json : " +
                   OBJECT_MAPPER.writeValueAsString(bidRequest));
        }
        context.setSitePage(bidRequest.getSite().getPage());
//        context.setSitePublisherDomain(bidRequest.getSite());
//        context.setSitePublisherName(bidRequest.getSite().publisher.name);

        context.setAdSize(toStr(bidRequest.getImp().get(0).getBanner().getW())
                + "x" +
                toStr(bidRequest.getImp().get(0).getBanner().getH()));

        context.setLastModuleRunInPipeline("NONE_YET");

        context.setBidFloor(bidRequest.getImp().get(0).getBidFloor());

        if (bidRequest.getImp().get(0).getSecure() == 1) {
            context.setSecureCreativeRequired(true);
        } else {
            context.setSecureCreativeRequired(false);
        }

        context.setRawBidRequest(requestBody);

        //recordStatesForDebugging(context);//produces too much state. do something else
        metricReporterService.addStateModuleForEntity("CONTEXT_BUILDING_SUCCESS", "OpportunityContextBuilderOpenRtb2_3", "ALL");
    }

    private boolean isMobileRequest(BidRequest bidRequest) {
        return false; //TODO : fix this
    }

    private void parseUserAgent(OpportunityContext context) {

        UserAgent uaParsed = uaParser.parse(context.getDeviceUserAgent());
        String os = null;
        try {

         os = uaParsed.getValue(OPERATING_SYSTEM_NAME).toUpperCase();
         String osClassName = uaParsed.getValue(OPERATING_SYSTEM_CLASS).toUpperCase(); // DESKTOP , MOBILE, UNKNOWN
         LOGGER.debug("osClass {}", osClassName);
         if (os.contains("NAMOROKA") ||
                 os.contains("OS/2") ||
                 os.contains("FIREFOX") ||
                 os.contains("MANDRIVA")) {
             context.setDeviceOsFamily(OsType.UNKNOWN);
         } else if (
                 os.contains("MAC") ||
                 os.contains("IOS")) {
             context.setDeviceOsFamily(OsType.MAC);
         } else  if (os.contains("WINDOWS")) {
             context.setDeviceOsFamily(OsType.WINDOWS);
         } else if (
                 os.contains("FEDORA") ||
                 os.contains("SUSE") ||
                 os.contains("LINUX") ||
                 os.contains("OPENBSD") ||
                 os.contains("UBUNTU") ||
                 os.contains("FREEBSD") ||
                 os.contains("SLACKWARE") ||
                 os.contains("SOLARIS") ||
                 os.contains("CENTOS") ||
                 os.contains("NETBSD") ||
                 os.contains("CHROME OS") ||
                 os.contains("RED HAT") ||
                 os.contains("ANDROID") ||
                 os.contains("DEBIAN") ||
                 os.contains("SUNOS")
         ) {
             context.setDeviceOsFamily(OsType.LINUX);
         } else {
             context.setDeviceOsFamily(OsType.valueOf(os));
         }
        } catch (RuntimeException e) {
            context.setDeviceOsFamily(OsType.UNKNOWN);
            LOGGER.warn("not recognizing os {}", os);
        }

        //TODO add browser info to context

//        //LOGGER.debug("uaParsed : {} ",  uaParsed.ua.toFullString()
//                , "os family : {} ",  uaParsed.ua.os.family
//                , "Browser : {} ",  uaParsed.ua.browser.toString();;
//
//        if (
//                containsCaseInSensitive(uaParsed.ua.os.family, "iOs") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "Mac OS")
//                ) {
//            context.setDeviceOsFamily(Device.GENERAL_BIDDING_DEVICE_OS_TYPE_MAC);
//        } else if (containsCaseInSensitive(uaParsed.ua.os.family, "windows")) {
//            context.setDeviceOsFamily(Device.GENERAL_BIDDING_DEVICE_OS_TYPE_WINDOWS);
//        } else if (
//                containsCaseInSensitive(uaParsed.ua.os.family, "linux") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "solaris") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "FreeBSD") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "SUSE") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "Gentoo") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "Slackware") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "NetBSD") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "OpenBSD") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "Mandriva") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "CentOS") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "Chrome OS") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "red hat") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "Android") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "Debian") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "Fedora") ||
//                        containsCaseInSensitive(uaParsed.ua.os.family, "Ubuntu")) {
//            context.setDeviceOsFamily(Device.GENERAL_BIDDING_DEVICE_OS_TYPE_LINUX);
//        } else {
//
//            if (!uaParsed.ua.os.family.isEmpty() &&
//                    !equalsIgnoreCase(uaParsed.ua.os.family, "Other")) {
//                //LOG_EVERY_N(ERROR, 10), "setting os family as unknown for os family", uaParsed.ua.os.family;
//            }
//            context.setDeviceOsFamily("UNKNOWN");
//        }
    }

    private void setGeoLocationInfoToContext(BidRequest bidRequest,
                                             OpportunityContext context) throws IOException {
        //soosan : todo .. compare the bid request geo info with max mind and fill the lacking info with max mind data or
        //discard the bid request info if needed //TODO : make this its own class later

        if (isMobileRequest(bidRequest)) {
            context.setDeviceCountry(Country.valueOf(bidRequest.getDevice().getGeo().getCountry()));
            context.setDeviceCity(replaceString(bidRequest.getDevice().getGeo().getCity(), "'", "", true));
            //TODO : get this from a hash map in memory that comes from DB
            //getTimeZone based on city
            context.setDeviceState(State.UNKNOWN);
        } else {
            Instant time = Instant.now();
            ipInfoFetcherModule.getIpInfo(context);
            //LOG_EVERY_N_TIMES(100000) , google.COUNTER ,"th getIpInfoOverHttp took " , time.elapsed () / 1000 ," milis ";
        }

        context.setBidRequestId(bidRequest.getId());
        context.setImpressionIdInBidRequest(bidRequest.getImp().get(0).getId());
//         context.setPublisherId(BidRequest.getSite().publisher.id);

        Instant time;
        Long randomNumber = transactionIdRandomizer.randomPositiveNumberV2();
        //LOG_EVERY_N_TIMES(100000) , google.COUNTER,"th randomPositiveNumberV2 took " , time.elapsed () / 1000 ," milis ";

        // this will be the eventLog in Json Format
        context.setTransactionId(bidRequest.getId() + toStr(randomNumber));

        context.setDeviceUserAgent(bidRequest.getDevice().getUa());

        if (context.getDeviceCountry() == null ||
                context.getDeviceCountry() != Country.USA) {
//            LOGGER.debug("bid request coming from outside US ", context.deviceCountry;
            //we don't bid on requests coming from other than US
            metricReporterService.addStateModuleForEntity("NON_US_REQUEST",
                    "OpportunityContextBuilderOpenRtb2_3",
                    "ALL");
            context.stopProcessing(this.getClass().getSimpleName());
            return;
        } else {

            //logTheUSipForBookKeeping(context);
            context.setDeviceCountry(USA);
        }

        fetchUserTimeZone(context);
        if (context.getDeviceCountry() == null) {
            context.setDeviceCountry(UNKNOWN);
        }
        if (context.getDeviceState() == null) {
            context.setDeviceState(State.UNKNOWN);
        }
        if (context.getDeviceCity() == null || context.getDeviceCity().isEmpty()) {
            context.setDeviceCity("unknown");
        }

    }

    private void fetchUserTimeZone(
            OpportunityContext context) {
        Instant timeZoneLookUpTime;
        if (context.getUserTimeZone() == null || context.getUserTimeZone().isEmpty()) {
            metricReporterService.addStateModuleForEntity(
                    "USER_TIME_ZONE_NOT_DEFINED",
                    "OpportunityContextBuilderOpenRtb2_3");
            context.stopProcessing(this.getClass().getSimpleName());
            return;

        }

        Integer timeZoneValue = timeZoneToOffsetWithUtcMap.get(context.getUserTimeZone());
        if (timeZoneValue != null) {
            context.setUserTimeZonDifferenceWithUTC(timeZoneValue);
        } else {
            context.setUserTimeZonDifferenceWithUTC(DateTimeUtil.getUtcOffsetBasedOnTimeZone(context.getUserTimeZone()));
            Integer value = context.getUserTimeZonDifferenceWithUTC();
            timeZoneToOffsetWithUtcMap.put(context.getUserTimeZone(), value);
        }
    }

    private void setDeviceForContext(
            OpportunityContext context,
            BidRequest bidRequest) {
        assertAndThrow(!context.getExchangeName().isEmpty());
        context.setDeviceClass(DeviceClassConverter.convert
                (bidRequest.getDevice().deviceType, OpenRtbVersion.V2_3));
        context.setDeviceType(DeviceTypeValue.DISPLAY);
        if (bidRequest.getUser().getBuyeruid().isEmpty()) {
            //exchange has not sent us our id or the user is unknown to us
            metricReporterService.addStateModuleForEntity(
                    "UNMAPPED_USER_FROM-exchange" + context.getExchangeName(),
                    "OpportunityContextBuilderOpenRtb2_3",
                    "ALL");
            context.stopProcessing(this.getClass().getSimpleName());
            context.setDevice(
                    new NomadiniDevice(NomadiniDevice.UNMAPPED_USER,
                            context.getDeviceType(),
                            context.getDeviceClass()));
        } else {
            //we rebuild the device to make sure id sent to us is in correct format
            context.setDevice(new NomadiniDevice(
                    bidRequest.getUser().getBuyeruid(),
                    DeviceTypeValue.DISPLAY,
                    context.getDeviceClass()));

            metricReporterService.addStateModuleForEntity("MAPPED_USER", "OpportunityContextBuilderOpenRtb2_3", "ALL");
        }
    }

    void recordStatesForDebugging(OpportunityContext context) throws JsonProcessingException {
        metricReporterService.addStateModuleForEntity("CREATIVE_SECURE_REQUIRED:" + toStr(context.isSecureCreativeRequired()),
                "OpportunityContextBuilderOpenRtb2_3",
                "ALL");

        metricReporterService.addStateModuleForEntity("DEVICE_TYPE : " + context.getDeviceType(),
                "OpportunityContextBuilderOpenRtb2_3",
                "ALL");

        metricReporterService.addStateModuleForEntity("AD_SIZE : " + context.getAdSize(),
                "OpportunityContextBuilderOpenRtb2_3",
                "ALL");

        metricReporterService.addStateModuleForEntity("DEVICE_COUNTRY : " + context.getDeviceCountry(),
                "OpportunityContextBuilderOpenRtb2_3",
                "ALL");

        metricReporterService.addStateModuleForEntity("CREATIVE_API:" + OBJECT_MAPPER.writeValueAsString(context.getCreativeAPI()),
                "OpportunityContextBuilderOpenRtb2_3",
                "ALL");

    }
}

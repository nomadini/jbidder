package com.dstillery.bidder.contexts;


import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.OpenRtbVersion;
import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class OpportunityContextBuilder {

    private AtomicLong uniqueNumberOfContext;
    private OpportunityContextBuilderOpenRtb2_3 opportunityContextBuilderOpenRtb2_3;
    private final MetricReporterService metricReporterService;
    public OpportunityContextBuilder(AtomicLong uniqueNumberOfContext,
                                     OpportunityContextBuilderOpenRtb2_3 opportunityContextBuilderOpenRtb2_3,
                                     MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.uniqueNumberOfContext = uniqueNumberOfContext;
        this.opportunityContextBuilderOpenRtb2_3 = opportunityContextBuilderOpenRtb2_3;
    }

    public void configureContextFromBidRequest(
            OpportunityContext context,
            OpenRtbVersion openRtbVersion,
            String exchangeName) throws IOException {
        context.setExchangeName(exchangeName);
        context.setOpenRtbVersion(openRtbVersion);
        boolean parsed = false;
        if (openRtbVersion == OpenRtbVersion.V2_3) {
            opportunityContextBuilderOpenRtb2_3.configureContextFromBidRequest(
                    context,
                    context.getRequestBody());
            parsed = true;

        } else if (openRtbVersion == OpenRtbVersion.V2_2) {
            //configure the other version

        } else {
            metricReporterService.addStateModuleForEntity(
                    "UNKNOWN_OPEN_RTB_SEEN",
                    "OpportunityContextBuilder",
                    "ALL");
        }
        assertAndThrow(parsed);
    }

}

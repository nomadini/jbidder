package com.dstillery.bidder.contexts;

import com.dstillery.common.metric.OpenRtbVersion;
import com.dstillery.common.targetgroup.AdPosition;

import static com.dstillery.common.targetgroup.AdPosition.ABOVE_THE_FOLD;
import static com.dstillery.common.targetgroup.AdPosition.ANY;
import static com.dstillery.common.targetgroup.AdPosition.BELOW_THE_FOLD;
import static com.dstillery.common.targetgroup.AdPosition.DEPRECATED;
import static com.dstillery.common.targetgroup.AdPosition.FOOTER;
import static com.dstillery.common.targetgroup.AdPosition.FULL_SCREEN;
import static com.dstillery.common.targetgroup.AdPosition.HEADER;
import static com.dstillery.common.targetgroup.AdPosition.SIDEBAR;

public class AdPositionConverter {

    public static AdPosition convert(int position, OpenRtbVersion openRtbVersion) {
        if (openRtbVersion == OpenRtbVersion.V2_3 ||
                openRtbVersion == OpenRtbVersion.V2_2) {
                /*
                   based on open rtb 2.3 these are the ad positions
                   0 -> Unknown
                   1 ->Above the Fold
                   2 -> DEPRECATED - May or may not be initially visible depending on screen size/resolution.
                   3 ->Below the Fold
                   4 ->Header
                   5 ->Footer
                   6 ->Sidebar
                   7 ->Full Screen

                   These are the options in Mango!!! lines 27 till 34 should exactly match with lines 35 to 45
                   <option value="ANY" >Any</option>
                      <option value="ABOVE_THE_FOLD" >Above the Fold</option>
                      <option value="BELOW_THE_FOLD">Below the Fold</option>
                      <option value="HEADER" >Header</option>
                      <option value="FOOTER" >Footer</option>
                      <option value="SIDEBAR" >Sidebar</option>
                      <option value="FULL_SCREEN">Full Screen</option>
                 */
            if (position == 0) { return ANY; }
            if (position == 1) { return ABOVE_THE_FOLD; }
            if (position == 2) { return DEPRECATED; }
            if (position == 3) { return BELOW_THE_FOLD; }
            if (position == 4) { return HEADER; }
            if (position == 5) { return FOOTER; }
            if (position == 6) { return SIDEBAR; }
            if (position == 7) { return FULL_SCREEN; }
            throw new RuntimeException("unknown position " + position + " for version " + openRtbVersion);
        }

        throw new RuntimeException("open rtb version not supported " + openRtbVersion);
    }
}

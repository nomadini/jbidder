package com.dstillery.bidder.contexts;

import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.metric.OpenRtbVersion;

import static com.dstillery.common.device.DeviceClassValue.DESKTOP;
import static com.dstillery.common.device.DeviceClassValue.SMART_PHONE;
import static com.dstillery.common.device.DeviceClassValue.TABLET;
import static com.dstillery.common.device.DeviceClassValue.UNKNOWN;

public class DeviceClassConverter {

/**
 * this function takes a number from openRtbVersion and return the device type that
 * we support which are desktop, SMART_PHONE, tablet, or unknown
 ["DESKTOP","TABLET","SMART_PHONE"] the values that we support are found
 in targetgroup_device_type_target_map table
 */
static DeviceClassValue convert(int deviceTypeId, OpenRtbVersion openRtbVersion) {

        if (openRtbVersion == OpenRtbVersion.V2_3) {
            if (deviceTypeId == 1 ) {
                return TABLET;
            }
            if (deviceTypeId == 2 ) {
                return DESKTOP;
            }
            if (deviceTypeId == 3 ) {
                return UNKNOWN;//this is not of a type that we support , so we return unknown
            }
            if (deviceTypeId == 4 ) {
                return SMART_PHONE;
            }
            if (deviceTypeId == 5 ) {
                return TABLET;
            }
            if (deviceTypeId == 6 ) {
                return UNKNOWN;
            }
            if (deviceTypeId == 7 ) {
                return UNKNOWN;
            }
        }
        throw new RuntimeException ("open rtb version doesn't have a specific device class type, " + openRtbVersion);
    }

}

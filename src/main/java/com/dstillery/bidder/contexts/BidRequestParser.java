package com.dstillery.bidder.contexts;

import com.dstillery.openrtb.beans.BidRequest;

import java.io.IOException;

import static com.dstillery.openrtb.BidRequestParser.PARSER_MAPPER;

public class BidRequestParser {
    public BidRequest parseBidRequest(String requestBody) {
        try {
            return PARSER_MAPPER.readValue(requestBody, BidRequest.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

package com.dstillery.bidder.contexts;

import com.dstillery.common.creative.CreativeApi;

import java.util.ArrayList;
import java.util.List;

public class CreativeAPIConverter {
    public static List<CreativeApi> convertApiToName(List<Integer> apis, String openRtbVersion) {
        List<CreativeApi> apiNames = new ArrayList<>();

        if (openRtbVersion.equalsIgnoreCase("2.3")) {
            for (int api : apis) {
                if (api < 1 || api > 5) {
                    throw new RuntimeException("wrong value for api : " + api);
                }
                if (api == 1) {
                    apiNames.add(CreativeApi.VPAID1_0);
                } else if (api == 2) {
                    apiNames.add(CreativeApi.VPAID2_0);
                } else if (api == 3) {
                    apiNames.add(CreativeApi.MRAID_1);
                } else if (api == 4) {
                    apiNames.add(CreativeApi.ORMMA);
                } else if (api == 5) {
                    apiNames.add(CreativeApi.MRAID_2);
                }
            }
            return apiNames;
        }
        throw new RuntimeException ("openRtb version is not supported : " + openRtbVersion);
    }
}

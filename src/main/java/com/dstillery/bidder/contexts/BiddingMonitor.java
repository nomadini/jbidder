package com.dstillery.bidder.contexts;
//
// Created by Mahmoud Taabodi on 5/7/16.
//

import static com.dstillery.common.util.StringUtil.assertAndThrow;
import java.util.concurrent.atomic.AtomicLong;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

/*
   a BiddingMonitor is used by BidModeControllerService to determineBidMode
 */
public class BiddingMonitor {

    private AtomicLong failureThresholdPercentage;
    private String nameOfMonitor;
    protected AtomicLong numberOfFailures;
    protected AtomicLong numberOfTotal;

    public BiddingMonitor(
            int failureThresholdPercentageArg,
            String nameOfMonitor)  {
        numberOfFailures = new AtomicLong();
        numberOfTotal = new AtomicLong();
        this.nameOfMonitor = nameOfMonitor;

        assertAndThrow(failureThresholdPercentageArg >= 0);
        assertAndThrow(failureThresholdPercentageArg <= 100);
        failureThresholdPercentage = new AtomicLong(failureThresholdPercentageArg);

    }

    public String getName() {
        return nameOfMonitor;
    }

    public void resetMeasures() {
        numberOfFailures.set(0);
        numberOfTotal.set(0);
    }

    public boolean isFailing() {

        if (numberOfTotal.get() == 0 || numberOfTotal.get() < 100) {
            //we return false for the first 100 requests
            return false;
        }
        double actualPercentageFailure = (numberOfFailures.get() * 100 ) / numberOfTotal.get();
        //                //LOG_EVERY_N(ERROR, 1) ,
        //                        "nameOfMonitor : {} ",  nameOfMonitor,
        //                        "actualPercentageFailure: " , actualPercentageFailure ,
        //                        "numberOfFailures: " , numberOfFailures.getValue() ,
        //                        "numberOfTotal: " , numberOfTotal.getValue() ,
        //                        "failureThresholdPercentage: " , failureThresholdPercentage.getValue();
        return actualPercentageFailure >= failureThresholdPercentage.get();

    }

    public AtomicLong getFailureThresholdPercentage() {
        return failureThresholdPercentage;
    }

    public void setFailureThresholdPercentage(AtomicLong failureThresholdPercentage) {
        this.failureThresholdPercentage = failureThresholdPercentage;
    }

    public String getNameOfMonitor() {
        return nameOfMonitor;
    }

    public void setNameOfMonitor(String nameOfMonitor) {
        this.nameOfMonitor = nameOfMonitor;
    }

    public AtomicLong getNumberOfFailures() {
        return numberOfFailures;
    }

    public void setNumberOfFailures(AtomicLong numberOfFailures) {
        this.numberOfFailures = numberOfFailures;
    }

    public AtomicLong getNumberOfTotal() {
        return numberOfTotal;
    }

    public void setNumberOfTotal(AtomicLong numberOfTotal) {
        this.numberOfTotal = numberOfTotal;
    }
}

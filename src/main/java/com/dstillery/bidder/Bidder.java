package com.dstillery.bidder;

import java.util.Collections;
import java.util.HashSet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.dstillery.common.config.BidderAdServerCacheConfig;
import com.dstillery.common.config.EssentialConfig;
import com.dstillery.common.config.CacheServiceInitializer;
import com.dstillery.common.config.CommonBeansConfig;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@PropertySources({
        @PropertySource(value = "classpath:bidder.properties"),
        @PropertySource(value = "file://${PROPERTY_FILE:/etc/bidder/bidder.properties}",
                ignoreResourceNotFound = true) })
@Import({
        BidderEssentialConfig.class,
        EssentialConfig.class,
        CommonBeansConfig.class,
        BidderAdServerCacheConfig.class,
        BidderConfig.class})
@ComponentScan(basePackages = {
        "com.dstillery.bidder",
        "com.dstillery.bidder.requesthandling",
})
@EnableDiscoveryClient
public class Bidder {
    public static void main(String[] args) {
        SpringApplication sa = new SpringApplication();
        sa.addListeners(new CacheServiceInitializer());
        sa.setSources(new HashSet<>(Collections.singletonList(Bidder.class.getName())));
        ConfigurableApplicationContext context = sa.run(args);
    }
}

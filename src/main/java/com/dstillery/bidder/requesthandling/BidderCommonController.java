package com.dstillery.bidder.requesthandling;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dstillery.common.service.dicovery.DiscoveryService;

@RestController
public class BidderCommonController {

    @Autowired
    private DiscoveryService discoveryService;

    @RequestMapping("/service-instances/{applicationName}")
    public List<ServiceInstance> serviceInstancesByApplicationName(
            @PathVariable String applicationName) {
        return this.discoveryService.getInstances(applicationName);
    }

    @RequestMapping("/service-instances/all")
    public List<String> getAllServices() {
        return this.discoveryService.getAllServices();
    }
}


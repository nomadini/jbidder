package com.dstillery.bidder.requesthandling;

import com.dstillery.bidder.main.BiddingMode;

/**
 tasks of this class :
 1. provides an end point for other apps to get the status of bidder
 */
public class BidderStatusService {

    private BiddingMode bidMode;

    public BidderStatusService(BiddingMode bidMode) {
        this.bidMode = bidMode;
    }

    public String handleRequest() {
        return bidMode.getBidMode().toString();
    }

}


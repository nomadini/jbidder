package com.dstillery.bidder.requesthandling;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.OpenRtbVersion;

@RestController
public class BidderRequestController{

    private BidRequestHandler bidRequestHandler;
    private BidderStatusService bidderStatusService;

    private AtomicBoolean isNoBidModeIsTurnedOn;
    private AtomicLong bidPercentage;

    public BidderRequestController(BidderStatusService bidderStatusService,
                                   BidRequestHandler bidRequestHandler,
                                   AtomicBoolean isNoBidModeIsTurnedOn,
                                   AtomicLong bidPercentage) {
        this.bidderStatusService = bidderStatusService;
        this.bidRequestHandler = bidRequestHandler;
        this.isNoBidModeIsTurnedOn = isNoBidModeIsTurnedOn;
        this.bidPercentage = bidPercentage;

    }

    @RequestMapping(value = "slowBidMode/{percentage}", method = RequestMethod.GET)
    public void slowBidMode(@PathVariable("percentage") final Integer percentage) {

        if (percentage < 0 || percentage > 100) {
            throw new RuntimeException("wrong bidPercentage " + percentage);
        }
        bidPercentage.set(percentage);
    }

    @RequestMapping(value = "no_bid_mode_turn_on", method = RequestMethod.GET)
    public void slowBidOn() {
        isNoBidModeIsTurnedOn.set(true);
    }

    @RequestMapping(value = "bid/google/openrtb2.3", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String processGoogleBidRequest(
            HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        OpportunityContext context = new OpportunityContext();
        context.setOpenRtbVersion(OpenRtbVersion.V2_3);
        context.setExchangeName("google");

        bidRequestHandler.handleRequest(context, request, response);
        return context.getFinalBidResponseContent();
    }

    @RequestMapping(value = "no_bid_mode_turn_off", method = RequestMethod.GET)
    public void slowBidOff() {
        isNoBidModeIsTurnedOn.set(false);
    }

    @RequestMapping(value = "status", method = RequestMethod.GET)
    public String status() {
        return bidderStatusService.handleRequest();
    }
}


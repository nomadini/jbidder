package com.dstillery.bidder.requesthandling;


import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Timer;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.pipeline.BidRequestHandlerPipelineProcessor;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.GUtil;
import com.dstillery.common.util.HttpUtil;

public class BidRequestHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidRequestHandler.class);

    private BidRequestHandlerPipelineProcessor bidRequestHandlerPipelineProcessor;
    private MetricReporterService metricReporterService;
    private OpportunityContext context;
    private AtomicLong totalNumberOfRequestsProcessedInFilterCountPeriod;
    private AtomicLong numberOfRequestsProcessingNow;

    public BidRequestHandler(
            BidRequestHandlerPipelineProcessor bidRequestHandlerPipelineProcessor,
            MetricReporterService metricReporterService,
            AtomicLong totalNumberOfRequestsProcessedInFilterCountPeriod,
            AtomicLong numberOfRequestsProcessingNow) {
        this.bidRequestHandlerPipelineProcessor = bidRequestHandlerPipelineProcessor;
        this.metricReporterService = metricReporterService;
        this.totalNumberOfRequestsProcessedInFilterCountPeriod = totalNumberOfRequestsProcessedInFilterCountPeriod;
        this.numberOfRequestsProcessingNow = numberOfRequestsProcessingNow;
    }

    public void handleRequest(OpportunityContext context,
                              HttpServletRequest request,
                              HttpServletResponse response) throws IOException {

        final Timer.Context timerContext = metricReporterService.getTimerContext(this.getClass().getSimpleName());
        try {
            numberOfRequestsProcessingNow.getAndIncrement();
            handleRequestLevel1(context, request, response);
            numberOfRequestsProcessingNow.decrementAndGet();
        } finally {
            timerContext.stop();
        }
    }

    private void handleRequestLevel1(OpportunityContext context,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws IOException {

        try {
            //this is an important metric, don't remove
            metricReporterService.addStateModuleForEntity("numberOfBidsRecieved",
                    this.getClass().getSimpleName(),
                    "ALL");
            populateRequestBody(context, request);
            bidRequestHandlerPipelineProcessor.process(context);
            if (context.getFinalBidResponseContent().isEmpty()) {
                context.setBidResponseStatusCode(HTTP_NO_CONTENT);
                if (GUtil.allowedToCall(1000)) {
                    LOGGER.debug("no bid response : {}", context.getFinalBidResponseContent());
                }
            } else {
                if (GUtil.allowedToCall(1000)) {
                    LOGGER.debug("do bid response : {}", context.getFinalBidResponseContent());
                }
            }
            assertAndThrow(context.getBidResponseStatusCode() == HTTP_OK
                    || context.getBidResponseStatusCode() == HTTP_NO_CONTENT);
            response.setStatus(context.getBidResponseStatusCode());
        } catch (Exception e) {
            LOGGER.warn("exception happened during bidding", e);
            metricReporterService.addStateModuleForEntity("Unknown Exception ",
                    this.getClass().getSimpleName(),
                    "ALL");
            //TODO we must go red here
            HttpUtil.setEmptyResponse (response);
        }
    }

    private void populateRequestBody(OpportunityContext context, HttpServletRequest request) throws IOException {
        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String requestBody = buffer.toString();
        LOGGER.trace("requestBody : {}", requestBody);
        context.setRequestBody(buffer.toString());
    }
}


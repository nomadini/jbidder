package com.dstillery.bidder.main;




import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static com.dstillery.common.util.StringUtil.toStr;
import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoFetcher;
import com.dstillery.bidder.modules.filters.CacheUpdateWatcher;
import com.dstillery.bidder.pipeline.DataReloadPipeline;
import com.dstillery.common.DataWasReloadedEvent;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.whitelistedModelingDomains.GlobalWhiteListedModelCacheService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetGroupCacheService;
import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.common.util.GUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

public class BidderAsyncJobsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidderAsyncJobsService.class);
    private AtomicLong lastTimeEventWasProcessed = new AtomicLong(DateTimeUtil.getNowInSecond());

    private final MetricReporterService metricReporterService;
    private List<CacheUpdateWatcher> cacheUpdateWatcherModules = new ArrayList<>();//TODO finish this feature

    private AtomicBoolean isReloadingDataInProgress;

    private Set<String> mapOfInterestingFeatures;
    private EntityDeliveryInfoFetcher entityDeliveryInfoFetcher;private List<TargetGroup> targetGroupsEligibleForBidding;

    private DataReloadPipeline dataReloadPipeline;
    private TargetGroupCacheService targetGroupCacheService;
    private GlobalWhiteListedModelCacheService globalWhiteListedModelCacheService;
    private AtomicBoolean forceRefreshCachesFlag;

    public BidderAsyncJobsService(
            EventBus eventBus,
            Set<String> mapOfInterestingFeatures,
            EntityDeliveryInfoFetcher entityDeliveryInfoFetcher,
            TargetGroupCacheService targetGroupCacheService,
            ConfigService configService,
            GlobalWhiteListedModelCacheService globalWhiteListedModelCacheService,
            List<TargetGroup> targetGroupsEligibleForBidding,
            AtomicBoolean isReloadingDataInProgress,
            AtomicBoolean forceRefreshCachesFlag,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.mapOfInterestingFeatures = requireNonNull(mapOfInterestingFeatures);
        this.entityDeliveryInfoFetcher = requireNonNull(entityDeliveryInfoFetcher);
        this.forceRefreshCachesFlag = requireNonNull(forceRefreshCachesFlag);

        this.isReloadingDataInProgress = requireNonNull(isReloadingDataInProgress);
        this.targetGroupsEligibleForBidding = requireNonNull(targetGroupsEligibleForBidding);
        this.targetGroupCacheService = requireNonNull(targetGroupCacheService);
        this.globalWhiteListedModelCacheService = requireNonNull(globalWhiteListedModelCacheService);

        eventBus.register(this);
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this::checkRefreshFlag, 0, 10, TimeUnit.SECONDS);
    }

    private void refreshBidderData() {
        try {

            isReloadingDataInProgress.set(true);

            metricReporterService.addStateModuleForEntity("refreshBidderData",
                    "BidderAsyncJobsService",
                    "ALL");

            List<TargetGroup> currentTgs = targetGroupCacheService.getAllCurrentTargetGroups();
            if (currentTgs.isEmpty()) {
                LOGGER.error("no current targetgroups exists out of {} targetgroups",
                        targetGroupCacheService.getAllEntities().size());
                return;
            }

            List<Long> allTgIds = targetGroupCacheService.getAllEntityIds();
            if (allTgIds.isEmpty()) {
                LOGGER.error("no targetgroups ids loaded from data master");
                return;
            }

            List<Long> allCampaignIds = new ArrayList<>();
            for (TargetGroup tg : currentTgs) {
                allCampaignIds.add(tg.getCampaignId());
            }

            //TODO add client delivery cap too
            // List<Integer> allClientIds;
            // beanFactory.clientCacheService.getAllCurrentTargetGroups();
            // entityDeliveryInfoFetcher.getLatestRealTimeInfo(Client.getEntityName(), allCampaignIds);

            entityDeliveryInfoFetcher.getLatestRealTimeInfo("targetGroup", new HashSet<>(allTgIds));
            entityDeliveryInfoFetcher.getLatestRealTimeInfo("campaign", new HashSet<>(allCampaignIds));

            String size = toStr(currentTgs.size());
            metricReporterService.addStateModuleForEntity("#CurrentTargetGroups:" + size,
                    "BidderAsyncJobsService",
                    "ALL");

            // here we run the targetgroups data filters
            List<TargetGroup> targetgroupsAvailableAfterPipeline = dataReloadPipeline.process(currentTgs);

            size = toStr(targetgroupsAvailableAfterPipeline.size());
            metricReporterService.addStateModuleForEntity("#ClearedTargetGroups:" + size,
                    "BidderAsyncJobsService",
                    "ALL");

            //here we refill the targetgroups that are eligible for bidding
            refillTargetGroupsEligibleForBidding(targetgroupsAvailableAfterPipeline);
            mapOfInterestingFeatures.clear();

            for (Entry<String, Boolean> entry :
                    globalWhiteListedModelCacheService.getAllWhiteListedDomains().entrySet()) {
                mapOfInterestingFeatures.add(entry.getKey());
            }

            //very important to clear these caches
            for (CacheUpdateWatcher module : cacheUpdateWatcherModules) {
                // we clear all the filtered calculated caches.
                //very important!!
                module.cacheWasUpdatedEvent();
            }
        } catch (Exception e) {
            LOGGER.error("error happening when reloading caches", e);
            metricReporterService.addStateModuleForEntity(
                    "ERROR_WHILE_RELOADING_CACHES",
                    "BidderAsyncJobsService",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.EXCEPTION);
            LOGGER.warn("exception occurred", e);
        }

        //sleep 4 seconds for all AdServers and Pacers to reload their caches as well
        //so they have updated data
        GUtil.sleepInSeconds( 4);

        isReloadingDataInProgress.set(false);
        LOGGER.debug("done with cache reload");
    }

    private void refillTargetGroupsEligibleForBidding(
            List<TargetGroup> targetgroupsAvailableAfterPipeline) {
        targetGroupsEligibleForBidding.clear();
        targetGroupsEligibleForBidding.addAll(targetgroupsAvailableAfterPipeline);

        if (targetGroupsEligibleForBidding.isEmpty()) {
            metricReporterService.addStateModuleForEntity(
                    "NO_TARGET_GROUP_ELIGIBLE_FOR_BIDDING",
                    "BidderAsyncJobsService",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.EXCEPTION);
            LOGGER.error("no targetgroups selected for pipeline, initial loaded current tgs : {}, initial loaded tgs : {}"
                    ,targetGroupCacheService.getAllCurrentTargetGroups().size()
                    ,targetGroupCacheService.getAllEntities().size());

        }
    }

    public void checkRefreshFlag() {
        if (forceRefreshCachesFlag.get()) {
            long secondsAgoWasProcessed = DateTimeUtil.getNowInSecond() - lastTimeEventWasProcessed.get();
            if (secondsAgoWasProcessed >= 10) {
                LOGGER.info("refreshing bidder data based on event");
                lastTimeEventWasProcessed.set(DateTimeUtil.getNowInSecond());
                refreshBidderData();
            } else {
                LOGGER.info("refreshing bidder data is in progress. ignoring event to refresh data, secondsAgoWasProcessed : {}",
                        secondsAgoWasProcessed);
            }
            forceRefreshCachesFlag.set(false);
        }
    }

    public void run() {
        try {
            GUtil.sleepInSeconds(1);
            LOGGER.info("reloading the caches now");
            refreshBidderData();
        } catch(Exception e) {
            LOGGER.warn("exception when reloading caches", e);
        }
    }

    /*
        setter-based injection used by spring boot to avoid circular dependency
    */
    @Autowired
    public void setDataReloadPipeline(DataReloadPipeline dataReloadPipeline) {
        this.dataReloadPipeline = dataReloadPipeline;
    }


    @Subscribe
    public void processEvent(DataWasReloadedEvent event) {
        LOGGER.info("event received : {} by class : {}", event);
        run();
    }
}

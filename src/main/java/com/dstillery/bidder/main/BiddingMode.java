package com.dstillery.bidder.main;/*
  BiddingMode.h
 *
   Created on: Aug 26, 2015
       Author: mtaabodi
 */



import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class BiddingMode {

    private List<String> bidModeReasons = new ArrayList<>();
    private AtomicBoolean bidMode;

    public BiddingMode() {
        bidMode = new AtomicBoolean(true);
    }

    public void setStatus(boolean mode, String state, MetricReporterService metricReporterService) {
        bidModeReasons.add(state);
        bidMode.set(mode);
        metricReporterService.addStateModuleForEntity(
                (bidMode.get() ? "DO_BID" : "NO_BID") +"_BECAUSE_"  + state, "BiddingMode", "ALL");
    }

    public List<String> getBidModeReasons() {
        return bidModeReasons;
    }

    public void setBidModeReasons(List<String> bidModeReasons) {
        this.bidModeReasons = bidModeReasons;
    }

    public AtomicBoolean getBidMode() {
        return bidMode;
    }

    public void setBidMode(AtomicBoolean bidMode) {
        this.bidMode = bidMode;
    }

    @Override
    public String toString() {
        return "BiddingMode{" +
                ", bidModeReasons=" + bidModeReasons +
                ", bidMode=" + bidMode +
                '}';
    }
}

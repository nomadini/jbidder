package com.dstillery.bidder.main.asynctasks;


import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;


import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.main.BidModeControllerService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.common.util.FileNomadiniUtil;

public class MinutelyTask implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(MinutelyTask.class);
    private BidModeControllerService bidModeControllerService;
    private AtomicLong numberOfExceptionsInWritingEvents;
    private AtomicLong numberOfBidRequestRecievedPerMinute;

    private final MetricReporterService metricReporterService;
    public MinutelyTask(
            BidModeControllerService bidModeControllerService,
            AtomicLong numberOfExceptionsInWritingEvents,
            AtomicLong numberOfBidRequestRecievedPerMinute,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.bidModeControllerService = bidModeControllerService;
        this.numberOfBidRequestRecievedPerMinute = numberOfBidRequestRecievedPerMinute;
        this.numberOfExceptionsInWritingEvents = numberOfExceptionsInWritingEvents;
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0, 1, TimeUnit.MINUTES);
    }

    @PostConstruct
    public void populateCache() {
        LOGGER.info("populating feature registry cache");
//        realTimeFeatureRegistryCacheUpdaterService.populateAerospikeFeatureCache();
    }

    @Override
    public void run() {

        try {

            updateProcessAliveFile();
            bidModeControllerService.resetAllMonitors();

            metricReporterService.addStateModuleForEntity("runEveryMinute",
                    "BidderAsyncJobsService",
                    "ALL");

            numberOfBidRequestRecievedPerMinute.set(0);

            if (numberOfExceptionsInWritingEvents.get() < 2 &&
                    numberOfExceptionsInWritingEvents.get() >0 ) {
                //in case of two exceptions per minute , we force the event log writer to become healthy again.
                numberOfExceptionsInWritingEvents.set(0);
                //LOGGER.warn("forcing eventlog writer to become healthy again";
            }
        } catch (Exception e) {
            LOGGER.warn("exception occurred", e);
            metricReporterService.addStateModuleForEntity(
                    "EXCEPTION_IN_RUN_EVERY_MINUTE",
                    "BidderAsyncJobsService",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.EXCEPTION
            );
        }

    }

    private void updateProcessAliveFile() throws IOException {
        FileNomadiniUtil.writeStringToFile("/tmp/lastTimeBidderRan.txt", DateTimeUtil.getNowInSecond() + "");
    }

}

package com.dstillery.bidder.main.asynctasks;



import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.MySqlTargetGroupFilterCountDbRecordService;
import com.dstillery.common.targetgroup.TargetGroupFilterCountDbRecord;
import com.dstillery.common.targetgroup.TargetGroupFilterStatistic;
import com.dstillery.common.util.StringUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

public class FilterStatsPersistTask implements Runnable{
    private static final Logger LOGGER = LoggerFactory.getLogger(FilterStatsPersistTask.class);

    private MySqlTargetGroupFilterCountDbRecordService mySqlTargetGroupFilterCountDbRecordService;
    private AtomicLong totalNumberOfRequestsProcessedInFilterCountPeriod;

    private Map<String, TgMarker> allMarkersMap;
    private final MetricReporterService metricReporterService;
    public FilterStatsPersistTask(Map<String, TgMarker> allMarkersMap,
                                  MySqlTargetGroupFilterCountDbRecordService mySqlTargetGroupFilterCountDbRecordService,
                                  AtomicLong totalNumberOfRequestsProcessedInFilterCountPeriod,
                                  MetricReporterService metricReporterService) {
        this.totalNumberOfRequestsProcessedInFilterCountPeriod = totalNumberOfRequestsProcessedInFilterCountPeriod;
        this.mySqlTargetGroupFilterCountDbRecordService = mySqlTargetGroupFilterCountDbRecordService;
        this.metricReporterService = metricReporterService;
        this.allMarkersMap = allMarkersMap;
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0, 1, TimeUnit.MINUTES);
    }

    private void persistFilterCounts() throws JsonProcessingException {

        List<TargetGroupFilterCountDbRecord> allFilterCounts = new ArrayList<>();

        long totalCount = totalNumberOfRequestsProcessedInFilterCountPeriod.get();
        if (totalCount < 100) {
            LOGGER.warn("low number of request processed : {} ", totalCount);
            metricReporterService.addStateModuleForEntity("LOW_NUMBER_OF_REQUEST_PROCESSED",
                    "BidderAsyncJobsService",
                    "ALL",
                    MetricReporterService.MetricPriority.EXCEPTION);

        }
        if (totalCount == 0) {
            totalCount = 1;
        }
        LOGGER.debug("totalCount : {}", totalCount);

        for (Entry<String, TargetGroupFilterStatistic> entry :  TgMarker.getFilterNameToFailureCounts().entrySet()) {
            TargetGroupFilterStatistic targetGroupFilterStatistic = entry.getValue();
            String tgAndNamePair = entry.getKey();

            LOGGER.debug("tgAndNamePair : {}, targetGroupFilterStatistic : {}",
                    tgAndNamePair, OBJECT_MAPPER.writeValueAsString(targetGroupFilterStatistic));
            List<String> tgAndNamePairVector = StringUtil.tokenizeString(tgAndNamePair, "___");

            assertAndThrow(tgAndNamePairVector.size() == 3);
            TargetGroupFilterCountDbRecord dbRecord = new TargetGroupFilterCountDbRecord();
            dbRecord.setEntityId(tgAndNamePairVector.get(1) + tgAndNamePairVector.get(0)); //we want to make this searchable in datatables
            dbRecord.setEntityType(tgAndNamePairVector.get(1));
            dbRecord.setFilterName(tgAndNamePairVector.get(2));
            long numberOfFailures = targetGroupFilterStatistic.getNumberOfFailures().get();
            long numberOfPasses = targetGroupFilterStatistic.getNumberOfPasses().get();
            dbRecord.getNumberOfFailures().set(numberOfFailures);
            dbRecord.getNumberOfPasses().set(numberOfPasses);
            dbRecord.getTotalNumberOfRequest().set(totalCount);
            TgMarker pair = allMarkersMap.get(dbRecord.getFilterName());
            if (pair == null) {
                LOGGER.warn("no marker filter found for filterName : {}" , dbRecord.getFilterName());
                dbRecord.setFilterType(TgMarker.FREQUNT_MARKER);
            } else {
                dbRecord.setFilterType(pair.markerType);
            }

            long total = numberOfFailures +  numberOfPasses;
            double failurePercentage = 0.0;
            if (total > 0) {
                failurePercentage = numberOfFailures * 100 / (total);
            }
            dbRecord.getPercentageOfFailures().set(failurePercentage);

            long totalCalls = numberOfFailures +  numberOfPasses;
            dbRecord.getTotalNumberOfCalls().set(totalCalls);

            Instant now = Instant.now();
            dbRecord.setTimeReported(now);
            allFilterCounts.add(dbRecord);
        }
        TgMarker.getFilterNameToFailureCounts().clear();
        //
        mySqlTargetGroupFilterCountDbRecordService.insert(allFilterCounts);
        totalNumberOfRequestsProcessedInFilterCountPeriod.set(0);
        mySqlTargetGroupFilterCountDbRecordService.deleteDataOlderThanNDays(100);
    }

    @Override
    public void run() {
        try {
            persistFilterCounts();
        } catch (Exception e) {
            LOGGER.warn("exception occurred", e);
        }
    }
}

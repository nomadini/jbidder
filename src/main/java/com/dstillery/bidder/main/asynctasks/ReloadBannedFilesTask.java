package com.dstillery.bidder.main.asynctasks;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.common.util.FileNomadiniUtil;
import com.dstillery.common.util.StringUtil;

public class ReloadBannedFilesTask implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReloadBannedFilesTask.class);

    private ConcurrentSkipListSet<String> bannedDevices;
    private ConcurrentSkipListSet<String> bannedIps;
    private ConcurrentSkipListSet<String> bannedMgrs10ms;

    public ReloadBannedFilesTask(ConcurrentSkipListSet<String> bannedDevices,
                                 ConcurrentSkipListSet<String> bannedIps,
                                 ConcurrentSkipListSet<String> bannedMgrs10ms) {
        this.bannedDevices = bannedDevices;
        this.bannedIps = bannedIps;
        this.bannedMgrs10ms = bannedMgrs10ms;
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0, 10, TimeUnit.MINUTES);
    }

    void reloadBannedDevices(String fileName) throws IOException {
        String fileFullName = "/tmp/" + fileName + ".txt";
        List<String> allLines = FileNomadiniUtil.readFileLineByLine(fileFullName);
        for (String line : allLines) {
            List<String> contents = StringUtil.tokenizeString(line, ",");
            assertAndThrow(contents.size() == 2);
            bannedDevices.add(contents.get(0));
        }
    }

    void reloadBannedIps(String fileName) throws IOException {
        String fileFullName = "/tmp/" + fileName + ".txt";
        List<String> allLines = FileNomadiniUtil.readFileLineByLine(fileFullName);
        for (String line : allLines) {
            List<String> contents = StringUtil.tokenizeString(line, ",");
            assertAndThrow(contents.size() == 2);
            bannedIps.add(contents.get(0));
        }
    }

    private void reloadBannedMgrs10ms(String fileName) throws IOException {
        String fileFullName = "/tmp/" + fileName + ".txt";
        List<String> allLines = FileNomadiniUtil.readFileLineByLine(fileFullName);
        for (String line : allLines) {
            List<String> contents = StringUtil.tokenizeString(line, ",");
            assertAndThrow(contents.size() == 2);
            //a bannedLatLon has a form like this "lat_lon" as string like "72.23_54.34"
            bannedMgrs10ms.add(contents.get(0));
        }
    }

    @Override
    public void run() {

        try {
            refreshFiles("top_common_devices_last7days");
            reloadBannedDevices("top_common_devices_last7days");

            refreshFiles("top_common_ips_last7days");
            reloadBannedIps("top_common_ips_last7days");

            refreshFiles("top_common_mgrs10m_last7days");
            reloadBannedMgrs10ms("top_common_mgrs10m_last7days");
        } catch (IOException e) {
            LOGGER.warn("exception occurred", e);
        }
    }

    private void refreshFiles(String fileName) throws IOException {
        // String fileName = "/tmp/top_common_devices";
        String fileFullName = "/tmp/" + fileName + ".txt";
        if (!FileNomadiniUtil.checkIfFileExists(fileFullName)
                || DateTimeUtil.getNowInSecond() - FileNomadiniUtil.getLastModifiedInSeconds(fileFullName) > 600) {
            LOGGER.debug("downloading file from server : {}", fileFullName);
            String url = "scp://67.205.170.169/tmp/" + fileName + "/part-00000";
            if (FileNomadiniUtil.checkIfFileExists(fileFullName)) {
                FileNomadiniUtil.deleteFile(fileFullName);
            }
            LOGGER.info("reloading banned devices feature is not working as expected");
//            HttpUtil.downloadFileUsingScpWithCurl(url, fileFullName);
        }
    }
}

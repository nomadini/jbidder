package com.dstillery.bidder.main.asynctasks;



import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.common.util.FileNomadiniUtil;

public class HourlyTask implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(HourlyTask.class);
    private AtomicLong numberOfBidsForPixelMatchingPurposePerHour;

    private final MetricReporterService metricReporterService;
    public HourlyTask(AtomicLong numberOfBidsForPixelMatchingPurposePerHour,
                      MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.numberOfBidsForPixelMatchingPurposePerHour = numberOfBidsForPixelMatchingPurposePerHour;
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0, 1, TimeUnit.HOURS);
    }

    @Override
    public void run() {

        try {
                metricReporterService.addStateModuleForEntity("runEveryHour",
                        "BidderAsyncJobsService",
                        "ALL");
          numberOfBidsForPixelMatchingPurposePerHour.set(0);

        } catch (Exception e) {
            LOGGER.warn("exception occurred", e);
        }
    }

    private void refreshFiles(String fileName) throws IOException {
        // String fileName = "/tmp/top_common_devices";
        String fileFullName = "/tmp/" + fileName + ".txt";
        if (!FileNomadiniUtil.checkIfFileExists(fileFullName)
                || DateTimeUtil.getNowInSecond() - FileNomadiniUtil.getLastModifiedInSeconds(fileFullName) > 600) {
            LOGGER.debug("downloading file from server : {}", fileFullName);
            String url = "scp://67.205.170.169/tmp/" + fileName + "/part-00000";
            if (FileNomadiniUtil.checkIfFileExists(fileFullName)) {
                FileNomadiniUtil.deleteFile(fileFullName);
            }
            LOGGER.info("reloading banned devices feature is not working as expected");
//            HttpUtil.downloadFileUsingScpWithCurl(url, fileFullName);
        }
    }
}

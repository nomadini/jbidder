package com.dstillery.bidder.main;




import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.StringUtil.toStr;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.contexts.BiddingMonitor;
import com.dstillery.bidder.contexts.IpInfoFetcherModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.filters.GeoFeatureFilter;
import com.dstillery.bidder.modules.healthcheck.AdServerHealthChecker;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.util.GUtil;
import com.dstillery.common.util.StringUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

// this service updates this field, which is read by NoBidModeEnforcerModule to decided whether to bid or not
public class BidModeControllerService implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidModeControllerService.class);

    private final MetricReporterService metricReporterService;
    private BiddingMode globalBidMode;
    private DiscoveryService discoveryService;
    private AdServerHealthChecker adServerHealthChecker;
    private ConfigService configService;
    private AtomicLong numberOfExceptionsInWritingEvents;
    private AtomicBoolean isNoBidModeIsTurnedOn;
    private AtomicBoolean isReloadProcessHealthy;
    private AtomicBoolean entityDeliveryInfoCacheServiceIsHealthy;
    private AtomicLong sizeOfKafkaScoringQueue;
    private AtomicLong queueSizeWaitingForAck;
    private AtomicLong numberOfCuncurrentReuqestsNow;
    private BlockingQueue<OpportunityContext> queueOfContexts;
    private AtomicLong numberOfContinuousCallInNoBidMode;

    private List<BiddingMonitor> allBiddingMonitors;

    public BidModeControllerService(AdServerHealthChecker adServerHealthChecker,
                                    DiscoveryService discoveryService,
                                    ConfigService configService,
                                    AtomicLong numberOfExceptionsInWritingEvents,
                                    AtomicBoolean isNoBidModeIsTurnedOn,
                                    AtomicBoolean isReloadingDataInProgress,
                                    AtomicBoolean isReloadProcessHealthy,
                                    AtomicLong numberOfExceptionsInWritingMessagesInKafka,
                                    AtomicLong sizeOfKafkaScoringQueue,
                                    AtomicLong queueSizeWaitingForAck,
                                    AtomicLong numberOfCuncurrentReuqestsNow,
                                    AtomicBoolean entityDeliveryInfoCacheServiceIsHealthy,
                                    BlockingQueue<OpportunityContext> queueOfContexts,
                                    GeoFeatureFilter geoFeatureFilter,
                                    IpInfoFetcherModule ipInfoFetcherModule,
                                    MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.entityDeliveryInfoCacheServiceIsHealthy = entityDeliveryInfoCacheServiceIsHealthy;
        this.discoveryService = discoveryService;
        this.adServerHealthChecker = adServerHealthChecker;
        this.configService = configService;
        this.numberOfExceptionsInWritingEvents = numberOfExceptionsInWritingEvents;
        this.isNoBidModeIsTurnedOn = isNoBidModeIsTurnedOn;
        this.isReloadProcessHealthy = isReloadProcessHealthy;
        this.sizeOfKafkaScoringQueue = sizeOfKafkaScoringQueue;
        this.queueSizeWaitingForAck = queueSizeWaitingForAck;
        this.numberOfCuncurrentReuqestsNow = numberOfCuncurrentReuqestsNow;
        this.queueOfContexts = queueOfContexts;
        numberOfContinuousCallInNoBidMode = new AtomicLong();
        allBiddingMonitors = new ArrayList<>();
        globalBidMode = new BiddingMode();
        allBiddingMonitors.add(geoFeatureFilter.getMonitor());
        allBiddingMonitors.add(ipInfoFetcherModule);

        LOGGER.info("checking bid mode data every {} seconds.", 5);
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0,
                5,
                TimeUnit.SECONDS);
    }

    public void run() {

        BiddingMode bidMode = new BiddingMode();
        List<String> reasons = new ArrayList<>();

        if (discoveryService.discoverAllServiceUrls("bidder").isEmpty()) {
            String reason = "There are no bidder instances up and running in eureka's world";
            reasons.add(reason);
            bidMode.setStatus(false, reason, metricReporterService);
        }

        if (!entityDeliveryInfoCacheServiceIsHealthy.get()) {
            String reason = "entityDeliveryInfoCacheServiceIsNotHealthy";
            reasons.add(reason);
            bidMode.setStatus(false, reason, metricReporterService);
        }

        if (!adServerHealthChecker.getAreAdserversHealthy().get()) {
            String reason = "AdServerStatusCheckerIsBusted";
            reasons.add(reason);
            bidMode.setStatus(false, reason, metricReporterService);
        }

        for (BiddingMonitor monitor : allBiddingMonitors) {
            if (monitor.isFailing()) {
                String reason = monitor.getClass().getSimpleName() + "IsBusted";
                reasons.add(reason);
                bidMode.setStatus(false, reason, metricReporterService);
            }
        }

        if (!isReloadProcessHealthy.get()) {
            String reason ="dataReloadServiceIsBusted";
            reasons.add(reason);
            bidMode.setStatus(false, reason, metricReporterService);
        }

        if (isNoBidModeIsTurnedOn.get()) {
            String reason = "bidderFactoryIsInNoBidMode";
            reasons.add(reason);
            bidMode.setStatus(false, reason, metricReporterService);
        }
        if (numberOfExceptionsInWritingEvents.get() > 10 ) {
            String reason = "eventLogHavingProblem:" + toStr(numberOfExceptionsInWritingEvents.get());
            reasons.add(reason);
            bidMode.setStatus(false, reason, metricReporterService);

        }

        if (sizeOfKafkaScoringQueue.get() > configService.getAsInt("scoring.limitOfQueueInBidder")) {
            metricReporterService.addStateModuleForEntity(
                    "sizeOfKafkaScoringQueue_too_large",
                    "BidModeControllerService",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.EXCEPTION);

            String reason = "EnqueueForScoringModule_sizeOfKafkaScoringQueue:" + toStr(sizeOfKafkaScoringQueue.get());
            reasons.add(reason);
            bidMode.setStatus(false, reason, metricReporterService);
        }

        if (queueOfContexts.size() > 10000 ) {
            LOGGER.warn("size_async_job_queue_is_too_large : {} ",  queueOfContexts.size());
        }
        if (queueOfContexts.size() > configService.getAsInt("limitOfAsyncPipelineQueueInBidder") ) {
            metricReporterService.addStateModuleForEntity(
                    "size_async_job_queue_is_too_large",
                    "BidModeControllerService",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.EXCEPTION);
            // queueOfContexts.size()
            String reason = "AsyncPipelineIsSlow queueSize :" + toStr(queueOfContexts.size());
            reasons.add(reason);
            bidMode.setStatus(false, reason, metricReporterService);
        }

        if (numberOfCuncurrentReuqestsNow.get() > 10) {
            metricReporterService.addStateModuleForEntity(
                    "tooMuchLoadOnBidder",
                    "BidModeControllerService",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.EXCEPTION);
            //LOG_EVERY_N(ERROR, 1000) ," numberOfCuncurrentReuqestsNow : "
            //, numberOfCuncurrentReuqestsNow.getValue() , std.endl;
        }

        if (queueSizeWaitingForAck.get() > 1000) {
            String reason = "EnqueueForScoringModule_queueSizeWaitingForAck:" + toStr(queueSizeWaitingForAck.get());
            reasons.add(reason);
            bidMode.setStatus(false, reason, metricReporterService);
            LOGGER.debug("kafka queueSizeWaitingForAck : {}" , queueSizeWaitingForAck.get());
        }

        //set to DO_BID, if none of NO_BID conditions are true
        if (bidMode.getBidMode().get()) {
            bidMode.setStatus(true, "ALL_IN_GOOD_CONDITIONS_BEFORE_EVALUATION", metricReporterService);
            metricReporterService.addStateModuleForEntity(
                    "DO_BID",
                    "BidModeControllerService",
                    "ALL"
            );
            numberOfContinuousCallInNoBidMode.set(0);
        } else {
            bidMode = goToNoBidMode(reasons);
            LOGGER.warn("bidMode : {}, reasons : {}", bidMode, reasons);
        }


        updateGlobalBidMode(bidMode);
    }

    private void updateGlobalBidMode(BiddingMode currentBidMode) {
        if (!currentBidMode.getBidMode().get()) {
            globalBidMode.getBidMode().set(false);
        } else {
            globalBidMode.getBidMode().set(true);
        }

        globalBidMode.setBidModeReasons(currentBidMode.getBidModeReasons());
    }

    private BiddingMode goToNoBidMode(
            List<String> reasons) {
        BiddingMode bidMode = new BiddingMode();

        String allReasons = null;
        try {
            allReasons = OBJECT_MAPPER.writeValueAsString(reasons);
            //remove the illegal characters for saving metric
            allReasons = StringUtil.replaceString(allReasons, "\"", "_");
            allReasons = StringUtil.replaceString(allReasons, ",", "_");
            numberOfContinuousCallInNoBidMode.getAndIncrement();
            if (numberOfContinuousCallInNoBidMode.get() > 10000) {
                metricReporterService.addStateModuleForEntity(
                        "ManyContinuousCallsInNoBidMode",
                        "BidModeControllerService",
                        ALL_ENTITIES,
                        MetricReporterService.MetricPriority.EXCEPTION);
            }

            //record no bid mode reasons
            recordNoBidModeReasons(reasons);

            bidMode.setStatus(false, allReasons, metricReporterService);
           if(GUtil.allowedToCall(100)) {
                LOGGER.error("bidder is NO_BID_MODE, reasons : {}", allReasons);
            }

            return bidMode;
        } catch (JsonProcessingException e) {
            LOGGER.warn("exception occurred", e);
            bidMode.setStatus(false, allReasons, metricReporterService);
            return bidMode;
        }
    }

    private void recordNoBidModeReasons(List<String> reasons) {
        for (String reason : reasons) {
            metricReporterService.addStateModuleForEntity(
                    "NO_BID_FOR_" + reason,
                    "BidModeControllerService",
                    "ALL"
            );
        }
    }

    public void resetAllMonitors() {
        //LOGGER.debug("resetting all monitors";
        for (BiddingMonitor monitor : allBiddingMonitors) {
            monitor.resetMeasures();
        }

    }

    public BiddingMode getGlobalBidMode() {
        return globalBidMode;
    }
}

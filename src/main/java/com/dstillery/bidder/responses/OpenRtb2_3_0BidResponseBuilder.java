package com.dstillery.bidder.responses;//
// Created by Mahmoud Taabodi on 5/7/16.
//


import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static com.dstillery.common.util.StringUtil.toStr;
import static com.dstillery.openrtb.BidRequestParser.PARSER_MAPPER;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.modules.BidResponseBuilder;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.advertiser.Advertiser;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.creative.Creative;
import com.dstillery.common.creative.CreativeCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.openrtb.beans.Bid;
import com.dstillery.openrtb.beans.BidResponse;
import com.dstillery.openrtb.beans.SeatBid;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableList;

public class OpenRtb2_3_0BidResponseBuilder extends BidResponseBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(OpenRtb2_3_0BidResponseBuilder.class);

    private final MetricReporterService metricReporterService;
    private CacheService<Advertiser> advertiserCacheService;
    private CreativeCacheService creativeCacheService;
    private DiscoveryService discoveryService;

    public OpenRtb2_3_0BidResponseBuilder(
            DiscoveryService discoveryService,
            CacheService<Advertiser> advertiserCacheService,
            CreativeCacheService creativeCacheService,
            CampaignCacheService campaignCacheService,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.advertiserCacheService = advertiserCacheService;
        this.creativeCacheService = creativeCacheService;
        this.discoveryService = discoveryService;
    }

    public String createTheResponse(OpportunityContext context) {
        BidResponse bidResponse = buildBidResponseFromContext(context);

        String responseStr;
        try {
            responseStr = PARSER_MAPPER.writeValueAsString(bidResponse);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return responseStr;
    }

    private BidResponse buildBidResponseFromContext(OpportunityContext context) {

        metricReporterService.addStateModuleForEntity("BID_RESPONSE_BUILDING_ATTEMPTS",
                "OpenRtb2_3_0BidResponseBuilder",
                "ALL");

        BidResponse bidResponse = new BidResponse();

        bidResponse.setId(context.getBidRequestId());
        bidResponse.setCur("USD");
//        bidResponse.customdata = "";
        // bidResponse.nbr = getTheRightReasonForNotBidding(context);
        SeatBid seatBid = new SeatBid();

        Bid bidPtr = new Bid();

        bidPtr.setImpid(context.getImpressionIdInBidRequest());

        Creative chosenCreative = creativeCacheService.findByEntityId(context.getChosenCreative().getId());
        Advertiser advertiser = advertiserCacheService.findByEntityId(chosenCreative.getAdvertiserId());
        List<String> allChosenDomains = new ArrayList<>(advertiser.getDomainNames());
        assertAndThrow(!allChosenDomains.isEmpty());

        bidPtr.setAdomain(allChosenDomains);

        bidPtr.setCrid(toStr(chosenCreative.getId()));//this is probably remote id for the exchange

        bidPtr.setId(context.getTransactionId());
        bidPtr.setIurl(createWinNotificationUrl(context)); //"http://adserver.com/winnotice?impid=102",

        bidPtr.setPrice((float) context.getBidPrice());
        seatBid.setBid(ImmutableList.of(bidPtr));

        bidResponse.setSeatbid(ImmutableList.of(seatBid));

        metricReporterService.addStateModuleForEntity("BID_RESPONSE_BUILDING_SUCCESS",
                "OpenRtb2_3_0BidResponseBuilder",
                "ALL");

        return bidResponse;
    }

    private String createWinNotificationUrl(OpportunityContext context) {

        String winNotificationUrl = discoveryService.discoverService("adserver");

        winNotificationUrl += "/win?won=${AUCTION_PRICE}&";
        winNotificationUrl += "transactionId=";
        //make sure this transactionId is valid characters
        winNotificationUrl += context.getEventLog().getEventId();

        // String eventParam = convertEventToParam(context);
        // winNotificationUrl.append(eventParam);
        LOGGER.debug("winNotificationUrl : {} ", winNotificationUrl);
        context.setWinNotificationUrl(winNotificationUrl);
        return context.getWinNotificationUrl();
    }

    int getTheRightReasonForNotBidding(OpportunityContext context) {
        return 0;
        //   0
        // Unknown Error
        // 1
        // Technical Error
        // 2
        // Invalid Request
        // OpenRTB API Specification Version 2.3 RTB Project
        // Page 47
        // 3
        // Known Web Spider
        // 4
        // Suspected Non-Human Traffic
        // 5
        // Cloud, Data center, or Proxy IP
        // 6
        // Unsupported Device
        // 7
        // Blocked Publisher or Site
        // 8
        // Unmatched User
    }
}


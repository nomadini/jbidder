package com.dstillery.bidder.responses;//
// Created by Mahmoud Taabodi on 5/7/16.
//

import com.dstillery.bidder.modules.OpportunityContext;

public interface BidResponseBuilder {

 int getTheRightReasonForNotBidding(OpportunityContext context);
 String createWinNotificationUrl(OpportunityContext context);
 String createTheResponse(OpportunityContext context);
 String convertEventToParam(OpportunityContext context);

}


package com.dstillery.bidder.modules;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.targetgroup.TargetGroup;

public abstract class BidderModule extends TgMarker {
	private static final Logger LOGGER = LoggerFactory.getLogger(BidderModule.class);

    public void beforeProcessing(OpportunityContext context) {

    }

	public void afterProcessing(OpportunityContext context) {

    }

    protected boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
		//a Bidder Module never filters any targetGroup
		LOGGER.warn("bidder module shouldn't be used as filter");
		return false;
	}

	public abstract void process(OpportunityContext context) throws IOException;
}


package com.dstillery.bidder.modules;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.pipeline.BidderAsyncPipelineProcessor;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class AsyncPipelineFeederAndInvokerModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncPipelineFeederAndInvokerModule.class);

    private BlockingQueue<OpportunityContext> queueOfContexts;
    private BidderAsyncPipelineProcessor bidderAsyncPipelineProcessor;
    private ExecutorService executorService = Executors.newCachedThreadPool();

    private final MetricReporterService metricReporterService;
    public AsyncPipelineFeederAndInvokerModule(
            BidderAsyncPipelineProcessor bidderAsyncPipelineProcessor,
            BlockingQueue<OpportunityContext> queueOfContexts,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

         this.queueOfContexts = queueOfContexts;
         this.bidderAsyncPipelineProcessor = bidderAsyncPipelineProcessor;
         executorService.submit(bidderAsyncPipelineProcessor);
    }

    public void process(OpportunityContext context) {

        try {
            metricReporterService.addStateModuleForEntity("ENQUEUE_MESSAGE_FOR_ASYNC_PROCESSING",
                    "AsyncPipelineFeederAndInvokerModule");
            queueOfContexts.put(context);
            LOGGER.debug("size of queue context : {}", queueOfContexts.size());
        } catch (InterruptedException e) {
            e.printStackTrace();}
        if (queueOfContexts.remainingCapacity() == 0) {
            //LOG_EVERY_N(ERROR, 10000), google.COUNTER, " th cannot put message in async queue, queue is too large ", queueOfContexts.size();
            metricReporterService.addStateModuleForEntity("CANT_ENQUEUE_MESSAGE_FOR_ASYNC_PROCESSING",
                    "AsyncPipelineFeederAndInvokerModule",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.EXCEPTION);
        }
        if (queueOfContexts.size() > 100) {
            //we submit a task to drain the queue
            metricReporterService.addStateModuleForEntity("SUBMITTED_TASK_FOR_CONSUMING        ",
                    "AsyncPipelineFeederAndInvokerModule");
            executorService.submit(bidderAsyncPipelineProcessor);
        }
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


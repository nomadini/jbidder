package com.dstillery.bidder.modules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.common_modules.FeatureDeviceHistoryUpdaterModule;
import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class DeviceHistoryOfGeoFeatureUpdaterModuleWrapper extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceHistoryOfGeoFeatureUpdaterModuleWrapper.class);

    private final MetricReporterService metricReporterService;
    private FeatureType featureCategory;
    private FeatureDeviceHistoryUpdaterModule featureDeviceHistoryUpdaterModule;

    public DeviceHistoryOfGeoFeatureUpdaterModuleWrapper(
            FeatureType featureCategory,
            FeatureDeviceHistoryUpdaterModule geoFeatureDeviceHistoryUpdaterModule,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.featureCategory = featureCategory;
        this.featureDeviceHistoryUpdaterModule = geoFeatureDeviceHistoryUpdaterModule;
    }

    public void process(OpportunityContext context) {
        if (context.getDevice() == null || context.getSiteDomain().isEmpty()) {
            metricReporterService.addStateModuleForEntity("SKIPPING_BECAUSEOF_UNKNOWN_USER ",
                    "DeviceHistoryOfGeoFeatureUpdaterModuleWrapper ",
                    "ALL ");

            return;
        }

        try {
            if (context.getMgrs100m().isEmpty()) {
                return;
            }
            LOGGER.debug("context.getMgrs100m() : {}", context.getMgrs100m());
            featureDeviceHistoryUpdaterModule.process(featureCategory,
                    context.getMgrs100m(),
                    context.getDevice());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        metricReporterService.addStateModuleForEntity("ADDING_FEATURE_FEATURE_HISTORY ",
                "DeviceHistoryOfGeoFeatureUpdaterModuleWrapper ",
                "ALL ");

    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


package com.dstillery.bidder.modules;


import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.metric.dropwizard.MetricReporterService.MetricPriority.IMPORTANT;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.cache_realtime.RealTimeEntityCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetGroupCacheService;
import com.google.common.collect.ImmutableMap;

public class DeviceIdSegmentModuleHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceIdSegmentModuleHelper.class);

    private MetricReporterService metricReporterService;
    private TargetGroupCacheService targetGroupCacheService;
    private TargetGroupWithoutSegmentFilterModule targetGroupWithoutSegmentFilterModule;

    //this service uses both aerospike and cassandra to read the segemnts as fast as it can
// it uses the aerospike as the caching layer
    private RealTimeEntityCacheService<DeviceSegmentHistory> realTimeEntityCacheServiceDeviceSegmentHistory;

    DeviceIdSegmentModuleHelper(
            MetricReporterService metricReporterService)  {
        this.metricReporterService = metricReporterService;
    }

    void recordTgHavingSpecificSegments(List<Long> segmentIds, TargetGroup tg) {

        for (Long segmentId : segmentIds) {
            metricReporterService.getCounter(
                    "TG_MATCH_SEGMENT",
                    ImmutableMap.of("tg", tg.getId(), "segmentId", segmentId)
            ).inc();
        }
    }

    void recordDeviceHavingSegments(
            OpportunityContext context,
            List<Long> segmentIdsForDevice) {
        if (!segmentIdsForDevice.isEmpty()) {
            metricReporterService.addStateModuleForEntity("FOUND_DEVICE_SEGMENT_FOR_USER",
                    "DeviceIdSegmentModuleHelper",
                    ALL_ENTITIES,
                    IMPORTANT);
            LOGGER.debug("read these deviceSegments for nomadiniDeviceId {} , " +
                            " deviceType : {} , segments : {}",
                        context.getDevice().getDeviceId(),
                        context.getDevice().getDeviceType(),
                        segmentIdsForDevice);
        } else {
            metricReporterService.addStateModuleForEntity("NO_DEVICE_SEGMENT_FOUND_FOR_USER",
                    "DeviceIdSegmentModuleHelper",
                    ALL_ENTITIES,
                    IMPORTANT);
        }

    }
}


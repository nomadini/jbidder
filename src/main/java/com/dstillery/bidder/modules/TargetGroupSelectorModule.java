package com.dstillery.bidder.modules;


import static com.dstillery.common.util.RandomUtil.sudoRandomNumber;
import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static com.dstillery.common.util.StringUtil.toStr;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.modules.filters.creativefilters.BlockedBannerAdTypeFilter;
import com.dstillery.bidder.modules.filters.creativefilters.BlockedCreativeAttributeFilter;
import com.dstillery.bidder.modules.filters.creativefilters.CreativeBannerApiFilter;
import com.dstillery.bidder.modules.filters.creativefilters.CreativeContentTypeFilter;
import com.dstillery.bidder.modules.filters.creativefilters.CreativeSizeFilter;
import com.dstillery.common.advertiser.Advertiser;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.campaign.Campaign;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.creative.Creative;
import com.dstillery.common.creative.CreativeCacheService;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.metric.ProcessingStatus;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetGroupCacheService;
import com.fasterxml.jackson.core.JsonProcessingException;

public class TargetGroupSelectorModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(TargetGroupSelectorModule.class);

    private CampaignCacheService campaignCacheService;
    private CreativeSizeFilter creativeSizeFilter;
    private CacheService<Advertiser> advertiserCacheService;
    private CreativeCacheService creativeCacheService;
    private TargetGroupCacheService targetGroupCacheService;
    private final MetricReporterService metricReporterService;
    public TargetGroupSelectorModule(
            CampaignCacheService campaignCacheService,
            CreativeSizeFilter creativeSizeFilter,
            CacheService<Advertiser> advertiserCacheService,
            CreativeCacheService creativeCacheService,
            TargetGroupCacheService targetGroupCacheService,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.campaignCacheService = campaignCacheService;
        this.creativeSizeFilter = creativeSizeFilter;
        this.advertiserCacheService = advertiserCacheService;
        this.creativeCacheService = creativeCacheService;
        this.targetGroupCacheService = targetGroupCacheService;
    }

    private boolean checkNextTargetGroupIfCurrentSelectionHasNoCreative(TargetGroup tgCandidate) {
        List<Creative> allCreatives = creativeSizeFilter.getCreativesAssignedToTg(tgCandidate.getId());
        return allCreatives.size() != 0;
    }

    private void setRequiredContextVariablesBasedOnChosenTg(
            OpportunityContext context,
            TargetGroup tgCandidate) {
        //LOGGER.debug("tgCandidate : " , tgCandidate.toJson ();
        context.setChosenTargetGroup(tgCandidate);
        context.setCampaignId(tgCandidate.getCampaignId());
        Campaign cmp = campaignCacheService.findByEntityId (tgCandidate.getCampaignId());
        context.setAdvertiserId(cmp.getAdvertiserId());
        Advertiser advertiser = advertiserCacheService.findByEntityId(context.getAdvertiserId());
        context.setClientId(advertiser.getClientId());
        //		context.bidPrice =
    }

    private void selectTheOptimalTargetGroup(OpportunityContext context,
                                             List<TargetGroup> rawOrdered) {
        //make this more intelligent later

        //TODO : this is a random selection strategy...add more strategy later

        Collections.shuffle(rawOrdered);

        //this is just to show how much are target groups , pass all the filtering
        for (TargetGroup tg : rawOrdered) {
            metricReporterService.
                    addStateModuleForEntity("PASSED_ALL_FILTERING",
                            "TargetGroupSelectorModule",
                            String.valueOf(tg.getId()));
        }

        metricReporterService.
                addStateModuleForEntityWithValue("GOOD_CANDIDATE_TARGETGROUP_SIZE",
                        rawOrdered.size(),
                        "TargetGroupSelectorModule",
                        "ALL");

        Long randomIndex = 0L;

        if (rawOrdered.size() > 1) {
            randomIndex = sudoRandomNumber(rawOrdered.size() - 1);
        }

        TargetGroup tgCandidate = rawOrdered.get(randomIndex.intValue());
        boolean result = checkNextTargetGroupIfCurrentSelectionHasNoCreative(tgCandidate);

        if (result) {
            //now we choose the candidate tg
            setRequiredContextVariablesBasedOnChosenTg(context, tgCandidate);
        }
    }

    private boolean creativeIsValidInTermsOfSSL(
            OpportunityContext context,
            Creative crt) {
        //LOGGER.debug("crt id : " , crt.getDeviceId() , " is not secure, this is a secure impression, skipping";
        return !context.isSecureCreativeRequired() || crt.IsSecure();
    }

    private void selectTheBestCreative(OpportunityContext context) {
        //make this more intelligent later

        //we should filter for size and type of creative too here, this should be done in tg filter module
        List<Creative> allCreatives = creativeSizeFilter.getCreativesAssignedToTg(context.getChosenTargetGroup().getId());
        //LOGGER.debug("number of creatives : "
//                , allCreatives.size() , " for chosen tg id : "
//                , context.chosenTargetGroupId;

        if (allCreatives.size() > 0) {

            for (Creative crt : allCreatives) {

                //LOGGER.debug("considering crt id : " , crt.getDeviceId();
                //TODO : add rigourous testing for this module,
                //by having a target group with multiple different creatives and
                //making sure we pick the right ones
                if (!creativeIsValidInTermsOfSSL(context, crt)) {
                    continue;
                }

                if (BlockedCreativeAttributeFilter.isCreativeBlockedByAttributeInContext(crt, context)) {
                    continue;
                }

                if (!CreativeSizeFilter.hasCreativeTheRequestedSize(context,crt)) {
                    continue;
                }

                if (BlockedBannerAdTypeFilter.doesCreativeHaveBlockedBannerType(context, crt)) {
                    continue;
                }

                if (!CreativeBannerApiFilter.doesCreativeHaveRightApi(context, crt)) {
                    continue;
                }

                if (!CreativeContentTypeFilter.doesCreativeHaveRightContentType(context, crt)) {
                    continue;
                }

                context.setChosenCreative(crt);
                //LOGGER.debug("this creative was chosen, id : " , context.chosenCreativeId;
                metricReporterService.addStateModuleForEntity("CHOSEN_CREATIVE_ID",
                        "TargetGroupSelectorModule",
                        "cr" + toStr(context.getChosenCreative().getId()));

                break;
            }
        } else {
            //LOGGER.warn("The chosen target group , id : " , context.chosenTargetGroupId ,
//                    " had no creative assigned";
            metricReporterService.addStateModuleForEntity("EXCEPTION: NO_CREATIVE_WAS_FOUND_FOR_CHOSEN_TG",
                    "TargetGroupSelectorModule",
                    "ALL");

            throw new RuntimeException("The chosen target group , id : " + toStr(context.getChosenTargetGroup().getId()) +
                    " had no creative assigned");
        }
    }

    public void process(OpportunityContext context) throws JsonProcessingException {

        List<TargetGroup> rawOrdered = TgMarker.getPassingTargetGroups(context);
        if (rawOrdered.isEmpty()) {
            context.stopProcessing(this.getClass().getSimpleName());
            return;
        }

        selectTheOptimalTargetGroup (context, rawOrdered);

        if (context.getChosenTargetGroup().getId() == 0) {
            //NO GOOD TG was chosen
            context.stopProcessing(this.getClass().getSimpleName());
            return;
        }

        selectTheBestCreative (context);

        context.setChosenTargetGroup(targetGroupCacheService.findByEntityId(context.getChosenTargetGroup().getId()));

        if (context.getChosenCreative() == null) {
            //LOG_EVERY_N(ERROR, 100),google.COUNTER,"th unable to select good creative. chosenTargetGroupId : {} ", context.chosenTargetGroupId;
            metricReporterService.
                    addStateModuleForEntity("NO_GOOD_CREATIVE_SIZE",
                            "TargetGroupSelectorModule",
                            "ALL");
            context.stopProcessing(this.getClass().getSimpleName());
        } else {
            context.setChosenCreative(creativeCacheService.findByEntityId(context.getChosenCreative().getId()));

            //this is very important, it shows us how many times we have
            //the qualified candidate
            metricReporterService.
                    addStateModuleForEntity("find_perfect_candidate_for_bidding",
                            "TargetGroupSelectorModule",
                            "ALL",
                            MetricReporterService.MetricPriority.IMPORTANT);
            //this is the only place in bidder that
            // we decide to bid
            context.setBidDecisionResult(EventLog.BidEventType.BID);
            LOGGER.debug("good candidate is chosen, crt id : {}, tg id : {}, BidDecisionResult : {}",
                    context.getChosenCreative().getId(),
                    context.getChosenTargetGroup().getId(),
                    context.getBidDecisionResult());
            LOGGER.debug("good context : {}, {}", context.getUniqueNumberOfContext()
                    , context.getBidDecisionResult());
            assertAndThrow(context.getStatus() != ProcessingStatus.STOP_PROCESSING);
            assertAndThrow(context.getChosenTargetGroup().getId() > 0 && context.getChosenCreative().getId() > 0);
        }
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }
}

package com.dstillery.bidder.modules;/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static com.dstillery.common.util.StringUtil.toStr;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.responses.OpenRtb2_3_0BidResponseBuilder;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.metric.OpenRtbVersion;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class BidResponseCreatorModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidResponseCreatorModule.class);

    private final MetricReporterService metricReporterService;
    private OpenRtb2_3_0BidResponseBuilder openRtb2_3_0BidResponseBuilder;

    public BidResponseCreatorModule(
            OpenRtb2_3_0BidResponseBuilder openRtb2_3_0BidResponseBuilder,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.openRtb2_3_0BidResponseBuilder = openRtb2_3_0BidResponseBuilder;
    }

    private void selectTheRightVersionOfResponseBuilder(OpportunityContext context) {
        if (context.getOpenRtbVersion() == OpenRtbVersion.V2_3) {
            context.setBidResponseBuilder(openRtb2_3_0BidResponseBuilder);

        } else if (context.getOpenRtbVersion() == OpenRtbVersion.V2_2) {
            //configure the other version
            LOGGER.warn("no bid response builder defined for openrtb v2.2");
        }

    }

    public void process(OpportunityContext context) {
        selectTheRightVersionOfResponseBuilder(context);

        if (context.getBidDecisionResult() == EventLog.BidEventType.NO_BID) {
            context.setFinalBidResponseContent("");
            context.setBidResponseStatusCode(HTTP_NO_CONTENT);

            metricReporterService.addStateModuleForEntity(
                    "numberOfNoBidsResponses",
                    this.getClass().getSimpleName(),
                    "ALL");
        } else if (context.getBidDecisionResult() == EventLog.BidEventType.BID) {
            context.setFinalBidResponseContent(
                    context.getBidResponseBuilder().createTheResponse(context));
            assertAndThrow(!context.getFinalBidResponseContent().isEmpty());
            context.setBidResponseStatusCode(HTTP_OK);
            LOGGER.info("sample bid response : {}", context.getFinalBidResponseContent());
            metricReporterService.addStateModuleForEntity("#GoodBids",
                    this.getClass().getSimpleName(),
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);

            //we show these in dashboards... very important metric
            metricReporterService.addStateModuleForEntity("#Bids_with_TG:" + toStr(context.getChosenTargetGroup().getId())
                            + "_CRT:"+toStr(context.getChosenCreative().getId()),
                    "BidStats");

            //we show these in dashboards... very important metric
            //we use this to track bids and wins,
            //there is metric in adserver that is called #TG_XX_Wins, that is related to this
            metricReporterService.addStateModuleForEntity("#TG_"+toStr(context.getChosenTargetGroup().getId())+"_Bids",
                    "BidWinTracker");

        }
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        return false;
    }

}


package com.dstillery.bidder.modules;


import static com.dstillery.common.util.DateTimeUtil.getNowInMilliSecond;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.audience.models.AudienceGroup;
import com.dstillery.common.cache_realtime.RealTimeEntityCacheService;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetGroupCacheService;

public class DeviceIdTargetGroupWithSegmentProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceIdTargetGroupWithSegmentProvider.class);

    private final MetricReporterService metricReporterService;
    private TargetGroupCacheService targetGroupCacheService;
    private long ignoreSegmentsOlderThanXMilliSeconds;
    private RealTimeEntityCacheService<DeviceSegmentHistory> realTimeEntityCacheServiceDeviceSegmentHistory;
    private DeviceIdSegmentModuleHelper deviceIdSegmentModuleHelper;

    DeviceIdTargetGroupWithSegmentProvider(
            TargetGroupCacheService targetGroupCacheService,
            SegmentCacheService segmentCacheService,
            MetricReporterService metricReporterService,
            RealTimeEntityCacheService<DeviceSegmentHistory> realTimeEntityCacheServiceDeviceSegmentHistory,
            long ignoreSegmentsOlderThanXMilliSeconds) {
        this.metricReporterService = metricReporterService;

        deviceIdSegmentModuleHelper = new DeviceIdSegmentModuleHelper(
                metricReporterService);

        this.targetGroupCacheService = targetGroupCacheService;
        this.realTimeEntityCacheServiceDeviceSegmentHistory = realTimeEntityCacheServiceDeviceSegmentHistory;
        this.ignoreSegmentsOlderThanXMilliSeconds = ignoreSegmentsOlderThanXMilliSeconds;
    }

    private List<TargetGroup> findAllTargetGroupIdsWithUserSegments(
            List<Long> segmentIdsForDevice) {

        List<TargetGroup> targetGroupsHavingSegments = fetchTargetGroupsHavingSegment(segmentIdsForDevice);
        LOGGER.debug("targetGroupsHavingSegments : {}", targetGroupsHavingSegments);
        return new ArrayList<>(targetGroupsHavingSegments);
    }

    private List<TargetGroup> fetchTargetGroupsHavingSegment(List<Long> segmentIdsForDevice) {
        List<TargetGroup> empty = new ArrayList<>();

        List<TargetGroup> targetGroupsHavingSegments =
                targetGroupCacheService.getAllCurrentTargetGroups().stream().filter(tg -> {
                    try {
                        Set<Boolean> allMatchResultsForIncluded = segmentIdsForDevice.stream().map(segmentId ->
                                tg.getPreCalculatedAssignedIncludedSegmentIds().contains(segmentId)).collect(Collectors.toSet());
                        boolean includeListIsQualified = false;
                        if (tg.getSegmentTargetBlueprint().getInclude().getOperand().equals(AudienceGroup.Operand.OR) &&
                                allMatchResultsForIncluded.contains(Boolean.TRUE)) {
                            includeListIsQualified = true;
                        } else if (tg.getSegmentTargetBlueprint().getInclude().getOperand().equals(AudienceGroup.Operand.AND) &&
                                allMatchResultsForIncluded.contains(Boolean.TRUE) &&
                                allMatchResultsForIncluded.size() == 1) {
                            includeListIsQualified = true;
                        }

                        Set<Boolean> allMatchResultsForExcluded = segmentIdsForDevice.stream().map(segmentId ->
                                tg.getPreCalculatedAssignedExcludedSegmentIds().contains(segmentId)).collect(Collectors.toSet());
                        boolean excludeListDisqualifiesTg = false;
                        if (tg.getSegmentTargetBlueprint().getExclude().getOperand().equals(AudienceGroup.Operand.OR) &&
                                allMatchResultsForExcluded.contains(Boolean.TRUE)) {
                            //at least one excluded segment was found in segmentsList
                            excludeListDisqualifiesTg = true;

                        } else if (tg.getSegmentTargetBlueprint().getExclude().getOperand().equals(AudienceGroup.Operand.AND) &&
                                allMatchResultsForExcluded.contains(Boolean.TRUE) &&
                                allMatchResultsForExcluded.size() == 1) {
                            //all of the excluded segments were matched
                            excludeListDisqualifiesTg = true;
                        }

                        LOGGER.debug("excludeListDisqualifiesTg : {}, includeListIsQualified : {}, " +
                                        "segmentIdsForDevice: {}, getSegmentTargetBlueprint: {}",
                                excludeListDisqualifiesTg,
                                includeListIsQualified,
                                segmentIdsForDevice,
                                tg.getSegmentTargetBlueprint());
                        boolean qualified = !excludeListDisqualifiesTg && includeListIsQualified;

                        if (qualified) {
                            deviceIdSegmentModuleHelper.
                                    recordTgHavingSpecificSegments(segmentIdsForDevice, tg);

                        } else {
                            segmentIdsForDevice.forEach(segmentId -> {
                                metricReporterService.addStateModuleForEntity(
                                        "NO_TG_FOR_SEGMENT_ID_FOUND",
                                        getClass().getSimpleName(),
                                        segmentId.toString());
                            });
                        }
                        return qualified;
                    } catch (Exception e) {
                        LOGGER.warn("exception when evaluating tg : {}", tg);
                        throw new RuntimeException(e);
                    }
                }).collect(Collectors.toList());
        LOGGER.debug("targetGroupsHavingSegments : {}", targetGroupsHavingSegments);
        return targetGroupsHavingSegments;
    }

    Map<Long, TargetGroup>
    readSegmentsAndFetchAllTgsAttached(OpportunityContext context) throws IOException {
        List<TargetGroup> allTgsWithCorrectSegmentsForDevice = new ArrayList<>();
        DeviceSegmentHistory deviceSegmentHistory = new DeviceSegmentHistory(context.getDevice());

        List<DeviceSegmentHistory> deviceSegments =
                realTimeEntityCacheServiceDeviceSegmentHistory.readDataOptional(deviceSegmentHistory);

        LOGGER.debug("deviceSegments : {} for device : {}", deviceSegments, context.getDevice());
        context.setDeviceSegmentReadHappened(true);
        if (deviceSegments != null) {
            metricReporterService.addStateModuleForEntity(
                    "DEVICE_SEGMENT_FOUND_FOR_DEVICE",
                    getClass().getSimpleName(),
                    "ALL");
            List<Long> segmentIdsForDevice = new ArrayList<>();

            for (DeviceSegmentHistory deviceSegmentHistoryObj : deviceSegments) {
                LOGGER.debug("deviceSegmentHistoryObj : {}", deviceSegmentHistoryObj);
                if (deviceSegmentHistoryObj.getSegment() == null) {
                    continue;
                }

                context.getAllSegmentIdsFoundList().add(deviceSegmentHistoryObj.getSegment().getId());
                if (getNowInMilliSecond() -
                        deviceSegmentHistoryObj.getSegment().getDateCreatedInMillis() >= ignoreSegmentsOlderThanXMilliSeconds) {
                    LOGGER.debug("ignoring this segment : {}", deviceSegmentHistoryObj.getSegment());
                } else {
                    segmentIdsForDevice.add(deviceSegmentHistoryObj.getSegment().getId());
                    context.getChosenSegmentIdsList().add(deviceSegmentHistoryObj.getSegment().getId());
                }
            }

            deviceIdSegmentModuleHelper.recordDeviceHavingSegments(context, segmentIdsForDevice);
            allTgsWithCorrectSegmentsForDevice =
                    findAllTargetGroupIdsWithUserSegments (
                            segmentIdsForDevice);
        } else {
            metricReporterService.addStateModuleForEntity(
                    "NO_DEVICE_SEGMENT_FOUND_FOR_DEVICE",
                    getClass().getSimpleName(),
                    "ALL");
        }

        ConcurrentHashMap<Long, TargetGroup>
                allTgsWithCorrectSegmentsForDeviceAsMap = new ConcurrentHashMap<>();
        for (TargetGroup tg : allTgsWithCorrectSegmentsForDevice) {
            allTgsWithCorrectSegmentsForDeviceAsMap.put(tg.getId(), tg);
        }

        return allTgsWithCorrectSegmentsForDeviceAsMap;
    }

}

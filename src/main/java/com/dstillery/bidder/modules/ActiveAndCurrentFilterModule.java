package com.dstillery.bidder.modules;

import static com.dstillery.common.util.StringUtil.toStr;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;

import com.dstillery.common.Status;
import com.dstillery.common.advertiser.Advertiser;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.campaign.Campaign;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.client.Client;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class ActiveAndCurrentFilterModule extends BidderModule {

    private CampaignCacheService campaignCacheService;
    private CacheService<Advertiser> advertiserCacheService;
    private CacheService<Client> clientCacheService;

    private final MetricReporterService metricReporterService;

    public ActiveAndCurrentFilterModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }

    public void process(OpportunityContext context) {
        metricReporterService.addStateModuleForEntity("sizeOfTargetGroup"  + toStr(context.getSizeOfUnmarkedTargetGroups()),
                "ActiveAndCurrentFilterModule",
                "ALL");

        markFilteredTgs(context);
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {

        boolean filterTg = true;
        if (isTargetGroupStatusActive(tg) &&
                isCampignStatusActive(tg) &&
                isAdvertiserStatusActive(tg) &&
                isClientStatusActive(tg)) {
            metricReporterService.addStateModuleForEntity("PASSED",
                    "ActiveAndCurrentFilterModule",
                    tg.getId());

            filterTg = false;
        }

        return filterTg;
    }

    private boolean isStatusActive(Status statusOfEntity,
                                   Long entityId,
                                   String entityType) {
        boolean result = false;
        if (statusOfEntity == Status.ACTIVE) {
            result = true;
        } else {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_INACTIVE",
                    "ActiveAndCurrentFilterModule",
                    entityType + toStr(entityId));
        }
        return result;
    }

    private boolean isTargetGroupStatusActive(TargetGroup tg) {
        return isStatusActive(tg.getStatus(), tg.getId(), "tg");
    }

    private boolean isCampignStatusActive(TargetGroup tg) {
        Campaign cmp = this.campaignCacheService.findByEntityId(tg.getCampaignId());
        return isStatusActive(cmp.getStatus(), cmp.getId(), "cmp") &&
                isCurrent(cmp.getStartDate(), cmp.getEndDate());

    }
    private boolean isAdvertiserStatusActive(TargetGroup tg) {
        Campaign cmp = this.campaignCacheService.findByEntityId(tg.getCampaignId());
        Advertiser adv = this.advertiserCacheService.findByEntityId(cmp.getAdvertiserId());
        return isStatusActive(adv.getStatus(), adv.getId(), "adv");
    }
    private boolean isClientStatusActive(TargetGroup tg) {
        Campaign cmp = this.campaignCacheService.findByEntityId(tg.getCampaignId());
        Advertiser adv = this.advertiserCacheService.findByEntityId(cmp.getAdvertiserId());
        Client cln = this.clientCacheService.findByEntityId(adv.getClientId());
        return isStatusActive(cln.getStatus(), cln.getId(), "cln");
    }

    private boolean isCurrent(
            Instant startDate,
            Instant endDate) {
        Instant now = Instant.now();
        return (now.isAfter(startDate) && now.isBefore(endDate));
    }

    @Autowired
    public void setCampaignCacheService(CampaignCacheService campaignCacheService) {
        this.campaignCacheService = campaignCacheService;
    }

    @Autowired
    public void setAdvertiserCacheService(CacheService<Advertiser> advertiserCacheService) {
        this.advertiserCacheService = advertiserCacheService;
    }

    @Autowired
    public void setClientCacheService(CacheService<Client> clientCacheService) {
        this.clientCacheService = clientCacheService;
    }
}


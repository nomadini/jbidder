package com.dstillery.bidder.modules;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.util.StatisticsUtil;

/*
  BidProbabilityEnforcerModule.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi

 */
public class BidProbabilityEnforcerModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidProbabilityEnforcerModule.class);

    private final int probabilityToBid;
    private StatisticsUtil statisticsUtil;
    private final MetricReporterService metricReporterService;
    public BidProbabilityEnforcerModule(AtomicLong bidPercentage,
                                        MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        statisticsUtil = new StatisticsUtil();
        probabilityToBid = bidPercentage.intValue() / 100;
        LOGGER.info("probabilityToBid : {}", probabilityToBid);
    }

    public void process(OpportunityContext context) {

        //LOGGER.debug("Processing probabilityToBid : {} ", probabilityToBid;
        if (!statisticsUtil.happensWithProb(probabilityToBid)) {

            metricReporterService.addStateModuleForEntity("NO_BID_BECAUSE_IN_SLOW_BIDE_MODE",
                    this.getClass().getSimpleName(),
                    "ALL");
            context.setFinalBidResponseContent("");
            context.setBidResponseStatusCode(HTTP_NO_CONTENT);
            context.stopProcessing(this.getClass().getSimpleName());
        }

    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }
}


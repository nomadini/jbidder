package com.dstillery.bidder.modules;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.adhistory.AdEntry;
import com.dstillery.common.adhistory.AdEntryDevicePair;
import com.dstillery.common.cache_realtime.RealTimeEntityCacheService;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.util.DateTimeUtil;

public class TargetGroupFrequencyCapModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(TargetGroupFrequencyCapModule.class);

    private final MetricReporterService metricReporterService;
    private RealTimeEntityCacheService<AdEntryDevicePair> adHistoryCassandraService;
    public TargetGroupFrequencyCapModule(RealTimeEntityCacheService<AdEntryDevicePair> adHistoryCassandraService,
                                         MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        assertAndThrow(adHistoryCassandraService != null);
        this.adHistoryCassandraService = adHistoryCassandraService;
    }

    private void filterTargetGroupsBasedOnFrequencyCap(OpportunityContext context) throws IOException {

        AdEntryDevicePair keyOfAdHistory = new AdEntryDevicePair(context.getDevice());
        context.setAdHistory(
                adHistoryCassandraService.readDataOptional(keyOfAdHistory)
        );
        if (context.getAdHistory() == null) {
            context.setAdHistory(Collections.emptyList());
        }

        markFilteredTgs(context);
    }

    public void process(OpportunityContext context) throws IOException {
        filterTargetGroupsBasedOnFrequencyCap (context);
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        LOGGER.debug("adHistory : {}", context.getAdHistory());
        List<AdEntry> setOfAdEntriesForTg = context.getAdHistory().stream().filter(
                adhistory -> adhistory.getAdEntry() != null && adhistory.getAdEntry().getTargetGroupId() == tg.getId())
                .map(AdEntryDevicePair::getAdEntry)
                .collect(Collectors.toList());
        LOGGER.debug("setOfAdEntriesForTg : {}", setOfAdEntriesForTg);
        if (setOfAdEntriesForTg.isEmpty()) {
            metricReporterService.addStateModuleForEntity("PASSING_FOR_NO_AD_HISTORY",
                    getClass().getSimpleName(),

                    tg.getId());
            filterTg = false;
        }

        long lastAdShownInSeconds = 0;
        for (AdEntry adEntry : setOfAdEntriesForTg) {
            if (adEntry.getAdInteractionType() != EventLog.BidEventType.IMPRESSION) {
                //ignoring non impression ad interaction types
                continue;
            }

            long currentAdShownInSecond = adEntry.getTimeAdShownInMillis() / 1000;
            if (currentAdShownInSecond >= lastAdShownInSeconds) {
                lastAdShownInSeconds = currentAdShownInSecond;
            }
        }

        long secondHasPassedSinceAdShown = DateTimeUtil.getNowInSecond () - lastAdShownInSeconds;
        //LOGGER.debug("secondHasPassedSinceAdShown : " , secondHasPassedSinceAdShown , " , tg.getFrequencyInSec() " , tg.getFrequencyInSec ();
        if (secondHasPassedSinceAdShown >= tg.getFrequencyInSeconds()) {
            //ad has been shown longer seconds than enough ago
            filterTg = false;
            //LOGGER.debug("FrequencyCap module is filtering out this tg  "
//                    , tg.getId() , " because tg.freq : " , tg.getFrequencyInSec () , ""
//            " now-AdEntry-timeAdShownInMillis =" , secondHasPassedSinceAdShown;
            metricReporterService.addStateModuleForEntity("PASSING_FOR_NOT_MEETING_FREQ_LIMIT",
                    getClass().getSimpleName(),

                    tg.getId());

        } else {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_MEETING_FRQ_LIMIT",
                    getClass().getSimpleName(),

                    tg.getId());
        }

        return filterTg;
    }
}

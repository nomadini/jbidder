package com.dstillery.bidder.modules;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.util.FileNomadiniUtil;

public class TopKeyWordsExtractorModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(TopKeyWordsExtractorModule.class);

    private final Set<String> mostCommonKeyWords;

    public TopKeyWordsExtractorModule() {

        try {
            mostCommonKeyWords = FileNomadiniUtil.readFileLineByLine("google-10000-english-usa.txt").stream()
                    .limit(300).collect(Collectors.toSet());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean isPureAscii(String v) {
        return Charset.forName("US-ASCII").newEncoder().canEncode(v);
        // or "ISO-8859-1" for ISO Latin 1
        // or StandardCharsets.US_ASCII with JDK1.7+
    }

    public void process(OpportunityContext context) {
        try {
            Document doc = Jsoup.connect(getUrl(context.getSiteDomain())).get();
            Set<String> keywords = new HashSet<>(Arrays.asList(doc.body().getElementsByTag("h1").text().split(" ")));


            Set<String> noNumberKeywords = keywords.stream().filter(
                    //doesnt have number in it, is pure ascii code and has more than 4 characters
                    s -> !s.matches(".*\\d.*") &&
                            isPureAscii(s) &&
                            s.length() >= 4)
                    //keep only alphanumeric characters
                    .map(s -> s.replaceAll("[^a-zA-Z0-9]", ""))
                    .collect(Collectors.toSet());
            noNumberKeywords.removeAll(mostCommonKeyWords);
            if (noNumberKeywords.size() > 1 && noNumberKeywords.size() <= 20) {
                LOGGER.debug("domain : {}, extracted keywords : {}, noNumberKeywords.size(): {}",
                        context.getSiteDomain(), noNumberKeywords, noNumberKeywords.size());
                context.setKeyWords(noNumberKeywords);
            }

            LOGGER.trace("siteDomain : {} , AllElements : {}",
                    context.getSiteDomain(), doc.body().getAllElements());
        } catch (Exception e) {
            LOGGER.debug("exception while extracting keywords {} for domain : {}", e.getMessage(), context.getSiteDomain());
        }

    }

    private String getUrl(String siteDomain) {
        //TODO change this to sitePage for real traffic
        if (siteDomain.startsWith("https://") || siteDomain.startsWith("http://") ) {
            return siteDomain;
        }

        return  "http://" + siteDomain;
    }

    public static void main(String[] args) throws IOException {
        Document doc = Jsoup.connect("https://edition.cnn.com/world/live-news/notre-dame-fire/index.html").get();
        String title = doc.title();

        System.out.println("title : " + title);
//        System.out.println(doc);
        Set<String> keywords = new HashSet<>(Arrays.asList(doc.body().getElementsByTag("h1").text().split(" ")));

//        System.out.println("h2 : " + doc.body().getElementsByTag("h2"));
//        System.out.println("h3 : " + doc.body().getElementsByTag("h3"));
//        System.out.println("h4 : " + doc.body().getElementsByTag("h4"));

    }

}

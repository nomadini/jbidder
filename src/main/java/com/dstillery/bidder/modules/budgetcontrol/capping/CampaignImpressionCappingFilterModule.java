package com.dstillery.bidder.modules.budgetcontrol.capping;

import static com.dstillery.common.targetgroup.PacingMethod.IMPRESSION;
import static com.dstillery.common.targetgroup.PacingSpeed.ASAP;

import org.springframework.beans.factory.annotation.Autowired;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.delivery_info.EntityRealTimeDeliveryInfo;
import com.dstillery.bidder.delivery_info.IntervalType;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.campaign.Campaign;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class CampaignImpressionCappingFilterModule extends BidderModule {

        private final MetricReporterService metricReporterService;
        private CampaignCacheService campaignCacheService;
        private EntityDeliveryInfoCacheService entityDeliveryInfoCacheService;

        public CampaignImpressionCappingFilterModule(CampaignCacheService campaignCacheService,
                                                     MetricReporterService metricReporterService) {
                this.metricReporterService = metricReporterService;
                this.campaignCacheService = campaignCacheService;
        }

        public void process(OpportunityContext context) {
                markFilteredTgs(context);
        }

        public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
                Campaign campaign = campaignCacheService.findCampaignByTgId(tg.getId());
                boolean filterTg = true;
                if (tg.getPacingSpeed() == ASAP) {
                        metricReporterService.addStateModuleForEntity("PASSED_FOR_BEING_ASAP",
                                this.getClass().getSimpleName(),

                                tg.getId());
                        filterTg = false;
                        return filterTg;
                }

                EntityRealTimeDeliveryInfo cmpRealTimeInfo =
                        entityDeliveryInfoCacheService.getDeliveryInfo(EntityDeliveryInfoCacheService.EntityType.CAMPAIGN,
                                tg.getCampaignId());

                if (tg.getPacingMethod() == IMPRESSION) {
                        if (cmpRealTimeInfo != null) {
                                //no impression capping for budget pacing tgs
                                if ((cmpRealTimeInfo.getImpressions().get(IntervalType.CURRENT_DAY) < campaign.getDailyMaxImpression ()
                                        && cmpRealTimeInfo.getImpressions().get(IntervalType.TOTAL) < campaign.getMaxImpression ())) {

                                        metricReporterService.addStateModuleForEntity("PASSED",
                                                "CampaignImpressionCappingFilterModule",
                                                tg.getId());
                                        filterTg = false;
                                } else {
                                        metricReporterService.addStateModuleForEntity("FAILED",
                                                "CampaignImpressionCappingFilterModule",
                                                tg.getId());
                                }
                        } else {

                                metricReporterService.addStateModuleForEntity("FAILED_FOR_NO_REAL_TIME_INFO",
                                        "CampaignImpressionCappingFilterModule",
                                        tg.getId());
                        }
                } else {
                        filterTg = false;
                        metricReporterService.addStateModuleForEntity("PASSED_FOR_NOT_BEING_BUDGET_PACED_TG",
                                "CampaignImpressionCappingFilterModule",
                                tg.getId());
                }
                return filterTg;
        }

        @Autowired
        public void setEntityDeliveryInfoCacheService(EntityDeliveryInfoCacheService entityDeliveryInfoCacheService) {
                this.entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
        }
}

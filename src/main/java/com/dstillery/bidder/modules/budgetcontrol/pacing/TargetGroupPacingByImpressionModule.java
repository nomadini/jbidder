package com.dstillery.bidder.modules.budgetcontrol.pacing;

import static com.dstillery.bidder.modules.budgetcontrol.pacing.CommonUtil.getLimitByHour;
import static com.dstillery.common.targetgroup.PacingMethod.BUDGET;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.delivery_info.EntityRealTimeDeliveryInfo;
import com.dstillery.bidder.delivery_info.IntervalType;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

/**
   this module filters out the target groups that have gone above their pacing limits
   in terms of impressions at some point.
 */
public class TargetGroupPacingByImpressionModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(TargetGroupPacingByImpressionModule.class);

    private MetricReporterService metricReporterService;
    private EntityDeliveryInfoCacheService entityDeliveryInfoCacheService;

    public TargetGroupPacingByImpressionModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }

    /**
      this will filter out the target groups that have shown impressions
      more than their hourly limit
     */
    private boolean needToBePaceByDailyImpression(TargetGroup tg,
                                                  EntityRealTimeDeliveryInfo tgRealTimePtr,
                                                  int limitOfHourInPercentage) {
        //lets say the daily max impression is 10000
        //we want to pace it. we want to show have shown 10% by 9am
        //20% by 12am , 50% by 5am , 100% by 10pm
        // so we will have a json array that
        //tells us how many percentage we should have used by an hour
        //the json will look like this
        //[ {"8":0} , {"12":30 }, {"16":50},{"20":100}]
        //this means that from hour 8.00 to 20.59th of the day
        //we should spend the daily impression count

        long impLimitByHour = (tg.getDailyMaxImpression() / 100) *
                 limitOfHourInPercentage;

        LOGGER.debug("tg.getDailyMaxImpression() : {}, tgRealTimePtr.numOfImpressionsServedInCurrentDateUpToNow : {}, impLimitByHour : {}" ,
                tg.getDailyMaxImpression(),
                tgRealTimePtr.getImpressions().get(IntervalType.TOTAL),
                impLimitByHour);

        if (tgRealTimePtr.getImpressions().get(IntervalType.CURRENT_DAY) <= impLimitByHour) {
            LOGGER.debug("tg {}, we still have impression to buy according to plan", tg.getId());
            metricReporterService.addStateModuleForEntity("TG_HAS_IMPRESSION_TO_SPEND",
                    getClass().getSimpleName(),

                    tg.getId());
            return false;
        } else {
            LOGGER.debug("tg {}, we can't buy impression anymore according to plan", tg.getId());
            metricReporterService.addStateModuleForEntity("TG_HAS_NO_IMPRESSION_TO_SPEND",
                    getClass().getSimpleName(),

                    tg.getId());
        }

        return true;
    }

    private EntityRealTimeDeliveryInfo getTgRealTimeDeliveryInfo(
            TargetGroup tg) {
        return entityDeliveryInfoCacheService.getDeliveryInfo(EntityDeliveryInfoCacheService.EntityType.TARGETGROUP, tg.getId());
    }

    private boolean needToBePaced(TargetGroup tg) {
        int hourlyLimit = getLimitByHour (tg, metricReporterService, "TargetGroupPacingByImpressionModule");
        //we should follow this plan now
        EntityRealTimeDeliveryInfo tgRealTimePtr = getTgRealTimeDeliveryInfo (
                tg);
        if (tg.getPacingPlan() == null) {
            LOGGER.debug("tg with id {}  doesn't have a pacing plan ", tg.getId());
            metricReporterService.addStateModuleForEntity("TG_HAS_NO_PACING_PLAN",
                    getClass().getSimpleName(),

                    tg.getId());
            return true;
        }

        if (tgRealTimePtr == null) {
            LOGGER.debug("tg with id {}  doesn't have a real time info ", tg.getId());
            metricReporterService.addStateModuleForEntity("TG_HAS_NO_REAL_TIME_INFO",
                    getClass().getSimpleName(),

                    tg.getId());
            return false;
        }

        return needToBePaceByDailyImpression(tg, tgRealTimePtr, hourlyLimit);

    }

    public void process(OpportunityContext context) {
        markFilteredTgs(context);
    }

    public boolean filterTargetGroup(TargetGroup tg,
                                     OpportunityContext context) {
        boolean filterTg = true;
        if (tg.getPacingMethod() == BUDGET) {
            metricReporterService.addStateModuleForEntity("PASSING",
                    getClass().getSimpleName(),

                            tg.getId());
            filterTg = false;
        } else if (needToBePaced(tg)) {
            metricReporterService.addStateModuleForEntity("FAILING",
                    getClass().getSimpleName(),

                            tg.getId());
        } else {
            metricReporterService.addStateModuleForEntity("PASSING",
                    getClass().getSimpleName(),

                            tg.getId());
            filterTg = false;

        }
        LOGGER.debug("tg with id {}, filterTg :{}", tg.getId(), filterTg);
        return filterTg;
    }

    @Autowired
    public void setEntityDeliveryInfoCacheService(EntityDeliveryInfoCacheService entityDeliveryInfoCacheService) {
        this.entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
    }
}

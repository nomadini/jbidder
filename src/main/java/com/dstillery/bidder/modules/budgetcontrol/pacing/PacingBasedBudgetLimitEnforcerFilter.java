package com.dstillery.bidder.modules.budgetcontrol.pacing;
//
// Created by Mahmoud Taabodi on 3/6/16.
//

import static com.dstillery.common.targetgroup.PacingMethod.IMPRESSION;
import static com.dstillery.common.targetgroup.PacingSpeed.ASAP;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.delivery_info.IntervalType;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class PacingBasedBudgetLimitEnforcerFilter extends TgMarker {
    private final EntityDeliveryInfoCacheService entityDeliveryInfoCacheService;
    private final MetricReporterService metricReporterService;
    public PacingBasedBudgetLimitEnforcerFilter(
            EntityDeliveryInfoCacheService entityDeliveryInfoCacheService,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        if (tg.getPacingSpeed() == ASAP) {
            metricReporterService.addStateModuleForEntity("PASSED_FOR_BEING_ASAP",
                    this.getClass().getSimpleName(),

                    tg.getId());
            filterTg = false;
            return filterTg;
        }

        if (tg.getPacingMethod() == IMPRESSION) {
            //we don't pace by hour budget limit if tg is paced based on imps
            metricReporterService.addStateModuleForEntity("PASSED_FOR_BEING_PACED_BY_IMPRESSION",
                    this.getClass().getSimpleName(),

                    tg.getId());
            filterTg = false;

        } else {
            if (entityDeliveryInfoCacheService.getAllTgRealTimeDeliveryInfoMap()
                    .get(tg.getId()).getPlatformCost().get(IntervalType.CURRENT_HOUR) <=
                    tg.getMetaData().getPacingBasedHourlyCalculatedBudgetLimit()) {
                metricReporterService.addStateModuleForEntity("PASSED_FOR_NOT_HITTING_HOUR_WIN_LIMIT",
                        this.getClass().getSimpleName(),

                        tg.getId());
                filterTg = false;

            } else {
                metricReporterService.addStateModuleForEntity("FAILED_FOR_NOT_HITTING_HOUR_WIN_LIMIT",
                        this.getClass().getSimpleName(),

                        tg.getId());

            }
        }

        return filterTg;
    }

}

package com.dstillery.bidder.modules.budgetcontrol.pacing;
//
// Created by Mahmoud Taabodi on 3/6/16.
//

import static com.dstillery.common.targetgroup.PacingMethod.BUDGET;
import static com.dstillery.common.targetgroup.PacingSpeed.ASAP;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.delivery_info.IntervalType;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

/*
 *
  this filter makes sure that we dont bid more than the pacing per hour limit on a target group
 */
public class PacingBasedImpressionLimitEnforcerFilter extends TgMarker {

    private final EntityDeliveryInfoCacheService entityDeliveryInfoCacheService;
    private final MetricReporterService metricReporterService;
    public PacingBasedImpressionLimitEnforcerFilter(
            EntityDeliveryInfoCacheService entityDeliveryInfoCacheService,
            MetricReporterService metricReporterService) {
            this.metricReporterService = metricReporterService;
             this.entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        if (tg.getPacingSpeed() == ASAP) {
            metricReporterService.addStateModuleForEntity("PASSED_FOR_BEING_ASAP",
                    this.getClass().getSimpleName(),

                    tg.getId());
            filterTg = false;
            return filterTg;
        }

        if (tg.getPacingMethod() == BUDGET) {
            //we don't pace by hour imps limit if tg is paced based on budget
            metricReporterService.addStateModuleForEntity("PASSED_FOR_BEING_BASED_ON_BUDGET",
                    this.getClass().getSimpleName(),

                    tg.getId());
            filterTg = false;
        } else {
            Double lastHourWins = entityDeliveryInfoCacheService.
                    getAllTgRealTimeDeliveryInfoMap()
                    .get(tg.getId()).getPlatformCost().get(IntervalType.CURRENT_HOUR);
            if (lastHourWins <=
                    tg.getMetaData().getPacingBasedHourlyCalculatedImpressionLimit()) {
                metricReporterService.addStateModuleForEntity("PASSED_FOR_NOT_HITTING_HOUR_WIN_LIMIT",
                        this.getClass().getSimpleName(),

                        tg.getId());
                filterTg = false;

            } else {
                metricReporterService.addStateModuleForEntity("FAILED_FOR_NOT_HITTING_HOUR_WIN_LIMIT",
                        this.getClass().getSimpleName(),

                        tg.getId());

                //LOG_EVERY_N(INFO, 1000)
//                        ,"tg is paced id : {} ", tg.getId()
//                        ," , lastHourWins : {} ",  lastHourWins
//                        , " ,limit : {} ", tg.pacingBasedHourlyCalculatedImpressionLimit.getValue();

            }
        }

        return filterTg;
    }

}

package com.dstillery.bidder.modules.budgetcontrol.pacing;

import static com.dstillery.bidder.modules.budgetcontrol.pacing.CommonUtil.getLimitByHour;
import static com.dstillery.common.targetgroup.PacingMethod.IMPRESSION;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.delivery_info.EntityRealTimeDeliveryInfo;
import com.dstillery.bidder.delivery_info.IntervalType;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetGroupFilterStatistic;

/**
 this module filters out the target groups that have gone above their pacing limits
 in terms of budget at some point.
 */
public class TargetGroupPacingByBudgetModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(TargetGroupPacingByBudgetModule.class);

    private EntityDeliveryInfoCacheService entityDeliveryInfoCacheService;
    private MetricReporterService metricReporterService;
    public TargetGroupPacingByBudgetModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }

    private EntityRealTimeDeliveryInfo getTgRealTimeDeliveryInfo(TargetGroup tg) {
        return entityDeliveryInfoCacheService.getDeliveryInfo(EntityDeliveryInfoCacheService.EntityType.TARGETGROUP, tg.getId());
    }

    /**
     it will filter out target groups that have spent more than
     their hourly budget
     */
    private boolean needToBePaceByDailyBudget(TargetGroup tg,
                                              EntityRealTimeDeliveryInfo tgRealTimePtr,
                                              int percentageLimitByHour) {

        double percentOfDailyBudget = tg.getDailyMaxBudget() / 100;
        double budgetLimitByHour = percentOfDailyBudget * percentageLimitByHour;

        LOGGER.debug("going to process pacing for this tg : {} " +
                     " based on this realtime info : {} , percentOfDailyBudget : {} " +
                     " limitByNow : {}", tg.getId(), tgRealTimePtr,
                     percentOfDailyBudget,
                percentageLimitByHour);

        if (tgRealTimePtr.getPlatformCost().get(IntervalType.CURRENT_DAY) < budgetLimitByHour) {
            LOGGER.debug("tg is still under limit : id : {} budgetLimitByHour : {} , " +
                    " today platform cost : {}" ,
                    tg.getId(),
                    budgetLimitByHour,
                    tgRealTimePtr.getPlatformCost().get(IntervalType.CURRENT_DAY));
            metricReporterService.addStateModuleForEntity("TG_HAS_MONEY_TO_SPEND",
                    getClass().getSimpleName(),

                            tg.getId());
            return false;
        } else {
            LOGGER.debug("pacing this tg  {}, budgetLimitByHour : {} budgetLimitByHour :{} ," +
                    " total platform cost : {}" ,
                    tg.getId(), budgetLimitByHour,
                    tgRealTimePtr.getPlatformCost().get(IntervalType.TOTAL));
            metricReporterService.addStateModuleForEntity("TG_HAS_NO_MONEY_TO_SPEND",
                    getClass().getSimpleName(),

                            tg.getId());
        }
        return true;
    }

    private boolean paceTargetGroup(TargetGroup tg) {
        int maxHourIndex = getLimitByHour (tg, metricReporterService, "TargetGroupPacingByBudgetModule");
        //we should follow this plan now
        EntityRealTimeDeliveryInfo tgRealTimePtr = getTgRealTimeDeliveryInfo (tg);
        if (tg.getPacingPlan() == null) {
            LOGGER.debug("tg with id {} doesnt have a pacing plan", tg.getId());
            metricReporterService.addStateModuleForEntity("TG_HAS_NO_PACING_PLAN",
                    getClass().getSimpleName(),

                            tg.getId());
            return false;
        }

        if (tgRealTimePtr == null) {
            metricReporterService.addStateModuleForEntity("TG_HAS_NO_REAL_TIME_INFO",
                    getClass().getSimpleName(),
                     tg.getId());
            return true;
        }

        return !needToBePaceByDailyBudget(tg, tgRealTimePtr, maxHourIndex);
    }

    public void process(OpportunityContext context) {
        markFilteredTgs(context);
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;
        if (tg.getPacingMethod() ==  IMPRESSION || paceTargetGroup (tg)) {
            metricReporterService.addStateModuleForEntity("TG_PASSING_BUDGET_PACING",
                    getClass().getSimpleName(),

                            tg.getId());
            filterTg = false;
        } else {

            metricReporterService.addStateModuleForEntity("TG_FAILING_BUDGET_PACING",
                    getClass().getSimpleName(),

                            tg.getId());
        }

        return filterTg;
    }

    @Autowired
    public void setEntityDeliveryInfoCacheService(EntityDeliveryInfoCacheService entityDeliveryInfoCacheService) {
        this.entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
    }
}

package com.dstillery.bidder.modules.budgetcontrol.capping;/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */

import static com.dstillery.common.targetgroup.PacingMethod.IMPRESSION;
import static com.dstillery.common.targetgroup.PacingSpeed.ASAP;

import org.springframework.beans.factory.annotation.Autowired;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.delivery_info.EntityRealTimeDeliveryInfo;
import com.dstillery.bidder.delivery_info.IntervalType;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class TargetGroupBudgetCappingFilterModule extends BidderModule {

    private EntityDeliveryInfoCacheService entityDeliveryInfoCacheService;
    private final MetricReporterService metricReporterService;
    public TargetGroupBudgetCappingFilterModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }

    public void process(OpportunityContext context) {
        markFilteredTgs(context);
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        if (tg.getPacingSpeed() == ASAP) {
            metricReporterService.addStateModuleForEntity("PASSED_FOR_BEING_ASAP",
                    getClass().getSimpleName(),

                            tg.getId());
            filterTg = false;
            return filterTg;
        }

        if (tg.getPacingMethod() == IMPRESSION) {
            filterTg = false;
            metricReporterService.addStateModuleForEntity("PASSED_FOR_BEING_IMP_PACED_TG",
                    "TargetGroupBudgetCappingFilterModule",
                     tg.getId());
        } else {
            EntityRealTimeDeliveryInfo tgRealTimeInfo = entityDeliveryInfoCacheService.
                    getDeliveryInfo(EntityDeliveryInfoCacheService.EntityType.TARGETGROUP, tg.getId());
            if (tgRealTimeInfo != null) {
                if (tgRealTimeInfo.getPlatformCost().get(IntervalType.CURRENT_DAY) < tg.getDailyMaxBudget () &&
                        tgRealTimeInfo.getPlatformCost().get(IntervalType.TOTAL) < tg.getMaxBudget ()) {
                    //we are under budget still for today
                    metricReporterService.addStateModuleForEntity("PASSED",
                            "TargetGroupBudgetCappingFilterModule",
                            tg.getId());
                    filterTg = false;
                } else {

                    metricReporterService.addStateModuleForEntity("FAILED",
                            "TargetGroupBudgetCappingFilterModule",
                            tg.getId());
                }
            } else {
                metricReporterService.addStateModuleForEntity("IGNORED_FOR_NO_REAL_TIME_INFO",
                        "TargetGroupBudgetCappingFilterModule",
                        tg.getId());
            }
        }

        return filterTg;
    }

    @Autowired
    public void setEntityDeliveryInfoCacheService(EntityDeliveryInfoCacheService entityDeliveryInfoCacheService) {
        this.entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
    }
}


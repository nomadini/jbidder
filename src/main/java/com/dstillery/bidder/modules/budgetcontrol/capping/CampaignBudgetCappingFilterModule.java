package com.dstillery.bidder.modules.budgetcontrol.capping;
/*
  CampaignBudgetCappingFilterModule.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */

import static com.dstillery.common.targetgroup.PacingMethod.BUDGET;
import static com.dstillery.common.targetgroup.PacingSpeed.ASAP;

import org.springframework.beans.factory.annotation.Autowired;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.delivery_info.EntityRealTimeDeliveryInfo;
import com.dstillery.bidder.delivery_info.IntervalType;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.campaign.Campaign;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class CampaignBudgetCappingFilterModule extends BidderModule {

        private final MetricReporterService metricReporterService;
        private CampaignCacheService campaignCacheService;
        private EntityDeliveryInfoCacheService entityDeliveryInfoCacheService;

        public CampaignBudgetCappingFilterModule(
                CampaignCacheService campaignCacheService,
                MetricReporterService metricReporterService) {
                this.metricReporterService = metricReporterService;
                this.campaignCacheService = campaignCacheService;
        }

        public void process(OpportunityContext context) {
                markFilteredTgs(context);
        }

        public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
                boolean filterTg = true;
                if (tg.getPacingSpeed() == ASAP) {
                        metricReporterService.addStateModuleForEntity("PASSED_FOR_BEING_ASAP",
                                this.getClass().getSimpleName(),

                                        tg.getId());
                        filterTg = false;
                        return filterTg;
                }

                Campaign campaign = campaignCacheService.findCampaignByTgId(tg.getId());

                EntityRealTimeDeliveryInfo cmpRealTimeInfo =
                        entityDeliveryInfoCacheService.getDeliveryInfo(EntityDeliveryInfoCacheService.EntityType.CAMPAIGN,
                                tg.getCampaignId());

                if (tg.getPacingMethod() == BUDGET) {
                        if (cmpRealTimeInfo != null) {
                                if (cmpRealTimeInfo.getPlatformCost().get(IntervalType.CURRENT_DAY) < campaign.getDailyMaxBudget () &&
                                        cmpRealTimeInfo.getPlatformCost().get(IntervalType.TOTAL) < campaign.getMaxBudget ()) {
                                        //we are under budget still for today
                                        metricReporterService.addStateModuleForEntity("PASSED",
                                                "CampaignBudgetCappingFilterModule",
                                                tg.getId());
                                        filterTg = false;
                                } else {

                                        metricReporterService.addStateModuleForEntity("FAILED",
                                                "CampaignBudgetCappingFilterModule",
                                                tg.getId());
                                }
                        } else {

                                metricReporterService.addStateModuleForEntity("IGNORED_FOR_NO_REAL_TIME_INFO",
                                        "CampaignBudgetCappingFilterModule",
                                        tg.getId());
                        }
                } else {
                        metricReporterService.addStateModuleForEntity("PASSED_FOR_BEING_IMP_PACED_TG",
                                "CampaignBudgetCappingFilterModule",
                                tg.getId());
                        filterTg = false;
                }

                return filterTg;

        }

        @Autowired
        public void setEntityDeliveryInfoCacheService(EntityDeliveryInfoCacheService entityDeliveryInfoCacheService) {
                this.entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
        }
}


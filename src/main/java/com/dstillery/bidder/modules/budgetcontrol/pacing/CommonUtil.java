package com.dstillery.bidder.modules.budgetcontrol.pacing;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.util.DateTimeUtil;

public class CommonUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonUtil.class);

    public static int getLimitByHour(TargetGroup tg,
                              MetricReporterService metricReporterService,
                              String nameOfModule) {

        //LOGGER.debug("tg.getPacingPlan() : " , tg.getPacingPlan().toJson();
        int timeOffSet = DateTimeUtil.getUtcOffsetBasedOnTimeZone(tg.getPacingPlan().getTimeZone());

        int hourWanted = DateTimeUtil.getCurrentHourAsIntegerInUTC () + timeOffSet;
        LOGGER.debug("hourWanted before change : {}, dateTimeService.getCurrentHourAsIntegerInUTC () : {}, timeOffSet : {}",
                hourWanted , DateTimeUtil.getCurrentHourAsIntegerInUTC (), timeOffSet);
        if (hourWanted < 0) {
            hourWanted = 24 + hourWanted;
        } else if (hourWanted > 24) {
            hourWanted = hourWanted - 24;
        }

        //LOGGER.debug("tg.getPacingPlan().timeZone : " , tg.getPacingPlan().timeZone
        // ," , timeOffSet : " , timeOffSet, " hourWanted : {} ",  hourWanted;

        int index = 0;
        if (tg.getPacingPlan() == null) {
            metricReporterService.addStateModuleForEntity("EXCEPTION:NULL_PACING_PLAN",
                    nameOfModule,
                    tg.getId());
            throw new RuntimeException("tg with id  doesn't have a pacing plan " + tg.getId());
        }
        Integer pairPtr = tg.getPacingPlan().getHourToCumulativePercentageLimit().get(hourWanted);
        if (pairPtr == null) {
            metricReporterService.addStateModuleForEntity("EXCEPTION:HOUR_WANTED_NOT_FOUND:HOUR:" + hourWanted,
                    nameOfModule,
                    tg.getId());
            throw new RuntimeException("wrong pacing plan, hourWanted was not found in plan, hour wanted : "
                    + hourWanted + " plan : " + tg.getPacingPlan());

        }
        Integer limitByNow = pairPtr;
        //LOGGER.debug("limit selected is " , limitByNow;

        return limitByNow;
    }
}

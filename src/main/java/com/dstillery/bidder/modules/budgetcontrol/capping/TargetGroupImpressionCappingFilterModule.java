package com.dstillery.bidder.modules.budgetcontrol.capping;/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */

import static com.dstillery.common.targetgroup.PacingMethod.BUDGET;
import static com.dstillery.common.targetgroup.PacingSpeed.ASAP;

import org.springframework.beans.factory.annotation.Autowired;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.delivery_info.EntityRealTimeDeliveryInfo;
import com.dstillery.bidder.delivery_info.IntervalType;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class TargetGroupImpressionCappingFilterModule extends BidderModule {

    private EntityDeliveryInfoCacheService entityDeliveryInfoCacheService;
    private final MetricReporterService metricReporterService;
    public TargetGroupImpressionCappingFilterModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }

    public void process(OpportunityContext context) {
        markFilteredTgs(context);
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        if (tg.getPacingSpeed() == ASAP) {
            metricReporterService.addStateModuleForEntity("PASSED_FOR_BEING_ASAP",
                    this.getClass().getSimpleName(),

                    tg.getId());
            filterTg = false;
            return filterTg;
        }

        if (tg.getPacingMethod() == BUDGET) {
            filterTg = false;
        } else {

            if (tg.getDailyMaxImpression () >= 0) {
                EntityRealTimeDeliveryInfo tgRealTimeInfo =
                        entityDeliveryInfoCacheService.getDeliveryInfo(EntityDeliveryInfoCacheService.EntityType.TARGETGROUP, tg.getId());
                if (tgRealTimeInfo != null) {
                    if (tgRealTimeInfo.getImpressions().get(IntervalType.CURRENT_DAY) < tg.getDailyMaxImpression () &&
                            tgRealTimeInfo.getImpressions().get(IntervalType.TOTAL) < tg.getMaxImpression ()) {

                        metricReporterService.addStateModuleForEntity("PASSED",
                                "TargetGroupImpressionCappingFilterModule",
                                tg.getId());
                        filterTg = false;
                    } else {

                        metricReporterService.addStateModuleForEntity("FAILED",
                                "TargetGroupImpressionCappingFilterModule",
                                tg.getId());
                    }
                } else {

                    metricReporterService.addStateModuleForEntity("FAILED_FOR_NO_REAL_TIME_INFO",
                            "TargetGroupImpressionCappingFilterModule",
                            tg.getId());
                }
            } else {

                metricReporterService.addStateModuleForEntity("FAILED_FOR_MINUS_DAILY_MAX",
                        "TargetGroupImpressionCappingFilterModule",
                        tg.getId());
            }
        }

        return filterTg;
    }

    @Autowired
    public void setEntityDeliveryInfoCacheService(EntityDeliveryInfoCacheService entityDeliveryInfoCacheService) {
        this.entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
    }
}

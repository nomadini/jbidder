package com.dstillery.bidder.modules.budgetcontrol.capping;
/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */

import java.util.Set;

import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetingType;

public class TargetingTypeLessTargetGroupFilter extends BidderModule {

    private final MetricReporterService metricReporterService;

    public TargetingTypeLessTargetGroupFilter(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }

    public void process(OpportunityContext context) {
        markFilteredTgs(context);
    }

    //for some weird reasons like sampling, throttling, we might end up with
    //target groups that have passed all the filtering without any targeting type
    //we must run this filter before target group selector module
    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        Set<TargetingType> typesOfTargetingChosen = context.getTargetingTypesForTg(tg.getId());

        if (typesOfTargetingChosen.isEmpty()) {
            metricReporterService.
                    addStateModuleForEntity("TG_WITHOUT_TARGETTING_TYPE",
                            "TargetingTypeLessTargetGroupFilter",
                             tg.getId());
        } else {
            filterTg = false;
        }

        return filterTg;
    }

}


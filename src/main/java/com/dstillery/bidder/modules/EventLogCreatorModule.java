package com.dstillery.bidder.modules;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.util.HashSet;

import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.util.DateTimeUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

public class EventLogCreatorModule extends BidderModule {

    private String appHostname;
    private String appVersion;

    public EventLogCreatorModule(
            String appHostname,
            String appVersion) {

        this.appHostname = appHostname;
        this.appVersion = appVersion;
    }

    public void process(OpportunityContext context) throws JsonProcessingException {
        //this module runs after we have selected a bid

        assertAndThrow(context.getBidDecisionResult().equals(EventLog.BidEventType.BID));
        context.setEventLog(createEventLogFrom(context));
    }

    EventLog createEventLogFrom(
            OpportunityContext context) throws JsonProcessingException {
        EventLog eventLog = new EventLog(
                context.getTransactionId(),
                context.getBidDecisionResult(),
                context.getChosenTargetGroup() != null ?
                        OBJECT_MAPPER.writeValueAsString(context.getTargetingTypesForTg(context.getChosenTargetGroup().getId()))
                        : OBJECT_MAPPER.writeValueAsString(new HashSet<>()),
                context.getDeviceClass(),
                context.getDeviceType());

        eventLog.setEventTime(DateTimeUtil.getNowInMilliSecond());
        eventLog.setExchangeName(context.getExchangeName());
        eventLog.setClientId(context.getClientId());
        if (context.getChosenTargetGroup() != null) {
            eventLog.setTargetGroupId(context.getChosenTargetGroup().getId());
        }
        if (context.getChosenCreative() != null) {
            eventLog.setCreativeId(context.getChosenCreative().getId());
        }

        eventLog.setKeyWords(context.getKeyWords());
        eventLog.setCampaignId(context.getCampaignId());
        eventLog.setAdvertiserId(context.getAdvertiserId());
        eventLog.setPublisherId(context.getPublisherId());

        eventLog.setGeoSegmentListId(context.getGeoSegmentListId());//this is optional ,defaults to 0
        eventLog.setWinBidPrice(context.getWinBidPrice());
        eventLog.setBidPrice(context.getBidPrice());
        eventLog.setPlatformCost(context.getPlatformCost());

        eventLog.setUserTimeZone(context.getUserTimeZone());
        eventLog.setUserTimeZonDifferenceWithUTC(context.getUserTimeZonDifferenceWithUTC());
        eventLog.setDeviceUserAgent(context.getDeviceUserAgent());
        eventLog.setDeviceIp(context.getDeviceIp());
        eventLog.setDevice(context.getDevice());
        eventLog.setDeviceLat(context.getDeviceLat());
        eventLog.setDeviceLon(context.getDeviceLon());
        eventLog.setDeviceCountry(context.getDeviceCountry());
        eventLog.setDeviceState(context.getDeviceState());
        eventLog.setDeviceCity(context.getDeviceCity());
        eventLog.setDeviceZipcode(context.getDeviceZipcode());
        eventLog.setDeviceIpAddress(context.getDeviceIp());

        eventLog.setWinBidPrice(0);
        eventLog.setSiteCategory(context.getSiteCategory());
        eventLog.setSiteDomain(context.getSiteDomain());
        eventLog.setSitePage(context.getSitePage());
        eventLog.setSitePublisherDomain(context.getSitePublisherDomain());
        eventLog.setSitePublisherName(context.getSitePublisherName());
        eventLog.setAdSize(context.getAdSize());
        eventLog.setLastModuleRunInPipeline(context.getLastModuleRunInPipeline());
        eventLog.setBiddingOnlyForMatchingPixel(context.isBiddingOnlyForMatchingPixel());
        eventLog.setExchangeName(context.getExchangeName());
        eventLog.setImpressionType(context.getImpressionType());

        eventLog.setMgrs1km(context.getMgrs1km());
        eventLog.setMgrs100m(context.getMgrs100m());
        eventLog.setMgrs10m(context.getMgrs10m());

        eventLog.setAppHostname(appHostname);
        eventLog.setAppVersion(appVersion);
        eventLog.setBidderCallbackServletUrl(context.getBidderCallbackServletUrl());
        eventLog.setChosenSegmentIdsList(context.getChosenSegmentIdsList());
        eventLog.setAllSegmentIdsFoundList(context.getAllSegmentIdsFoundList());
        if (context.isStrictEventLogChecking()) {
            //sometimes we want to record no_bid events in cassandra,
            //in BidEventRecorderModule so we set this boolean flag as false
            //and we don't check these
            assertAndThrow(eventLog.getEventTime() > 0);
            assertAndThrow(!eventLog.getEventId().isEmpty());
            assertAndThrow(eventLog.getTargetGroupId() > 0);
            assertAndThrow(eventLog.getCreativeId() > 0);
            assertAndThrow(eventLog.getCampaignId() > 0);
            assertAndThrow(eventLog.getAdvertiserId() > 0);
            assertAndThrow(eventLog.getClientId() > 0);

            assertAndThrow(eventLog.getWinBidPrice() == 0);
            assertAndThrow(eventLog.getBidPrice() > 0);
            // assertAndThrow(!eventLog.userTimeZone.isEmpty());
            assertAndThrow(!eventLog.getDeviceUserAgent().isEmpty());
            assertAndThrow(!eventLog.getDeviceIp().isEmpty());
            assertAndThrow(eventLog.getDeviceLat() != 0);
            assertAndThrow(eventLog.getDeviceLon() != 0);
            assertAndThrow(eventLog.getDeviceCountry() != null);
            assertAndThrow(!eventLog.getDeviceCity().isEmpty());
            assertAndThrow(eventLog.getDeviceState() != null);
            assertAndThrow(!eventLog.getDeviceZipcode().isEmpty());
            assertAndThrow(!eventLog.getDeviceIpAddress().isEmpty());
            assertAndThrow(!eventLog.getSiteCategory().isEmpty());
            assertAndThrow(!eventLog.getSiteDomain().isEmpty());
            assertAndThrow(!eventLog.getSitePage().isEmpty());
//			assertAndThrow(!eventLog.getSitePublisherDomain().isEmpty());
//			assertAndThrow(!eventLog.getSitePublisherName().isEmpty());
            assertAndThrow(!eventLog.getAdSize().isEmpty());

            assertAndThrow(!eventLog.getLastModuleRunInPipeline().isEmpty());
            assertAndThrow(!eventLog.getExchangeName().isEmpty());
            assertAndThrow(!eventLog.getImpressionType().isEmpty());

            assertAndThrow(!eventLog.getAppHostname().isEmpty());
            assertAndThrow(!eventLog.getBidderCallbackServletUrl().isEmpty());
            assertAndThrow(!eventLog.getAppVersion().isEmpty());
        }

        return eventLog;

    }
    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}

package com.dstillery.bidder.modules.pricing;

import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;

import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetGroupFilterStatistic;
import com.dstillery.common.util.NumberUtil;

import java.util.Map;

public class BidPriceCalculatorModule extends BidderModule {

    public BidPriceCalculatorModule() {

    }

    public void process(OpportunityContext context) {
        //improve the logic based on different strategies later
        context.setBidPrice(NumberUtil.roundDouble(context.getBidFloor() * 1.1, 2));

    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }
}

package com.dstillery.bidder.modules;

import static com.dstillery.common.feature.FeatureType.GTLD;
import static com.dstillery.common.feature.FeatureType.IAB_CAT;

import com.dstillery.common.common_modules.DeviceHistoryUpdaterModule;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
public class VisitFeatureHistoryOfDeviceUpdaterModuleWrapper extends BidderModule {
    private DeviceHistoryUpdaterModule visitFeatureDeviceHistoryUpdaterModule;

    private final MetricReporterService metricReporterService;
    public VisitFeatureHistoryOfDeviceUpdaterModuleWrapper(DeviceHistoryUpdaterModule visitFeatureDeviceHistoryUpdaterModule,
                                                           MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.visitFeatureDeviceHistoryUpdaterModule = visitFeatureDeviceHistoryUpdaterModule;
    }

    public void process(OpportunityContext context) {
        if (context.getDevice() == null || context.getSiteDomain().isEmpty()) {
            metricReporterService.addStateModuleForEntity("SKIPPING_BECAUSEOF_UNKNOWN_USER",
                    "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper",
                    "ALL");

            return;
        }
        try {
            visitFeatureDeviceHistoryUpdaterModule.process(GTLD,
                    context.getSiteDomain(),
                    context.getDevice());

            //we save device to iabcat here which is used by clustering app
            for (String cat : context.getSiteCategory()) {
                visitFeatureDeviceHistoryUpdaterModule.process(IAB_CAT,
                        cat,
                        context.getDevice());
            }
            metricReporterService.addStateModuleForEntity("ADDING_DEVICE_FEATURE_HISTORY",
                    "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper",
                    "ALL");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


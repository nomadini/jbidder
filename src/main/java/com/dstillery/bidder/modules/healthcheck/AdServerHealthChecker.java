package com.dstillery.bidder.modules.healthcheck;


import static com.dstillery.common.adserver.AppStatusRequest.AdServerStatus.GREEN;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.adserver.AppStatusRequest;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.util.GUtil;
import com.dstillery.common.util.HttpUtil;
import com.dstillery.common.util.StringUtil;
/**
 this class is now responsible for making sure adserves are functioning
 in order to bid
 *
 */
public class AdServerHealthChecker implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdServerHealthChecker.class);

    private final MetricReporterService metricReporterService;
    private AtomicBoolean areAdserversHealthy;
    private DiscoveryService discoveryService;

    public AdServerHealthChecker(DiscoveryService discoveryService,
                                 int adservStatusCheckIntervalInSecond,
                                 MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.discoveryService = discoveryService;
        areAdserversHealthy = new AtomicBoolean();
        LOGGER.info("checking adserver data every {} seconds.", adservStatusCheckIntervalInSecond);
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0,
                adservStatusCheckIntervalInSecond,
                TimeUnit.SECONDS);
    }

    public void run() {
        try {
            checkStatus ();
        } catch (Exception e) {
            LOGGER.warn("exception occurred", e);
            areAdserversHealthy.set(false);
            metricReporterService.addStateModuleForEntity(
                    "EXCEPTION : NO_BID_BECAUSE_THREAD_FAILED", "AdServerHealthChecker",
                    "ALL");

        }

    }

    private void checkStatus() {

        AppStatusRequest appStatusRequest = new AppStatusRequest();

        List<String> allAdServerHostUrls = discoveryService.discoverAllServiceUrls("adserver");
        LOGGER.trace("allAdServerHostUrls : {}", allAdServerHostUrls);
        List<String> bustedAdServers = new ArrayList<>();
        for (String adServUrl : allAdServerHostUrls) {
            String adServerUrl = adServUrl + StringUtil.toStr("/status");

            for (int numberOfAttempts = 0; numberOfAttempts < 3; numberOfAttempts++) {
                try {
                    checkStatusOfOneAdServer(adServerUrl, bustedAdServers);
                    //if we suceeded in getting the adserver status we break
                    //out of the loop
                    break;
                } catch(Exception e) {
                    // in case of any http excetion we try for at most 3 times
                    if (numberOfAttempts >= 2) {
                        LOGGER.warn("cannot connect to adserver : {}" , adServerUrl);
                        bustedAdServers.add(adServerUrl);
                        metricReporterService.addStateModuleForEntity
                                ("EXCEPTION : NO_BID_BECAUSE_THIS_ADSERVER_IS_BUSTED:" + adServerUrl,
                                        "AdServerHealthChecker", "ALL");

                    }
                    //sleep 2 seconds before trying again
                    GUtil.sleepInSeconds(2);
                }
            }
        }

        if (allAdServerHostUrls.isEmpty()) {
            // dicoveryService gave us no adservers
            areAdserversHealthy.set(false);
        } else if (bustedAdServers.isEmpty()) {
            areAdserversHealthy.set(true);
            metricReporterService.addStateModuleForEntity(
                    "DO_BID_BECAUSE_ALL_ADSERVERES_ARE_GOOD",
                    "AdServerHealthChecker",
                    "ALL");
        } else {
            //this else statement is very important to make sure if a adserver goes down, after
            //it has been up, we go to no bid mode
            areAdserversHealthy.set(false);
        }
    }

    private void checkStatusOfOneAdServer(String adServerUrl, List<String> bustedAdServers) throws IOException {
        LOGGER.trace("adServerUrl is {}", adServerUrl);
        //TODO : fix this
        String adServerResponsePayload = HttpUtil.sendGetRequest (
                adServerUrl,
                1000,
                2); //we try 2 times for 3 times each, then we go RED
        AppStatusRequest payload = OBJECT_MAPPER.readValue(adServerResponsePayload
                , AppStatusRequest.class);
        LOGGER.trace("adserver response {}" , adServerResponsePayload);
        if (payload.getStatus() != GREEN) {
            //adserver is busted , we should go to no bid mode
            areAdserversHealthy.set(false);
            bustedAdServers.add(adServerUrl);
            metricReporterService.addStateModuleForEntity("ADSERVER_IS_RED", "AdServerHealthChecker", "ALL");
            LOGGER.error("ADSERVER IS RED. going to no bid mode, adserver response {}", adServerResponsePayload);
        }
    }

    public AtomicBoolean getAreAdserversHealthy() {
        return areAdserversHealthy;
    }
}

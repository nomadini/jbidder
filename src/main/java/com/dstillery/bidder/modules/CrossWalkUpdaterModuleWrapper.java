package com.dstillery.bidder.modules;

import com.dstillery.common.common_modules.CrossWalkUpdaterModule;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class CrossWalkUpdaterModuleWrapper extends BidderModule {

    private CrossWalkUpdaterModule crossWalkUpdaterModule;

    private final MetricReporterService metricReporterService;
    public CrossWalkUpdaterModuleWrapper(
            CrossWalkUpdaterModule crossWalkUpdaterModule,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.crossWalkUpdaterModule = crossWalkUpdaterModule;
    }

    public void process(OpportunityContext context) {
        if (context.getDevice() == null || context.getDeviceIp().isEmpty()) {
            metricReporterService.addStateModuleForEntity("SKIPPING_BECAUSEOF_UNKNOWN_USER",
                    "CrossWalkUpdaterModuleWrapper",
                    "ALL");

            return;
        }
        crossWalkUpdaterModule.recordIpToDeviceAssociations(
                context.getDeviceIp(),
                context.getDevice());

    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        return false;
    }

}

package com.dstillery.bidder.modules;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.pipeline.BidderMainPipelineProcessor;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.util.GUtil;

public class ProcessorInvokerModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessorInvokerModule.class);

    private List<TargetGroup> targetGroupsEligibleForBidding;//TODO this should be set after each reload
    private BidderMainPipelineProcessor bidderMainPipelineProcessor;

    private final MetricReporterService metricReporterService;
    private AtomicBoolean forceRefreshCachesFlag;
    public ProcessorInvokerModule(BidderMainPipelineProcessor bidderMainPipelineProcessor,
                                  MetricReporterService metricReporterService,
                                  List<TargetGroup> targetGroupsEligibleForBidding,
                                  AtomicBoolean forceRefreshCachesFlag) {
        this.metricReporterService = metricReporterService;
        this.targetGroupsEligibleForBidding = requireNonNull(targetGroupsEligibleForBidding);
        this.bidderMainPipelineProcessor = bidderMainPipelineProcessor;
        this.forceRefreshCachesFlag = forceRefreshCachesFlag;
    }

    public void process(OpportunityContext context) {
        context.setAllAvailableTgs(new ArrayList<>(targetGroupsEligibleForBidding));

        if (context.getAllAvailableTgs().isEmpty()) {
            if (GUtil.allowedToCall(100)) {
                metricReporterService.addStateModuleForEntity("NO_TG_TO_PROCESS",
                        this.getClass().getSimpleName(),
                        "ALL",
                        MetricReporterService.MetricPriority.EXCEPTION);
            }
            forceRefreshCachesFlag.set(true);
        }

        //even if we don't have tg to bid, we want to send data to scoring and record other data
        metricReporterService.addStateModuleForEntity("INVOKING_MAIN_PROCESSOR",
                this.getClass().getSimpleName(),
                "ALL");
        bidderMainPipelineProcessor.process(context);

    }
    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

    public void setTargetGroupsEligibleForBidding(List<TargetGroup> targetGroupsEligibleForBidding) {
        this.targetGroupsEligibleForBidding = targetGroupsEligibleForBidding;
    }
}

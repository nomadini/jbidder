package com.dstillery.bidder.modules;

import static com.dstillery.common.feature.FeatureType.IAB_CAT;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.common_modules.FeatureDeviceHistoryUpdaterModule;
import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class DeviceHistoryOfVisitFeatureUpdaterModuleWrapper extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceHistoryOfVisitFeatureUpdaterModuleWrapper.class);

    private final MetricReporterService metricReporterService;
    private FeatureType featureCategory;
    private FeatureDeviceHistoryUpdaterModule featureDeviceHistoryUpdaterModule;

    public DeviceHistoryOfVisitFeatureUpdaterModuleWrapper(
            FeatureType featureCategory,
            FeatureDeviceHistoryUpdaterModule featureDeviceHistoryUpdaterModule,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.featureCategory = featureCategory;
        this.featureDeviceHistoryUpdaterModule = featureDeviceHistoryUpdaterModule;
    }

    public void process(OpportunityContext context) {
        if (context.getDevice() == null || context.getSiteDomain().isEmpty()) {
            metricReporterService.addStateModuleForEntity("SKIPPING_BECAUSEOF_UNKNOWN_USER",
                    "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper",
                    "ALL");

            return;
        }
        try {
            featureDeviceHistoryUpdaterModule.process(featureCategory,
                    context.getSiteDomain(),
                    context.getDevice());
            metricReporterService.addStateModuleForEntity("adding_gtld_feature_device_history",
                    "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper",
                    "ALL");

            if (context.getDevice() == null || context.getSiteCategory().isEmpty()) {
                throw new RuntimeException("unknown siteCategory or deviceType");
            }

            metricReporterService.addStateModuleForEntity("adding_iabcat_feature_device_history",
                    "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper",
                    "ALL");

            //we save iabcat to device here which is used by clustering app
            for (String cat : context.getSiteCategory()) {
                featureDeviceHistoryUpdaterModule.process(IAB_CAT,
                        cat,
                        context.getDevice());
            }
        } catch (Exception e) {
            LOGGER.warn("exception", e);
            throw new RuntimeException(e);
        }

    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}

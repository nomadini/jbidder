package com.dstillery.bidder.modules;/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;

import java.util.concurrent.atomic.AtomicLong;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class ThrottlerModule extends BidderModule {

    private AtomicLong numberOfBidRequestRecievedPerMinute;
    private int numberOfbidsRequestAllowedToProcessPerSecond;
    private final MetricReporterService metricReporterService;

    public ThrottlerModule(
            AtomicLong numberOfBidRequestRecievedPerMinute,
            int numberOfbidsRequestAllowedToProcessPerSecond,
            MetricReporterService metricReporterService) {
            this.metricReporterService = metricReporterService;
            this.numberOfbidsRequestAllowedToProcessPerSecond = numberOfbidsRequestAllowedToProcessPerSecond;
            this.numberOfBidRequestRecievedPerMinute = numberOfBidRequestRecievedPerMinute;
    }

    public void process( OpportunityContext context) {
        if (throttleBidRequests()) {
            metricReporterService.addStateModuleForEntity("numberOfBidsThrottled",
                    "ThrottlerModule",
                    "ALL");
            //LOGGER.debug("throttling bid requests");

            context.setFinalBidResponseContent("");
            context.setBidResponseStatusCode(HTTP_NO_CONTENT);
            context.stopProcessing(this.getClass().getSimpleName());
            return;
        }

        //this should be right after throttle function,
        //this will help us count the number of requests recieved and compare against the number
        //we are allowed to process
        numberOfBidRequestRecievedPerMinute.getAndIncrement();
    }

    private boolean throttleBidRequests() {
        return numberOfBidRequestRecievedPerMinute.get() >= numberOfbidsRequestAllowedToProcessPerSecond;
    }
    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


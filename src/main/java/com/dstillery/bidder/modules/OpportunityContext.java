package com.dstillery.bidder.modules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.RecentVisitHistoryScoreCard;
import com.dstillery.common.adhistory.AdEntryDevicePair;
import com.dstillery.common.creative.Creative;
import com.dstillery.common.creative.CreativeApi;
import com.dstillery.common.creative.CreativeContentType;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceEnvironmentType;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.device.OsType;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.geo.Country;
import com.dstillery.common.geo.State;
import com.dstillery.common.geo.places.Place;
import com.dstillery.common.metric.OpenRtbVersion;
import com.dstillery.common.metric.ProcessingStatus;
import com.dstillery.common.targetgroup.AdPosition;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetingType;
public class OpportunityContext {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpportunityContext.class);

    private List<TargetGroup> allAvailableTgs;

    private List<Place> allPlaces = new ArrayList<>();
    private List<Long> tagIds = new ArrayList<>();
    private List<Long> chosenSegmentIdsList = new ArrayList<>();
    private List<Long> allSegmentIdsFoundList = new ArrayList<>();
    private Map<Long, TargetGroup> mapOfDomainBlockingTgs;
    private boolean strictEventLogChecking;
    private boolean isAsyncPiplineExecuting;
    private boolean fetchingPlacesFromRemoteExecuted;
    private Integer acceptableModuleLatencyInMillis;
    private Integer bidResponseStatusCode;

    //we need this for -PASSING_VALUE states that we record for every entity
    private String allBlockedBannerTypesAsStr = "";
    private String bidderCallbackServletUrl = "";
    private String processingBidderUrl = "";
    private String servletPathToForwardTo = "";
    private String requestBody = "";
    private Map<Long, TargetGroup> allTgsWithCorrectSegmentsForDeviceAsMap = new HashMap<>();
    private Set<String> blockedBannerTypesMap;
    private RecentVisitHistoryScoreCard recentVisitHistoryScoreCardOfDevice;
    private Set<String> blockedAdvertiserMap;
    private Set<String> blockedIabCategoriesMap;
    private Map<Long, Set<TargetingType>> tgIdToTypesOfTargetingChosen;

    //this is incremented for every context // its good for sampling purposes
    private Long uniqueNumberOfContext;
    private List<AdEntryDevicePair> adHistory = new ArrayList<>();
    private BidResponseBuilder bidResponseBuilder;
    private EventLog eventLog;
    private NomadiniDevice device;
    private String exchangeUserId = "";
    private String impressionType = "";
    private String bidRequestId = "";
    private String impressionIdInBidRequest = "";
    private OpenRtbVersion openRtbVersion;
    private String exchangeId = "";
    private String exchangeName = "";
    private String winNotificationUrl = "";
    private DeviceEnvironmentType environmentType;
    private String transactionId = "";
    private CreativeContentType creativeContentType = CreativeContentType.DISPLAY;
    private String finalBidResponseContent = "";
    private String protocolVersion = "";     //something like OpenRtb2.3 or Casale2.1 that tells us what protocol this bid request is formed based on
    private String eventType = "";
    private Long campaignId;
    private Long advertiserId;
    private Long clientId;
    private Long geoSegmentListId;
    private Long publisherId;
    private boolean biddingOnlyForMatchingPixel;
    private Long chosenSegmentId;//this field is deprecated
    private AdPosition adPosition = AdPosition.ANY;

    private double bidPrice;     //this is the price that we bid with
    private double bidFloor;
    private double winBidPrice;
    private double platformCost;

    private String userTimeZone = "";
    private Integer userTimeZonDifferenceWithUTC;

    /**
     *
      these are the variables that are common , and will
      be mapped to by bid request coming from any exchange
     *
     */

    private String deviceUserAgent = "";
    private OsType deviceOsFamily = OsType.LINUX;
    private String deviceIp = "";
    private DeviceTypeValue deviceType;

    private double deviceLat;
    private double deviceLon;
    private Country deviceCountry;
    private String deviceCity = "";
    private State deviceState;
    private String deviceZipcode= "";
    private List<String> siteCategory = new ArrayList<>();
    private String siteDomain = "";
    private String sitePage = "";
    private String sitePublisherDomain = "";
    private String sitePublisherName = "";
    private String adSize = "";
    private List<CreativeApi> creativeAPI = new ArrayList<>();
    private boolean secureCreativeRequired;

    private List<String> blockedAttributes = new ArrayList<>();
    private String inventory = "";

    private String mgrs1km = "";
    private String mgrs100m = "";
    private String mgrs10m = "";

    private String rawBidRequest = "";     //this is just for debugging purposes

    private EventLog.BidEventType bidDecisionResult;
    private String lastModuleRunInPipeline = "";
    private String finalCreativeContent = "";

    private Map<Long, TargetGroup> targetGroupIdsToRemove;

    private TargetGroup chosenTargetGroup;
    private Creative chosenCreative;

    private ProcessingStatus status = ProcessingStatus.CONTINUE_PROCESSING;
    private List<String> modulesStoppedProcessing = new ArrayList<>();
    private DeviceClassValue deviceClass = DeviceClassValue.UNKNOWN;
    private boolean deviceSegmentReadHappened;
    private Set<String> keyWords = new HashSet<>();
    private String bidRequestBody = "unset";

    public OpportunityContext() {
        //this field allow us to know if context is being called in asyncPipeline or not
        //depending on that, we decided to execute some logic or not
        isAsyncPiplineExecuting = false;
        strictEventLogChecking = true;
        allAvailableTgs = new ArrayList<>();
        mapOfDomainBlockingTgs = null;
        targetGroupIdsToRemove = new ConcurrentHashMap<> ();
        //the primitives has to be initialized to 0
        //because if not, they will have some random number from stack memory values
        campaignId = 0L;
        advertiserId = 0L;
        winBidPrice = 0;
        bidPrice = 0;
        clientId = 0L;
        geoSegmentListId = 0L;
        publisherId = 0L;
        biddingOnlyForMatchingPixel = false;
        chosenSegmentId = 0L;//this field is deprecated
        userTimeZonDifferenceWithUTC = 0;
        platformCost = 0;
        geoSegmentListId = 0L;
        clientId = 0L;
        deviceLat = 0;
        deviceLon = 0;
        bidFloor = 0;
        bidDecisionResult = EventLog.BidEventType.NO_BID;
        acceptableModuleLatencyInMillis = 2;
        blockedBannerTypesMap = new HashSet<>();
        blockedBannerTypesMap = new HashSet<>();
        blockedAdvertiserMap = new HashSet<>();
        blockedIabCategoriesMap = new HashSet<>();
        biddingOnlyForMatchingPixel = false;
        fetchingPlacesFromRemoteExecuted = false;

        this.tgIdToTypesOfTargetingChosen = new HashMap<>();
        uniqueNumberOfContext = ThreadLocalRandom.current().nextLong();
    }

    public void addTargetingTypeForTg(Long tgId, TargetingType typesOfTargeting) {
        Set<TargetingType> pairPtr = tgIdToTypesOfTargetingChosen.get(tgId);
        if (pairPtr == null) {
            Set<TargetingType> value = new HashSet<>();
            value.add(typesOfTargeting);
            tgIdToTypesOfTargetingChosen.put(tgId, value);

        } else {
            pairPtr.add(typesOfTargeting);
        }
    }

    public Set<TargetingType> getTargetingTypesForTg(Long tgId) {
        Set<TargetingType> valuePair = tgIdToTypesOfTargetingChosen.get(tgId);
        if (valuePair != null) {
            return valuePair;
        }
        return new HashSet<>();
    }

    public Integer getBidResponseStatusCode() {
        return bidResponseStatusCode;
    }

    public void setBidResponseStatusCode(Integer bidResponseStatusCode) {
        this.bidResponseStatusCode = bidResponseStatusCode;
    }

    public List<TargetGroup> getAllAvailableTgs() {
        return allAvailableTgs;
    }

    public void setAllAvailableTgs(List<TargetGroup> allAvailableTgs) {
        this.allAvailableTgs = allAvailableTgs;
    }

    public List<Place> getAllPlaces() {
        return allPlaces;
    }

    public void setAllPlaces(List<Place> allPlaces) {
        this.allPlaces = allPlaces;
    }

    public List<Long> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<Long> tagIds) {
        this.tagIds = tagIds;
    }

    public List<Long> getChosenSegmentIdsList() {
        return chosenSegmentIdsList;
    }

    public void setChosenSegmentIdsList(List<Long> chosenSegmentIdsList) {
        this.chosenSegmentIdsList = chosenSegmentIdsList;
    }

    public List<Long> getAllSegmentIdsFoundList() {
        return allSegmentIdsFoundList;
    }

    public void setAllSegmentIdsFoundList(List<Long> allSegmentIdsFoundList) {
        this.allSegmentIdsFoundList = allSegmentIdsFoundList;
    }

    public Map<Long, TargetGroup> getMapOfDomainBlockingTgs() {
        return mapOfDomainBlockingTgs;
    }

    public void setMapOfDomainBlockingTgs(Map<Long, TargetGroup> mapOfDomainBlockingTgs) {
        this.mapOfDomainBlockingTgs = mapOfDomainBlockingTgs;
    }

    public boolean isStrictEventLogChecking() {
        return strictEventLogChecking;
    }

    public void setStrictEventLogChecking(boolean strictEventLogChecking) {
        this.strictEventLogChecking = strictEventLogChecking;
    }

    public boolean isAsyncPiplineExecuting() {
        return isAsyncPiplineExecuting;
    }

    public void setAsyncPiplineExecuting(boolean asyncPiplineExecuting) {
        isAsyncPiplineExecuting = asyncPiplineExecuting;
    }

    public boolean isFetchingPlacesFromRemoteExecuted() {
        return fetchingPlacesFromRemoteExecuted;
    }

    public void setFetchingPlacesFromRemoteExecuted(boolean fetchingPlacesFromRemoteExecuted) {
        this.fetchingPlacesFromRemoteExecuted = fetchingPlacesFromRemoteExecuted;
    }

    public Integer getAcceptableModuleLatencyInMillis() {
        return acceptableModuleLatencyInMillis;
    }

    public void setAcceptableModuleLatencyInMillis(Integer acceptableModuleLatencyInMillis) {
        this.acceptableModuleLatencyInMillis = acceptableModuleLatencyInMillis;
    }

    public String getAllBlockedBannerTypesAsStr() {
        return allBlockedBannerTypesAsStr;
    }

    public void setAllBlockedBannerTypesAsStr(String allBlockedBannerTypesAsStr) {
        this.allBlockedBannerTypesAsStr = allBlockedBannerTypesAsStr;
    }

    public String getBidderCallbackServletUrl() {
        return bidderCallbackServletUrl;
    }

    public void setBidderCallbackServletUrl(String bidderCallbackServletUrl) {
        this.bidderCallbackServletUrl = bidderCallbackServletUrl;
    }

    public String getProcessingBidderUrl() {
        return processingBidderUrl;
    }

    public void setProcessingBidderUrl(String processingBidderUrl) {
        this.processingBidderUrl = processingBidderUrl;
    }

    public String getServletPathToForwardTo() {
        return servletPathToForwardTo;
    }

    public void setServletPathToForwardTo(String servletPathToForwardTo) {
        this.servletPathToForwardTo = servletPathToForwardTo;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public Map<Long, TargetGroup> getAllTgsWithCorrectSegmentsForDeviceAsMap() {
        return allTgsWithCorrectSegmentsForDeviceAsMap;
    }

    public void setAllTgsWithCorrectSegmentsForDeviceAsMap(Map<Long, TargetGroup> allTgsWithCorrectSegmentsForDeviceAsMap) {
        this.allTgsWithCorrectSegmentsForDeviceAsMap = allTgsWithCorrectSegmentsForDeviceAsMap;
    }

    public Set<String> getBlockedBannerTypesMap() {
        return blockedBannerTypesMap;
    }

    public void setBlockedBannerTypesMap(Set<String> blockedBannerTypesMap) {
        this.blockedBannerTypesMap = blockedBannerTypesMap;
    }

    public RecentVisitHistoryScoreCard getRecentVisitHistoryScoreCardOfDevice() {
        return recentVisitHistoryScoreCardOfDevice;
    }

    public void setRecentVisitHistoryScoreCardOfDevice(RecentVisitHistoryScoreCard recentVisitHistoryScoreCardOfDevice) {
        this.recentVisitHistoryScoreCardOfDevice = recentVisitHistoryScoreCardOfDevice;
    }

    public Set<String> getBlockedAdvertiserMap() {
        return blockedAdvertiserMap;
    }

    public void setBlockedAdvertiserMap(Set<String> blockedAdvertiserMap) {
        this.blockedAdvertiserMap = blockedAdvertiserMap;
    }

    public Set<String> getBlockedIabCategoriesMap() {
        return blockedIabCategoriesMap;
    }

    public void setBlockedIabCategoriesMap(Set<String> blockedIabCategoriesMap) {
        this.blockedIabCategoriesMap = blockedIabCategoriesMap;
    }

    public Map<Long, Set<TargetingType>> getTgIdToTypesOfTargetingChosen() {
        return tgIdToTypesOfTargetingChosen;
    }

    public void setTgIdToTypesOfTargetingChosen(Map<Long, Set<TargetingType>> tgIdToTypesOfTargetingChosen) {
        this.tgIdToTypesOfTargetingChosen = tgIdToTypesOfTargetingChosen;
    }

    public Long getUniqueNumberOfContext() {
        return uniqueNumberOfContext;
    }

    public void setAdHistory(List<AdEntryDevicePair> adHistory) {
        this.adHistory = adHistory;
    }

    public List<AdEntryDevicePair> getAdHistory() {
        return adHistory;
    }

    public BidResponseBuilder getBidResponseBuilder() {
        return bidResponseBuilder;
    }

    public void setBidResponseBuilder(BidResponseBuilder bidResponseBuilder) {
        this.bidResponseBuilder = bidResponseBuilder;
    }

    public EventLog getEventLog() {
        return eventLog;
    }

    public void setEventLog(EventLog eventLog) {
        this.eventLog = eventLog;
    }

    public NomadiniDevice getDevice() {
        return device;
    }

    public void setDevice(NomadiniDevice device) {
        this.device = device;
    }

    public String getExchangeUserId() {
        return exchangeUserId;
    }

    public void setExchangeUserId(String exchangeUserId) {
        this.exchangeUserId = exchangeUserId;
    }

    public String getImpressionType() {
        return impressionType;
    }

    public void setImpressionType(String impressionType) {
        this.impressionType = impressionType;
    }

    public String getBidRequestId() {
        return bidRequestId;
    }

    public void setBidRequestId(String bidRequestId) {
        this.bidRequestId = bidRequestId;
    }

    public String getImpressionIdInBidRequest() {
        return impressionIdInBidRequest;
    }

    public void setImpressionIdInBidRequest(String impressionIdInBidRequest) {
        this.impressionIdInBidRequest = impressionIdInBidRequest;
    }

    public OpenRtbVersion getOpenRtbVersion() {
        return openRtbVersion;
    }

    public void setOpenRtbVersion(OpenRtbVersion openRtbVersion) {
        this.openRtbVersion = openRtbVersion;
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public void setExchangeId(String exchangeId) {
        this.exchangeId = exchangeId;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getWinNotificationUrl() {
        return winNotificationUrl;
    }

    public void setWinNotificationUrl(String winNotificationUrl) {
        this.winNotificationUrl = winNotificationUrl;
    }

    public DeviceEnvironmentType getEnvironmentType() {
        return environmentType;
    }

    public void setEnvironmentType(DeviceEnvironmentType environmentType) {
        this.environmentType = environmentType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public CreativeContentType getCreativeContentType() {
        return creativeContentType;
    }

    public void setCreativeContentType(CreativeContentType creativeContentType) {
        this.creativeContentType = creativeContentType;
    }

    public String getFinalBidResponseContent() {
        return finalBidResponseContent;
    }

    public void setFinalBidResponseContent(String finalBidResponseContent) {
        this.finalBidResponseContent = finalBidResponseContent;
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Long getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(Long advertiserId) {
        this.advertiserId = advertiserId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getGeoSegmentListId() {
        return geoSegmentListId;
    }

    public void setGeoSegmentListId(Long geoSegmentListId) {
        this.geoSegmentListId = geoSegmentListId;
    }

    public Long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }

    public boolean isBiddingOnlyForMatchingPixel() {
        return biddingOnlyForMatchingPixel;
    }

    public void setBiddingOnlyForMatchingPixel(boolean biddingOnlyForMatchingPixel) {
        this.biddingOnlyForMatchingPixel = biddingOnlyForMatchingPixel;
    }

    public Long getChosenSegmentId() {
        return chosenSegmentId;
    }

    public void setChosenSegmentId(Long chosenSegmentId) {
        this.chosenSegmentId = chosenSegmentId;
    }

    public AdPosition getAdPosition() {
        return adPosition;
    }

    public void setAdPosition(AdPosition adPosition) {
        this.adPosition = adPosition;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(double bidPrice) {
        this.bidPrice = bidPrice;
    }

    public double getBidFloor() {
        return bidFloor;
    }

    public void setBidFloor(double bidFloor) {
        this.bidFloor = bidFloor;
    }

    public double getWinBidPrice() {
        return winBidPrice;
    }

    public void setWinBidPrice(double winBidPrice) {
        this.winBidPrice = winBidPrice;
    }

    public double getPlatformCost() {
        return platformCost;
    }

    public void setPlatformCost(double platformCost) {
        this.platformCost = platformCost;
    }

    public String getUserTimeZone() {
        return userTimeZone;
    }

    public void setUserTimeZone(String userTimeZone) {
        this.userTimeZone = userTimeZone;
    }

    public Integer getUserTimeZonDifferenceWithUTC() {
        return userTimeZonDifferenceWithUTC;
    }

    public void setUserTimeZonDifferenceWithUTC(Integer userTimeZonDifferenceWithUTC) {
        this.userTimeZonDifferenceWithUTC = userTimeZonDifferenceWithUTC;
    }

    public String getDeviceUserAgent() {
        return deviceUserAgent;
    }

    public void setDeviceUserAgent(String deviceUserAgent) {
        this.deviceUserAgent = deviceUserAgent;
    }

    public OsType getDeviceOsFamily() {
        return deviceOsFamily;
    }

    public void setDeviceOsFamily(OsType deviceOsFamily) {
        this.deviceOsFamily = deviceOsFamily;
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp;
    }

    public DeviceTypeValue getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceTypeValue deviceType) {
        this.deviceType = deviceType;
    }

    public double getDeviceLat() {
        return deviceLat;
    }

    public void setDeviceLat(double deviceLat) {
        this.deviceLat = deviceLat;
    }

    public double getDeviceLon() {
        return deviceLon;
    }

    public void setDeviceLon(double deviceLon) {
        this.deviceLon = deviceLon;
    }

    public Country getDeviceCountry() {
        return deviceCountry;
    }

    public void setDeviceCountry(Country deviceCountry) {
        this.deviceCountry = deviceCountry;
    }

    public String getDeviceCity() {
        return deviceCity;
    }

    public void setDeviceCity(String deviceCity) {
        this.deviceCity = deviceCity;
    }

    public State getDeviceState() {
        return deviceState;
    }

    public void setDeviceState(State deviceState) {
        this.deviceState = deviceState;
    }

    public String getDeviceZipcode() {
        return deviceZipcode;
    }

    public void setDeviceZipcode(String deviceZipcode) {
        this.deviceZipcode = deviceZipcode;
    }

    public List<String> getSiteCategory() {
        return siteCategory;
    }

    public void setSiteCategory(List<String> siteCategory) {
        this.siteCategory = siteCategory;
    }

    public String getSiteDomain() {
        return siteDomain;
    }

    public void setSiteDomain(String siteDomain) {
        this.siteDomain = siteDomain;
    }

    public String getSitePage() {
        return sitePage;
    }

    public void setSitePage(String sitePage) {
        this.sitePage = sitePage;
    }

    public String getSitePublisherDomain() {
        return sitePublisherDomain;
    }

    public void setSitePublisherDomain(String sitePublisherDomain) {
        this.sitePublisherDomain = sitePublisherDomain;
    }

    public String getSitePublisherName() {
        return sitePublisherName;
    }

    public void setSitePublisherName(String sitePublisherName) {
        this.sitePublisherName = sitePublisherName;
    }

    public String getAdSize() {
        return adSize;
    }

    public void setAdSize(String adSize) {
        this.adSize = adSize;
    }

    public List<CreativeApi> getCreativeAPI() {
        return creativeAPI;
    }

    public void setCreativeAPI(List<CreativeApi> creativeAPI) {
        this.creativeAPI = creativeAPI;
    }

    public boolean isSecureCreativeRequired() {
        return secureCreativeRequired;
    }

    public void setSecureCreativeRequired(boolean secureCreativeRequired) {
        this.secureCreativeRequired = secureCreativeRequired;
    }

    public List<String> getBlockedAttributes() {
        return blockedAttributes;
    }

    public void setBlockedAttributes(List<String> blockedAttributes) {
        this.blockedAttributes = blockedAttributes;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getMgrs1km() {
        return mgrs1km;
    }

    public void setMgrs1km(String mgrs1km) {
        this.mgrs1km = mgrs1km;
    }

    public String getMgrs100m() {
        return mgrs100m;
    }

    public void setMgrs100m(String mgrs100m) {
        this.mgrs100m = mgrs100m;
    }

    public String getMgrs10m() {
        return mgrs10m;
    }

    public void setMgrs10m(String mgrs10m) {
        this.mgrs10m = mgrs10m;
    }

    public String getRawBidRequest() {
        return rawBidRequest;
    }

    public void setRawBidRequest(String rawBidRequest) {
        this.rawBidRequest = rawBidRequest;
    }

    public EventLog.BidEventType getBidDecisionResult() {
        return bidDecisionResult;
    }

    public void setBidDecisionResult(EventLog.BidEventType bidDecisionResultArg) {
        if (this.bidDecisionResult == EventLog.BidEventType.BID
                && bidDecisionResultArg == EventLog.BidEventType.NO_BID) {
            throw new IllegalStateException("cannot change a bid event to no bid");
        }

        if (bidDecisionResultArg == EventLog.BidEventType.NO_BID) {
            setStatus(ProcessingStatus.STOP_PROCESSING);
        } else if (bidDecisionResultArg == EventLog.BidEventType.BID && getStatus() == ProcessingStatus.STOP_PROCESSING) {
            LOGGER.warn("status is wrongly set to stop by modules : {}", getModulesStoppedProcessing());
            throw new IllegalStateException("status is wrongly set to stop by modules");
        }
        this.bidDecisionResult = bidDecisionResultArg;
    }

    public String getLastModuleRunInPipeline() {
        return lastModuleRunInPipeline;
    }

    public void setLastModuleRunInPipeline(String lastModuleRunInPipeline) {
        this.lastModuleRunInPipeline = lastModuleRunInPipeline;
    }

    public String getFinalCreativeContent() {
        return finalCreativeContent;
    }

    public void setFinalCreativeContent(String finalCreativeContent) {
        this.finalCreativeContent = finalCreativeContent;
    }

    public Map<Long, TargetGroup> getTargetGroupIdsToRemove() {
        return targetGroupIdsToRemove;
    }

    public TargetGroup getChosenTargetGroup() {
        return chosenTargetGroup;
    }

    public void setChosenTargetGroup(TargetGroup chosenTargetGroup) {
        this.chosenTargetGroup = chosenTargetGroup;
    }

    public Creative getChosenCreative() {
        return chosenCreative;
    }

    public void setChosenCreative(Creative chosenCreative) {
        this.chosenCreative = chosenCreative;
    }

    public ProcessingStatus getStatus() {
        return status;
    }

    public void setStatus(ProcessingStatus status) {
        this.status = status;
    }

    public int getSizeOfUnmarkedTargetGroups() {
            return allAvailableTgs.size() - targetGroupIdsToRemove.size();
    }

    public void addBlockedAdvertiser(String badv) {
        blockedAdvertiserMap.add(badv);
    }

    public void addBlockedIabCategory(String cat) {
        blockedIabCategoriesMap.add(cat);
    }

    public void addBlockedBannerType(String bannerAdType) {
        blockedBannerTypesMap.add(bannerAdType);
    }

    public void stopProcessing(String moduleName) {
        modulesStoppedProcessing.add(moduleName);
        status = ProcessingStatus.STOP_PROCESSING;
    }

    public List<String> getModulesStoppedProcessing() {
        return modulesStoppedProcessing;
    }

    public void setDeviceClass(DeviceClassValue deviceClass) {
        this.deviceClass = deviceClass;
    }

    public DeviceClassValue getDeviceClass() {
        return deviceClass;
    }

    public void setDeviceSegmentReadHappened(boolean deviceSegmentReadHappened) {
        this.deviceSegmentReadHappened = deviceSegmentReadHappened;
    }

    public boolean getDeviceSegmentReadHappened() {
        return deviceSegmentReadHappened;
    }

    public void setKeyWords(Set<String> keyWords) {
        this.keyWords = keyWords;
    }

    public Set<String> getKeyWords() {
        return keyWords;
    }

    public void setBidRequestBody(String bidRequestBody) {
        this.bidRequestBody = bidRequestBody;
    }

    public String getBidRequestBody() {
        return bidRequestBody;
    }
}

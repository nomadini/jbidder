package com.dstillery.bidder.modules;/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */

import java.util.concurrent.ConcurrentSkipListSet;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class TargetGroupWithoutSegmentFilterModule extends BidderModule {

    private final MetricReporterService metricReporterService;
    private ConcurrentSkipListSet<Long> targetGroupsWithoutSegments;

    public TargetGroupWithoutSegmentFilterModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.targetGroupsWithoutSegments = new ConcurrentSkipListSet<> ();
    }

    public void process(OpportunityContext context) {
        targetGroupsWithoutSegments.clear();//we clear the set before each data reload pipeline run
        markFilteredTgs(context);
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        if (tg.getRunOnAnySegment()) {
            metricReporterService.addStateModuleForEntity(
                    "RUN_ON_ANY_SEGMENT",
                    "TargetGroupWithoutSegmentFilterModule",
                     tg.getId());
            //we save these targetgroups so that they can pass the TargetGroupSegmentFilter
            targetGroupsWithoutSegments.add(tg.getId());
            return false;
        }

        if ((tg.getSegmentTargetBlueprint() == null
            || (tg.getSegmentTargetBlueprint().getInclude().getData().isEmpty() &&
                        tg.getSegmentTargetBlueprint().getExclude().getData().isEmpty()))) {

            filterTg = false;
            //we save these targetgroups so that they can pass the TargetGroupSegmentFilter
            targetGroupsWithoutSegments.add(tg.getId());
            metricReporterService.addStateModuleForEntity(
                    "NO_SEGMENT_TG_QUALIFIED",
                    "TargetGroupWithoutSegmentFilterModule",
                     tg.getId());
        }

        return filterTg;
    }

    public ConcurrentSkipListSet<Long> getTargetGroupsWithoutSegments() {
        return targetGroupsWithoutSegments;
    }
}


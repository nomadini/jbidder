package com.dstillery.bidder.modules;

import static com.dstillery.common.targetgroup.TargetingType.SEGMENTLESS_TARGETING;
import static com.dstillery.common.targetgroup.TargetingType.SEGMENT_TARGETING;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.LoadPercentageBasedSampler;
import com.dstillery.common.cache_realtime.RealTimeEntityCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetGroupCacheService;

public class TargetGroupSegmentFilter extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(TargetGroupSegmentFilter.class);

    private final MetricReporterService metricReporterService;
    private LoadPercentageBasedSampler segmentFetchThrottler;
    private TargetGroupWithoutSegmentFilterModule targetGroupWithoutSegmentFilterModule;
    //this service uses both aerospike and cassandra to read the segemnts as fast as it can
// it uses the aerospike as the caching layer
    private RealTimeEntityCacheService<DeviceSegmentHistory> realTimeEntityCacheServiceDeviceSegmentHistory;
    private DeviceIdTargetGroupWithSegmentProvider deviceIdTargetGroupWithSegmentProvider;

    public TargetGroupSegmentFilter(MetricReporterService metricReporterService,
                                    SegmentCacheService segmentCacheService,
                                    RealTimeEntityCacheService<DeviceSegmentHistory> realTimeEntityCacheServiceDeviceSegmentHistory,
                                    long ignoreSegmentsOlderThanXMilliSeconds,
                                    LoadPercentageBasedSampler segmentFetchThrottler,
                                    TargetGroupCacheService targetGroupCacheService,
                                    TargetGroupWithoutSegmentFilterModule targetGroupWithoutSegmentFilterModule) {
        this.metricReporterService = metricReporterService;

        this.segmentFetchThrottler = segmentFetchThrottler;

        this.targetGroupWithoutSegmentFilterModule = targetGroupWithoutSegmentFilterModule;

        deviceIdTargetGroupWithSegmentProvider = new DeviceIdTargetGroupWithSegmentProvider(
                targetGroupCacheService,
                segmentCacheService,
                metricReporterService,
                realTimeEntityCacheServiceDeviceSegmentHistory,
                ignoreSegmentsOlderThanXMilliSeconds);

    }

    /**
     *
     reads the uniqueNameOfSegment list for the device and filter out all the
     target groups that don't have the user segments, in case the target groups
     have assigned segments .
     */
    public void process(
            OpportunityContext context) throws IOException {

        if (segmentFetchThrottler.isPartOfSample()) {
            //this is a very expensive call that we want to avoid based on a sampler
            //there might be some targetgroups that are not doing segment targeting
            //and  dont get enough bids because of slowness of bid processing
            //as a direct result of this call
            context.setAllTgsWithCorrectSegmentsForDeviceAsMap(
                    deviceIdTargetGroupWithSegmentProvider.readSegmentsAndFetchAllTgsAttached(context));
        }

        markFilteredTgs(context);

    }

    public boolean filterTargetGroup(
            TargetGroup tg,
            OpportunityContext context) {
        boolean filterTg = true;

        if (tg.getRunOnAnySegment()) {
            metricReporterService.addStateModuleForEntity(
                    "PASS_TG_MATCH_RUN_ON_ANY_SEGMENT",
                    getClass().getSimpleName(),
                    "tg " + tg.getId());
            context.addTargetingTypeForTg(tg.getId(), SEGMENTLESS_TARGETING);
            return false;
        }

        if (tg.getSegmentTargetBlueprint().getInclude().getData().isEmpty() &&
                tg.getSegmentTargetBlueprint().getExclude().getData().isEmpty()) {
            LOGGER.debug("tg segmnets are not set up while run on any segment is true");
            return false;
        }
        if (context.getAllTgsWithCorrectSegmentsForDeviceAsMap().get(tg.getId()) != null) {

            metricReporterService.addStateModuleForEntity(
                    "PASS_TG_MATCH_DEVICE_SEGMENT",
                    getClass().getSimpleName(),
                    "tg " + tg.getId());
            context.addTargetingTypeForTg(tg.getId(), SEGMENT_TARGETING);
            filterTg = false;
        } else {

            //we check if tg belongs to segmentLess tgs and targeting type is segmentLess, we pass it
            if (targetGroupWithoutSegmentFilterModule.getTargetGroupsWithoutSegments().contains(tg.getId())) {
                filterTg = false;
                metricReporterService.addStateModuleForEntity(
                        "PASS_TG_HAS_NO_SEGMENT",
                        getClass().getSimpleName(),

                                tg.getId()
                );
                context.addTargetingTypeForTg(tg.getId(), SEGMENTLESS_TARGETING);
            }
        }

        return filterTg;
    }

}


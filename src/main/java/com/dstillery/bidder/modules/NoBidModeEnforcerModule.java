package com.dstillery.bidder.modules;/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.main.BidModeControllerService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.util.GUtil;

public class NoBidModeEnforcerModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(NoBidModeEnforcerModule.class);

    private BidModeControllerService bidModeControllerService;
    private final MetricReporterService metricReporterService;
    public NoBidModeEnforcerModule(
            BidModeControllerService bidModeControllerService,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.bidModeControllerService = bidModeControllerService;
    }

    public void process( OpportunityContext context) {
        if (!bidModeControllerService.getGlobalBidMode().getBidMode().get()) {
            if(GUtil.allowedToCall(100)) {
                LOGGER.warn("sending no bid as we are in NoBid mode : {}", bidModeControllerService.getGlobalBidMode());
            }
            metricReporterService.addStateModuleForEntity("NO_BID_BECAUSE_IN_NO_BIDE_MODE",
                    this.getClass().getSimpleName(),
                    "ALL");

            //TODO : change the line below to pass "NO_BID_MODE" only in case of test
            context.setFinalBidResponseContent("");
            context.setBidResponseStatusCode(HTTP_NO_CONTENT);

            // we send this text
            //so that we don't count these in the loadtester as a good valid bid
            context.stopProcessing(this.getClass().getSimpleName());
        }
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


package com.dstillery.bidder.modules;

import static com.dstillery.common.device.NomadiniDevice.UNMAPPED_USER;
import static com.dstillery.common.util.StringUtil.assertAndThrow;

import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.LoadPercentageBasedSampler;
import com.dstillery.common.eventlog.BidEventLog;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.eventlog.UniqueDeviceBidEventLog;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.fasterxml.jackson.core.JsonProcessingException;

/*
    records some percentage of all bids and of no bid events in cassandra bideventlog table.
    records some percentage of devices in daily_unique_device_bid_eventlog table in cassandra too.
 */
public class BidEventRecorderModule extends BidderModule {

    private LoadPercentageBasedSampler bidEventRecorderModuleSampler;
    private LoadPercentageBasedSampler noBidEventRecorderModuleSampler;
    private LoadPercentageBasedSampler uniqueDeviceBidEventRecorderModuleSampler;
    private CassandraService bidEventLogCassandraService;
    private CassandraService uniqueDeviceBidEventLogCassandraService;
    private EventLogCreatorModule eventLogCreatorModule;
    private final MetricReporterService metricReporterService;
    public BidEventRecorderModule(LoadPercentageBasedSampler bidEventRecorderModuleSampler,
                                  LoadPercentageBasedSampler noBidEventRecorderModuleSampler,
                                  LoadPercentageBasedSampler uniqueDeviceBidEventRecorderModuleSampler,
                                  CassandraService bidEventLogCassandraService,
                                  CassandraService uniqueDeviceBidEventLogCassandraService,
                                  EventLogCreatorModule eventLogCreatorModule,
                                  MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.bidEventRecorderModuleSampler = bidEventRecorderModuleSampler;
        this.noBidEventRecorderModuleSampler =  noBidEventRecorderModuleSampler;
        this.bidEventLogCassandraService = bidEventLogCassandraService;
        this.uniqueDeviceBidEventLogCassandraService = uniqueDeviceBidEventLogCassandraService;
        this.eventLogCreatorModule = eventLogCreatorModule;
        this.uniqueDeviceBidEventRecorderModuleSampler = uniqueDeviceBidEventRecorderModuleSampler;

    }

    public void process(OpportunityContext context) throws JsonProcessingException {

        if (context.getEventLog() == null) {
            context.setStrictEventLogChecking(false);
            context.setEventLog(eventLogCreatorModule.createEventLogFrom(context));
        }

        if (!context.getDevice().getDeviceId().equalsIgnoreCase(UNMAPPED_USER)) {
            recordBidEvent(context);
            recordNoBidEvent(context);
            recordUniqueDeviceBidEvent(context);
        }
    }

    private void recordUniqueDeviceBidEvent(OpportunityContext context) {
        boolean isInSample = uniqueDeviceBidEventRecorderModuleSampler.isPartOfSample();
        if (isInSample) {
            metricReporterService.addStateModuleForEntity("uniqueDevice_bid_writing_to_queue",
                    "BidEventRecorderModule",
                    "ALL");

            UniqueDeviceBidEventLog uniqueDeviceBidEventLog = new UniqueDeviceBidEventLog(
                    context.getEventLog());

            /*
                we don't truncate the date to DAYS anymore.
                we want to write multiple events per device per day
                so we can analyze it when creating the placeTag segments for example
             */
            uniqueDeviceBidEventLogCassandraService.write(uniqueDeviceBidEventLog);
        }
    }

    private void recordBidEvent(OpportunityContext context) {
        boolean isInSample = bidEventRecorderModuleSampler.isPartOfSample();
        if (isInSample) {
            if (context.getBidDecisionResult() == EventLog.BidEventType.BID) {

                metricReporterService.addStateModuleForEntity("bid_writing_to_queue",
                        "BidEventRecorderModule",
                        "ALL");

                assertAndThrow(context.getEventLog().getEventType() == EventLog.BidEventType.BID);
                assertAndThrow(context.getChosenTargetGroup().getId() > 0 && context.getChosenCreative().getId() > 0);
                BidEventLog bidEventLog = new BidEventLog(context.getEventLog());
                bidEventLogCassandraService.write(bidEventLog);
            }
        }
    }

    private void recordNoBidEvent(OpportunityContext context) {
        boolean isInSample = noBidEventRecorderModuleSampler.isPartOfSample();
        if (isInSample) {

            if (context.getBidDecisionResult() == EventLog.BidEventType.NO_BID) {

                metricReporterService.addStateModuleForEntity("no_bid_writing_to_queue",
                        "BidEventRecorderModule",
                        "ALL");
                if (context.getEventLog().getEventType() == EventLog.BidEventType.BID) {
                    throw new RuntimeException("eventLog for a no bid event is " + context.getEventLog());
                }

                BidEventLog bidEventLog = new BidEventLog(context.getEventLog());
                bidEventLogCassandraService.write(bidEventLog);
            }
        }
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        return false;
    }

}

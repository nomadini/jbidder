package com.dstillery.bidder.modules;

import com.dstillery.common.common_modules.DeviceHistoryUpdaterModule;
import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class GeoFeatureHistoryOfDeviceUpdaterModuleWrapper extends BidderModule {

    private FeatureType featureType;
    private DeviceHistoryUpdaterModule deviceGeoFeatureUpdaterModule;

    private final MetricReporterService metricReporterService;
    public GeoFeatureHistoryOfDeviceUpdaterModuleWrapper(
            FeatureType featureType,
            DeviceHistoryUpdaterModule deviceGeoFeatureUpdaterModule,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.featureType = featureType;
        this.deviceGeoFeatureUpdaterModule = deviceGeoFeatureUpdaterModule;
    }

    public void process(OpportunityContext context) {
        if (context.getDevice() == null || context.getMgrs100m().isEmpty()) {
            metricReporterService.addStateModuleForEntity("SKIPPING_UNKNOWN_USER_MGRS100",
                    "GeoFeatureHistoryOfDeviceUpdaterModuleWrapper",
                    "ALL");

            return;
        }
        try {
            deviceGeoFeatureUpdaterModule.process(featureType, context.getMgrs100m(), context.getDevice());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        metricReporterService.addStateModuleForEntity("ADDING_DEVICE_FEATURE_HISTORY",
                "GeoFeatureHistoryOfDeviceUpdaterModuleWrapper",
                "ALL");

    }

}


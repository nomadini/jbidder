package com.dstillery.bidder.modules.filters;

import static com.dstillery.common.targetgroup.AdPosition.ANY;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class AdPositionFilter extends TgMarker implements CacheUpdateWatcher {

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

    private final MetricReporterService metricReporterService;
    public AdPositionFilter(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        attributesToGoodTgs = new ConcurrentHashMap<> ();
    }

    public void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "AdPositionFilter cache cleared";
        attributesToGoodTgs.clear();
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;
        metricReporterService.addStateModuleForEntity(context.getAdPosition().name(),
                this.getClass().getSimpleName() + "-AdpositionsFromRequest",
                "ALL");
        if (tg.getAdPositions().size() == 0) {
            //user has no preference in ad positions , so we pass
            metricReporterService.addStateModuleForEntity("PASSED_FOR_NO_PREFERENCE",
                    this.getClass().getSimpleName(),

                    tg.getId());
            filterTg = false;

        }

        if (tg.getAdPositions().contains(ANY)) {
            metricReporterService.addStateModuleForEntity("PASSED_FOR_MATCHING_ANY",
                    this.getClass().getSimpleName(),

                    tg.getId());
            filterTg = false;

        } else {
            if (!tg.getAdPositions().contains(context.getAdPosition())) {
                metricReporterService.addStateModuleForEntity("FAILED",
                        this.getClass().getSimpleName(),

                        tg.getId());
                //LOGGER.debug("Ad position filter not matching the tg, filtered out, adPosistion from context"
//                        , context.adPosition , " , tg adpositions : "
//                        , JsonArrayUtil.convertListToJson (tg.getAdPositions().keySet());
            } else {
                metricReporterService.addStateModuleForEntity("PASSED_FOR_MATCHING_AD_POSITIONS",
                        this.getClass().getSimpleName(),

                        tg.getId());
                filterTg = false;
            }

        }

        return filterTg;
    }

}


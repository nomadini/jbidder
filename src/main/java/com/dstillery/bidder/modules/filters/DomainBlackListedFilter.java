package com.dstillery.bidder.modules.filters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.targetgroup_maps.tgBwlist.TargetGroupBWListCacheService;
import com.dstillery.common.util.StringUtil;

public class DomainBlackListedFilter extends TgMarker implements CacheUpdateWatcher {

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

    private final MetricReporterService metricReporterService;
    private TargetGroupBWListCacheService targetGroupBWListCacheService;

    public DomainBlackListedFilter(TargetGroupBWListCacheService targetGroupBWListCacheService,
                                   MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;


        this.targetGroupBWListCacheService = targetGroupBWListCacheService;

        attributesToGoodTgs = new ConcurrentHashMap<String, List<TargetGroup>> ();

    }
    public void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "DomainBlackListedFilter cache cleared";
        attributesToGoodTgs.clear();
    }

    void buildMapOfTgsBlockingThisDomain(OpportunityContext context) {
        ConcurrentHashMap<String, List<TargetGroup>> map = targetGroupBWListCacheService.getBlackListedDomainToListOfTargetGroups();
        context.setMapOfDomainBlockingTgs(new HashMap<>());

        List<TargetGroup> passingTargetGroups = new ArrayList<>();
        List<TargetGroup> tgVector = map.get(StringUtil.toLowerCase(context.getSiteDomain()));

        if (tgVector != null) {
            metricReporterService.addStateModuleForEntity("SOME_HAVE_BLACK_LISTS",
                    "DomainBlackListedFilter",
                    "someTargetGroups");

            for (TargetGroup value :tgVector) {
                context.getMapOfDomainBlockingTgs().put(value.getId(), null);
            }

        } else {

            metricReporterService.addStateModuleForEntity("NONE_HAVE_BLACK_LISTS",
                    "DomainBlackListedFilter",
                    "NoTargetGroups");
        }
    }

    String convertMapToJson(
            OpportunityContext context) {
        String allTgsIds = " blockingTgIds:";
        for (Entry<Long, TargetGroup> pair : context.getMapOfDomainBlockingTgs().entrySet()) {
            allTgsIds += pair.getKey();
        }
        return "blackListedDomain:" + context.getSiteDomain() + allTgsIds;
    }

    public boolean filterTargetGroup(TargetGroup tg,
                           OpportunityContext context) {
        boolean filterTg = true;
        if (context.getMapOfDomainBlockingTgs() == null) {
            buildMapOfTgsBlockingThisDomain(context);
        }

        //LOGGER.debug("considering tg ",tg.getDeviceId()," with mapOfDomainBlockingTgs : {} ", convertMapToJson(context);
        TargetGroup pairPtr = context.getMapOfDomainBlockingTgs().get(tg.getId());
        if (pairPtr != null) {

            //LOGGER.debug("targetgroup id : " , tg.getDeviceId () ,
//                    "has blacklist.",
//                    " disqualified siteDomain '", context.getSiteDomain(),"'";
            metricReporterService.addStateModuleForEntity("FAILED_FOR_BLACK_LIST",
                    "DomainBlackListedFilter",

                    tg.getId());

        } else {
            //LOGGER.debug("targetgroup id : " , tg.getDeviceId () ,
//                    "has no blacklist.",
//                    " qualifying siteDomain '", context.getSiteDomain(),"'";
            metricReporterService.addStateModuleForEntity("PASSED",
                    "DomainBlackListedFilter",

                    tg.getId());

            filterTg = false;
        }
        return filterTg;
    }
}


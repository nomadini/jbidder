package com.dstillery.bidder.modules.filters;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.targetgroup_maps.tgBwlist.TargetGroupBWListCacheService;
import com.dstillery.common.util.StringUtil;

public class DomainWhiteListedFilter extends TgMarker implements CacheUpdateWatcher {

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

    private TargetGroupBWListCacheService targetGroupBWListCacheService;

    private final MetricReporterService metricReporterService;

    public DomainWhiteListedFilter(TargetGroupBWListCacheService targetGroupBWListCacheService,
                                   MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.targetGroupBWListCacheService = targetGroupBWListCacheService;
        attributesToGoodTgs = new ConcurrentHashMap<> ();

    }

    public void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "DomainWhiteListedFilter cache cleared";
        attributesToGoodTgs.clear();
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        ConcurrentHashMap<Long, ConcurrentHashMap<String, Long>> map = targetGroupBWListCacheService.getTargetGroupIdToWhiteListsMap();

        boolean filterTg = true;
        //this is map of target gruop ids to map of whitelisted domains
        ConcurrentHashMap<String, Long> domainMapContainer = map.get(tg.getId());
        if ( domainMapContainer != null) {
            //LOGGER.debug("targetgroup id : " , tg.getDeviceId () ,
//                    "has whitelist in the map ",
//                    " cosidering siteDomain '", context.getSiteDomain(),"'";
            ConcurrentHashMap<String, Long> domainMap = domainMapContainer;
            if (domainMap.keySet().contains(StringUtil.toLowerCase (context.getSiteDomain()))) {
                //LOGGER.debug("targetgroup id : " , tg.getDeviceId () ,
//                        "qualified for siteDomain '", context.getSiteDomain(),"'";
                metricReporterService.addStateModuleForEntity("PASSED",
                        "DomainWhiteListedFilter",

                        tg.getId());

                filterTg = false;
            } else {
                //LOGGER.debug("targetgroup id : " , tg.getDeviceId () ,
//                        "disqualified for siteDomain '", context.getSiteDomain(),"'";
                metricReporterService.addStateModuleForEntity("FAILED",
                        "DomainWhiteListedFilter",

                        tg.getId());

            }

        } else {
            //LOGGER.debug("targetgroup id : " , tg.getDeviceId () ,
//                    "qualified for siteDomain '", context.getSiteDomain(),"' for not having whitelist";

            metricReporterService.addStateModuleForEntity("PASSED_FOR_NO_WHITE_LIST",
                    "DomainWhiteListedFilter",

                    tg.getId());

            filterTg = false;
        }

        return filterTg;
    }
}


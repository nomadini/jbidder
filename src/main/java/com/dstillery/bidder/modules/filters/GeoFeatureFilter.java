package com.dstillery.bidder.modules.filters;/*
  GeoFeatureFilter.h
 *
   Created on: Sep 6, 2015
       Author: mtaabodi
 */

import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.metric.dropwizard.MetricReporterService.MetricPriority.WARNING;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.contexts.BiddingMonitor;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.LoadPercentageBasedSampler;
import com.dstillery.common.PlacesCachedResult;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.geo.places.Place;
import com.dstillery.common.geo.places.PlaceFinderModule;
import com.dstillery.common.geo.places.Tag;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GeoFeatureFilter extends TgMarker {
    private static final Logger LOGGER = LoggerFactory.getLogger(GeoFeatureFilter.class);

    public static final ObjectMapper MAPPER = new ObjectMapper();
    private BiddingMonitor monitor;
    private LoadPercentageBasedSampler geoFeatureFilterRemoteCallThrottler;

    private AeroCacheService<PlacesCachedResult> aerospikePlaceCache;
    private String placeFinderAppHostUrl;
    private PlaceFinderModule placeFinderModule;

    private final MetricReporterService metricReporterService;
    public GeoFeatureFilter(
            BiddingMonitor monitor,
            LoadPercentageBasedSampler geoFeatureFilterRemoteCallThrottler,
            AeroCacheService<PlacesCachedResult> aerospikePlaceCache,
            ConfigService configService,
            PlaceFinderModule placeFinderModule,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.monitor = monitor;
        this.geoFeatureFilterRemoteCallThrottler = geoFeatureFilterRemoteCallThrottler;
        this.aerospikePlaceCache = aerospikePlaceCache;
    }

    private void fetchPlacesAndTags(OpportunityContext context) {

        int maxNumberOfWantedFeatures = 5;

        context.setAllPlaces(getPlacesFromCache(
                context.getMgrs10m(),
                context.getDeviceLat(),
                context.getDeviceLon(),
                maxNumberOfWantedFeatures));
        //we set this to true, in order no to repeat this call in AsyncPipelineFeederAndInvokerModule
        //and only call this based on throttler, because this is an expensive call
        context.setFetchingPlacesFromRemoteExecuted(true);
        context.setTagIds(getTagIdsOfFeatures(context.getAllPlaces()));

    }

    List<TargetGroup> filterTargetGroups(OpportunityContext context) {

        if (!context.isFetchingPlacesFromRemoteExecuted()) {
            if (geoFeatureFilterRemoteCallThrottler.isPartOfSample()) {
                //this is a very expensive call that we want to avoid based on a sampler
                fetchPlacesAndTags(context);
            }
        }
        markFilteredTgs(context);
        return new ArrayList<>();//TODO
    }

    public boolean filterTargetGroup(TargetGroup tg,
                                     OpportunityContext context) {
        return false;
    }

    private List<Place> getPlacesFromCache(
            String mgrs10m,
            double deviceLat,
            double deviceLon,
            int maxNumberOfWantedFeatures) {

        PlacesCachedResult cacheKey = new PlacesCachedResult(mgrs10m, maxNumberOfWantedFeatures);

        Optional<PlacesCachedResult> inMemoryValue = aerospikePlaceCache.readDataOptionalAsObject(cacheKey);
        if (inMemoryValue.isPresent()) {
            metricReporterService.
                    addStateModuleForEntity("IN_MEMORY_CACHE_HIT",
                            "GeoFeatureFilter",
                            "ALL",
                            MetricReporterService.MetricPriority.IMPORTANT);

            return inMemoryValue.get().getPlaces();
        }

        List<Place> allPlaces = new ArrayList<>();
        try {
            monitor.getNumberOfTotal().getAndIncrement();
            allPlaces = placeFinderModule.findNearestFeatures(
                    deviceLat,
                    deviceLon,
                    maxNumberOfWantedFeatures,
                    1
            );
            metricReporterService.
                    addStateModuleForEntity("IN_MEMORY_CACHE_MISS",
                            "GeoFeatureFilter",
                            "ALL");
            if (!allPlaces.isEmpty()) {
                cacheKey.setPlaces(allPlaces);
                aerospikePlaceCache.putDataInCache(cacheKey);
            }
        } catch(Exception e) {
            metricReporterService.addStateModuleForEntity("EXCEPTION_IN_PLACES_FETCH",
                    "GeoFeatureFilter",
                    ALL_ENTITIES,
                    WARNING);

//            diagnosticInformation = e);
//            //LOGGER.debug("Got the " , google.COUNTER , "th exception "
//        ," while handling request : {} ", diagnosticInformation;

            monitor.getNumberOfFailures().getAndIncrement();
        }

        return allPlaces;
    }

    private List<Long> getTagIdsOfFeatures(
            List<Place> allPlaces) {
        Set<Long> tagIdsSet = new HashSet<>();
        for (Place place : allPlaces) {
            List<Long> tagIds = getTagsOfPlace(place);
            tagIdsSet.addAll(tagIds);
        }

        return new ArrayList<>(tagIdsSet);
    }

    private List<Long> getTagsOfPlace(Place place) {
        List<Long> tagIds = new ArrayList<>();
        for (Tag tag : place.getTags()) {
            tagIds.add(tag.getId());
        }

        return tagIds;
    }

    public BiddingMonitor getMonitor() {
        return monitor;
    }
}

package com.dstillery.bidder.modules.filters.creativefilters;

import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.creative.Creative;
import com.dstillery.common.creative.CreativeCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.targetgroup_maps.tgCreative.TargetGroupCreativeCacheService;

public class CreativeContentTypeFilter extends TgMarker {

    private TargetGroupCreativeCacheService targetGroupCreativeCacheService;
    private CreativeCacheService creativeCacheService;

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

    private final MetricReporterService metricReporterService;
    public CreativeContentTypeFilter(TargetGroupCreativeCacheService targetGroupCreativeCacheService,
                                     CreativeCacheService creativeCacheService,
                                     MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.targetGroupCreativeCacheService = targetGroupCreativeCacheService;
        this.creativeCacheService = creativeCacheService;
        this.attributesToGoodTgs = new ConcurrentHashMap<>();
    }

    public static boolean doesCreativeHaveRightContentType(
            OpportunityContext context,
            Creative crt) {

        return context.getCreativeContentType() == crt.getCreativeContentType();

    }

    private boolean findAtLeastOneCreativeWithMatchingContentType(ConcurrentHashMap<Long, Creative> allCreativeListForThisTargetGroup,
                                                                  TargetGroup tg,
                                                                  String protocolVersion,
                                                                  OpportunityContext context) {

        for (Entry<Long, Creative> crtIdPair : allCreativeListForThisTargetGroup.entrySet()) {
            Creative crt = crtIdPair.getValue();
            if (doesCreativeHaveRightContentType(context, crt)) {
                metricReporterService.addStateModuleForEntity("PASSED",
                        "CreativeContentTypeFilter",
                        BlockedBannerAdTypeFilter.createElementId (tg.getId(),
                        crt.getId()));

                return true;
            } else {
                metricReporterService.addStateModuleForEntity("FAILED",
                        "CreativeContentTypeFilter",
                        BlockedBannerAdTypeFilter.createElementId (tg.getId(),
                        crt.getId()));
            }
        }

        return false;
    }

    @Override
    public boolean filterTargetGroup(TargetGroup tg,
                                     OpportunityContext context) {

        boolean filterTg = true;

        ConcurrentHashMap<Long, Creative> tgCreativeEntry =
                targetGroupCreativeCacheService.getAllTargetGroupCreativesMap().get(tg.getId());

        if (tgCreativeEntry != null) {

            ConcurrentHashMap<Long, Creative> creativeMap = tgCreativeEntry;
            if (creativeMap.isEmpty()) {
                metricReporterService.addStateModuleForEntity("FAILED_FOR_ZERO_CREATIVES_IN_MAP",
                        "CreativeContentTypeFilter",
                        "tg"  + tg.getId());

            }
            if (findAtLeastOneCreativeWithMatchingContentType (creativeMap,
                    tg,
                    context.getProtocolVersion(),
                    context)) {
                filterTg = false;
            }
        } else {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_NO_CREATIVES",
                    "CreativeContentTypeFilter",

                            tg.getId());
        }

        return filterTg;
    }
}

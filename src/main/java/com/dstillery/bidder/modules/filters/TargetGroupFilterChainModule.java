package com.dstillery.bidder.modules.filters;

import static com.dstillery.common.util.StringUtil.equalsIgnoreCase;
import static com.dstillery.common.util.StringUtil.toStr;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.codahale.metrics.Timer;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.bidder.modules.filters.creativefilters.BlockedBannerAdTypeFilter;
import com.dstillery.bidder.modules.filters.creativefilters.BlockedCreativeAttributeFilter;
import com.dstillery.bidder.modules.filters.creativefilters.CreativeBannerApiFilter;
import com.dstillery.bidder.modules.filters.creativefilters.CreativeContentTypeFilter;
import com.dstillery.bidder.modules.filters.creativefilters.CreativeSizeFilter;
import com.dstillery.bidder.pipeline.BidderMainPipelineProcessor;
import com.dstillery.common.config.ConfigService;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class TargetGroupFilterChainModule extends BidderModule {

    private final MetricReporterService metricReporterService;
    private TgMarker geoFeatureFilter;
    private TgMarker matchingIabCategoryFilter;
    private TgMarker domainBlackListedFilter;
    private TgMarker domainWhiteListedFilter;
    private TgMarker creativeBannerApiFilter;

    private TgMarker adPositionFilter;
    private TgMarker blockedAdvertiserDomainFilter;
    private TgMarker blockedIabCategoryFilter;
    private TgMarker creativeSizeFilter;

    private TgMarker creativeContentTypeFilter;
    private TgMarker blockedBannerAdTypeFilter;
    private TgMarker blockedCreativeAttributeFilter;
    private TgMarker deviceTypeFilter;
    private TgMarker osTypeFilter;
    private TgMarker geoLocationFilter;
    private TgMarker topMgrsFilter;

    private AtomicBoolean filtersAreReady;
    private ConfigService configService;

    private List<TgMarker> orderedListOfFiltersToRun;
    private String originalName;

    public TargetGroupFilterChainModule(
            MetricReporterService metricReporterService,
            GeoFeatureFilter geoFeatureFilter,
            DomainBlackListedFilter domainBlackListedFilter,
            MatchingIabCategoryFilter matchingIabCategoryFilter,
            DomainWhiteListedFilter domainWhiteListedFilter,
            CreativeBannerApiFilter creativeBannerApiFilter,
            CreativeContentTypeFilter creativeContentTypeFilter,
            AdPositionFilter adPositionFilter,
            BlockedIabCategoryFilter blockedIabCategoryFilter,
            CreativeSizeFilter creativeSizeFilter,
            BlockedBannerAdTypeFilter blockedBannerAdTypeFilter,
            BlockedCreativeAttributeFilter blockedCreativeAttributeFilter,
            DeviceTypeFilter deviceTypeFilter,
            OsTypeFilter osTypeFilter,
            GeoLocationFilter geoLocationFilter,
            BlockedAdvertiserDomainFilter blockedAdvertiserDomainFilter,
            ConfigService configService) {


        this.configService = configService;
        this.blockedAdvertiserDomainFilter = blockedAdvertiserDomainFilter;
        this.domainBlackListedFilter = domainBlackListedFilter;
        this.matchingIabCategoryFilter=  matchingIabCategoryFilter;
        this.domainWhiteListedFilter = domainWhiteListedFilter;
        this.creativeBannerApiFilter = creativeBannerApiFilter;
        this.creativeContentTypeFilter=  creativeContentTypeFilter;
        this.adPositionFilter = adPositionFilter;
        this.blockedIabCategoryFilter = blockedIabCategoryFilter;
        this.creativeSizeFilter = creativeSizeFilter;
        this.blockedBannerAdTypeFilter = blockedBannerAdTypeFilter;
        this.blockedCreativeAttributeFilter = blockedCreativeAttributeFilter;
        this.deviceTypeFilter = deviceTypeFilter;
        this.osTypeFilter = osTypeFilter;
        this.geoLocationFilter = geoLocationFilter;
        this.metricReporterService = metricReporterService;
        this.geoFeatureFilter = geoFeatureFilter;

        orderedListOfFiltersToRun = new ArrayList<>();

        originalName = this.getClass().getSimpleName();
        filtersAreReady = new AtomicBoolean(false);

    }
    void addFilters() {

        //LOGGER.debug("initializing the filter map";
        //we start by adding the cheapest filters operationally
        initFilter(matchingIabCategoryFilter);
        initFilter(blockedAdvertiserDomainFilter);
        initFilter(blockedIabCategoryFilter);
        initFilter(adPositionFilter);
        initFilter(creativeSizeFilter);
        initFilter(blockedBannerAdTypeFilter);
        initFilter(blockedCreativeAttributeFilter);
        initFilter(deviceTypeFilter);
        initFilter(osTypeFilter);

        initFilter(domainBlackListedFilter);
        initFilter(domainWhiteListedFilter);
        initFilter(creativeBannerApiFilter);
        initFilter(creativeContentTypeFilter);

        initFilter (geoLocationFilter);
        initFilter (topMgrsFilter);
        initFilter(geoFeatureFilter);
        filtersAreReady.set(true);
    }

    private void initFilter(TgMarker filter) {
        String moduleName = filter.getClass().getSimpleName();
        if (configService.contains(moduleName + "IsEnabled") &&
                equalsIgnoreCase(configService.get(moduleName + "IsEnabled"), "false")) {
            BidderMainPipelineProcessor.logExclusion(moduleName, metricReporterService);
        } else {
            this.orderedListOfFiltersToRun.add(filter);
        }
    }

    public void process(OpportunityContext context) {
        if (!filtersAreReady.get()) {
            metricReporterService.addStateModuleForEntity("FILTERS_ARE_NOT_READY",
                    this.getClass().getSimpleName(),
                    "ALL");

            return;
        }
        metricReporterService.addStateModuleForEntity(
                "sizeOfTargetGroup_StartOfFilterChain"  + toStr(context.getSizeOfUnmarkedTargetGroups()),
                this.getClass().getSimpleName(),
                "ALL");
        //make all these filters use tgs in the context. you don't need to pass any tgs around
        for (TgMarker filter : orderedListOfFiltersToRun) {

            //I am adding try catch only around filterTargetGroups method invocation since I don't want
            //any exception thrown in that function , winds up the stack and I dont get the correct lastFilterName
            try {

                int beforeTgSize = context.getSizeOfUnmarkedTargetGroups();
                if (beforeTgSize > 0) {

                    runFilter(filter,
                            context,
                            metricReporterService);

                    int afterTgSize = context.getSizeOfUnmarkedTargetGroups();

                    int filterPercentage = (beforeTgSize - afterTgSize) * 100 / beforeTgSize;
                    metricReporterService.addStateModuleForEntityWithValue((filter).getClass().getSimpleName(),
                            filterPercentage,
                            "TargetGroupFilterChainModule-PercentOfFilteringTg",
                            "ALL");

                    //LOG_EVERY_N(ERROR, 10000), google.COUNTER,"th percenteOfFilteringTg for filter ", filter.getClass().getSimpleName(), " is ", filterPercentage;
                    if (afterTgSize == 0) {
                        metricReporterService.addStateModuleForEntity(
                                (filter).getClass().getSimpleName(),
                                "TargetGroupFilterChainModule-LastChainFilter",
                                "ALL"
                        );
                    }
                    //LOGGER.debug("percenteOfFilteringTg for filter ", filter.getClass().getSimpleName(), " is ", filterPercentage;
                }
            } catch (RuntimeException e) {
                metricReporterService.addStateModuleForEntity("Exception : " + toStr(e.getMessage()),
                        this.getClass().getSimpleName(),
                        "ALL");
            }
            if (context.getSizeOfUnmarkedTargetGroups() == 0) {
                break;
            }
        }

        //http://69.200.247.201:9980/pages/filtermap
        //printFilterNameToTargetGroupCounterMap();
        //this line should be commented out later, this line is for disabling the target group filter!

        if (context.getSizeOfUnmarkedTargetGroups() == 0) {
            metricReporterService.addStateModuleForEntity("AllTargetGroupsWereFiltered",
                    this.getClass().getSimpleName(),
                    "ALL");

            context.stopProcessing(this.getClass().getSimpleName());
        }

    }

    static void runFilter(TgMarker filter,
                          OpportunityContext context,
                          MetricReporterService metricReporterService) {

        final Timer.Context timerContext = metricReporterService.getTimerContext(filter.getClass().getSimpleName());
        try {
            filter.beforeMarking(context);
            filter.markFilteredTgs(context);
            filter.afterMarking(context);
        } finally {
            timerContext.stop();
        }
    }

    String getName() {
        return originalName;
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

/**************************************/
/**************************************/
/*********** helper methods***********/
/**************************************/
/**************************************/

//<bean id="evalTargetFilterChain" class="org.apache.commons.chain.impl.ChainBase">
//	<constructor-arg type="java.util.Collection">
//		<!-- order by filter % - heavy-duty filtering first -.
//		<list>
//			<ref bean="filterBySpendSegmentTargetCommand" />
//			<!-- geo filters a lot -.
//			<ref bean="filterBySpendGeoTargetCommand" />
//			<ref bean="filterByCreativeGeoTargetCommand" />
//
//			<!-- pacing filters need to run after spend-segment & geo (as per DS) -.
//			<ref bean="filterByCampaignPacingTargetCommand" />
//			<ref bean="filterBySpendPacingTargetCommand" />
//			<ref bean="filterByCreativePacingTargetCommand" />
//
//			<ref bean="filterByATExclusionCommand" />
//			<ref bean="filterByHostIdsTargetCommand" />
//            <ref bean="filterByAppIdsTargetCommand" />
//			<ref bean="filterByInventoryMarketerTargetCommand" />
//			<ref bean="filterBySpendDeviceClassTargetCommand" />
//			<ref bean="filterBySpendEnvTypeTargetCommand" />
//
//			<ref bean="filterByBmsExtCommand" />
//			<ref bean="filterByBmsIntCommand" />
//
//			<ref bean="filterByCreativeVideoDataCommand" />
//			<ref bean="filterBySpendSectionTargetCommand" />
//			<ref bean="filterSpendByRemoteAgencyCommand"/>
//			<ref bean="filterBySpendInventoryTargetCommand" />
//			<ref bean="filterBySpendDayPartCommand" />
//			<ref bean="filterByDelayFreqRecencyTargetsCommand" />
//			<ref bean="filterByIndustryCategoriesCommand" />
//			<ref bean="filterByIabCategoriesCommand" />
//			<ref bean="filterByTopLevelDomainCommand" />
//			<ref bean="filterByTechSpecTargetCommand" />
//			<ref bean="filterBySpendAdPositionTargetCommand" />
//			<ref bean="filterBySpendPublisherCategoryTargetCommand" />
//			<ref bean="filterByCreativeRemoteStatusCommand" />
//			<ref bean="filterByCreativeVendorCommand" />
//			<ref bean="filterByCreativeAdAttributeCommand" />
//			<ref bean="filterInventoryByEnvTypeCommand" />

}

package com.dstillery.bidder.modules.filters.creativefilters;

import static com.dstillery.common.util.StringUtil.equalsIgnoreCase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.bidder.modules.filters.CacheUpdateWatcher;
import com.dstillery.common.creative.Creative;
import com.dstillery.common.creative.CreativeCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.targetgroup_maps.tgCreative.TargetGroupCreativeCacheService;

public class CreativeSizeFilter extends TgMarker implements CacheUpdateWatcher {

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

    private CreativeCacheService creativeCacheService;
    private TargetGroupCreativeCacheService targetGroupCreativeCacheService;

    private final MetricReporterService metricReporterService;
    public CreativeSizeFilter(TargetGroupCreativeCacheService targetGroupCreativeCacheService,
                              CreativeCacheService creativeCacheService,
                              MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.targetGroupCreativeCacheService = targetGroupCreativeCacheService;
        this.creativeCacheService = creativeCacheService;
        attributesToGoodTgs = new ConcurrentHashMap<> ();

    }

    public void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "BlockedCreativeAttributeFilter cache cleared";
        attributesToGoodTgs.clear();
    }

    boolean rightSizeCreativeExist(String requstedCreativeSize,
                                   List<Creative> crtList) {
        for (Creative crt : crtList) {
            if (equalsIgnoreCase (crt.getSize(), requstedCreativeSize)) {
                return true;
            } else {
                // //MLOG (10) , "requstedCreativeSize " , requstedCreativeSize , " vs crtSize " , crt.getSize();
            }

        }

        return false;
    }

    public List<Creative> getCreativesAssignedToTg(Long targetGroupId) {

        ConcurrentHashMap<Long, Creative> tgCreativeEntry = targetGroupCreativeCacheService
                .getAllTargetGroupCreativesMap()
                .get(targetGroupId);
        List<Creative> allAssignedCreatives = new ArrayList<>();
        if (tgCreativeEntry != null) {

            for (Entry<Long, Creative> entity : tgCreativeEntry.entrySet()) {
                Long crtId = entity.getKey();
                try {
                    Creative crt = creativeCacheService.findByEntityId (crtId);
                    allAssignedCreatives.add(crt);
                } catch (RuntimeException e) {

                    metricReporterService.addStateModuleForEntity("ERROR_NO_CREATIVE_IN_MAP_FOR_ID",
                            "CreativeSizeFilter",
                            "crt" +
                            crtId);
                }
            }
        }
        return allAssignedCreatives;
    }

    public static boolean hasCreativeTheRequestedSize(OpportunityContext context,
                                                      Creative crt) {
        return equalsIgnoreCase(crt.getSize(), context.getAdSize());

    }

    @Override
    protected boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;
        List<Creative> creatives = getCreativesAssignedToTg (tg.getId());
        if (creatives.size() == 0) {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_NO_CREATIVES",
                    "CreativeSizeFilter",

                    tg.getId());
            return filterTg;
        }
        if (rightSizeCreativeExist (context.getAdSize(), creatives)) {
            filterTg = false;

            metricReporterService.addStateModuleForEntity("PASSED",
                    "CreativeSizeFilter",

                    tg.getId());

        } else {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_MISMATCHING_SIZE",
                    "CreativeSizeFilter",

                    tg.getId());
        }

        return filterTg;
    }
}


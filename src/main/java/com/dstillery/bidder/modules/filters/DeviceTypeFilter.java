package com.dstillery.bidder.modules.filters;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class DeviceTypeFilter extends TgMarker implements CacheUpdateWatcher {

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

    private final MetricReporterService metricReporterService;
    public DeviceTypeFilter(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        attributesToGoodTgs = new ConcurrentHashMap<> ();

    }

    public void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "DeviceTypeFilter cache cleared";
        attributesToGoodTgs.clear();
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;
        assertAndThrow(tg!= null);

        metricReporterService.addStateModuleForEntity(context.getDevice().getDeviceType().getTypeValue(),
                getClass().getSimpleName() + "-DeviceTypeFromRequests",
                "ALL");

        DeviceClassValue bidDeviceClass = DeviceClassValue.UNKNOWN;
        DeviceTypeValue deviceType = context.getDevice().getDeviceType();
        if (deviceType == DeviceTypeValue.DISPLAY) {
            bidDeviceClass = DeviceClassValue.DESKTOP;
        } else if (deviceType == DeviceTypeValue.G_IDFA || deviceType == DeviceTypeValue.IDFA) {
            bidDeviceClass = DeviceClassValue.SMART_PHONE;
        }

        boolean typePairPtr = tg.getDeviceTypeTargets().contains(bidDeviceClass);
        if (typePairPtr) {
            metricReporterService.addStateModuleForEntity("PASSED_FOR_MATCHING_DEVICE_TYPE",
                    getClass().getSimpleName(),

                            tg.getId());

            filterTg = false;
        } else {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_UNMATCHING_DEVICE_TYPE",
                    getClass().getSimpleName(),

                            tg.getId());

        }
        return filterTg;
    }
}


package com.dstillery.bidder.modules.filters.creativefilters;

import static com.dstillery.common.util.StringUtil.toLowerCase;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.creative.Creative;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class BlockedCreativeAttributeFilter extends TgMarker {

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;
    private CreativeSizeFilter creativeSizeFilter;

    private final MetricReporterService metricReporterService;
    public BlockedCreativeAttributeFilter(CreativeSizeFilter creativeSizeFilter,
                                          MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

            this.creativeSizeFilter = creativeSizeFilter;
        attributesToGoodTgs = new ConcurrentHashMap<> ();
    }

    public static boolean isCreativeBlockedByAttributeInContext(
            Creative crt, OpportunityContext context) {
        boolean blockCreative = false;
        for (String contextAttribute : context.getBlockedAttributes()) {
            String attributesLowerCased = toLowerCase(contextAttribute);
            Boolean pair = crt.getAttributes().contains(attributesLowerCased);
            if (pair) {
                //found a blocking attribute
                blockCreative = true;
                //LOGGER.debug("crt id : " , crt.getDeviceId() ,
//                        " is blocked by bid request, skipping, commonAttributes : "
//                        , attributesLowerCased;
                break;
            }
        }
        return blockCreative;
    }

    void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "BlockedCreativeAttributeFilter cache cleared";
        attributesToGoodTgs.clear();
    }

    public boolean filterTargetGroup(TargetGroup tg,
                           OpportunityContext context) {
        boolean filterTg = true;

        List<Creative> allCreatives = creativeSizeFilter.getCreativesAssignedToTg (tg.getId());
        //LOGGER.debug("number of creatives : " , allCreatives.size() , " for chosen tg id : " , tg.getDeviceId();

        if (allCreatives.isEmpty()) {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_NO_CREATIVES",
                    "BlockedCreativeAttributeFilter",

                    tg.getId());
        }
        for (Creative crt : allCreatives) {

            String stateId = BlockedBannerAdTypeFilter.createElementId(tg.getId(), crt.getId());

            //LOGGER.debug("stateId is : {} ",  stateId;
             if (isCreativeBlockedByAttributeInContext(crt, context)) {

                metricReporterService.addStateModuleForEntity("FAILED",
                        "BlockedCreativeAttributeFilter",
                        stateId);

                continue;
            } else {
                metricReporterService.addStateModuleForEntity("PASSED",
                        "BlockedCreativeAttributeFilter",
                        stateId);
                filterTg = false;
            }
        }

        return filterTg;
    }
}

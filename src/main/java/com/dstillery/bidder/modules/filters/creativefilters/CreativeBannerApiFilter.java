package com.dstillery.bidder.modules.filters.creativefilters;

import static com.dstillery.common.util.StringUtil.equalsIgnoreCase;

import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.creative.Creative;
import com.dstillery.common.creative.CreativeApi;
import com.dstillery.common.creative.CreativeCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.targetgroup_maps.tgCreative.TargetGroupCreativeCacheService;

public class CreativeBannerApiFilter extends TgMarker {

    private TargetGroupCreativeCacheService targetGroupCreativeCacheService;
    private CreativeCacheService creativeCacheService;

    private final MetricReporterService metricReporterService;
    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

/*From OpenRtb 2.3
   /List of supported API frameworks for this impression.
   /Refer to List 5.6. If an API is not explicitly listed, it is assumed not to be supported.
   5.6 API Frameworks
   The following table is a list of API frameworks supported by the publisher. Note that MRAID-1 is a subset of MRAID-2. In OpenRTB 2.1 and prior, value “3” was “MRAID”. However, not all MRAID capable APIs understand MRAID-2 features and as such the only safe interpretation of value “3” is MRAID-1. In OpenRTB 2.2, this was made explicit and MRAID-2 has been added as value “5”.
   Value Description
   1       VPAID 1.0
   2       VPAID 2.0
   3       MRAID-1
   4       ORMMA
   5       MRAID-2
 */

    public CreativeBannerApiFilter(TargetGroupCreativeCacheService targetGroupCreativeCacheService,
                                   CreativeCacheService creativeCacheService,
                                   MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.targetGroupCreativeCacheService = targetGroupCreativeCacheService;
        this.creativeCacheService = creativeCacheService;
        attributesToGoodTgs = new ConcurrentHashMap<> ();

    }
    void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "CreativeBannerApiFilter cache cleared";
        attributesToGoodTgs.clear();
    }

    public static boolean doesCreativeHaveRightApi(
            OpportunityContext context,
            Creative crt) {

        if (equalsIgnoreCase(context.getImpressionType(), "banner" )) {
            //request that come from banner . AKA desktop requests, don't need to have an pi
            //TODO : this is important, test it and have metrics for it
            return true;
        }
        for (CreativeApi apiFromRequest : context.getCreativeAPI()) {
            if (crt.getApis().contains(apiFromRequest)) {
                return true;
            }
        }
        return false;
    }

    private boolean findAtLeastOneCreativeWithMatchingApi(ConcurrentHashMap<Long, Creative> allCreativeListForThisTargetGroup,
                                                          TargetGroup tg,
                                                          String protocolVersion,
                                                          OpportunityContext context) {

        for (Entry<Long, Creative> crtIdPair : allCreativeListForThisTargetGroup.entrySet()) {
            Creative crt = crtIdPair.getValue();
            if (doesCreativeHaveRightApi(context, crt)) {
                metricReporterService.addStateModuleForEntity("PASSED",
                        "CreativeBannerApiFilter",
                        BlockedBannerAdTypeFilter.createElementId (tg.getId(),
                                crt.getId()));

                return true;
            } else {
                metricReporterService.addStateModuleForEntity("FAILED",
                        "CreativeBannerApiFilter",
                        BlockedBannerAdTypeFilter.createElementId (tg.getId(),
                                crt.getId()));
            }
        }

        return false;
    }
    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {

        if (context.getUniqueNumberOfContext() % 100 == 1) {
            //we do this every 100 times
            for (CreativeApi creativeApi : context.getCreativeAPI()) {
                metricReporterService.addStateModuleForEntity(creativeApi.name(),
                        "CreativeBannerApiFilter-DataFromRequests"
                        ,"ALL");
            }
        }

        boolean filterTg = true;

        ConcurrentHashMap<Long, Creative> creativeMap = targetGroupCreativeCacheService.
                getAllTargetGroupCreativesMap().get(tg.getId());

        if (creativeMap != null) {
            if (creativeMap.isEmpty()) {
                metricReporterService.addStateModuleForEntity("FAILED_FOR_ZERO_CREATIVES_IN_MAP",
                        "CreativeBannerApiFilter",

                                tg.getId());

            }
            if (findAtLeastOneCreativeWithMatchingApi (creativeMap,
                    tg,
                    context.getProtocolVersion(),
                    context)) {
                filterTg = false;
            }
        } else {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_NO_CREATIVES",
                    "CreativeBannerApiFilter",

                            tg.getId());
        }

        return filterTg;
    }
}

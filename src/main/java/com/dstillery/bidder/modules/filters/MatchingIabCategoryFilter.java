package com.dstillery.bidder.modules.filters;

import static com.dstillery.common.util.StringUtil.equalsIgnoreCase;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class MatchingIabCategoryFilter extends TgMarker {

    private final MetricReporterService metricReporterService;
    public MatchingIabCategoryFilter(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }


    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        if (tg.getRunOnAnyIabCategory()) {
            metricReporterService.addStateModuleForEntity(
                    "PASSED_FOR_RUN_ON_ANY_IAB_CATEGORY",
                    getClass().getSimpleName(),
                     tg.getId());
            return false;
        }
        /*
          this filter cares about subCategory and Not IAB Category
          This filter checks a site category agains a tg iab category, to make sure that tg is showing
          impressions on the site with the exact site category, I think we can have an option for tgs like
          show on matching iab category site , to make this filter relax in case we are not showing enough impressions
         *
         */
        // //MLOG (10) , "target group id : " , tg.getDeviceId()
//                , " tg iabCategories : " , JsonArrayUtil.convertListToJson(tg.getIabCategory())
//                  , " tg iabSubCategories : " , JsonArrayUtil.convertListToJson(tg.getIabSubCategory());

        boolean failed = true;

        for (String siteCat : context.getSiteCategory()) {
            for (String tgSubCat : tg.getIabSubCategory()) {
                if (equalsIgnoreCase (tgSubCat, siteCat)) {
                    metricReporterService.addStateModuleForEntity(
                            "PASSED_FOR_MATCHING_IAB_SUB_CATEGORY",
                            getClass().getSimpleName(),

                                    tg.getId());
                    filterTg = false;

                    failed = false;
                    break;
                }
            }
        }
        for (String siteCat : context.getSiteCategory()) {
            for (String tgCat : tg.getIabCategory()) {
                if (equalsIgnoreCase (tgCat, siteCat)) {
                    metricReporterService.addStateModuleForEntity(
                            "PASSED_FOR_MATCHING_IAB_CATEGORY",
                            getClass().getSimpleName(),

                                    tg.getId());
                    filterTg = false;
                    failed = false;
                    break;
                }
            }
        }

        if (failed) {
            //LOGGER.debug("not matching categories : {} ",  JsonArrayUtil.convertListToJson (tg.getIabSubCategory ()) , " , "
//                        , " tg iabCategories : " , JsonArrayUtil.convertListToJson(tg.getIabCategory())
//                                ," context.siteCategory : " , JsonArrayUtil.convertListToJson (context.siteCategory);

            metricReporterService.addStateModuleForEntity("FAILED_FOR_UN_MATCHING_IAB_SUB_CATEGORY",
                    getClass().getSimpleName(),

                            tg.getId());
        }


        return filterTg;
    }

}


package com.dstillery.bidder.modules.filters;/*
  DayPartFilter.h
 *
   Created on: Sep 6, 2015
       Author: mtaabodi
 */

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.targetgroup_maps.tgDaypart.DayNumber;
import com.dstillery.common.targetgroup.targetgroup_maps.tgDaypart.TargetGroupDayPartCacheService;
import com.dstillery.common.util.DateTimeUtil;

public class DayPartFilter extends BidderModule implements CacheUpdateWatcher {

	private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;
	private TargetGroupDayPartCacheService targetGroupDayPartCacheService;
	private final MetricReporterService metricReporterService;

	public DayPartFilter(TargetGroupDayPartCacheService targetGroupDayPartCacheService,
						 MetricReporterService metricReporterService) {
			this.metricReporterService = metricReporterService;
		 	this.targetGroupDayPartCacheService = targetGroupDayPartCacheService;
			attributesToGoodTgs = new ConcurrentHashMap<> ();
	}

	public void cacheWasUpdatedEvent() {
		//LOG_EVERY_N(INFO, 1), "DayPartFilter cache cleared";
		attributesToGoodTgs.clear();
	}

	private int getUserTimeZoneDifferenceWithUTC(OpportunityContext context) {
		//to be implemented later //read this from the IPInfo that comes from maxmind
		return context.getUserTimeZonDifferenceWithUTC();
	}

	public void process(OpportunityContext context) {
		//this filter is a bidder module so we call this function here
		markFilteredTgs (context);
	}

	public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
		boolean filterTg = true;

		if (tg.getRunOnAnyDayParting()) {
			metricReporterService.addStateModuleForEntity("PASSED_FOR_RUN_ON_ANY_DAYPARTING",
					getClass().getSimpleName(),
					tg.getId());
			return false;
		}
		ConcurrentHashMap<DayNumber, Set<Integer>> tgToHourMap = targetGroupDayPartCacheService.mapOfTargetGroupsToMapOfHoursInWeek.get(tg.getId());

		int localTimeHourOfUser = DateTimeUtil.getCurrentHourAsIntegerInUTC ()
				+ getUserTimeZoneDifferenceWithUTC (context);
		DayOfWeek weekNumber = LocalDateTime.now().atOffset(ZoneOffset.UTC).getDayOfWeek();
		DayNumber today = DayNumber.NUMBER_TO_DAY_BI_MAP.get(weekNumber.getValue());
		Set<Integer> hoursAllowedForToday = tgToHourMap.get(today);

		if (hoursAllowedForToday.contains(localTimeHourOfUser)) {
			metricReporterService.addStateModuleForEntity("PASSED_FOR_RIGHT_HOUR",
					getClass().getSimpleName(),

					tg.getId());
			filterTg = false;
		} else {
			//LOGGER.debug("hoursMap content for tg : {} ", userActiveInWeekHourNumber , " : " ,
//						JsonArrayUtil.convertListToJson(hoursMap.map.keySet());

			metricReporterService.addStateModuleForEntity("FAILED_FOR_INACTIVE_HOUR",
					this.getClass().getSimpleName(),

					tg.getId());

			filterOutTgBasedOnDayparting (tg, localTimeHourOfUser, context);
		}
		return filterTg;
	}

	void filterOutTgBasedOnDayparting(TargetGroup tg,
									  int userActiveInUTC,
									  OpportunityContext context) {
		// //MLOG (3) , " getUserTimeZoneDifferenceWithUTC (context) " ,
//				getUserTimeZoneDifferenceWithUTC (context)
//				, " DateTimeUtil.getCurrentHourAsIntegerInUTC() " , DateTimeUtil.getCurrentHourAsIntegerInUTC ()
//				, "filtering tg based on dayparting userActiveInUTC is " , userActiveInUTC;

	}

}

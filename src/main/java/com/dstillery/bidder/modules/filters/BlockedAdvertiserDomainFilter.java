package com.dstillery.bidder.modules.filters;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.advertiser.Advertiser;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.campaign.Campaign;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.util.StringUtil;

public class BlockedAdvertiserDomainFilter extends TgMarker implements CacheUpdateWatcher {

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

    private final MetricReporterService metricReporterService;
    private CampaignCacheService campaignCacheService;
    private CacheService<Advertiser> advertiserCacheService;

    public BlockedAdvertiserDomainFilter(CacheService<Advertiser> advertiserCacheService,
                                         CampaignCacheService campaignCacheService,
                                         MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

            this.campaignCacheService = campaignCacheService;
        this.advertiserCacheService = advertiserCacheService;
        attributesToGoodTgs = new ConcurrentHashMap<> ();

    }

    public void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "BlockedAdvertiserDomainFilter cache cleared";
        attributesToGoodTgs.clear();
    }

    boolean hasTargetGroupAnyBlockingAdvertiserDomain(
            OpportunityContext context, TargetGroup tg) {

        Set<String> advertiserDomains = getAdvertiserDomainForTargetGroup(tg);
        StringBuilder allAdvDomains = new StringBuilder();

        for (String domainName : advertiserDomains) {
            allAdvDomains.append(domainName).append(" ,");
            if (context.getBlockedAdvertiserMap().contains(StringUtil.toLowerCase (domainName))) {
                // //MLOG (3) , "tg id : " , tg.getDeviceId() , "has is blocked by bid request domain name : " , domainName;
                return true;
            }

            /*
              TODO : later , add a flag to addStateModuleForEntity to have this
              be registered only in tests , we don't need this in prod
             */
            metricReporterService.addStateModuleForEntity("TG_WAS_EVALUATED",
                    this.getClass().getSimpleName(),

                    tg.getId());
        }

        StringBuilder blockedAdvertiserInRequest = new StringBuilder();
        for (String iter : context.getBlockedAdvertiserMap()) {
            blockedAdvertiserInRequest.append(iter).append(" , ");
        }

        // //MLOG (3) , "tg id : " , tg.getDeviceId() , "with adv domains " , allAdvDomains , " is not blocked by bid request blocking domains : {} ", blockedAdvertiserInRequest;

        return false;
    }

    private Set<String> getAdvertiserDomainForTargetGroup(TargetGroup tg) {
        Campaign campaign = this.campaignCacheService.findByEntityId(tg.getCampaignId());
        assertAndThrow(campaign != null);

        assertAndThrow(this.advertiserCacheService != null);
        assertAndThrow(this.advertiserCacheService.getAllEntitiesMap() != null);

        Advertiser advertiser = this.advertiserCacheService.findByEntityId(campaign.getAdvertiserId());
        assertAndThrow(advertiser != null);
        return advertiser.getDomainNames();
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        boolean found = hasTargetGroupAnyBlockingAdvertiserDomain(context, tg);

        if (!found) {
            filterTg = false;
            metricReporterService.addStateModuleForEntity("PASSED",
                    this.getClass().getSimpleName(),

                    tg.getId());

        } else {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_BLOCKED_ADVERTISER",
                    this.getClass().getSimpleName(),

                    tg.getId());
        }

        return filterTg;
    }

}


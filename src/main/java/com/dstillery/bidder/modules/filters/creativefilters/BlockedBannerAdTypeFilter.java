package com.dstillery.bidder.modules.filters.creativefilters;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.common.creative.Creative;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class BlockedBannerAdTypeFilter extends TgMarker {

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

    private final MetricReporterService metricReporterService;
    private CreativeSizeFilter creativeSizeFilter;

    public BlockedBannerAdTypeFilter(CreativeSizeFilter creativeSizeFilter,
                                     MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.creativeSizeFilter = creativeSizeFilter;
        attributesToGoodTgs = new ConcurrentHashMap<> ();

    }

    public static String createElementId(Long targetGroupId, Long crtId) {
        String stateId = "tg" +
                targetGroupId +
                "crt" +
                crtId;
        return stateId;
    }

    public static boolean doesCreativeHaveBlockedBannerType(
            OpportunityContext context,
            Creative crt) {
        return context.getBlockedBannerTypesMap().contains(crt.getAdType().name());
    }

    void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "BlockedBannerAdTypeFilter cache cleared";

        attributesToGoodTgs.clear();
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;
        List<Creative> allCreatives = creativeSizeFilter.getCreativesAssignedToTg (tg.getId());

        if (allCreatives.isEmpty()) {

            metricReporterService.addStateModuleForEntity("FAILED_FOR_NO_CREATIVES",
                    "BlockedBannerAdTypeFilter",

                    tg.getId());
        }

        for (Creative crt : allCreatives) {

            String stateId = createElementId(tg.getId(), crt.getId());

            //LOGGER.debug("stateId is : {} ",  stateId;

            if (doesCreativeHaveBlockedBannerType(context, crt)) {
                //LOGGER.debug("crt id : " , crt.getDeviceId() , " is blocked by bid request because of adType ";
                metricReporterService.addStateModuleForEntity("FAILED",
                        "BlockedBannerAdTypeFilter",
                        stateId);
            } else {
                metricReporterService.addStateModuleForEntity("PASSED",
                        "BlockedBannerAdTypeFilter",
                        stateId);
                //LOGGER.debug("targetgroup id : "
//                        , tg.getId()
//                        , " has at least one good creative in terms of adType, passing the filter "
//                        , " crt.getDeviceId : " , crt.getId ()
//                        , "crt.getAdType() : " , crt.getAdType();
                filterTg = false;


                //TODO : we shouldn't break here..
                //we should add this tg+crt combo to the tgCreativeCandidates map that
                //is mapped by tg . tg+crt ids
                //so we don't have to rerun this logic in selectOptimalCreative filter
                break;
            }
        }

        return filterTg;
    }
}

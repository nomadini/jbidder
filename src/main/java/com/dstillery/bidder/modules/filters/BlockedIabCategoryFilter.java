package com.dstillery.bidder.modules.filters;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.targetgroup_maps.tgDaypart.TargetGroupDayPartCacheService;
import com.dstillery.common.util.StringUtil;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class BlockedIabCategoryFilter extends TgMarker implements CacheUpdateWatcher {

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

    private final MetricReporterService metricReporterService;

    public BlockedIabCategoryFilter(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        attributesToGoodTgs = new ConcurrentHashMap<> ();

    }

    public void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "BlockedIabCategoryFilter cache cleared";

        attributesToGoodTgs.clear();
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        boolean foundCat = false;
        for (String cat : tg.getIabCategory()) {
            foundCat = context.getBlockedIabCategoriesMap().contains (StringUtil.toLowerCase (cat));
            if (foundCat) {
                //LOGGER.debug("found blocking category : {} ",  StringUtil.toLowerCase (cat);
                break;
            }
        }

        boolean foundSubCat = false;
        for (String subCat : tg.getIabSubCategory()) {
            foundSubCat = context.getBlockedIabCategoriesMap().contains (StringUtil.toLowerCase (subCat));
            if (foundSubCat) {
                //LOGGER.debug("found blocking subCategory : {} ",  StringUtil.toLowerCase (subCat);
                break;
            }
        }

        if (foundCat || foundSubCat) {

            StringBuilder blockedCats = new StringBuilder();
            for (String entry : context.getBlockedIabCategoriesMap()) {
                blockedCats.append(entry).append(" , ");
            }

            // //MLOG (10) , "target group cat or sub cat is blocked by user : "
//                    , "foundCat : " , foundCat
//                    , "foundSubCat : " , foundSubCat
//                    , "blcokedCats : " , blcokedCats
//                    , " for chosen tg id : " , tg.getId()
//                    , " tg iabCategories : " , JsonArrayUtil.convertListToJson(tg.getIabCategory())
//                          , " tg iabSubCategories : " , JsonArrayUtil.convertListToJson(tg.getIabSubCategory());
        } else {
            // //MLOG (10) , "target group cat or sub cat is not blocked by user : "
//                    , " for chosen tg id : " , tg.getId()
//                    , " tg iabCategories : " , JsonArrayUtil.convertListToJson(tg.getIabCategory())
//                          , " tg iabSubCategories : " , JsonArrayUtil.convertListToJson(tg.getIabSubCategory());
        }

        if (foundCat) {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_BLOCKED_CATEGORY",
                    this.getClass().getSimpleName(),

                    tg.getId());
        }

        if (foundSubCat) {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_BLOCKED_SUB_CATEGORY",
                    this.getClass().getSimpleName(),

                    tg.getId());

        } else {
            metricReporterService.addStateModuleForEntity("PASSED",
                    this.getClass().getSimpleName(),

                    tg.getId());
            filterTg = false;
        }

        return filterTg;
    }

}


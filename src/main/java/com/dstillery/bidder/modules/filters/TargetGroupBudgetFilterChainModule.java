package com.dstillery.bidder.modules.filters;

import static com.dstillery.common.util.StringUtil.toStr;

import java.util.ArrayList;
import java.util.List;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.bidder.modules.budgetcontrol.pacing.PacingBasedBudgetLimitEnforcerFilter;
import com.dstillery.bidder.modules.budgetcontrol.pacing.PacingBasedImpressionLimitEnforcerFilter;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

/*
   this class combines all the filters that are related to budget in a filterChain
 */
public class TargetGroupBudgetFilterChainModule extends BidderModule {

    private MetricReporterService metricReporterService;

    private List<TgMarker> orderedListOfFiltersToRun = new ArrayList<>();

    private String originalName;

    public TargetGroupBudgetFilterChainModule(
            EntityDeliveryInfoCacheService entityDeliveryInfoCacheService,
            MetricReporterService metricReporterService) {


        this.metricReporterService = metricReporterService;;
        this.metricReporterService = metricReporterService;
        originalName = "TgBudgetFilterChain";

        PacingBasedImpressionLimitEnforcerFilter pacingBasedImpressionLimitEnforcerFilter =
                new PacingBasedImpressionLimitEnforcerFilter(
                        entityDeliveryInfoCacheService, metricReporterService);

        PacingBasedBudgetLimitEnforcerFilter pacingBasedBudgetLimitEnforcerFilter =
                new PacingBasedBudgetLimitEnforcerFilter(
                        entityDeliveryInfoCacheService, metricReporterService);

        initFilter (pacingBasedBudgetLimitEnforcerFilter);
    }

    private void initFilter(TgMarker filter) {
        orderedListOfFiltersToRun.add(filter);
    }

    public void process(OpportunityContext context) {
        metricReporterService.addStateModuleForEntity(
                "sizeOfTargetGroup_StartOfFilterChain"
                        + toStr(context.getSizeOfUnmarkedTargetGroups()),
                originalName,
                "ALL");        //make all these filters use tgs in the context. you don't need to pass any tgs around
        for (TgMarker filter : orderedListOfFiltersToRun) {

            //I am adding try catch only around filterTargetGroups method invocation since I don't want
            //any exception thrown in that function , winds up the stack and I dont get the correct lastFilterName
            try {

                int beforeTgSize = context.getSizeOfUnmarkedTargetGroups();
                if (beforeTgSize > 0) {
                    TargetGroupFilterChainModule.runFilter(filter,
                            context,
                            metricReporterService);

                    int afterTgSize = context.getSizeOfUnmarkedTargetGroups();
                    int filterPercentage = 0;
                    if (beforeTgSize != 0) {
                        filterPercentage = (beforeTgSize - afterTgSize) * 100 / beforeTgSize;
                    }
                    metricReporterService.addStateModuleForEntityWithValue(filter.getClass().getSimpleName(),
                            filterPercentage,
                            originalName+"-PercentOfFilteringTg",
                            "ALL");
                    if (afterTgSize == 0) {
                        metricReporterService.addStateModuleForEntity(
                                filter.getClass().getSimpleName() + "-LastFilter",
                                "TargetGroupBudgetFilterChainModule",
                                "ALL"
                        );
                    }
                    //LOG_EVERY_N(INFO, 10000), "percenteOfFilteringTg for filter ", filter.getClass().getSimpleName(), " is ", filterPercentage;
                }
            } catch (RuntimeException e) {
                metricReporterService.addStateModuleForEntity("Exception : " + toStr(e.getMessage()),
                        originalName,
                        "ALL");
            }
        }

        //http://69.200.247.201:9980/pages/filtermap
        //printFilterNameToTargetGroupCounterMap();
        //this line should be commented out later, this line is for disabling the target group filter!

    }

    String getName() {

        return originalName;
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be callsed");
    }

/**************************************/
/**************************************/
/*********** helper methods***********/
/**************************************/
/**************************************/

//<bean id="evalTargetFilterChain" class="org.apache.commons.chain.impl.ChainBase">
//	<constructor-arg type="java.util.Collection">
//		<!-- order by filter % - heavy-duty filtering first -.
//		<list>
//			<ref bean="filterBySpendSegmentTargetCommand" />
//			<!-- geo filters a lot -.
//			<ref bean="filterBySpendGeoTargetCommand" />
//			<ref bean="filterByCreativeGeoTargetCommand" />
//
//			<!-- pacing filters need to run after spend-segment & geo (as per DS) -.
//			<ref bean="filterByCampaignPacingTargetCommand" />
//			<ref bean="filterBySpendPacingTargetCommand" />
//			<ref bean="filterByCreativePacingTargetCommand" />
//
//			<ref bean="filterByATExclusionCommand" />
//			<ref bean="filterByHostIdsTargetCommand" />
//            <ref bean="filterByAppIdsTargetCommand" />
//			<ref bean="filterByInventoryMarketerTargetCommand" />
//			<ref bean="filterBySpendDeviceClassTargetCommand" />
//			<ref bean="filterBySpendEnvTypeTargetCommand" />
//
//			<ref bean="filterByBmsExtCommand" />
//			<ref bean="filterByBmsIntCommand" />
//
//			<ref bean="filterByCreativeVideoDataCommand" />
//			<ref bean="filterBySpendSectionTargetCommand" />
//			<ref bean="filterSpendByRemoteAgencyCommand"/>
//			<ref bean="filterBySpendInventoryTargetCommand" />
//			<ref bean="filterBySpendDayPartCommand" />
//			<ref bean="filterByDelayFreqRecencyTargetsCommand" />
//			<ref bean="filterByIndustryCategoriesCommand" />
//			<ref bean="filterByIabCategoriesCommand" />
//			<ref bean="filterByTopLevelDomainCommand" />
//			<ref bean="filterByTechSpecTargetCommand" />
//			<ref bean="filterBySpendAdPositionTargetCommand" />
//			<ref bean="filterBySpendPublisherCategoryTargetCommand" />
//			<ref bean="filterByCreativeRemoteStatusCommand" />
//			<ref bean="filterByCreativeVendorCommand" />
//			<ref bean="filterByCreativeAdAttributeCommand" />
//			<ref bean="filterInventoryByEnvTypeCommand" />

}

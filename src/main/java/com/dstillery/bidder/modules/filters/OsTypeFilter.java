package com.dstillery.bidder.modules.filters;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.TgMarker;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetGroupFilterStatistic;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static com.dstillery.common.util.StringUtil.toStr;

public class OsTypeFilter extends TgMarker implements CacheUpdateWatcher {

    private ConcurrentHashMap<String, List<TargetGroup>> attributesToGoodTgs;

    private final MetricReporterService metricReporterService;
    public OsTypeFilter(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        attributesToGoodTgs = new ConcurrentHashMap<> ();
    }

    public void cacheWasUpdatedEvent() {
        //LOG_EVERY_N(INFO, 1), "OsTypeFilter cache cleared";
        attributesToGoodTgs.clear();
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;
        assertAndThrow(tg!= null);

        Boolean typePairPtr = tg.getOsTypeTargets().contains(context.getDeviceOsFamily());
        if (typePairPtr) {
            metricReporterService.addStateModuleForEntity("PASSED_FOR_MATCHING_DEVICE_TYPE",
                    getClass().getSimpleName(),

                    tg.getId());


            filterTg = false;
        } else {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_UNMATCHING_DEVICE_TYPE",
                    getClass().getSimpleName(),

                    tg.getId());

        }

        //LOGGER.debug("osFamily is : {} ", context.deviceOsFamily, ", tg id : {} ",  tg.getDeviceId(),", result : {} ",  filterTg;
        return filterTg;
    }
}


package com.dstillery.bidder.modules.filters;

/*
   you need to add the instance of CacheUpdateWatcher to cacheUpdateWatcherModules in bidderAsyncJobsService

   like this bidderAsyncJobsService.cacheUpdateWatcherModules.add(moduleContainer.deviceTypeFilter.get());
 */
public interface CacheUpdateWatcher {
    void cacheWasUpdatedEvent();
} //COMMON_CacheUpdateWatcher_H

package com.dstillery.bidder.modules.frauddetection;import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.recency.DeviceCounterService;
import com.dstillery.common.recency.LastTimeSeenSource;
import com.dstillery.common.targetgroup.TargetGroup;

public class LastXSecondBidOnDeviceModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(LastXSecondBidOnDeviceModule.class);

    private LastTimeSeenSource lastTimeSeenSource;
    private DeviceCounterService deviceCounterService;
    private Histogram histoOfIpCounts;
    private final int lastTimeSeenPeriodInSeconds;

    private final MetricReporterService metricReporterService;
    public LastXSecondBidOnDeviceModule(LastTimeSeenSource lastTimeSeenSource,
                                        DeviceCounterService deviceCounterService,
                                        MetricRegistry influxMetrics,
                                        int lastTimeSeenPeriodInSeconds,
                                        MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.lastTimeSeenSource = lastTimeSeenSource;
        this.deviceCounterService = deviceCounterService;
        this.histoOfIpCounts = influxMetrics.histogram("ip_count_time_seen");
        this.lastTimeSeenPeriodInSeconds = lastTimeSeenPeriodInSeconds;
    }

    public void process(OpportunityContext context) {

        processDeviceCountFilter(context);

        processDeviceLastSeenFilter(context);
    }

    private void processDeviceLastSeenFilter(OpportunityContext context) {
        if (context.isAsyncPiplineExecuting()) {
            //this logic is only allowed to run in sync pipeline
            return;
        }
        //we mark user as seen, if we have bid on it ever...we don't want to bid on a user too often
        boolean seen = lastTimeSeenSource.hasSeenInLastXSeconds(context.getDevice());
        if (seen) {
            /*
             we have seen this user before
             */
            context.stopProcessing(this.getClass().getSimpleName());
            metricReporterService.addStateModuleForEntity("FAILED_BECAUSE_SAW_LATELY",
                    "LastXSecondBidOnDeviceModule",
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);

        } else {
            metricReporterService.addStateModuleForEntity("PASSED_BECAUSE_DIDNT_SEE_LATELY",
                    "LastXSecondBidOnDeviceModule",
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);

        }
    }

    private void recordDeviceInBadDeviceIds(OpportunityContext context) {

    }

    private void processDeviceCountFilter(OpportunityContext context) {
        if (!context.isAsyncPiplineExecuting()) {
            //this logic is only allowed to run in async pipeline
            return;
        }
        long timesSeen = deviceCounterService.countAndGetTimesSeen(
                context.getDevice(),
                lastTimeSeenPeriodInSeconds);
        LOGGER.debug("nth : device id seen times : {}", timesSeen);

        //TODO : block the highest offenders
        histoOfIpCounts.update(timesSeen);
        if (timesSeen >= histoOfIpCounts.getSnapshot().get95thPercentile()  && histoOfIpCounts.getCount() >= 10000) {
            recordDeviceInBadDeviceIds(context);
        }
    }
    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


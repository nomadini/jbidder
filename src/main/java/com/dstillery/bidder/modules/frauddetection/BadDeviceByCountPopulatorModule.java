package com.dstillery.bidder.modules.frauddetection;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.MetricPriority.IMPORTANT;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.recency.DeviceCounterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class BadDeviceByCountPopulatorModule extends BidderModule {

    private DeviceCounterService deviceCounterService;
    private Histogram histoOfDeviceCounts;
    private long maxAcceptableTimeSeenInLastTenMinute;
    private int periodOfCountingDevicesInSeconds;
    private final MetricReporterService metricReporterService;

    public BadDeviceByCountPopulatorModule(DeviceCounterService deviceCounterService,
                                           long maxAcceptableTimeSeenInLastTenMinute,
                                           MetricRegistry influxMetrics,
                                           int periodOfCountingDevicesInSeconds,
                                           MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.periodOfCountingDevicesInSeconds = periodOfCountingDevicesInSeconds;
        this.deviceCounterService = deviceCounterService;
            histoOfDeviceCounts =  influxMetrics.histogram("bad_device_time_seen");
        this.maxAcceptableTimeSeenInLastTenMinute = maxAcceptableTimeSeenInLastTenMinute;
    }

    public void process(OpportunityContext context) {

         if (!context.isAsyncPiplineExecuting()) {
            //this logic is only allowed to run in async pipeline
            return;
        }

        //we check to see if we have seen the device in the past 10 minutes
        // and its among 20 percent of devices and timesSeen is more than 30
        long timesSeen = deviceCounterService.countAndGetTimesSeen(
                context.getDevice(),
                60 * 10);

        // //TODO : remove this...
        // //fixing the messed up bad devices for now
        // lastTimeSeenSource.markAsGoodDeviceByCount(
        //         context.device,
        //         2  24 * 3600);

        //TODO : block the highest offenders
        histoOfDeviceCounts.update(timesSeen);

        if (histoOfDeviceCounts.getSnapshot().get95thPercentile() >= timesSeen
                && timesSeen > maxAcceptableTimeSeenInLastTenMinute) {
            //the top x percent of devices will go to BadDeviceCache in AeroSpike, we maintain
            //each record for two days in this cache
            //
            //LOG_EVERY_N(ERROR, 10) , google.COUNTER,"nth : timeSeen marked as Bad is ",timesSeen;
            metricReporterService.addStateModuleForEntity(
                    "MARKING_DEVICE_AS_BAD_BY_COUNT",
                    "BadDeviceByCountPopulatorModule",
                    "ALL",
                    IMPORTANT
            );
            deviceCounterService.markAsBadDeviceByCount(
                    context.getDevice(),
                    periodOfCountingDevicesInSeconds);
        }

    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


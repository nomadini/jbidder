package com.dstillery.bidder.modules.frauddetection;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.MetricPriority.IMPORTANT;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.recency.DeviceIpCounterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class BadIpByCountPopulatorModule extends BidderModule {

    private DeviceIpCounterService deviceIpCounterService;
    private Histogram histoOfIpCounts;

    private final MetricReporterService metricReporterService;
    public BadIpByCountPopulatorModule(DeviceIpCounterService deviceIpCounterService,
                                       MetricRegistry influxMetrics,
                                       MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.deviceIpCounterService = deviceIpCounterService;
        this.histoOfIpCounts = influxMetrics.histogram("ip_count_time_seen");
    }

    public void process(OpportunityContext context) {
        /*
           we populate the cache in async pipeline
         */
        populateBadIpByCountCache(context);

    }

    void populateBadIpByCountCache(OpportunityContext context) {
        if (!context.isAsyncPiplineExecuting()) {
            //this logic is only allowed to run in async pipeline
            return;
        }

        long timesSeen = deviceIpCounterService.countAndGetTimesSeen(
                context.getDeviceIp(),
                24 * 60 * 60 );//days in second
        //TODO : block the highest offenders

        histoOfIpCounts.update(timesSeen);

        if (histoOfIpCounts.getSnapshot().get95thPercentile() >= timesSeen && timesSeen > 100) {
            //the top x percent of devices will go to BadDeviceCache in AeroSpike, we maintain
            //each record for two days in this cache
            //
            metricReporterService.
                    addStateModuleForEntity("MARK_AS_BAD_IP",
                            "BadIpByCountPopulatorModule",
                            "ALL",
                            IMPORTANT);
            deviceIpCounterService.markAsBadIpByCount(
                    context.getDeviceIp(),
                    2 * 24 * 3600);
        }

    }
    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


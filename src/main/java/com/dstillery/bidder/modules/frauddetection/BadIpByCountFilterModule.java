package com.dstillery.bidder.modules.frauddetection;import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.recency.DeviceIpCounterService;
import com.dstillery.common.recency.LastTimeIpSeenSource;
import com.dstillery.common.targetgroup.TargetGroup;

public class BadIpByCountFilterModule extends BidderModule {

    private LastTimeIpSeenSource lastTimeIpSeenSource;
    private DeviceIpCounterService deviceIpCounterService;
    private Histogram histoOfIpCounts;

    private final MetricReporterService metricReporterService;
    public BadIpByCountFilterModule(LastTimeIpSeenSource lastTimeIpSeenSource,
                                    DeviceIpCounterService deviceIpCounterService,
                                    MetricRegistry influxMetrics,
                                    MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.deviceIpCounterService = deviceIpCounterService;
        this.lastTimeIpSeenSource = lastTimeIpSeenSource;
        this.histoOfIpCounts = influxMetrics.histogram("bad_ip_time_seen");
    }

    public void process(OpportunityContext context) {

        processBadIpByCountFilter(context);

    }

    private void processBadIpByCountFilter(OpportunityContext context) {
        boolean isBadDeviceByCount = deviceIpCounterService.isBadIpByCount(context.getDeviceIp());
        if (isBadDeviceByCount) {
            /**
             we have seen this user before
             */
            context.stopProcessing(this.getClass().getSimpleName());
            metricReporterService.addStateModuleForEntity("FAILED_BECAUSE_BAD_DEVICE_BY_IP",
                    "BadIpByCountFilterModule",
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);

        } else {
            metricReporterService.addStateModuleForEntity("PASSED_BECAUSE_NOT_BAD_DEVICE_BY_IP",
                    "BadIpByCountFilterModule",
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);
        }

    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


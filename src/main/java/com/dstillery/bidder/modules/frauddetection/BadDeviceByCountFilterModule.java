package com.dstillery.bidder.modules.frauddetection;import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.recency.DeviceCounterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class BadDeviceByCountFilterModule extends BidderModule {

    private DeviceCounterService deviceCounterService;
    private Histogram histoOfDeviceCounts;

    private final MetricReporterService metricReporterService;
    public BadDeviceByCountFilterModule(DeviceCounterService deviceCounterService,
                                        MetricRegistry influxMetrics,
                                        MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.deviceCounterService = deviceCounterService;
        this.histoOfDeviceCounts = influxMetrics.histogram("device_count_time_seen");
    }

    public void process(OpportunityContext context) {

     boolean isBadDeviceByCount =
                deviceCounterService.isBadDeviceByCount(context.getDevice());
        if (isBadDeviceByCount) {
            /*
             we have seen this user before
             */
            context.stopProcessing(this.getClass().getSimpleName());
            metricReporterService.addStateModuleForEntity("FAILED_BECAUSE_BAD_DEVICE_BY_COUNT",
                    "BadDeviceByCountFilterModule",
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);

        } else {
            metricReporterService.addStateModuleForEntity("PASSED_BECAUSE_NOT_BAD_DEVICE_BY_COUNT",
                    "BadDeviceByCountFilterModule",
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);
        }

    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


package com.dstillery.bidder.modules.frauddetection;import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.recency.LastTimeIpSeenSource;
import com.dstillery.common.targetgroup.TargetGroup;

public class LastXSecondSeenIpVisitorModule extends BidderModule {

    private LastTimeIpSeenSource lastTimeIpSeenSource;
    private Histogram histoOfIpCounts;
    private final int lastTimeIpSeenSourcePeriodInSeconds;

    private final MetricReporterService metricReporterService;

    public LastXSecondSeenIpVisitorModule(LastTimeIpSeenSource lastTimeIpSeenSource,
                                          MetricRegistry influxMetrics,
                                          int lastTimeIpSeenSourcePeriodInSeconds,
                                          MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.lastTimeIpSeenSource = lastTimeIpSeenSource;
        this.histoOfIpCounts = influxMetrics.histogram("ip_count_time_seen");
        this.lastTimeIpSeenSourcePeriodInSeconds = lastTimeIpSeenSourcePeriodInSeconds;
    }

    public void process(OpportunityContext context) {

        //we check if we have seen this ip in the last 30 seconds or not
        boolean seen = lastTimeIpSeenSource.hasSeenInLastXSeconds(context.getDeviceIp());
        if (seen) {
            /**
              we have seen this user before
             */
            //LOGGER.debug("context.deviceIp ", context.deviceIp, " was seen in last n seconds.aborting";
            context.stopProcessing(this.getClass().getSimpleName());
            metricReporterService.addStateModuleForEntity("FAILED_BECAUSE_SAW_LATELY",
                    "LastXSecondSeenIpVisitorModule",
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);

        } else {
            metricReporterService.addStateModuleForEntity("PASSED_BECAUSE_DIDNT_SEE_LATELY",
                    "LastXSecondSeenIpVisitorModule",
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);
        }

        //we mark this ip as seen for 30 seconds in cache....
        lastTimeIpSeenSource.markAsSeenInLastXSeconds(context.getDeviceIp(), lastTimeIpSeenSourcePeriodInSeconds);
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


package com.dstillery.bidder.modules;

import static com.dstillery.common.feature.FeatureType.GTLD;
import static com.dstillery.common.feature.FeatureType.IAB_CAT;
import static com.dstillery.common.feature.FeatureType.MGRS100;
import static com.dstillery.common.util.StringUtil.assertAndThrow;

import com.dstillery.common.common_modules.FeatureToFeatureHistoryUpdaterModule;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class FeatureToFeatureHistoryUpdaterModuleWrapper extends BidderModule {

    private FeatureToFeatureHistoryUpdaterModule featureToFeatureHistoryUpdaterModule;

    private final MetricReporterService metricReporterService;
    public FeatureToFeatureHistoryUpdaterModuleWrapper(
            FeatureToFeatureHistoryUpdaterModule featureToFeatureHistoryUpdaterModule,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.featureToFeatureHistoryUpdaterModule = featureToFeatureHistoryUpdaterModule;
    }

    public void process(OpportunityContext context) {

        if (context.getDevice() == null ||
            context.getSiteDomain().isEmpty() ||
            context.getMgrs100m().isEmpty()) {
            metricReporterService.addStateModuleForEntity("SKIPPING_UNKNOWN_USER_MGRS100",
                    "FeatureToFeatureHistoryUpdaterModuleWrapper",
                    "ALL");

            return;
        }
        //this will save domain . mgrs100m pair
        featureToFeatureHistoryUpdaterModule.process(
                GTLD,
                context.getSiteDomain(),
                MGRS100,
                context.getMgrs100m());

        //this will save mgrs100m.domain   pair
        featureToFeatureHistoryUpdaterModule.process(
                MGRS100,
                context.getMgrs100m(),
                GTLD,
                context.getSiteDomain());

        //very important to check we have siteCategory
        assertAndThrow(!context.getSiteCategory().isEmpty());
        //this will save iab category . mgrs100m pair
        //this will save iab category . domain pair
        for (String siteCat : context.getSiteCategory()) {
            featureToFeatureHistoryUpdaterModule.process(
                    IAB_CAT,
                    siteCat,
                    MGRS100,
                    context.getMgrs100m());

            featureToFeatureHistoryUpdaterModule.process(
                    IAB_CAT,
                    siteCat,
                    GTLD,
                    context.getSiteDomain());
        }    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        return false;
    }
}


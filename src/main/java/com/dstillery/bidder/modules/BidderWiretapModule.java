package com.dstillery.bidder.modules;

import com.dstillery.common.LoadPercentageBasedSampler;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.wiretap.Wiretap;
import com.dstillery.common.wiretap.WiretapCassandraService;
import com.dstillery.common.targetgroup.TargetGroup;

/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */
public class BidderWiretapModule extends BidderModule {
        private WiretapCassandraService wiretapCassandraService;
        private LoadPercentageBasedSampler wiretapBidRequestResponseSampler;

        public BidderWiretapModule(WiretapCassandraService wiretapCassandraService,
                                   LoadPercentageBasedSampler wiretapBidRequestResponseSampler) {
                this.wiretapCassandraService = wiretapCassandraService;
                this.wiretapBidRequestResponseSampler = wiretapBidRequestResponseSampler;
        }

        public void process(OpportunityContext context) {
                if (context.getBidDecisionResult() == EventLog.BidEventType.NO_BID) {
                        return;
                }
                if (!wiretapBidRequestResponseSampler.isPartOfSample()) {
                        return;
                }

                Wiretap wiretap = new Wiretap();
                wiretap.setEventId(context.getTransactionId());
                wiretap.setAppName("Bidder");
                // String appVersion;
                // String appHost;
                wiretap.setModuleName(this.getClass().getSimpleName());
                wiretap.setRequest(context.getRequestBody());
                wiretap.setResponse(context.getFinalBidResponseContent());
                wiretapCassandraService.record(wiretap);

        }

        public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
                return false;
        }
}

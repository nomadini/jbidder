package com.dstillery.bidder.modules;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.whiteListedBiddingDomains.GlobalWhiteListedCacheService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.util.StringUtil;

/*
   globalWhiteListedCacheService has a list of domains that we are interested on bidding
   this module makes sure that we bid only on those modules
 */
public class GlobalWhiteListModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalWhiteListModule.class);

    private GlobalWhiteListedCacheService globalWhiteListedCacheService;

    public GlobalWhiteListModule(GlobalWhiteListedCacheService globalWhiteListedCacheService) {
        this.globalWhiteListedCacheService = globalWhiteListedCacheService;
    }

    public void process(OpportunityContext context) {

        Set<String> set =
                globalWhiteListedCacheService.getAllWhiteListedDomains();

        if (set.size() <= 100) {
            LOGGER.warn("th, loaded whiteListedDomains are too small {}.", set.size());
        }
        if (context.getSiteDomain() == null) {
            LOGGER.warn("siteDomain is null for bid request {}", context.getBidRequestBody());
            context.stopProcessing(this.getClass().getSimpleName());
        } else {
            boolean found = set.contains(StringUtil.toLowerCase(context.getSiteDomain()));
            if (found) {
                LOGGER.debug("passing whitelisted domain {}.", context.getSiteDomain());

            } else {
                LOGGER.debug("failing on not whitelisted domain {}.", context.getSiteDomain());
                context.stopProcessing(this.getClass().getSimpleName());
            }
        }


    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        return false;
    }

}


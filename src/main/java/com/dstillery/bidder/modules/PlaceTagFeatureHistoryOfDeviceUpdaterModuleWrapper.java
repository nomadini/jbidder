package com.dstillery.bidder.modules;

import static com.dstillery.common.util.StringUtil.toStr;

import com.dstillery.common.common_modules.DeviceHistoryUpdaterModule;
import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper extends BidderModule {

    private FeatureType featureType;

    private DeviceHistoryUpdaterModule devicePlaceTagFeatureUpdaterModule;

    private final MetricReporterService metricReporterService;
    public PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper(
            FeatureType featureType,
            DeviceHistoryUpdaterModule devicePlaceTagFeatureUpdaterModule,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.featureType = featureType;
        this.devicePlaceTagFeatureUpdaterModule = devicePlaceTagFeatureUpdaterModule;
    }

    public void process(OpportunityContext context) {

        if (context.getDevice() == null) {
            return;
        }

        if (context.getTagIds().isEmpty()) {
            metricReporterService.addStateModuleForEntity("SKIPPING_BECAUSEOF_UNKNOWN_TAG",
                    "PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper",
                    "ALL");
            return;
        }

        for (Long tagId : context.getTagIds()) {
            try {
                devicePlaceTagFeatureUpdaterModule.process(featureType,
                        toStr(tagId),
                        context.getDevice());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        metricReporterService.addStateModuleForEntity("ADDING_DEVICE_FEATURE_HISTORY",
                "PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper",
                "ALL");

    }
    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        return false;
    }

}


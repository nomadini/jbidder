package com.dstillery.bidder.modules;/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */

import java.util.concurrent.ConcurrentHashMap;

import com.dstillery.common.creative.Creative;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.targetgroup_maps.tgCreative.TargetGroupCreativeCacheService;

public class TargetGroupWithoutCreativeFilterModule extends BidderModule {
    private final MetricReporterService metricReporterService;
    private TargetGroupCreativeCacheService targetGroupCreativeCacheService;

    public TargetGroupWithoutCreativeFilterModule(TargetGroupCreativeCacheService targetGroupCreativeCacheService,
                                                  MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.targetGroupCreativeCacheService = targetGroupCreativeCacheService;
    }

    public void process(OpportunityContext context) {
        markFilteredTgs(context);
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        boolean filterTg = true;

        ConcurrentHashMap<Long, Creative> tgCreativeEntry =
                targetGroupCreativeCacheService.getAllTargetGroupCreativesMap().get(tg.getId());

        if (tgCreativeEntry != null) {
            if (tgCreativeEntry.isEmpty()) {
                metricReporterService.addStateModuleForEntity("FAILED_FOR_ZERO_CREATIVES_IN_MAP",
                        "TargetGroupWithoutCreativeFilterModule",

                        tg.getId());

            } else {
                //targetGroup has some creatives assigned to it
                filterTg = false;
            }

        } else {
            metricReporterService.addStateModuleForEntity("FAILED_FOR_NO_CREATIVES",
                    "TargetGroupWithoutCreativeFilterModule",

                    tg.getId());
        }

        return filterTg;
    }
}


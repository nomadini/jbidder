package com.dstillery.bidder.modules;
import java.util.List;

import com.dstillery.common.geo.LatLonToMGRSConverter;
import com.dstillery.common.targetgroup.TargetGroup;

public class MGRSBuilderModule extends BidderModule {

    private LatLonToMGRSConverter latLonToMGRSConverter;
    public MGRSBuilderModule(LatLonToMGRSConverter latLonToMGRSConverter) {

        this.latLonToMGRSConverter = latLonToMGRSConverter;
    }

    public void process(OpportunityContext context) {
        List<String> allMGrsValues =
                latLonToMGRSConverter.convertToAllMGPRSValues(
                        context.getDeviceLat(),
                        context.getDeviceLon());

        context.setMgrs1km(allMGrsValues.get(0));
        context.setMgrs100m(allMGrsValues.get(1));
        context.setMgrs10m(allMGrsValues.get(2));
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}

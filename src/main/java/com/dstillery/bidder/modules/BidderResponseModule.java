package com.dstillery.bidder.modules;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.recency.LastTimeSeenSource;
import com.dstillery.common.targetgroup.TargetGroup;

/**
  this module does the necessary operations that we need to do after we make a bid
 */
public class BidderResponseModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidderResponseModule.class);

    private final MetricReporterService metricReporterService;
    private LastTimeSeenSource lastTimeSeenSource;
    private AeroCacheService<EventLog> realTimeEventLogCacheService;
    private int acceptableIntervalToBidOnAUserInSeconds;
    //this will be reset every hour
    private AtomicLong numberOfBidsForPixelMatchingPurposePerHour = new AtomicLong();

    public BidderResponseModule(
                                LastTimeSeenSource lastTimeSeenSource,
                                AeroCacheService<EventLog> realTimeEventLogCacheService,
                                int acceptableIntervalToBidOnAUserInSeconds,
                                MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.lastTimeSeenSource = lastTimeSeenSource;
        this.realTimeEventLogCacheService = realTimeEventLogCacheService;
        this.acceptableIntervalToBidOnAUserInSeconds = acceptableIntervalToBidOnAUserInSeconds;
    }

    public void process(OpportunityContext context) {

        //we increment the number of bids we did for this target group
        LOGGER.debug("context.getChosenTargetGroup() : {}", context.getChosenTargetGroup());
        LOGGER.debug("context.getChosenCreative() : {}", context.getChosenCreative());
        assertAndThrow (context.getChosenTargetGroup().getId() > 0);
        assertAndThrow (context.getChosenCreative().getId() > 0);
        assertAndThrow (context.getChosenTargetGroup() != null);
        Long chosenTargetGroupId = context.getChosenTargetGroup().getId();
        assertAndThrow(chosenTargetGroupId != 0);
        if (context.isBiddingOnlyForMatchingPixel()) {
            //we are only bidding on this user to match our pixels with exchangeName
            metricReporterService.addStateModuleForEntity("BIDDING_FOR_MATCHING_PIXEL",
                    "BidderResponseModule",
                    "ALL");
            numberOfBidsForPixelMatchingPurposePerHour.getAndIncrement();
            //we set the exchangeId as nomadiniDeviceId to store it in aerospike in order
            //not to bid too much for pixel matching on this user
            NomadiniDevice device = NomadiniDevice.buildAStandardDeviceIdBasedOn(
                    context.getExchangeUserId(),
                    context.getDeviceClass());
            //we want to make sure the device is already unknown to us at this point
            assertAndThrow(context.getDevice() == null);
            context.setDevice(device);
        }

        lastTimeSeenSource.markAsSeenInLastXSeconds(context.getDevice(),
                acceptableIntervalToBidOnAUserInSeconds);
        LOGGER.debug("bid event TransactionId : {}", context.getTransactionId());
        realTimeEventLogCacheService.putDataInCache(context.getEventLog());
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}

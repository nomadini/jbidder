package com.dstillery.bidder.modules;import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;

import java.util.concurrent.atomic.AtomicLong;

import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.partnermap.NomadiniIdToExchangeIdsMapCassandraService;
import com.dstillery.common.partnermap.NomadiniIdToPartnerId;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceEnvironmentType;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.targetgroup.TargetGroup;

/*
   if the request is from a desktop requst && gicapodsId is not present
   this module will query the gicapodsIdToExchangeIdsMap in cassandra with the exchange id to get the nomadiniDeviceId.
   at the end of this module, if nomadiniDeviceId is still empty, we nobid on this request

   if request is mobile,
   this module sets the idfa if not present we set sha1 as the nomadiniDeviceId

 */
public class GicapodsIdToExchangeIdMapModule extends BidderModule {

    private final MetricReporterService metricReporterService;
    private NomadiniIdToExchangeIdsMapCassandraService gicapodsIdToExchangeIdsMapCassandraService;
    private Long numberOfBidsForPixelMatchingPurposePerHourLimit;
    private AtomicLong numberOfBidsForPixelMatchingPurposePerHour;

    public GicapodsIdToExchangeIdMapModule(NomadiniIdToExchangeIdsMapCassandraService gicapodsIdToExchangeIdsMapCassandraService,
                                           Long numberOfBidsForPixelMatchingPurposePerHourLimit,
                                           AtomicLong numberOfBidsForPixelMatchingPurposePerHour,
                                           MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.gicapodsIdToExchangeIdsMapCassandraService = gicapodsIdToExchangeIdsMapCassandraService;
        this.numberOfBidsForPixelMatchingPurposePerHourLimit = numberOfBidsForPixelMatchingPurposePerHourLimit;
        this.numberOfBidsForPixelMatchingPurposePerHour = numberOfBidsForPixelMatchingPurposePerHour;
    }

    public void process(OpportunityContext context) {
        if (context.getDevice() != null) {
            //we already have our own userId from sent to us from the exchange
            return;
        }

        if (context.getEnvironmentType() == DeviceEnvironmentType.WEB) {
            //user is unknown to us, we query our db to see if we already have a mapping for this user
            //this means that exchange has not sent us our mapped id, but we can find our id
            //in db based on previous mapping... this case shouldn't happen alot
            NomadiniIdToPartnerId nomadiniIdToPartnerId =
                    gicapodsIdToExchangeIdsMapCassandraService.
                    readGicapodsIdMappedToExchangeId(
                            context.getExchangeName(),
                            context.getExchangeId());
            if (!nomadiniIdToPartnerId.getNomadiniDeviceId().isEmpty()) {
                context.setDevice(new NomadiniDevice(
                        nomadiniIdToPartnerId.getNomadiniDeviceId(), DeviceTypeValue.DISPLAY, DeviceClassValue.DESKTOP));
                metricReporterService.addStateModuleForEntity("MAPPED_INITIAL_UNKNOW_USER_WITH_SUCCESS",
                        "GicapodsIdToExchangeIdMapModule",
                        ALL_ENTITIES,
                        MetricReporterService.MetricPriority.IMPORTANT);
            } else {
                metricReporterService.addStateModuleForEntity("UNMAPPED_USER", "GicapodsIdToExchangeIdMapModule", "ALL");
                if (doWeNeedToBidForPixelMatchingOpportunity(context)) {
                    //TODO : add a limit per hour for this
                    metricReporterService.addStateModuleForEntity(
                            "BIDDING_TO_TRY_MATCHING_PIXEL",
                            "GicapodsIdToExchangeIdMapModule",
                            ALL_ENTITIES,
                            MetricReporterService.MetricPriority.IMPORTANT);
                    //we set this to true to ignore the DeviceSegments filter for this user
                    context.setBiddingOnlyForMatchingPixel(true);

                } else {
                    metricReporterService.addStateModuleForEntity(
                            "IGNORE_UNKNOWN_DESKTOP_USER",
                            "GicapodsIdToExchangeIdMapModule",
                            ALL_ENTITIES,
                            MetricReporterService.MetricPriority.IMPORTANT);
                    context.stopProcessing(this.getClass().getSimpleName());
                }

            }

        } else {
            metricReporterService.addStateModuleForEntity("NO_DESKTOP_ENV", "GicapodsIdToExchangeIdMapModule", "ALL");
        }

    }

    boolean doWeNeedToBidForPixelMatchingOpportunity(OpportunityContext context) {
        if (numberOfBidsForPixelMatchingPurposePerHour.get() >=
                numberOfBidsForPixelMatchingPurposePerHourLimit) {
            metricReporterService.addStateModuleForEntity(
                    "BIDDING_FOR_MATCHING_PIXEL_LIMIT_REACHED",
                    "GicapodsIdToExchangeIdMapModule",
                    "ALL");

            return false;
        }
        return true;
    }
    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        return false;
    }

}


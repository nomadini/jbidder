package com.dstillery.bidder.modules;
import java.util.concurrent.ConcurrentSkipListSet;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class BannedDevicesFilterModule extends BidderModule {

    private ConcurrentSkipListSet<String> bannedDevices;
    private ConcurrentSkipListSet<String> bannedIps;
    private ConcurrentSkipListSet<String> bannedMgrs10ms;

    private final MetricReporterService metricReporterService;
    public BannedDevicesFilterModule(ConcurrentSkipListSet<String> bannedDevices,
                                     ConcurrentSkipListSet<String> bannedIps,
                                     ConcurrentSkipListSet<String> bannedMgrs10ms,
                                     MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
        this.bannedDevices = bannedDevices;
        this.bannedIps = bannedIps;
        this.bannedMgrs10ms = bannedMgrs10ms;
    }

    public void process(OpportunityContext context) {
        if (context.getDevice() == null) {
            context.stopProcessing(this.getClass().getSimpleName());
            return;
        }
        if (bannedDevices.contains(context.getDevice().getDeviceId())) {
            metricReporterService.addStateModuleForEntity("bannedDevicesSeen",
                    "BannedDevicesFilterModule",
                    "ALL");

            context.stopProcessing(this.getClass().getSimpleName());
        }

        if (bannedIps.contains(context.getDeviceIp())) {
            metricReporterService.addStateModuleForEntity("bannedIpsSeen",
                    "BannedDevicesFilterModule",
                    "ALL");

            context.stopProcessing(this.getClass().getSimpleName());
        }

        if (bannedMgrs10ms.contains(context.getMgrs10m())) {
            metricReporterService.addStateModuleForEntity("bannedMgrs10msSeen",
                    "BannedDevicesFilterModule",
                    "ALL");

            context.stopProcessing(this.getClass().getSimpleName());
        }

    }
    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }

}


package com.dstillery.bidder.modules;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.kafka.KafkaProducerService;
import com.dstillery.common.LoadPercentageBasedSampler;
import com.dstillery.common.TransAppMessage;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.net.HttpClientService;
import com.dstillery.common.targetgroup.TargetGroup;

public class ScoringFeederModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScoringFeederModule.class);

    private final MetricReporterService metricReporterService;
    private final KafkaProducerService kafkaProducerService;
    private final LoadPercentageBasedSampler sampler;
    private final AtomicBoolean enqueueForScoringModuleIsDisabled;
    private final boolean callScoringViaHttp;
    private final boolean waitForKafkaConfirmation;
    private final TransAppBuilderService transAppBuilderService;
    private final HttpClientService scoringCallerHttpService;
    public ScoringFeederModule(
                               KafkaProducerService kafkaProducerService,
                               TransAppBuilderService transAppBuilderService,
                               LoadPercentageBasedSampler sampler,
                               AtomicBoolean enqueueForScoringModuleIsDisabled,
                               HttpClientService scoringCallerHttpService,
                               boolean callScoringViaHttp,
                               boolean waitForKafkaConfirmation,
                               MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.waitForKafkaConfirmation = waitForKafkaConfirmation;
        this.kafkaProducerService = kafkaProducerService;
        this.transAppBuilderService = transAppBuilderService;
        this.sampler = sampler;
        this.enqueueForScoringModuleIsDisabled = enqueueForScoringModuleIsDisabled;
        this.callScoringViaHttp = callScoringViaHttp;
        this.scoringCallerHttpService = scoringCallerHttpService;
    }

    public void process(OpportunityContext context) throws IOException {
        if (enqueueForScoringModuleIsDisabled.get()) {
            metricReporterService.addStateModuleForEntity("DISABLED",
                    "ScoringFeederModule",
                    "ALL",
                    MetricReporterService.MetricPriority.EXCEPTION);
            return;
        }

        boolean isInSample = sampler.isPartOfSample();
        if (isInSample &&
                !context.getDevice().getDeviceId().equalsIgnoreCase(NomadiniDevice.UNMAPPED_USER)) {

            TransAppMessage msg = transAppBuilderService.constructTransAppMessage (context);
            callScoring(msg);
            metricReporterService.addStateModuleForEntity("MSG_PUT_TO_KAFKA_QUEUE",
                    "ScoringFeederModule",
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);
        } else {
            metricReporterService.addStateModuleForEntity("REQUEST_DOESNT_BELONG_TO_SAMPLE",
                    "ScoringFeederModule",
                    "ALL");

        }
    }

    private void callScoring(TransAppMessage msg) throws IOException {
        if (callScoringViaHttp) {
            scoringCallerHttpService.sendPostRequestWithJsonBody("/scoring/message",
                    OBJECT_MAPPER.writeValueAsString(msg),
                    HttpClientService.PathOption.RELATIVE);
        } else {
            kafkaProducerService.send(msg.getDevice().getDeviceId(),
                    OBJECT_MAPPER.writeValueAsString(msg),
                    waitForKafkaConfirmation);
        }
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }
}


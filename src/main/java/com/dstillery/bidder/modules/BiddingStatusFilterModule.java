package com.dstillery.bidder.modules;

import static com.dstillery.common.targetgroup.BiddingStatus.RUNNING;
import static com.dstillery.common.util.StringUtil.toStr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.dstillery.common.campaign.Campaign;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class BiddingStatusFilterModule extends BidderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(BiddingStatusFilterModule.class);
    private CampaignCacheService campaignCacheService;
    private final MetricReporterService metricReporterService;

    public BiddingStatusFilterModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }

    public void process(OpportunityContext context) {
        metricReporterService.addStateModuleForEntity("sizeOfTargetGroup"  + toStr(context.getSizeOfUnmarkedTargetGroups()),
                "BiddingStatusFilterModule",
                "ALL");

        markFilteredTgs(context);
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {

        Campaign campaign = campaignCacheService.findByEntityId(tg.getCampaignId());
        if (isTgRunning(tg) && isCampaignRunning(campaign)) {
            metricReporterService.addStateModuleForEntity("PASSED",
                    "BiddingStatusFilterModule", tg.getId());
            return false;
        }

        LOGGER.debug("tg is filtered : {}, {}, campaign : {}", tg.getId(), tg.getMetaData(), campaign.getMetaData());
        return true;
    }

    private boolean isCampaignRunning(Campaign campaign) {
        return campaign.getMetaData().getBiddingStatus().equals(RUNNING);
    }

    private boolean isTgRunning(TargetGroup tg) {
        return tg.getMetaData().getBiddingStatus().equals(RUNNING);
    }

    @Autowired
    public void setCampaignCacheService(CampaignCacheService campaignCacheService) {
        this.campaignCacheService = campaignCacheService;
    }
}


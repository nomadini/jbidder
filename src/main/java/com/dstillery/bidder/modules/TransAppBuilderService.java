package com.dstillery.bidder.modules;

import java.util.List;

import com.dstillery.common.TransAppMessage;
import com.dstillery.common.adhistory.AdEntryDevicePair;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.eventlog.UniqueDeviceBidEventLog;
import com.google.common.collect.ImmutableList;

public class TransAppBuilderService {


    public TransAppMessage constructTransAppMessage(UniqueDeviceBidEventLog deviceBidEventLog, List<AdEntryDevicePair> adEntryDevicePair) {
        TransAppMessage msg = new TransAppMessage();
        EventLog context = deviceBidEventLog.getEventLog();

        msg.setTransactionId(context.getEventId());
        if (context.getTargetGroupId() != 0) {
            msg.setChosenTargetGroupId(context.getTargetGroupId());
        }
        if (context.getCreativeId() != 0) {
            msg.setChosenCreativeId(context.getCreativeId());
        }
        msg.setAdEntryDevicePair(adEntryDevicePair);

        msg.setBidPrice(context.getBidPrice());

        msg.setUserTimeZone(context.getUserTimeZone());
        msg.setUserTimeZonDifferenceWithUTC(context.getUserTimeZonDifferenceWithUTC());

        msg.setDeviceUserAgent(context.getDeviceUserAgent());
        msg.setDeviceIp(context.getDeviceIp());
        msg.setDevice(context.getDevice());

        msg.setDeviceLat(context.getDeviceLat());
        msg.setDeviceLon(context.getDeviceLon());
        msg.setDeviceCountry(context.getDeviceCountry());
        msg.setDeviceCity(context.getDeviceCity());
        msg.setDeviceState(context.getDeviceState());
        msg.setDeviceZipcode(context.getDeviceZipcode());
        msg.setSiteCategory(context.getSiteCategory());
        msg.setSiteDomain(context.getSiteDomain());
        msg.setSitePage(context.getSitePage());
        msg.setSitePublisherDomain(context.getSitePublisherDomain());
        msg.setSitePublisherName(context.getSitePublisherName());

        msg.setMgrs1km(context.getMgrs1km());
        msg.setDeviceSegmentReadHappened(false);
        msg.setMgrs100m(context.getMgrs100m());
        msg.setMgrs10m(context.getMgrs10m());

        return msg;
    }

    public TransAppMessage constructTransAppMessage(OpportunityContext context) {
        //LOGGER.debug("constructing trans app message";
        TransAppMessage msg = new TransAppMessage();
        msg.setTransactionId(context.getTransactionId());
        if (context.getChosenTargetGroup() != null) {
            msg.setChosenTargetGroupId(context.getChosenTargetGroup().getId());
        }
        if (context.getChosenCreative() != null) {
            msg.setChosenCreativeId(context.getChosenCreative().getId());
        }
        msg.setAdEntryDevicePair(context.getAdHistory());

        msg.setBidPrice(context.getBidPrice());

        msg.setUserTimeZone(context.getUserTimeZone());
        msg.setUserTimeZonDifferenceWithUTC(context.getUserTimeZonDifferenceWithUTC());

        msg.setDeviceUserAgent(context.getDeviceUserAgent());
        msg.setDeviceIp(context.getDeviceIp());
        msg.setDevice(context.getDevice());

        msg.setDeviceLat(context.getDeviceLat());
        msg.setDeviceLon(context.getDeviceLon());
        msg.setDeviceCountry(context.getDeviceCountry());
        msg.setDeviceCity(context.getDeviceCity());
        msg.setDeviceState(context.getDeviceState());
        msg.setDeviceZipcode(context.getDeviceZipcode());
        msg.setSiteCategory(context.getSiteCategory());
        msg.setSiteDomain(context.getSiteDomain());
        msg.setSitePage(context.getSitePage());
        msg.setSitePublisherDomain(context.getSitePublisherDomain());
        msg.setSitePublisherName(context.getSitePublisherName());

        msg.setMgrs1km(context.getMgrs1km());
        msg.setDeviceSegmentReadHappened(context.getDeviceSegmentReadHappened());
        msg.setMgrs100m(context.getMgrs100m());
        msg.setMgrs10m(context.getMgrs10m());

        return msg;
    }
}

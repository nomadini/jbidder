package com.dstillery.bidder.modules;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetGroupFilterStatistic;

abstract public class TgMarker {
    public static final String FREQUNT_MARKER = "FREQUENT_MARKER";
    public static final String NON_FREQUENT_FILTER = "NON_FREQUENT_FILTER";
    private static Map<String, TargetGroupFilterStatistic> filterNameToFailureCounts;
    public String markerType;

    public TgMarker() {
    }

    public static void setFilterNameToFailureCounts(Map<String, TargetGroupFilterStatistic> filterNameToFailureCounts) {
        TgMarker.filterNameToFailureCounts = filterNameToFailureCounts;
    }

    abstract protected boolean filterTargetGroup(TargetGroup tg, OpportunityContext context);

    public void afterMarking(OpportunityContext context) {

    }

    public void markFilteredTgs(OpportunityContext context) {

        for (TargetGroup tg : context.getAllAvailableTgs()) {
            if (context.getTargetGroupIdsToRemove().get(tg.getId()) == null) {
                Boolean filtered = filterTargetGroup(tg, context);
                if (filtered) {
                    context.getTargetGroupIdsToRemove().put(tg.getId(), tg);
                }
                recordFilterCount(filtered, tg.getId(), this.getClass().getSimpleName());
            }
        }

        if (context.getSizeOfUnmarkedTargetGroups() == 0) {
            context.stopProcessing(this.getClass().getSimpleName());
        }
    }

    public static List<TargetGroup>
    getPassingTargetGroups(
            OpportunityContext context) {
        List<TargetGroup> passingTargetGroups =
                new ArrayList<>();

        for (TargetGroup tg : context.getAllAvailableTgs()) {
            if (context.getTargetGroupIdsToRemove().get(tg.getId()) == null) {
                //tg is a good one
                passingTargetGroups.add(tg);
            }
        }
        return passingTargetGroups;
    }

    private void recordFilterCount(Boolean filterTg,
                                   String entityId,
                                   String entityType,
                                   String nameOfTgMarker) {
        // //LOGGER.debug("adding filter count for tgId : " ,entityId , " , tgMarker : {} ",  nameOfTgMarker , " , filterTg : {} ",  filterTg;

        String entitySpecificKey = entityId + "___" + entityType  + "___" + nameOfTgMarker;

        TargetGroupFilterStatistic filterStats1 = filterNameToFailureCounts.computeIfAbsent(
                entitySpecificKey, key -> new TargetGroupFilterStatistic());
        if (filterStats1 == null) {
            filterStats1 = new TargetGroupFilterStatistic();
            filterStats1.setNumberOfFailures(new AtomicLong (0));
            filterStats1.setNumberOfPasses(new AtomicLong (0));
            filterNameToFailureCounts.put(entitySpecificKey, filterStats1);
        }

        if (filterTg) {
            filterStats1.getNumberOfFailures().getAndIncrement();
        } else {
            filterStats1.getNumberOfPasses().getAndIncrement();
        }
        //now adding to ALL key, because recording how a filter did for all target groups is very important

        String allKey = "ALL" + "___" + entityType  + "___" + nameOfTgMarker;
        TargetGroupFilterStatistic filterStats = filterNameToFailureCounts.get(allKey);
        if (filterStats == null) {

            filterStats = new TargetGroupFilterStatistic();
            filterStats.setNumberOfFailures(new AtomicLong (0));
            filterStats.setNumberOfPasses(new AtomicLong (0));
            filterNameToFailureCounts.put(allKey, filterStats);
        }

        if (filterTg) {
            filterStats.getNumberOfFailures().getAndIncrement();
        } else {
            filterStats.getNumberOfPasses().getAndIncrement();
        }

    }

    private void recordFilterCount(Boolean filterTg, Long targetGroupId, String nameOfTgMarker) {
        recordFilterCount(filterTg, String.valueOf(targetGroupId), "tg", nameOfTgMarker);
    }

    public void beforeMarking(OpportunityContext context) {

    }

    public static Map<String, TargetGroupFilterStatistic> getFilterNameToFailureCounts() {
//        if (filterNameToFailureCounts == null) {
//            filterNameToFailureCounts = new ConcurrentHashMap<>();
//        }
        return filterNameToFailureCounts;
    }
}

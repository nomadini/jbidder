package com.dstillery.bidder.modules;/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */

import java.io.IOException;

import com.dstillery.bidder.contexts.OpportunityContextBuilder;
import com.dstillery.common.targetgroup.TargetGroup;

public class ContextPopulatorModule extends BidderModule {

    private OpportunityContextBuilder opportunityContextBuilder;

    public ContextPopulatorModule(OpportunityContextBuilder opportunityContextBuilder) {

        this.opportunityContextBuilder = opportunityContextBuilder;
    }

    public void process(OpportunityContext context) throws IOException {
        opportunityContextBuilder.configureContextFromBidRequest
                        (context,
                        context.getOpenRtbVersion(),
                        context.getExchangeName());

    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        return false;
    }

}


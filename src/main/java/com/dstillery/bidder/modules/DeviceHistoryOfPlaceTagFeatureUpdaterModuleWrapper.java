package com.dstillery.bidder.modules;

import static com.dstillery.common.util.StringUtil.toStr;

import com.dstillery.common.common_modules.FeatureDeviceHistoryUpdaterModule;
import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;

public class DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper extends BidderModule {
    private FeatureType featureCategory;
    private FeatureDeviceHistoryUpdaterModule featureDeviceHistoryUpdaterModule;

    private final MetricReporterService metricReporterService;
    public DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper(
            FeatureType featureCategory,
            FeatureDeviceHistoryUpdaterModule placeTagFeatureDeviceHistoryUpdaterModule,
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;

        this.featureCategory = featureCategory;
        this.featureDeviceHistoryUpdaterModule = placeTagFeatureDeviceHistoryUpdaterModule;
    }

    public void process(OpportunityContext context) {

        if (context.getDevice() == null) {
            return;
        }

        if (context.getTagIds().isEmpty()) {
            metricReporterService.addStateModuleForEntity("SKIPPING_BECAUSEOF_UNKNOWN_TAG",
                    "PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper",
                    "ALL");
            return;
        }

        for (Long tagId : context.getTagIds()) {
            try{
                featureDeviceHistoryUpdaterModule.process(featureCategory,
                        toStr(tagId),
                        context.getDevice());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        metricReporterService.addStateModuleForEntity("ADDING_FEATURE_FEATURE_HISTORY ",
                "DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper ",
                "ALL ");

    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        return false;
    }

}


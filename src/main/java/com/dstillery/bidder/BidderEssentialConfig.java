package com.dstillery.bidder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BidderEssentialConfig {

    @Bean
    public String appPropertyFileName() {
        return "bidder.properties";
    }
}

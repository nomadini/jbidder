package com.dstillery.bidder;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.modules.ActiveAndCurrentFilterModule;
import com.dstillery.bidder.modules.BiddingStatusFilterModule;
import com.dstillery.bidder.modules.TargetGroupWithoutCreativeFilterModule;
import com.dstillery.bidder.modules.TargetGroupWithoutSegmentFilterModule;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.bidder.modules.budgetcontrol.capping.CampaignBudgetCappingFilterModule;
import com.dstillery.bidder.modules.budgetcontrol.capping.CampaignImpressionCappingFilterModule;
import com.dstillery.bidder.modules.budgetcontrol.capping.TargetGroupBudgetCappingFilterModule;
import com.dstillery.bidder.modules.budgetcontrol.capping.TargetGroupImpressionCappingFilterModule;
import com.dstillery.bidder.modules.budgetcontrol.pacing.TargetGroupPacingByBudgetModule;
import com.dstillery.bidder.modules.budgetcontrol.pacing.TargetGroupPacingByImpressionModule;
import com.dstillery.bidder.modules.filters.DayPartFilter;
import com.dstillery.bidder.pipeline.DataReloadPipeline;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.targetgroup_maps.tgCreative.TargetGroupCreativeCacheService;
import com.dstillery.common.targetgroup.targetgroup_maps.tgDaypart.TargetGroupDayPartCacheService;

@Configuration
public class DataReloadConfig {

    @Autowired
    private  Map<String, TgMarker> allMarkersMap;

    @Autowired
    private MetricReporterService metricReporterService;

    @Bean
    public TargetGroupPacingByBudgetModule targetGroupPacingByBudgetModule() {
        TargetGroupPacingByBudgetModule targetGroupPacingByBudgetModule = new TargetGroupPacingByBudgetModule(metricReporterService);
        targetGroupPacingByBudgetModule.markerType = TgMarker.NON_FREQUENT_FILTER;
        allMarkersMap.put(targetGroupPacingByBudgetModule.getClass().getSimpleName(),
                targetGroupPacingByBudgetModule);

        return targetGroupPacingByBudgetModule;
    }

    @Bean
    public TargetGroupPacingByImpressionModule targetGroupPacingByImpressionModule() {
        TargetGroupPacingByImpressionModule targetGroupPacingByImpressionModule = new TargetGroupPacingByImpressionModule(metricReporterService);
        targetGroupPacingByImpressionModule.markerType = TgMarker.NON_FREQUENT_FILTER;
        allMarkersMap.put(targetGroupPacingByImpressionModule.getClass().getSimpleName(),
                targetGroupPacingByImpressionModule);
        return targetGroupPacingByImpressionModule;
    }

    @Bean
    public TargetGroupImpressionCappingFilterModule targetGroupImpressionCappingFilterModule() {
        TargetGroupImpressionCappingFilterModule targetGroupImpressionCappingFilterModule = new TargetGroupImpressionCappingFilterModule(metricReporterService);
        targetGroupImpressionCappingFilterModule.markerType = TgMarker.NON_FREQUENT_FILTER;
        allMarkersMap.put(targetGroupImpressionCappingFilterModule.getClass().getSimpleName(),
                targetGroupImpressionCappingFilterModule);
        return targetGroupImpressionCappingFilterModule;
    }

    @Bean
    public TargetGroupBudgetCappingFilterModule targetGroupBudgetCappingFilterModule() {
        TargetGroupBudgetCappingFilterModule targetGroupBudgetCappingFilterModule = new TargetGroupBudgetCappingFilterModule(metricReporterService);
        targetGroupBudgetCappingFilterModule.markerType = TgMarker.NON_FREQUENT_FILTER;
        allMarkersMap.put(targetGroupBudgetCappingFilterModule.getClass().getSimpleName(),
                targetGroupBudgetCappingFilterModule);
        return targetGroupBudgetCappingFilterModule;
    }

    @Bean
    public CampaignImpressionCappingFilterModule campaignImpressionCappingFilterModule(CampaignCacheService campaignCacheService) {
        CampaignImpressionCappingFilterModule campaignImpressionCappingFilterModule =
                new CampaignImpressionCappingFilterModule(campaignCacheService, metricReporterService);
        campaignImpressionCappingFilterModule.markerType = TgMarker.NON_FREQUENT_FILTER;
        allMarkersMap.put(campaignImpressionCappingFilterModule.getClass().getSimpleName(),
                campaignImpressionCappingFilterModule);

        return campaignImpressionCappingFilterModule;
    }

    @Bean
    public CampaignBudgetCappingFilterModule campaignBudgetCappingFilterModule(CampaignCacheService campaignCacheService) {
        CampaignBudgetCappingFilterModule campaignBudgetCappingFilterModule =
                new CampaignBudgetCappingFilterModule(campaignCacheService, metricReporterService);
        campaignBudgetCappingFilterModule.markerType = TgMarker.NON_FREQUENT_FILTER;
        allMarkersMap.put(campaignBudgetCappingFilterModule.getClass().getSimpleName(),
                campaignBudgetCappingFilterModule);
        return campaignBudgetCappingFilterModule;
    }

    @Bean
    public TargetGroupWithoutCreativeFilterModule targetGroupWithoutCreativeFilterModule(
                                                                                   TargetGroupCreativeCacheService targetGroupCreativeCacheService) {
        TargetGroupWithoutCreativeFilterModule targetGroupWithoutCreativeFilterModule =
                new TargetGroupWithoutCreativeFilterModule(targetGroupCreativeCacheService, metricReporterService);
        targetGroupWithoutCreativeFilterModule.markerType = TgMarker.NON_FREQUENT_FILTER;
        allMarkersMap.put(targetGroupWithoutCreativeFilterModule.getClass().getSimpleName(),
                targetGroupWithoutCreativeFilterModule);

        return targetGroupWithoutCreativeFilterModule;
    }

    @Bean
    public DayPartFilter dayPartFilter(
            TargetGroupDayPartCacheService targetGroupDayPartCacheService) {
        DayPartFilter dayPartFilter = new DayPartFilter(
                targetGroupDayPartCacheService, metricReporterService);
        dayPartFilter.markerType = TgMarker.NON_FREQUENT_FILTER;
        allMarkersMap.put(dayPartFilter.getClass().getSimpleName(), dayPartFilter);

        return dayPartFilter;
    }


    @Bean
    public ActiveAndCurrentFilterModule activeFilterModule() {
        ActiveAndCurrentFilterModule activeAndCurrentFilterModule = new ActiveAndCurrentFilterModule(metricReporterService);

        activeAndCurrentFilterModule.markerType = TgMarker.NON_FREQUENT_FILTER;
        allMarkersMap.put(activeAndCurrentFilterModule.getClass().getSimpleName(), activeAndCurrentFilterModule);

        return activeAndCurrentFilterModule;
    }

    @Bean
    public BiddingStatusFilterModule biddingStatusFilterModule() {
        BiddingStatusFilterModule biddingStatusFilterModule = new BiddingStatusFilterModule(metricReporterService);
        biddingStatusFilterModule.markerType = TgMarker.NON_FREQUENT_FILTER;
        allMarkersMap.put(biddingStatusFilterModule.getClass().getSimpleName(), biddingStatusFilterModule);
        return biddingStatusFilterModule;
    }

    @Bean
    public DataReloadPipeline dataReloadPipeline(
            EntityDeliveryInfoCacheService entityDeliveryInfoCacheService,
            TargetGroupCreativeCacheService targetGroupCreativeCacheService,
            CampaignCacheService campaignCacheService,
            ActiveAndCurrentFilterModule activeAndCurrentFilterModule,
            BiddingStatusFilterModule biddingStatusFilterModule,
            TargetGroupWithoutSegmentFilterModule targetGroupWithoutSegmentFilterModule,
            TargetGroupDayPartCacheService targetGroupDayPartCacheService,
             DayPartFilter dayPartFilter,
            CampaignBudgetCappingFilterModule campaignBudgetCappingFilterModule,
            CampaignImpressionCappingFilterModule campaignImpressionCappingFilterModule,
            TargetGroupWithoutCreativeFilterModule targetGroupWithoutCreativeFilterModule,
            TargetGroupImpressionCappingFilterModule targetGroupImpressionCappingFilterModule,
            TargetGroupBudgetCappingFilterModule targetGroupBudgetCappingFilterModule) {
        return new DataReloadPipeline(
                 entityDeliveryInfoCacheService,
                 targetGroupCreativeCacheService,
                 campaignCacheService,
                 activeAndCurrentFilterModule,
                 biddingStatusFilterModule,
                 targetGroupWithoutSegmentFilterModule,
                 targetGroupDayPartCacheService,
                 targetGroupPacingByBudgetModule(),
                 targetGroupPacingByImpressionModule(),
                 dayPartFilter,
                 campaignBudgetCappingFilterModule,
                 campaignImpressionCappingFilterModule,
                 targetGroupWithoutCreativeFilterModule,
                 targetGroupImpressionCappingFilterModule,
                 targetGroupBudgetCappingFilterModule,
                 metricReporterService);
    }
}

package com.dstillery.bidder.controller;

import com.codahale.metrics.MetricRegistry;
import com.dstillery.openrtb.beans.BidRequest;
import com.dstillery.openrtb.beans.BidResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OpenRtbController {
    public OpenRtbController(MetricRegistry influxMetrics) {

    }

    @PostMapping(value = "/openrtb")
    @ResponseBody
    public BidResponse processBidRequest(@RequestBody BidRequest request) {

        long t0 = System.currentTimeMillis();
        try {
            return new BidResponse();
        } catch (RuntimeException e) {
            // Re-throw the exception so that it will be handled by the ExceptionReporter.
            throw e;
        }
    }
}

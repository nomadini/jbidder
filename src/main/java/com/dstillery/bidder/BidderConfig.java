package com.dstillery.bidder;

import static com.dstillery.common.feature.FeatureType.GTLD;
import static com.dstillery.common.feature.FeatureType.MGRS100;
import static com.dstillery.common.feature.FeatureType.PLACE_TAG;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.codahale.metrics.MetricRegistry;
import com.dstillery.bidder.contexts.BidRequestParser;
import com.dstillery.bidder.contexts.BiddingMonitor;
import com.dstillery.bidder.contexts.IpInfoFetcherModule;
import com.dstillery.bidder.contexts.OpportunityContextBuilder;
import com.dstillery.bidder.contexts.OpportunityContextBuilderOpenRtb2_3;
import com.dstillery.bidder.delivery_info.EntityDeliveryInfoCacheService;
import com.dstillery.bidder.delivery_info.EntityDeliveryInfoFetcher;
import com.dstillery.bidder.delivery_info.MangoApiClient;
import com.dstillery.bidder.main.BidModeControllerService;
import com.dstillery.bidder.main.BidderAsyncJobsService;
import com.dstillery.bidder.main.BiddingMode;
import com.dstillery.bidder.main.asynctasks.FilterStatsPersistTask;
import com.dstillery.bidder.main.asynctasks.HourlyTask;
import com.dstillery.bidder.main.asynctasks.MinutelyTask;
import com.dstillery.bidder.main.asynctasks.ReloadBannedFilesTask;
import com.dstillery.bidder.modules.AsyncPipelineFeederAndInvokerModule;
import com.dstillery.bidder.modules.BannedDevicesFilterModule;
import com.dstillery.bidder.modules.BidEventRecorderModule;
import com.dstillery.bidder.modules.BidProbabilityEnforcerModule;
import com.dstillery.bidder.modules.BidResponseCreatorModule;
import com.dstillery.bidder.modules.BidderModule;
import com.dstillery.bidder.modules.BidderResponseModule;
import com.dstillery.bidder.modules.BidderWiretapModule;
import com.dstillery.bidder.modules.ContextPopulatorModule;
import com.dstillery.bidder.modules.CrossWalkUpdaterModuleWrapper;
import com.dstillery.bidder.modules.DeviceHistoryOfGeoFeatureUpdaterModuleWrapper;
import com.dstillery.bidder.modules.DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper;
import com.dstillery.bidder.modules.DeviceHistoryOfVisitFeatureUpdaterModuleWrapper;
import com.dstillery.bidder.modules.EventLogCreatorModule;
import com.dstillery.bidder.modules.FeatureToFeatureHistoryUpdaterModuleWrapper;
import com.dstillery.bidder.modules.GeoFeatureHistoryOfDeviceUpdaterModuleWrapper;
import com.dstillery.bidder.modules.GicapodsIdToExchangeIdMapModule;
import com.dstillery.bidder.modules.GlobalWhiteListModule;
import com.dstillery.bidder.modules.MGRSBuilderModule;
import com.dstillery.bidder.modules.NoBidModeEnforcerModule;
import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.bidder.modules.PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper;
import com.dstillery.bidder.modules.ProcessorInvokerModule;
import com.dstillery.bidder.modules.ScoringFeederModule;
import com.dstillery.bidder.modules.TargetGroupFrequencyCapModule;
import com.dstillery.bidder.modules.TargetGroupSegmentFilter;
import com.dstillery.bidder.modules.TargetGroupSelectorModule;
import com.dstillery.bidder.modules.TargetGroupWithoutSegmentFilterModule;
import com.dstillery.bidder.modules.TgMarker;
import com.dstillery.bidder.modules.ThrottlerModule;
import com.dstillery.bidder.modules.TopKeyWordsExtractorModule;
import com.dstillery.bidder.modules.TransAppBuilderService;
import com.dstillery.bidder.modules.VisitFeatureHistoryOfDeviceUpdaterModuleWrapper;
import com.dstillery.bidder.modules.budgetcontrol.capping.TargetingTypeLessTargetGroupFilter;
import com.dstillery.bidder.modules.filters.AdPositionFilter;
import com.dstillery.bidder.modules.filters.BlockedAdvertiserDomainFilter;
import com.dstillery.bidder.modules.filters.BlockedIabCategoryFilter;
import com.dstillery.bidder.modules.filters.DeviceTypeFilter;
import com.dstillery.bidder.modules.filters.DomainBlackListedFilter;
import com.dstillery.bidder.modules.filters.DomainWhiteListedFilter;
import com.dstillery.bidder.modules.filters.GeoFeatureFilter;
import com.dstillery.bidder.modules.filters.GeoLocationFilter;
import com.dstillery.bidder.modules.filters.MatchingIabCategoryFilter;
import com.dstillery.bidder.modules.filters.OsTypeFilter;
import com.dstillery.bidder.modules.filters.TargetGroupBudgetFilterChainModule;
import com.dstillery.bidder.modules.filters.TargetGroupFilterChainModule;
import com.dstillery.bidder.modules.filters.creativefilters.BlockedBannerAdTypeFilter;
import com.dstillery.bidder.modules.filters.creativefilters.BlockedCreativeAttributeFilter;
import com.dstillery.bidder.modules.filters.creativefilters.CreativeBannerApiFilter;
import com.dstillery.bidder.modules.filters.creativefilters.CreativeContentTypeFilter;
import com.dstillery.bidder.modules.filters.creativefilters.CreativeSizeFilter;
import com.dstillery.bidder.modules.frauddetection.BadDeviceByCountFilterModule;
import com.dstillery.bidder.modules.frauddetection.BadDeviceByCountPopulatorModule;
import com.dstillery.bidder.modules.frauddetection.BadIpByCountFilterModule;
import com.dstillery.bidder.modules.frauddetection.BadIpByCountPopulatorModule;
import com.dstillery.bidder.modules.frauddetection.LastXSecondBidOnDeviceModule;
import com.dstillery.bidder.modules.frauddetection.LastXSecondSeenIpVisitorModule;
import com.dstillery.bidder.modules.healthcheck.AdServerHealthChecker;
import com.dstillery.bidder.modules.pricing.BidPriceCalculatorModule;
import com.dstillery.bidder.pipeline.BidRequestHandlerPipelineProcessor;
import com.dstillery.bidder.pipeline.BidderAsyncPipelineProcessor;
import com.dstillery.bidder.pipeline.BidderMainPipelineProcessor;
import com.dstillery.bidder.pipeline.FeatureRecorderPipeline;
import com.dstillery.bidder.requesthandling.BidRequestHandler;
import com.dstillery.bidder.requesthandling.BidderStatusService;
import com.dstillery.bidder.responses.OpenRtb2_3_0BidResponseBuilder;
import com.dstillery.common.aerospike.AeroSpikeDriver;
import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.kafka.KafkaProducerService;
import com.dstillery.common.kafka.NoOpPartitioner;
import com.dstillery.common.partnermap.NomadiniIdToExchangeIdsMapCassandraService;
import com.dstillery.common.LoadPercentageBasedSampler;
import com.dstillery.common.PlacesCachedResult;
import com.dstillery.common.adhistory.AdEntryDevicePair;
import com.dstillery.common.advertiser.Advertiser;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.cache_realtime.RealTimeEntityCacheService;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.client.Client;
import com.dstillery.common.common_modules.CrossWalkUpdaterModule;
import com.dstillery.common.common_modules.DeviceHistoryUpdaterModule;
import com.dstillery.common.common_modules.FeatureDeviceHistoryUpdaterModule;
import com.dstillery.common.common_modules.FeatureGuardService;
import com.dstillery.common.common_modules.FeatureToFeatureHistoryUpdaterModule;
import com.dstillery.common.config.CommonModulesConfig;
import com.dstillery.common.creative.CreativeCacheService;
import com.dstillery.common.device.DeviceIdService;
import com.dstillery.common.eventlog.BidEventLog;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.eventlog.UniqueDeviceBidEventLog;
import com.dstillery.common.geo.GeoLocationCacheService;
import com.dstillery.common.geo.LatLonToMGRSConverter;
import com.dstillery.common.geo.PlaceTreeContainer;
import com.dstillery.common.geo.places.PlaceFinderModule;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.modeling.featuredevice.FeatureDeviceHistoryCassandraService;
import com.dstillery.common.modeling.featuredevice.FeatureToFeatureHistoryCassandraService;
import com.dstillery.common.net.HttpClientService;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.common.whiteListedBiddingDomains.GlobalWhiteListedCacheService;
import com.dstillery.common.whitelistedModelingDomains.GlobalWhiteListedModelCacheService;
import com.dstillery.common.wiretap.WiretapCassandraService;
import com.dstillery.common.recency.DeviceCounterService;
import com.dstillery.common.recency.DeviceIpCounterService;
import com.dstillery.common.recency.LastTimeIpSeenSource;
import com.dstillery.common.recency.LastTimeSeenSource;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.targetgroup.MySqlTargetGroupFilterCountDbRecordService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.targetgroup.TargetGroupCacheService;
import com.dstillery.common.targetgroup.bidding_performance_metric.models.TgBiddingPerformanceFilterMeasures;
import com.dstillery.common.targetgroup.targetgroup_maps.tgBwlist.TargetGroupBWListCacheService;
import com.dstillery.common.targetgroup.targetgroup_maps.tgCreative.TargetGroupCreativeCacheService;
import com.dstillery.common.targetgroup.targetgroup_maps.tgGeolocation.TargetGroupGeoLocationCacheService;
import com.google.common.collect.ImmutableList;
import com.google.common.eventbus.EventBus;

@Configuration
@Import({
        CommonModulesConfig.class
})
public class BidderConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidderConfig.class);

    @Value("${info.app.version}")
    private String appVersion;

    @Autowired
    private CacheService<Client> clientCacheService;

    @Autowired
    private MetricRegistry influxMetrics;

    @Autowired
    private DiscoveryService discoveryService;

    @Autowired
    private CacheService<Advertiser> advertiserCacheService;

    @Autowired
    private AtomicBoolean isReloadProcessHealthy;

    @Autowired
    private CampaignCacheService campaignCacheService;

    @Autowired
    private AeroCacheService<EventLog> realTimeEventLogCacheService;

    @Autowired
    private ConfigService configService;

    @Autowired
    public FeatureGuardService featureGuardService;

    private Set<String> mapOfInterestingFeatures = new ConcurrentSkipListSet<>();

    private AtomicLong totalNumberOfRequestsProcessedInFilterCountPeriod = new AtomicLong();

    private AtomicLong numberOfRequestsProcessingNow = new AtomicLong();

    private ConcurrentSkipListSet<String> bannedDevices = new ConcurrentSkipListSet<>();

    private ConcurrentSkipListSet<String> bannedIps = new ConcurrentSkipListSet<>();

    private ConcurrentSkipListSet<String> bannedMgrs10ms = new ConcurrentSkipListSet<>();

    private List<TargetGroup> targetGroupsEligibleForBidding = new ArrayList<>();
    private Set<String> targetGroupToGeoCollectionKeys = new HashSet<>();

    private AtomicLong numberOfExceptionsInWritingEvents = new AtomicLong();
    private AtomicBoolean isReloadingDataInProgress = new AtomicBoolean();
    private AtomicLong sizeOfKafkaScoringQueue = new AtomicLong();
    private AtomicLong queueSizeWaitingForAck = new AtomicLong();
    private AtomicLong numberOfBidsForPixelMatchingPurposePerHour = new AtomicLong();
    private AtomicLong uniqueNumberOfContext = new AtomicLong();
    private AtomicLong numberOfCuncurrentReuqestsNow = new AtomicLong();

    private Map<Long, TgBiddingPerformanceFilterMeasures> targetGroupIdToFilterMeasuresMap = new ConcurrentHashMap<>();

    @Autowired
    private TargetGroupCacheService targetGroupCacheService;

    @Bean
    public AtomicBoolean isNoBidModeIsTurnedOn() {
        return new AtomicBoolean();
    }

    private AtomicLong numberOfBidRequestRecievedPerMinute = new AtomicLong();

    @Autowired
    private AeroSpikeDriver aeroSpikeDriver;

    @Autowired
    private RealTimeEntityCacheService<AdEntryDevicePair> adhistoryCacheService;

    public AtomicBoolean forceRefreshCachesFlag = new AtomicBoolean(false);
    private AtomicBoolean entityDeliveryInfoCacheServiceIsHealthy = new AtomicBoolean(false);

    private AtomicLong numberOfExceptionsInWritingMessagesInKafka = new AtomicLong();//TODO add this to scoring

    static {
        TgMarker.setFilterNameToFailureCounts(new ConcurrentHashMap<>());
    }

    @Bean
    public Map<String, TgMarker> allMarkersMap() {
        return  new ConcurrentHashMap<>();
    }

    @Bean
    public AtomicLong bidPercentage() {
        AtomicLong bidPercentage = new AtomicLong();
        bidPercentage.set(configService.getAsInt("bidPercentage"));
        return bidPercentage;
    }

    @Bean
    public TargetGroupFilterChainModule targetGroupFilterChainModule(
            MetricReporterService metricReporterService,
            GeoFeatureFilter geoFeatureFilter,
            DomainBlackListedFilter domainBlackListedFilter,
            MatchingIabCategoryFilter matchingIabCategoryFilter,
            DomainWhiteListedFilter domainWhiteListedFilter,
            CreativeBannerApiFilter creativeBannerApiFilter,
            CreativeContentTypeFilter creativeContentTypeFilter,
            AdPositionFilter adPositionFilter,
            BlockedIabCategoryFilter blockedIabCategoryFilter,
            CreativeSizeFilter creativeSizeFilter,
            BlockedBannerAdTypeFilter blockedBannerAdTypeFilter,
            BlockedCreativeAttributeFilter blockedCreativeAttributeFilter,
            DeviceTypeFilter deviceTypeFilter,
            OsTypeFilter osTypeFilter,
            GeoLocationFilter geoLocationFilter,
            BlockedAdvertiserDomainFilter blockedAdvertiserDomainFilter

    ) {
        return new TargetGroupFilterChainModule (
                metricReporterService,
                geoFeatureFilter,
                domainBlackListedFilter,
                matchingIabCategoryFilter,
                domainWhiteListedFilter,
                creativeBannerApiFilter,
                creativeContentTypeFilter,
                adPositionFilter,
                blockedIabCategoryFilter,
                creativeSizeFilter,
                blockedBannerAdTypeFilter,
                blockedCreativeAttributeFilter,
                deviceTypeFilter,
                osTypeFilter,
                geoLocationFilter,
                blockedAdvertiserDomainFilter,
                configService
        );
    }

    @Bean
    public DeviceHistoryUpdaterModule deviceFeatureHistoryUpdaterModule(
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService,
            FeatureGuardService featureGuardService,
            DeviceIdService deviceIdService,
            MetricReporterService metricReporterService) {

        return new DeviceHistoryUpdaterModule(
                deviceIdService,
                featureGuardService,
                new LoadPercentageBasedSampler(
                        configService.getAsInt("percentageOfRecordingDeviceWebVisitDataFeatures"),
                        "percentageOfRecordingDeviceWebVisitDataFeatures"
                ),
                deviceFeatureHistoryCassandraService,
                metricReporterService
        );

    }
    @Bean
    public DeviceHistoryUpdaterModule deviceGeoFeatureUpdaterModule(
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService,
            FeatureGuardService featureGuardService,
            DeviceIdService deviceIdService,
            MetricReporterService metricReporterService) {
        return new DeviceHistoryUpdaterModule(
                deviceIdService,
                featureGuardService,
                new LoadPercentageBasedSampler(
                        configService.getAsInt("percentageOfRecordingDeviceGeoVisitDataFeatures"),
                        "percentageOfRecordingDeviceGeoVisitDataFeatures"
                ),
                deviceFeatureHistoryCassandraService,
                metricReporterService
        );

    }

    @Bean
    public DeviceHistoryUpdaterModule devicePlaceTagFeatureUpdaterModule(
            DeviceIdService deviceIdService,
            FeatureGuardService featureGuardService,
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService,
            MetricReporterService metricReporterService) {
        return new DeviceHistoryUpdaterModule(
                deviceIdService,
                featureGuardService,
                new LoadPercentageBasedSampler(
                        configService.getAsInt("percentageOfRecordingDevicePlaceTagFeatures"),
                        "percentageOfRecordingDevicePlaceTagFeatures"
                ),
                deviceFeatureHistoryCassandraService,
                metricReporterService
        );
    }

    @Bean
    public GeoFeatureHistoryOfDeviceUpdaterModuleWrapper geoFeatureHistoryOfDeviceUpdaterModuleWrapper(
            DeviceHistoryUpdaterModule deviceGeoFeatureUpdaterModule,
            MetricReporterService metricReporterService) {
        return new GeoFeatureHistoryOfDeviceUpdaterModuleWrapper(
                MGRS100,
                deviceGeoFeatureUpdaterModule,
                metricReporterService);
    }

    @Bean
    public PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper(
            DeviceHistoryUpdaterModule devicePlaceTagFeatureUpdaterModule,
            MetricReporterService metricReporterService) {
        return new PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper(
                PLACE_TAG,
                devicePlaceTagFeatureUpdaterModule,
                metricReporterService);
    }

    @Bean
    public DeviceHistoryOfVisitFeatureUpdaterModuleWrapper
    deviceHistoryOfVisitFeatureUpdaterModuleWrapper(
            FeatureGuardService featureGuardService,
            FeatureDeviceHistoryCassandraService featureDeviceHistoryCassandraService,
            MetricReporterService metricReporterService) {
        return new DeviceHistoryOfVisitFeatureUpdaterModuleWrapper(
                GTLD,
                visitFeatureDeviceHistoryUpdaterModule(
                        featureGuardService,
                        featureDeviceHistoryCassandraService,
                        metricReporterService),
                metricReporterService);

    }

    @Bean
    public DeviceHistoryOfGeoFeatureUpdaterModuleWrapper deviceHistoryOfGeoFeatureUpdaterModuleWrapper (
            FeatureDeviceHistoryUpdaterModule geoFeatureDeviceHistoryUpdaterModule,
            MetricReporterService metricReporterService) {
        return new DeviceHistoryOfGeoFeatureUpdaterModuleWrapper(
                MGRS100,
                geoFeatureDeviceHistoryUpdaterModule,
                metricReporterService);
    }

    @Bean
    public DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper deviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper(
            FeatureDeviceHistoryUpdaterModule placeTagFeatureDeviceHistoryUpdaterModule,
            MetricReporterService metricReporterService) {
        return new DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper(
                PLACE_TAG,
                placeTagFeatureDeviceHistoryUpdaterModule,
                metricReporterService);
    }

    @Bean
    public FeatureToFeatureHistoryUpdaterModuleWrapper domainFeatureToFeatureHistoryUpdaterModuleWrapper(
            FeatureToFeatureHistoryUpdaterModule domainFeatureToFeatureHistoryUpdaterModule,
            MetricReporterService metricReporterService) {
        return new FeatureToFeatureHistoryUpdaterModuleWrapper(domainFeatureToFeatureHistoryUpdaterModule, metricReporterService);
    }

    @Bean
    public CrossWalkUpdaterModuleWrapper crossWalkUpdaterModuleWrapper (
            CrossWalkUpdaterModule crossWalkUpdaterModule,
            MetricReporterService metricReporterService) {
        return new CrossWalkUpdaterModuleWrapper(crossWalkUpdaterModule, metricReporterService);
    }

    @Bean
    public BannedDevicesFilterModule bannedDevicesFilterModule(
            MetricReporterService metricReporterService) {
        return new BannedDevicesFilterModule(
                bannedDevices,
                bannedIps,
                bannedMgrs10ms,
                metricReporterService);
    }

    @Bean
    public TargetGroupWithoutSegmentFilterModule targetGroupWithoutSegmentFilterModule (MetricReporterService metricReporterService) {
        return new TargetGroupWithoutSegmentFilterModule(metricReporterService);
    }

    @Bean
    public BidPriceCalculatorModule bidPriceCalculatorModule(){
        return new BidPriceCalculatorModule ();
    }

    @Bean
    public TargetGroupBudgetFilterChainModule targetGroupBudgetFilterChainModule(
            EntityDeliveryInfoCacheService entityDeliveryInfoCacheService,
            MetricReporterService metricReporterService) {
        return new TargetGroupBudgetFilterChainModule(entityDeliveryInfoCacheService, metricReporterService);
    }

    @Bean
    public HttpClientService scoringCallerHttpService() {
        return new HttpClientService(configService.getAsInt("scoring.call.timeoutInMillis"), configService.get("scoring.host"));
    }

    @Bean
    public ScoringFeederModule enqueueForScoringModule(
            HttpClientService scoringCallerHttpService,
            KafkaProducerService scoringKafkaProducerService,
            MetricReporterService metricReporterService) {
        AtomicBoolean enqueueForScoringModuleIsDisabled = new AtomicBoolean(false);
        enqueueForScoringModuleIsDisabled.set(configService.getAsBoolean("enqueueForScoringModuleIsDisabled"));

        return new ScoringFeederModule(
                scoringKafkaProducerService,
                new TransAppBuilderService(),
                new LoadPercentageBasedSampler(
                        configService.getAsInt("scoring.percentOfRequestsToSend"),
                        "scoring.percentOfRequestsToSend"
                ),
                enqueueForScoringModuleIsDisabled,
                scoringCallerHttpService,
                configService.getAsBoolean("scoring.callViaHttp"),
                configService.getAsBoolean("scoring.waitForKafkaConfirmation"),
                metricReporterService
        );
    }

    @Bean
    public LastTimeSeenSource lastTimeSeenSource(AeroSpikeDriver aeroSpikeDriver) {
        return new LastTimeSeenSource(
                aeroSpikeDriver,
                "rtb2",
                "usersSeenSet6",
                "bidInLast1"
        );
    }

    @Bean
    public DeviceCounterService deviceCounterService(AeroSpikeDriver aeroSpikeDriver) {
        return new DeviceCounterService(
                aeroSpikeDriver,
                "rtb1",
                "deviceCount"
        );
    }

    @Bean
    public DeviceIpCounterService deviceIpCounterService(AeroSpikeDriver aeroSpikeDriver) {
        return new DeviceIpCounterService(
                aeroSpikeDriver,
                "rtb2",
                "deviceIpCount"
        );
    }

    @Bean
    public LastTimeIpSeenSource lastTimeIpSeenSource(AeroSpikeDriver aeroSpikeDriver) {
        return new LastTimeIpSeenSource(
                aeroSpikeDriver,
                "rtb2",
                "ipsSeenSet5",
                "bidInLastN"
        );
    }

    @Bean
    public BidderResponseModule bidderResponseModule(LastTimeSeenSource lastTimeSeenSource,
                                                     AeroCacheService<EventLog> realTimeEventLogCacheService,
                                                     MetricReporterService metricReporterService) {
        int acceptableIntervalToBidOnAUserInSeconds =
                configService.getAsInt("acceptableIntervalToBidOnAUserInSeconds");

        return new BidderResponseModule(
                lastTimeSeenSource,
                realTimeEventLogCacheService,
                acceptableIntervalToBidOnAUserInSeconds,
                metricReporterService
        );

    }

    @Bean
    public LastXSecondBidOnDeviceModule lastXSecondSeenVisitorModule(
            LastTimeSeenSource lastTimeSeenSource,
            DeviceCounterService deviceCounterService,
            MetricRegistry influxMetrics,
            MetricReporterService metricReporterService) {
        return new LastXSecondBidOnDeviceModule(
                lastTimeSeenSource,
                deviceCounterService,
                influxMetrics,
                configService.getAsInt("lastTimeSeenPeriodInSeconds"),
                metricReporterService);
    }

    @Bean
    public LastXSecondSeenIpVisitorModule lastXSecondSeenIpVisitorModule(LastTimeIpSeenSource lastTimeIpSeenSource,
                                                                         MetricRegistry influxMetrics,
                                                                         MetricReporterService metricReporterService) {
        return new LastXSecondSeenIpVisitorModule(
                lastTimeIpSeenSource,
                influxMetrics,
                configService.getAsInt("lastTimeIpSeenSourcePeriodInSeconds"),
                metricReporterService);
    }

    @Bean
    public BadIpByCountFilterModule badIpByCountFilterModule(
            LastTimeIpSeenSource lastTimeIpSeenSource,
            DeviceIpCounterService deviceIpCounterService,
            MetricRegistry influxMetrics,
            MetricReporterService metricReporterService) {
        return new BadIpByCountFilterModule(
                lastTimeIpSeenSource,
                deviceIpCounterService,
                influxMetrics,
                metricReporterService);
    }

    @Bean
    public BadDeviceByCountFilterModule badDeviceByCountFilterModule(
            DeviceCounterService deviceCounterService,
            MetricRegistry influxMetrics,
            MetricReporterService metricReporterService) {
        return new BadDeviceByCountFilterModule(
                deviceCounterService,
                influxMetrics,
                metricReporterService);
    }

    @Bean
    public BadIpByCountPopulatorModule badIpByCountPopulatorModule (
            DeviceIpCounterService deviceIpCounterService,
            MetricRegistry influxMetrics,
            MetricReporterService metricReporterService) {
        return new BadIpByCountPopulatorModule(
                deviceIpCounterService,
                influxMetrics,
                metricReporterService);
    }

    @Bean
    public AsyncPipelineFeederAndInvokerModule asyncPipelineFeederModule(
            BlockingQueue<OpportunityContext> queueOfContexts,
            BidderAsyncPipelineProcessor bidderAsyncPipelineProcessor,
            MetricReporterService metricReporterService) {

        return new AsyncPipelineFeederAndInvokerModule(
                bidderAsyncPipelineProcessor,
                queueOfContexts,
                metricReporterService);
    }

    @Bean
    public OpenRtb2_3_0BidResponseBuilder openRtb2_3_0BidResponseBuilder(
            CreativeCacheService creativeCacheService,
            MetricReporterService metricReporterService) {

        return new OpenRtb2_3_0BidResponseBuilder(
                discoveryService,
                advertiserCacheService,
                creativeCacheService,
                campaignCacheService,
                metricReporterService);

    }

    @Bean
    public TargetingTypeLessTargetGroupFilter targetingTypeLessTargetGroupFilter(
            MetricReporterService metricReporterService) {
        return new TargetingTypeLessTargetGroupFilter(metricReporterService);

    }

    @Bean
    public BidResponseCreatorModule bidResponseCreatorModule(
            OpenRtb2_3_0BidResponseBuilder openRtb2_3_0BidResponseBuilder,
            MetricReporterService metricReporterService) {
        return new BidResponseCreatorModule(openRtb2_3_0BidResponseBuilder, metricReporterService);
    }

    @Bean
    public HourlyTask hourlyTask(
            MetricReporterService metricReporterService) {
        return new HourlyTask(
                numberOfBidsForPixelMatchingPurposePerHour,
                metricReporterService
        );
    }

    @Bean
    public BidderAsyncPipelineProcessor bidderAsyncPipelineProcessor(
            FeatureRecorderPipeline featureRecorderPipeline,
            ScoringFeederModule scoringFeederModule,
            BannedDevicesFilterModule bannedDevicesFilterModule,
            BadIpByCountPopulatorModule badIpByCountPopulatorModule,
            BadDeviceByCountPopulatorModule badDeviceByCountPopulatorModule,
            BidEventRecorderModule bidEventRecorderModule,
            CrossWalkUpdaterModuleWrapper crossWalkUpdaterModuleWrapper,
            MetricReporterService metricReporterService,
            ConfigService configService,
            BlockingQueue<OpportunityContext> queueOfContexts,
            BidderWiretapModule bidderWiretapModule) {
        return new BidderAsyncPipelineProcessor(
                featureRecorderPipeline,
                scoringFeederModule,
                bannedDevicesFilterModule,
                badIpByCountPopulatorModule,
                badDeviceByCountPopulatorModule,
                bidEventRecorderModule,
                crossWalkUpdaterModuleWrapper,
                bidderWiretapModule,
                metricReporterService,
                configService,
                queueOfContexts
        );
    }

    @Bean
    public MinutelyTask minutelyTask(
            BidModeControllerService bidModeControllerService,
            MetricReporterService metricReporterService) {
        return new MinutelyTask(
                bidModeControllerService,
                numberOfExceptionsInWritingEvents,
                numberOfBidRequestRecievedPerMinute,
                metricReporterService);
    }

    @Bean
    public ReloadBannedFilesTask reloadBannedFilesTask() {
        return new ReloadBannedFilesTask(bannedDevices, bannedIps, bannedMgrs10ms);
    }

    @Bean
    public MangoApiClient mangoApiClient() {
        return new MangoApiClient(discoveryService);
    }

    @Bean
    public EntityDeliveryInfoFetcher entityDeliveryInfoFetcher(
            MangoApiClient mangoApiClient,
            EntityDeliveryInfoCacheService entityDeliveryInfoCacheService,
            MetricReporterService metricReporterService) {
        return new EntityDeliveryInfoFetcher(
                entityDeliveryInfoCacheService,
                entityDeliveryInfoCacheServiceIsHealthy,
                mangoApiClient,
                metricReporterService
        );
    }

    @Bean
    public PlaceFinderModule placeFinderModule(PlaceTreeContainer placeTreeContainer) {
        return new PlaceFinderModule(placeTreeContainer);
    }

    @Bean
    public BidderAsyncJobsService bidderAsyncJobsService(
            EntityDeliveryInfoFetcher entityDeliveryInfoFetcher,
            EventBus eventBus,
            TargetGroupCacheService targetGroupCacheService,
            ConfigService configService,
            GlobalWhiteListedModelCacheService globalWhiteListedModelCacheService,
            MetricReporterService metricReporterService) {
        BidderAsyncJobsService bidderAsyncJobsService = new BidderAsyncJobsService(
                eventBus,
                mapOfInterestingFeatures,
                entityDeliveryInfoFetcher,
                targetGroupCacheService,
                configService,
                globalWhiteListedModelCacheService,
                targetGroupsEligibleForBidding,
                isReloadingDataInProgress,
                forceRefreshCachesFlag,
                metricReporterService
        );
        return bidderAsyncJobsService;
    }

    @Bean
    public FilterStatsPersistTask filterStatsPersistTask(Map<String, TgMarker> allMarkersMap,
                                                         MySqlTargetGroupFilterCountDbRecordService mySqlTargetGroupFilterCountDbRecordService,
                                                         MetricReporterService metricReporterService) {
        return new FilterStatsPersistTask(
                allMarkersMap,
                mySqlTargetGroupFilterCountDbRecordService,
                totalNumberOfRequestsProcessedInFilterCountPeriod,
                metricReporterService
        );
    }

    @Bean
    public BidderMainPipelineProcessor bidderMainPipelineProcessor(
            MGRSBuilderModule mgrsBuilderModule,
            LastXSecondBidOnDeviceModule lastXSecondBidOnDeviceModule,
            LastXSecondSeenIpVisitorModule lastXSecondSeenIpVisitorModule,
            BadIpByCountFilterModule badIpByCountFilterModule,
            BadDeviceByCountFilterModule badDeviceByCountFilterModule,
            TargetGroupBudgetFilterChainModule targetGroupBudgetFilterChainModule,
            BidPriceCalculatorModule bidPriceCalculatorModule,
            TargetingTypeLessTargetGroupFilter targetingTypeLessTargetGroupFilter,
            TargetGroupSelectorModule targetGroupSelectorModule,
            EventLogCreatorModule eventLogCreatorModule,
            AsyncPipelineFeederAndInvokerModule asyncPipelineFeederAndInvokerModule,
            GicapodsIdToExchangeIdMapModule gicapodsIdToExchangeIdMapModule,
            GlobalWhiteListModule globalWhiteListModule,
            TargetGroupSegmentFilter targetGroupSegmentFilter,
            TargetGroupFrequencyCapModule targetGroupFrequencyCapModule,
            BidderResponseModule bidderResponseModule,
            ConfigService configService,
            BidResponseCreatorModule bidResponseCreatorModule,
            TargetGroupFilterChainModule targetGroupFilterChainModule,
            MetricReporterService metricReporterService,
            TopKeyWordsExtractorModule topKeyWordsExtractorModule

    ) {

        return new BidderMainPipelineProcessor(
                targetGroupFilterChainModule,
                metricReporterService,
                topKeyWordsExtractorModule,
                mgrsBuilderModule,
                lastXSecondBidOnDeviceModule,
                lastXSecondSeenIpVisitorModule,
                badIpByCountFilterModule,
                badDeviceByCountFilterModule,
                targetGroupBudgetFilterChainModule,
                bidPriceCalculatorModule,
                targetingTypeLessTargetGroupFilter,
                targetGroupSelectorModule,
                eventLogCreatorModule,
                asyncPipelineFeederAndInvokerModule,
                gicapodsIdToExchangeIdMapModule,
                globalWhiteListModule,
                targetGroupSegmentFilter,
                targetGroupFrequencyCapModule,
                bidderResponseModule,
                bidResponseCreatorModule,
                configService.getAsInt("acceptableModuleLatencyInMillis"),
                configService,
                discoveryService
        );
    }

    @Bean
    public ProcessorInvokerModule processorInvokerModule(
            BidderMainPipelineProcessor bidderMainPipelineProcessor,
            MetricReporterService metricReporterService) {
        return new ProcessorInvokerModule(
                bidderMainPipelineProcessor,
                metricReporterService,
                targetGroupsEligibleForBidding,
                forceRefreshCachesFlag
        );
    }

    @Bean
    public FeatureRecorderPipeline featureRecorderPipeline(
            DeviceHistoryOfVisitFeatureUpdaterModuleWrapper deviceHistoryOfVisitFeatureUpdaterModuleWrapper,
            DeviceHistoryOfGeoFeatureUpdaterModuleWrapper deviceHistoryOfGeoFeatureUpdaterModuleWrapper,
            VisitFeatureHistoryOfDeviceUpdaterModuleWrapper visitFeatureHistoryOfDeviceUpdaterModuleWrapper,
            GeoFeatureHistoryOfDeviceUpdaterModuleWrapper geoFeatureHistoryOfDeviceUpdaterModuleWrapper,
            GeoFeatureHistoryOfDeviceUpdaterModuleWrapper placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper,
            DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper deviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper,
            FeatureToFeatureHistoryUpdaterModuleWrapper domainFeatureToFeatureHistoryUpdaterModuleWrapper
    ) {
        return new FeatureRecorderPipeline(
                deviceHistoryOfVisitFeatureUpdaterModuleWrapper,
                deviceHistoryOfGeoFeatureUpdaterModuleWrapper,
                visitFeatureHistoryOfDeviceUpdaterModuleWrapper,
                geoFeatureHistoryOfDeviceUpdaterModuleWrapper,
                placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper,
                deviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper,
                domainFeatureToFeatureHistoryUpdaterModuleWrapper
        );
    }

    @Bean
    public NoBidModeEnforcerModule noBidModeEnforcerModule(BidModeControllerService bidModeControllerService,
                                                           MetricReporterService metricReporterService) {
        return new NoBidModeEnforcerModule(bidModeControllerService, metricReporterService);
    }

    @Bean
    public ImmutableList<BidderModule> biddingModules(
            ThrottlerModule throttlerModule,
            NoBidModeEnforcerModule  noBidModeEnforcerModule,
            BidProbabilityEnforcerModule bidProbabilityEnforcerModule,
            ContextPopulatorModule contextPopulatorModule,
            BidResponseCreatorModule bidResponseCreatorModule,
            ProcessorInvokerModule processorInvokerModule) {
        return ImmutableList.of(throttlerModule,
                noBidModeEnforcerModule,
                bidProbabilityEnforcerModule,
                contextPopulatorModule,
                processorInvokerModule);
    }

    @Bean
    public BidRequestHandlerPipelineProcessor bidRequestHandlerPipelineProcessor(
            MetricReporterService metricReporterService,
            ProcessorInvokerModule processorInvokerModule,
             ImmutableList<BidderModule> biddingModules
    ) {
        BidRequestHandlerPipelineProcessor
        bidRequestHandlerPipelineProcessor = new BidRequestHandlerPipelineProcessor (
                metricReporterService,
                configService.getAsInt("maxAllowedTimeToProcessARequestInMillis"),
                biddingModules
        );

        bidRequestHandlerPipelineProcessor.setProcessorInvokerModule(processorInvokerModule);
        return bidRequestHandlerPipelineProcessor;
    }

    @Bean
    public DomainBlackListedFilter domainBlackListedFilter(
            TargetGroupBWListCacheService targetGroupBWListCacheService,
            MetricReporterService metricReporterService) {
        return new DomainBlackListedFilter (targetGroupBWListCacheService, metricReporterService);
    }

    @Bean
    public MatchingIabCategoryFilter matchingIabCategoryFilter(
            MetricReporterService metricReporterService) {
        return new MatchingIabCategoryFilter(metricReporterService);
    }

    @Bean
    public DomainWhiteListedFilter domainWhiteListedFilter(TargetGroupBWListCacheService targetGroupBWListCacheService,
                                                           MetricReporterService metricReporterService) {
        return new DomainWhiteListedFilter(targetGroupBWListCacheService, metricReporterService);
    }

    @Bean
    public CreativeBannerApiFilter creativeBannerApiFilter(TargetGroupCreativeCacheService targetGroupCreativeCacheService,
                                                           CreativeCacheService creativeCacheService,
                                                           MetricReporterService metricReporterService) {
        return new CreativeBannerApiFilter(
                targetGroupCreativeCacheService,
                creativeCacheService,
                metricReporterService);
    }

    @Bean
    public CreativeContentTypeFilter creativeContentTypeFilter(TargetGroupCreativeCacheService targetGroupCreativeCacheService,
                                                               CreativeCacheService creativeCacheService,
                                                               MetricReporterService metricReporterService) {
        return new CreativeContentTypeFilter(targetGroupCreativeCacheService,
                creativeCacheService,
                metricReporterService);
    }

    @Bean
    public AdPositionFilter adPositionFilter(
            MetricReporterService metricReporterService) {
        return new AdPositionFilter(metricReporterService);
    }

    @Bean
    public BidRequestHandler bidRequestHandler(
            BidRequestHandlerPipelineProcessor bidRequestHandlerPipelineProcessor,
            MetricReporterService metricReporterService
    ) {
        return new BidRequestHandler(
                bidRequestHandlerPipelineProcessor,
                metricReporterService,
                totalNumberOfRequestsProcessedInFilterCountPeriod,
                numberOfRequestsProcessingNow);

    }

    @Bean
    public BlockedAdvertiserDomainFilter blockedAdvertiserDomainFilter (
            CacheService<Advertiser> advertiserCacheService,
            CampaignCacheService campaignCacheService,
            MetricReporterService metricReporterService) {

        return new BlockedAdvertiserDomainFilter (
                advertiserCacheService,
                campaignCacheService,
                metricReporterService
        );
    }

    @Bean
    public DeviceTypeFilter deviceTypeFilter(
            MetricReporterService metricReporterService) {
        return new DeviceTypeFilter(metricReporterService);
    }

    @Bean
    public OsTypeFilter osTypeFilter(
            MetricReporterService metricReporterService) {
        return new OsTypeFilter(metricReporterService);
    }

    @Bean
    public GeoLocationFilter geoLocationFilter(TargetGroupGeoLocationCacheService targetGroupGeoLocationCacheService,
                                               GeoLocationCacheService geoLocationCacheService,
                                               MetricReporterService metricReporterService) {
        return new GeoLocationFilter(
                targetGroupGeoLocationCacheService,
                geoLocationCacheService,
                metricReporterService);
    }

    @Bean
    public BlockedIabCategoryFilter blockedIabCategoryFilter(
            MetricReporterService metricReporterService) {
        return  new BlockedIabCategoryFilter(metricReporterService);
    }

    @Bean
    public BlockedBannerAdTypeFilter blockedBannerAdTypeFilter(
            CreativeSizeFilter creativeSizeFilter,
            MetricReporterService metricReporterService) {
        return new BlockedBannerAdTypeFilter (creativeSizeFilter, metricReporterService);
    }

    @Bean
    public BlockedCreativeAttributeFilter blockedCreativeAttributeFilter(
            CreativeSizeFilter creativeSizeFilter,
            MetricReporterService metricReporterService) {
        return new BlockedCreativeAttributeFilter(creativeSizeFilter, metricReporterService);

    }

    @Bean(name ="geoFeatureDeviceHistoryUpdaterModule")
    public FeatureDeviceHistoryUpdaterModule geoFeatureDeviceHistoryUpdaterModule(
            FeatureGuardService featureGuardService,
            FeatureDeviceHistoryCassandraService featureDeviceHistoryCassandraService,
            MetricReporterService metricReporterService) {
        return new FeatureDeviceHistoryUpdaterModule(
                mapOfInterestingFeatures,
                new LoadPercentageBasedSampler(configService.getAsInt("negativeGeoFeatureToStoreSamplerPercentage"),
                        "negativeGeoFeatureToStore"),
                featureGuardService,
                featureDeviceHistoryCassandraService,
                metricReporterService);
    }

    @Bean(name ="placeTagFeatureDeviceHistoryUpdaterModule")
    public FeatureDeviceHistoryUpdaterModule placeTagFeatureDeviceHistoryUpdaterModule(
            FeatureGuardService featureGuardService,
            FeatureDeviceHistoryCassandraService featureDeviceHistoryCassandraService,
            MetricReporterService metricReporterService) {
        return new FeatureDeviceHistoryUpdaterModule(
                mapOfInterestingFeatures,
                new LoadPercentageBasedSampler(configService.getAsInt("negativePlaceFeatureToStoreSamplerPercentage"),
                        "negativeFeatureToStore"),
                featureGuardService,
                featureDeviceHistoryCassandraService,
                metricReporterService);

    }

    @Bean
    public GeoFeatureFilter geoFeatureFilter(
            AeroCacheService<PlacesCachedResult> placesCachedResultAeroCacheService,
            PlaceFinderModule placeFinderModule,
            MetricReporterService metricReporterService) {
        return new GeoFeatureFilter(
                new BiddingMonitor(
                        configService.getAsInt("failureThresholdPercentageForPlaceFinderCalls"),
                        "geoFeatureFilter"),
                new LoadPercentageBasedSampler(
                        configService.getAsInt("percentageOfRequestsToConsiderForGeoTargeting"),
                        "percentageOfRequestsToConsiderForGeoTargeting"
                ),
                placesCachedResultAeroCacheService,
                configService,
                placeFinderModule,
                metricReporterService);
    }

    public FeatureDeviceHistoryUpdaterModule visitFeatureDeviceHistoryUpdaterModule(
            FeatureGuardService featureGuardService,
            FeatureDeviceHistoryCassandraService featureDeviceHistoryCassandraService,
            MetricReporterService metricReporterService) {
        return new FeatureDeviceHistoryUpdaterModule(
                mapOfInterestingFeatures,
                new LoadPercentageBasedSampler(configService.getAsInt("negativeFeatureToStoreSamplerPercentage"),
                        "negativeFeatureToStore"),
                featureGuardService,
                featureDeviceHistoryCassandraService,
                metricReporterService);
    }

    @Bean
    public BiddingMode biddingMode() {
        return new BiddingMode();
    }

    @Bean
    public BidderStatusService bidderStatusService(BiddingMode biddingMode) {
        return new BidderStatusService(biddingMode);
    }

    @Bean
    public TargetGroupFrequencyCapModule targetGroupFrequencyCapModule(
            RealTimeEntityCacheService<AdEntryDevicePair> adhistoryCacheService,
            MetricReporterService metricReporterService) {
        return new TargetGroupFrequencyCapModule(
                adhistoryCacheService,
                metricReporterService
        );
    }

    @Bean
    public TopKeyWordsExtractorModule topKeyWordsExtractorModule(
            RealTimeEntityCacheService<AdEntryDevicePair> adhistoryCacheService) {
        return new TopKeyWordsExtractorModule();
    }

    @Bean
    public ThrottlerModule throttlerModule(
            MetricReporterService metricReporterService) {
        return new ThrottlerModule(
                numberOfBidRequestRecievedPerMinute,
                configService.getAsInt("numberOfbidsRequestAllowedToProcessPerSecond"),
                metricReporterService
        );
    }

    @Bean
    public AdServerHealthChecker adServerStatusChecker(DiscoveryService discoveryService,
                                                       MetricReporterService metricReporterService) {
        return new AdServerHealthChecker(
                discoveryService,
                configService.getAsInt("adservStatusCheckIntervalInSecond"),
                metricReporterService);
    }

    @Bean
    public BidProbabilityEnforcerModule bidProbabilityEnforcerModule(AtomicLong bidPercentage,
                                                                     MetricReporterService metricReporterService) {
        return new BidProbabilityEnforcerModule(bidPercentage, metricReporterService);
    }


    @Bean
    public CreativeSizeFilter creativeSizeFilter(
            TargetGroupCreativeCacheService targetGroupCreativeCacheService,
            CreativeCacheService creativeCacheService,
            MetricReporterService metricReporterService) {
        return new CreativeSizeFilter(
                targetGroupCreativeCacheService,
                creativeCacheService,
                metricReporterService);
    }

    @Bean
    public TargetGroupSelectorModule targetGroupSelectorModule(
            CampaignCacheService campaignCacheService,
            CreativeSizeFilter creativeSizeFilter,
            CacheService<Advertiser> advertiserCacheService,
            CreativeCacheService creativeCacheService,
            TargetGroupCacheService targetGroupCacheService,
            MetricReporterService metricReporterService) {
        return new TargetGroupSelectorModule(
                campaignCacheService,
                creativeSizeFilter,
                advertiserCacheService,
                creativeCacheService,
                targetGroupCacheService,
                metricReporterService
        );
    }

    @Bean
    public GlobalWhiteListModule globalWhiteListModule(GlobalWhiteListedCacheService globalWhiteListedCacheService) {
        return new GlobalWhiteListModule(
                globalWhiteListedCacheService);
    }

    @Bean
    public GicapodsIdToExchangeIdMapModule gicapodsIdToExchangeIdMapModule(
            NomadiniIdToExchangeIdsMapCassandraService gicapodsIdToExchangeIdsMapCassandraService,
            ConfigService configService,
            MetricReporterService metricReporterService) {
        return new GicapodsIdToExchangeIdMapModule(
                gicapodsIdToExchangeIdsMapCassandraService,
                configService.getAsLong("numberOfBidsForPixelMatchingPurposePerHourLimit"),
                numberOfBidsForPixelMatchingPurposePerHour,
                metricReporterService);
    }

    @Bean
    public BlockingQueue<OpportunityContext> queueOfContexts() {
        return new ArrayBlockingQueue<>(100000);
    }

    @Bean
    public BidModeControllerService bidModeControllerService(
            AdServerHealthChecker adServerHealthChecker,
            ConfigService configService,
            BlockingQueue<OpportunityContext> queueOfContexts,
            GeoFeatureFilter geoFeatureFilter,
            IpInfoFetcherModule ipInfoFetcherModule,
            MetricReporterService metricReporterService) {

        return new BidModeControllerService(
                adServerHealthChecker,
                discoveryService,
                configService,
                numberOfExceptionsInWritingEvents,
                isNoBidModeIsTurnedOn(),
                isReloadingDataInProgress,
                isReloadProcessHealthy,
                numberOfExceptionsInWritingMessagesInKafka,
                sizeOfKafkaScoringQueue,
                queueSizeWaitingForAck,
                numberOfCuncurrentReuqestsNow,
                entityDeliveryInfoCacheServiceIsHealthy,
                queueOfContexts,
                geoFeatureFilter,
                ipInfoFetcherModule,
                metricReporterService);

    }

    @Bean
    public KafkaProducerService scoringKafkaProducerService() {
        return new KafkaProducerService(
                configService.get("scoringKafkaCluster"),
                configService.get("scoringTopicName"),
                new NoOpPartitioner(),
                "scoring-producer"
        );
    }

    @Bean
    public IpInfoFetcherModule ipInfoFetcherModule() throws IOException {
        return new IpInfoFetcherModule(
                configService,
                configService.getAsInt("failureThresholdPercentageForMaxMindCalls"));

    }

    @Bean
    public OpportunityContextBuilder opportunityContextBuilder(
            OpportunityContextBuilderOpenRtb2_3 opportunityContextBuilderOpenRtb2_3,
            MetricReporterService metricReporterService) {

        return new OpportunityContextBuilder(uniqueNumberOfContext,
                opportunityContextBuilderOpenRtb2_3,
                metricReporterService);
    }

    @Bean
    public OpportunityContextBuilderOpenRtb2_3 opportunityContextBuilderOpenRtb2_3(
            IpInfoFetcherModule ipInfoFetcherModule,
            MetricReporterService metricReporterService) {
        return new OpportunityContextBuilderOpenRtb2_3(
                new BidRequestParser(),
                ipInfoFetcherModule,
                uniqueNumberOfContext,
                metricReporterService);

    }

    @Bean
    public FeatureToFeatureHistoryUpdaterModule featureToFeatureHistoryUpdaterModule(
            FeatureToFeatureHistoryCassandraService featureToFeatureHistoryCassandraService,
            MetricReporterService metricReporterService) {

        int negativeFeatureToStoreSamplerPercentage =
                configService.getAsInt("negativeDomainFeatureToFeatureHistoryToSaveSamplerPercentage");
        return new FeatureToFeatureHistoryUpdaterModule(
                mapOfInterestingFeatures,
                new LoadPercentageBasedSampler(negativeFeatureToStoreSamplerPercentage,
                        "negativeFeatureToStore"),
                featureToFeatureHistoryCassandraService,
                metricReporterService);

    }

    @Bean
    public EntityDeliveryInfoCacheService entityDeliveryInfoCacheService() {
        return new EntityDeliveryInfoCacheService();
    }

    @Bean
    public TargetGroupSegmentFilter deviceIdSegmentModule(
            MetricReporterService metroMetricReporterService,
            SegmentCacheService segmentCacheService,
            RealTimeEntityCacheService<DeviceSegmentHistory> realTimeEntityCacheServiceDeviceSegmentHistory,
            TargetGroupWithoutSegmentFilterModule targetGroupWithoutSegmentFilterModule
    ) {
        return new TargetGroupSegmentFilter(
                metroMetricReporterService,
                segmentCacheService,
                realTimeEntityCacheServiceDeviceSegmentHistory,
                configService.getAsLong("ignoreSegmentsOlderThanXMilliSeconds"),
                new LoadPercentageBasedSampler (
                        configService.getAsInt("percentageOfRequestsToConsiderForSegmentTargeting"),
                        "percentageOfRequestsToConsiderForSegmentTargeting"
                ),
                targetGroupCacheService,
                targetGroupWithoutSegmentFilterModule
        );

    }

    @Bean
    public BadDeviceByCountPopulatorModule badDeviceByCountPopulatorModule(
            DeviceCounterService deviceCounterService,
            MetricRegistry influxMetrics,
            MetricReporterService metricReporterService) {
        return
                new BadDeviceByCountPopulatorModule(
                        deviceCounterService,
                        configService.getAsLong("maxAcceptableTimeSeenInLastTenMinute"),
                        influxMetrics,
                        configService.getAsInt("periodOfCountingDevicesInSeconds"),
                        metricReporterService);

    }

    @Bean
    public ContextPopulatorModule contextPopulatorModule(OpportunityContextBuilder opportunityContextBuilder) {
        return new ContextPopulatorModule(opportunityContextBuilder);
    }

    @Bean
    public LoadPercentageBasedSampler bidEventRecorderModuleSampler() {
        return new LoadPercentageBasedSampler(
                configService.getAsInt("percentOfBidsToRecord"),
                "percentOfBidsToRecord"
        );
    }

    @Bean
    public LoadPercentageBasedSampler uniqueDeviceBidEventRecorderModuleSampler() {
        return new LoadPercentageBasedSampler(
                configService.getAsInt("percentOfBidsToRecordWhenRecordingUniqueDevice"),
                "percentOfBidsToRecordWhenRecordingUniqueDevice"
        );
    }

    @Bean
    public LoadPercentageBasedSampler noBidEventRecorderModuleSampler() {
        return new LoadPercentageBasedSampler(
                configService.getAsInt("percentOfNoBidsToRecord"),
                "percentOfNoBidsToRecord"
        );
    }

    @Bean
    public LoadPercentageBasedSampler wiretapBidRequestResponseSampler(){
        return new LoadPercentageBasedSampler (
                configService.getAsInt("percentOfBidsToBeWiretapped"),
                "percentOfBidsToBeWiretapped"
        );
    }

    @Bean
    public MGRSBuilderModule mgrsBuilderModule(LatLonToMGRSConverter latLonToMGRSConverter) {
        return new MGRSBuilderModule(
                latLonToMGRSConverter
        );
    }

    @Bean
    public BidderWiretapModule bidderWiretapModule(WiretapCassandraService wiretapCassandraService) {
        return new BidderWiretapModule(
                wiretapCassandraService,
                new LoadPercentageBasedSampler(
                        configService.getAsInt("percentOfBidsToBeWiretapped"),
                        "percentOfBidsToBeWiretapped")
        );
    }

    @Bean
    public EventLogCreatorModule eventLogCreatorModule(@Value("info.app.version") String appVersion) throws UnknownHostException {
        return new EventLogCreatorModule (
                java.net.InetAddress.getLocalHost().getHostName(),
                appVersion);

    }
    @Bean
    public BidEventRecorderModule bidEventRecorderModule(
            EventLogCreatorModule eventLogCreatorModule,
            CassandraService<BidEventLog> bidEventLogCassandraService,
            CassandraService<UniqueDeviceBidEventLog> uniqueDeviceBidEventLogCassandraService,
            LoadPercentageBasedSampler bidEventRecorderModuleSampler,
            LoadPercentageBasedSampler noBidEventRecorderModuleSampler,
            LoadPercentageBasedSampler uniqueDeviceBidEventRecorderModuleSampler,
            MetricReporterService metricReporterService) {
        return new BidEventRecorderModule(
                bidEventRecorderModuleSampler,
                noBidEventRecorderModuleSampler,
                uniqueDeviceBidEventRecorderModuleSampler,
                bidEventLogCassandraService,
                uniqueDeviceBidEventLogCassandraService,
                eventLogCreatorModule,
                metricReporterService);
    }

    @Bean
    public VisitFeatureHistoryOfDeviceUpdaterModuleWrapper visitFeatureHistoryOfDeviceUpdaterModuleWrapper(
            DeviceHistoryUpdaterModule deviceFeatureHistoryUpdaterModule,
            MetricReporterService metricReporterService) {
        return new VisitFeatureHistoryOfDeviceUpdaterModuleWrapper(
                deviceFeatureHistoryUpdaterModule,
                metricReporterService
        );
    }
}

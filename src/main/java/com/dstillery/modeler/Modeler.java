package com.dstillery.modeler;

import com.dstillery.common.config.EssentialConfig;
import com.dstillery.common.config.CacheServiceInitializer;
import com.dstillery.common.config.CommonBeansConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import java.util.Collections;
import java.util.HashSet;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@PropertySources({

        @PropertySource(value = "classpath:modeler.properties"),
        @PropertySource(value = "file://${PROPERTY_FILE:/etc/modeler/modeler.properties}",
                ignoreResourceNotFound = true) })
@Import({
        ModelerEssentialConfig.class,
        EssentialConfig.class,
        CommonBeansConfig.class,
        ModelerConfig.class})
@ComponentScan(basePackages = {
        "com.dstillery.modeler",
})
public class Modeler {
    public static void main(String[] args) {
        SpringApplication sa = new SpringApplication();
        sa.addListeners(new CacheServiceInitializer());
        sa.setSources(new HashSet<>(Collections.singletonList(Modeler.class.getName())));
        ConfigurableApplicationContext context = sa.run(args);
    }
}

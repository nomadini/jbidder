package com.dstillery.modeler.engine;
/*
  FeatureValidator.h

   Created on: Jul 16, 2015
       Author: mtaabodi
 */

import com.dstillery.modeler.ModelUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class FeatureValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureValidator.class);

    void validateFeatures(List<Double> featureWeights,
						  Map<String, Integer> featureIndexMap) {
		if (featureWeights.size() == 0) {
			throw new IllegalArgumentException("feature weight cannot be zero");
		}
		if (featureWeights.size() != featureIndexMap.size()) {
			//this is just a sanity check
			LOGGER.warn("features weights size : {} is wrong, should be equal to featureIndexMap size: {}",
                    featureWeights.size(),
					featureIndexMap.size());
		}

		Map<Integer, String> indexFeatureMap =
				ModelUtil.getIndexFeatureMap(featureIndexMap);

		if (featureIndexMap.size() != indexFeatureMap.size()) {

			LOGGER.debug("indexFeatureMap.size() : {} , featureIndexMap.size() : {}",indexFeatureMap.size(),
                    featureIndexMap.size());
			throw new IllegalArgumentException(
					"indexFeatureMap and featureIndexMap should have equal size");
		}
	}

}
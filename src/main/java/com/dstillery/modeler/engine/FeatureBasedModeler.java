package com.dstillery.modeler.engine;

import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.feature.Feature;
import com.dstillery.common.feature.FeatureType;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.featuredevice.FeatureHistory;
import com.dstillery.common.modeling.predictive.ModelResult;
import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.modeler.ModelUtil;
import com.dstillery.modeler.ModelerContext;
import com.dstillery.modeler.ModelerModule;
import com.dstillery.modeler.engine.sgdtk.FeatureVector;
import com.dstillery.modeler.engine.sgdtk.Model;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;


import static com.dstillery.common.modeling.predictive.RedFlag.NO_FEATURE_SET_EXTRACTED_FROM_DEVICE_HISTORY;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.modeler.engine.SgdTkSolver.getFeatureVectorFromDeviceHistories;

/*
  FeatureBasedModeler.h

   Created on: Jul 16, 2015
       Author: mtaabodi
*/
public class FeatureBasedModeler extends ModelerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureBasedModeler.class);
    public static final ObjectMapper MAPPER = new ObjectMapper();

    private MetricReporterService metricReporterService;
    private FeatureValidator featureValidator;
    private FeatureWeightToModelConverter featureWeightToModelConverter;
    private FeatureWeightCalculator featureWeightCalculator;

    public FeatureBasedModeler(MetricReporterService metricReporterService,
                               FeatureValidator featureValidator,
                               FeatureWeightToModelConverter featureWeightToModelConverter,
                               FeatureWeightCalculator featureWeightCalculator) {
        this.metricReporterService = metricReporterService;;
        this.featureValidator = featureValidator;
        this.featureWeightToModelConverter = featureWeightToModelConverter;
        this.featureWeightCalculator = featureWeightCalculator;
    }

    public void process(ModelerContext context) {

        if (!areDeviceFeatureHistoriesValidForModeling(context)) {
            return;
        }

       context.buildFeatureIndexMap();

        LOGGER.debug("featureIndexMap created : {}", context.getFeatureIndexMap());
        if (context.getFeatureIndexMap().isEmpty()) {
            LOGGER.warn(
                    "could not successfully get feature set from " +
                            context.getAllDeviceHistories().size() + " device feature histories");
            context.getRedFlags().add(NO_FEATURE_SET_EXTRACTED_FROM_DEVICE_HISTORY);
            return;
        }
        Optional<Model> model = featureWeightCalculator.createModel(context);
        if (!model.isPresent()) {
            return;
        }
        List<Double> featureWeights = new ArrayList<>();
        Map<Integer, String> indexFeatureMap = ModelUtil.getIndexFeatureMap(
                                                context.getFeatureIndexMap());
        //we calculate the feature weights using a simple method
        context.getFeatureIndexMap().forEach((s, integer) -> featureWeights.add(0D));

        context.getFeatureIndexMap().forEach((key, index) -> {
            double weight = calculateFeatureWeight(index, context, model.get(), indexFeatureMap);
            LOGGER.trace("index {} has score {}", index, weight);
            featureWeights.set(index, weight);
        });
        LOGGER.debug("features weights size : {}", featureWeights.size());
        LOGGER.debug("features weights {}", featureWeights);
        featureValidator.validateFeatures(featureWeights, context.getFeatureIndexMap());

        featureWeightToModelConverter.createModelFromFeatureWeights(
                featureWeights,
                model.get().toJson(),
                indexFeatureMap,
                context.getModelRequest());

        context.getModelRequest().getModelResult().setProcessResult(ModelResult.Result.CREATED);
        context.getModelRequest().getModelResult().getRedFlags().clear();
        try {
        LOGGER.debug("this model was created : {}, for model request : {}",
                OBJECT_MAPPER.writeValueAsString(context.getModelRequest().getModelResult()),
                OBJECT_MAPPER.writeValueAsString(context.getModelRequest()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private double calculateFeatureWeight(int index,
                                          ModelerContext context,
                                          Model model,
                                          Map<Integer, String> indexFeatureMap) {

        DeviceFeaturePair deviceFeatureHistories = new DeviceFeaturePair(new NomadiniDevice());
        deviceFeatureHistories.setPositiveHistoryValue(true);
        deviceFeatureHistories.setFeatureHistory(new FeatureHistory(
                new Feature(FeatureType.GTLD, indexFeatureMap.get(index), Feature.FeatureStatus.ACTIVE),
                DateTimeUtil.getNowInMilliSecond()
        ));
        FeatureVector featureVectorWithOneX = getFeatureVectorFromDeviceHistories(
                Collections.singletonList(deviceFeatureHistories),
                context.getFeatureIndexMap());
        double[] score = model.score(featureVectorWithOneX);
        return score[0];
    }

    private boolean areDeviceFeatureHistoriesValidForModeling(ModelerContext context) {
        if (context.getPositiveDeviceHistories().isEmpty()) {
            metricReporterService.
                    addStateModuleForEntity("ABORTING_MODELING_FOR_EMPTY_POSITIVES",
                            "FeatureBasedModeler",
                            "model" + context.getModelRequest().getId(),
                            MetricReporterService.MetricPriority.EXCEPTION);
             LOGGER.error("ABORTING_MODELING_FOR_EMPTY_POSITIVES");
            return false;
        }

        if ( context.getNegativeDeviceHistories().isEmpty()) {
            metricReporterService.
                    addStateModuleForEntity("ABORTING_MODELING_FOR_EMPTY_NEGATIVES",
                            "FeatureBasedModeler",
                            "model" + context.getModelRequest().getId(),
                            MetricReporterService.MetricPriority.EXCEPTION);
             LOGGER.error("ABORTING_MODELING_FOR_EMPTY_NEGATIVES");
            return false;
        }

        return true;
    }
}

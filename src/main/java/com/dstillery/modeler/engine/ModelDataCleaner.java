package com.dstillery.modeler.engine;/*
  ModelDataCleaner.h

   Created on: Dec 13, 2017
       Author: mtaabodi
 */

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryUtil;
import com.dstillery.common.modeling.devicefeature.DeviceHistorySet;
import com.dstillery.common.modeling.predictive.MetaData;
import com.dstillery.modeler.ModelerContext;
import com.dstillery.modeler.ModelerModule;
import com.google.common.collect.ImmutableMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryUtil.getCleanHistoryClone;

public class ModelDataCleaner extends ModelerModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelDataCleaner.class);
    private MetricReporterService metricReporterService;
    public ModelDataCleaner(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }

    public void process(ModelerContext context) {

        LOGGER.debug("start of cleaning data: context.getPositiveDeviceHistories() : {}", context.getPositiveDeviceHistories().size());
        LOGGER.debug("start of cleaning data : context.getNegativeDeviceHistories() : {}", context.getNegativeDeviceHistories().size());
        context.setPositiveDeviceHistories(
                improveQualityOfDeviceHistories(context.getPositiveDeviceHistories(),
                        context.getModelRequest().getMetaData(),
                        context.getModelRequest().getId()));
        context.setNegativeDeviceHistories(
                improveQualityOfDeviceHistories(context.getNegativeDeviceHistories(),
                        context.getModelRequest().getMetaData(),
                        context.getModelRequest().getId()));

        LOGGER.debug("context.getPositiveDeviceHistories() size after weeding out too sparse features : {}"
                ,context.getPositiveDeviceHistories().size());

        LOGGER.debug("negativeDeviceHistories size after weeding out too sparse features : {}",
                context.getNegativeDeviceHistories().size());


        context.setPositiveDeviceHistories(removeDevicesWithZeroFeatures(context.getPositiveDeviceHistories()));
        context.setNegativeDeviceHistories(removeDevicesWithZeroFeatures(context.getNegativeDeviceHistories()));

        metricReporterService.getHistogram("size_of_positive_devices_used_for_modeling",
                ImmutableMap.of("model", context.getModelRequest().getId()))
                .update(context.getPositiveDeviceHistories().size());
        metricReporterService.getHistogram("size_of_negative_devices_used_for_modeling",
                ImmutableMap.of("model", context.getModelRequest().getId()))
                .update(context.getNegativeDeviceHistories().size());

        //combine devices into one group
        context.setAllDeviceHistories(context.combineDeviceHistories());

        LOGGER.debug("size of all devices after cleaning : {}", context.getAllDeviceHistories().size());
        metricReporterService.getHistogram("size_of_devices_used_for_modeling",
                ImmutableMap.of("model", context.getModelRequest().getId()))
                .update(context.getAllDeviceHistories().size());
    }
    private List<DeviceHistorySet> removeDevicesWithZeroFeatures(List<DeviceHistorySet> deviceHistories) {
        return deviceHistories.stream().filter(features -> !features.getHistories().isEmpty()).collect(Collectors.toList());
    }

    private List<List<DeviceFeaturePair>> removeDevicesFromHistory(
            List<List<DeviceFeaturePair>> deviceHistories,
                                          Set<String> commonDevicesToRemove) {

        if (commonDevicesToRemove.isEmpty()) {
            return deviceHistories;
        }

        List<List<DeviceFeaturePair>> cleanDevices = new ArrayList<>();

        for(List<DeviceFeaturePair> deviceFeatureHistory : deviceHistories) {
            if (commonDevicesToRemove.contains(deviceFeatureHistory.get(0).getDevice().getDeviceUniqueId())) {
                //ignore this bunch
            } else {
                cleanDevices.add(deviceFeatureHistory);
            }
        }
        return cleanDevices;
    }

    private List<List<DeviceFeaturePair>> removeFeaturesFromHistory(
            List<List<DeviceFeaturePair>> deviceHistories,
            Set<String> commonFeaturesToRemove) {
        if (commonFeaturesToRemove.isEmpty()) {
            return deviceHistories;
        }
            //then we clean all the common features between positive and negative devices
            int sizeOfFeaturesBeforeRemove = deviceHistories.size();

            deviceHistories = DeviceFeatureHistoryUtil.removeFeaturesFromHistory(deviceHistories, commonFeaturesToRemove);
            LOGGER.debug("sizeOfFeatures in deviceFeatureHistory after removing common features : {}" +
                            ",  sizeOfFeaturesBeforeRemove : {}", deviceHistories.size(),
                    sizeOfFeaturesBeforeRemove);
            return deviceHistories;
    }

    private List<DeviceHistorySet> improveQualityOfDeviceHistories(
            List<DeviceHistorySet> deviceHistories, MetaData metaData,
            long modelId) {
        List<DeviceHistorySet> cleanDeviceFeatureHistories = new ArrayList<>();

        for (DeviceHistorySet originalDeviceHistory : deviceHistories) {
            //here we clean out the features that were not seen enough times

            List<DeviceFeaturePair> cleanDevHist = getCleanHistoryClone(originalDeviceHistory.getHistories(),
                    metaData.getMinNumberOfFeaturesSeenToBeConsideredForModeling());
            LOGGER.trace("originalDeviceHistory : {}\ncleanDevHist : {}", originalDeviceHistory, cleanDevHist);
            if (cleanDevHist.isEmpty()) {
                if (originalDeviceHistory.getHistories().size() <= metaData.getMaxNumberOfHistoriesOnADeviceToBeConsideredForModeling()) {
                    //original device history is small, so we use it
                    cleanDeviceFeatureHistories.add(originalDeviceHistory);
                } else {
                    metricReporterService.getCounter("device_with_too_many_feature",
                            ImmutableMap.of("model_id", modelId)).inc();
                    metricReporterService.getHistogram("device_with_too_many_feature_size",
                            ImmutableMap.of("model_id", modelId)).update(originalDeviceHistory.getHistories().size());
                }
                if (!originalDeviceHistory.getHistories().isEmpty()) {
                    metricReporterService.getCounter("device_with_no_clean_feature",
                            ImmutableMap.of("model_id", modelId)).inc();
                    LOGGER.warn("device doesn't have any clean features, it had {} features", originalDeviceHistory.getHistories().size());
                }
            } else {
                cleanDeviceFeatureHistories.add(new DeviceHistorySet(cleanDevHist));
            }
        }

        return cleanDeviceFeatureHistories;

    }
}

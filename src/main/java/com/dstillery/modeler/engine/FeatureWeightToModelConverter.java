package com.dstillery.modeler.engine;

import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.modeler.ModelUtil;
import com.dstillery.modeler.ModelerContext;
import com.dstillery.modeler.ModelerModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

/*
  FeatureWeightToModelConverter.h

   Created on: Jul 16, 2015
       Author: mtaabodi
 */

public class FeatureWeightToModelConverter extends ModelerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureWeightToModelConverter.class);

    public void createModelFromFeatureWeights(
            List<Double> featureWeights,
            String modelInJson,
            Map<Integer, String> indexFeatureMap,
            ModelRequest model) {

        model.getModelResult().setOriginalModelAsJson(modelInJson);

        model.getModelResult().setFeatureScoreMap(ModelUtil.buildFeatureScoreMap(
                indexFeatureMap, featureWeights));

        assertAndThrow(!model.getModelResult().getFeatureScoreMap().isEmpty());

        model.getModelResult().setTopFeatureScoreMap(ModelUtil.getHighestFeatureScoreMap
                (model.getModelResult().getFeatureScoreMap(),
                        model.getNumberOfTopFeatures()));

        if (model.getModelResult().getTopFeatureScoreMap().isEmpty()) {
            LOGGER.warn("top features map is empty");
        }
    }

    @Override
    public void process(ModelerContext context) {

    }
}

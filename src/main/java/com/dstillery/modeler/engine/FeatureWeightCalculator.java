package com.dstillery.modeler.engine;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.modeler.ModelerContext;
import com.dstillery.modeler.engine.sgdtk.FeatureVector;
import com.dstillery.modeler.engine.sgdtk.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.dstillery.common.modeling.predictive.RedFlag.NUM_SAMPLES_IS_LESS_THAN_FEATURES;
import static com.dstillery.common.util.StringUtil.assertAndThrow;

/*
    FeatureWeightCalculator.h

    Created on:Jul 16,2015
    Author:mtaabodi
*/

public class FeatureWeightCalculator {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureWeightCalculator.class);

    private MetricReporterService metricReporterService;
    private FeatureVectorCreator featureVectorCreator = new FeatureVectorCreator();
    public FeatureWeightCalculator(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;;
    }

    /*
      creates a feature matrix and label matrix in memory based on
      positive and negative Devicehistories for svmSgd
     */
    Optional<Model> createModel(ModelerContext context) {

        //this should never throw, because we check before this step too
        assertAndThrow(!context.getAllDeviceHistories().isEmpty());
        assertAndThrow(!context.getFeatureIndexMap().isEmpty());

        Collections.shuffle(context.getAllDeviceHistories());
        Collections.shuffle(context.getAllDeviceHistories());
        int numSamples = context.getAllDeviceHistories().size();
        int numberOfFeatures = context.getFeatureIndexMap().size();

        if(numSamples <= numberOfFeatures) {
            context.getRedFlags().add(NUM_SAMPLES_IS_LESS_THAN_FEATURES);
            LOGGER.warn("numberOfSamples is not greater than number of features " +
                    numSamples + " <= " +
                    numberOfFeatures);
            return Optional.empty();
        }

        LOGGER.debug("Modeler : matrix created , numberOfFeatures :  {} , numSamples :  {}",
                numberOfFeatures,
                numSamples);

        List<FeatureVector> features = featureVectorCreator.createFeatureVectors(context);
        LOGGER.debug("about to process svm sgd");
        SgdTkSolver sgdTkSolver = new SgdTkSolver();
        Model model = sgdTkSolver.solve(features);

        LOGGER.debug("model created created.. with size : {} ", model);

        return Optional.of(model);
    }

}

package com.dstillery.modeler.engine.sgdtk.struct;

import java.io.IOException;

/**
 * Interface for streaming {@link com.dstillery.modeler.engine.sgdtk.struct.FeatureVectorSequence} from a source to a sink
 *
 * @author dpressel
 */
public interface SequentialFeatureProvider
{
    /**
     * Get the next feature vector sequence from a source
     * @return A sequence or null if the source is consumed fully
     * @throws IOException
     */
    FeatureVectorSequence next() throws IOException;
}

package com.dstillery.modeler.engine.sgdtk.exec;

import com.dstillery.modeler.engine.sgdtk.Learner;
import com.dstillery.modeler.engine.sgdtk.Model;

/**
 * Created by dpressel on 3/22/16.
 */
public interface TrainingEventListener
{
    void onEpochEnd(Learner learner, Model model, double epochSeconds);
}

package com.dstillery.modeler.engine.sgdtk;

import com.dstillery.modeler.engine.sgdtk.io.Config;

public interface LearnerCreator
{
    Learner newInstance(Config params) throws Exception;
}

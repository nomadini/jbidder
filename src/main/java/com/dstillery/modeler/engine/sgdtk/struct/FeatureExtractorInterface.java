package com.dstillery.modeler.engine.sgdtk.struct;

import java.util.List;

public interface FeatureExtractorInterface {
    String[] run(List<State> states, int current);

    int getOrder();

    int size();
}

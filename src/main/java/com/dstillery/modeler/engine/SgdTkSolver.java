package com.dstillery.modeler.engine;

import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.modeler.ModelUtil;
import com.dstillery.modeler.engine.sgdtk.FeatureVector;
import com.dstillery.modeler.engine.sgdtk.HingeLoss;
import com.dstillery.modeler.engine.sgdtk.Learner;
import com.dstillery.modeler.engine.sgdtk.LinearModelFactory;
import com.dstillery.modeler.engine.sgdtk.Metrics;
import com.dstillery.modeler.engine.sgdtk.Model;
import com.dstillery.modeler.engine.sgdtk.ModelFactory;
import com.dstillery.modeler.engine.sgdtk.Offset;
import com.dstillery.modeler.engine.sgdtk.SGDLearner;
import com.dstillery.modeler.engine.sgdtk.SparseVectorN;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

public class SgdTkSolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(SgdTkSolver.class);

    public Double lambda = 1e-5;
    public Double eta = 1e-5;

    public static void main(String[] args) throws Exception {
        int numberOfFeatures = 10;
        int numberOfHistoriesPerDevice = 10;
        List<FeatureVector> trainingSet = new ArrayList<>();
        List<FeatureVector> evalSet = new ArrayList<>();
        for (int j = 0 ; j < 100; j++) {

            FeatureVector fv = getFeatureVector(numberOfFeatures,
                                                numberOfHistoriesPerDevice);
            trainingSet.add(fv);
            evalSet.add(fv);
        }

        new SgdTkSolver().solveExample(
                trainingSet, evalSet
        );
    }

    private static FeatureVector getFeatureVector(int numberOfFeatures, int numberOfHistoriesPerDevice) {
        SparseVectorN sv = new SparseVectorN();
        for (int i = 0 ; i < numberOfHistoriesPerDevice; i++) {
            int idx = (int) (Math.random() * 1000 ) % numberOfFeatures ;
            double value = Math.random();
            sv.add(new Offset(idx, value));

        }
        sv.organize();

        return new FeatureVector(Math.random() % 2, sv);
    }

    public static FeatureVector getFeatureVectorFromDeviceHistories(
            List<DeviceFeaturePair> deviceFeatureHistories,
            Map<String, Integer> featureIndexMap) {
        Integer maxIndex = featureIndexMap.values().stream().max(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 >= o2 ? 1 : -1;
            }
        }).get();
        assertAndThrow(featureIndexMap != null);
        Set<Long> allIndexes =
                ModelUtil.getAllFeatureIndexesInDeviceFeatureHistory(
                        deviceFeatureHistories,
                        featureIndexMap);
        SparseVectorN sv = new SparseVectorN();
        allIndexes.forEach(index -> {
                sv.add(new Offset(index.intValue(), 1));
        });

        LOGGER.trace("getNonZeroOffsets : {}", sv.getNonZeroOffsets());
        LOGGER.trace("allIndexes : {}", allIndexes);
        sv.organize();

        List<Boolean> positiveValues = deviceFeatureHistories.stream()
                .map(DeviceFeaturePair::isPositiveHistoryValue)
                .distinct()
                .collect(Collectors.toList());
        LOGGER.trace("positiveValues : {}", positiveValues);
        assertAndThrow(positiveValues.size() == 1);
        int value = -1;
        if (positiveValues.get(0)) {
            value = 1;
        }

        return new FeatureVector(value, sv);
    }

    private void solveExample(List<FeatureVector> trainingSet, List<FeatureVector> evalSet) throws Exception {
        ModelFactory modelFactory = new LinearModelFactory();
        Learner learner = new SGDLearner(new HingeLoss(), lambda, eta, modelFactory);
        Model model = learner.create(trainingSet.size());

        double totalTrainingElapsed = 0.;

        for (int i = 0; i < 100; ++i)
        {
            Collections.shuffle(trainingSet);
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("EPOCH: " + (i + 1));
            Metrics metrics = new Metrics();
            double t0 = System.currentTimeMillis();
            learner.trainEpoch(model, trainingSet);
            double elapsedThisEpoch = (System.currentTimeMillis() - t0) /1000.;
            System.out.println("Epoch training time " + elapsedThisEpoch + "s");
            totalTrainingElapsed += elapsedThisEpoch;

            learner.eval(model, trainingSet, metrics);
            showMetrics(metrics, "Training Set Eval Metrics");
            metrics.clear();

            if (evalSet != null)
            {
                learner.eval(model, evalSet, metrics);
                showMetrics(metrics, "Test Set Eval Metrics");
                for (FeatureVector fv : evalSet) {
                    //Leaner model gives back only one score
                    double[] scores = model.score(fv);
                    System.out.println("score : " + scores[0]);
                    System.out.println("wights of features : " + model.getWeights());
                }
            }

        }

        System.out.println("Total training time " + totalTrainingElapsed + "s");
        System.out.println("model : " + model.toJson());
        //model.save(new FileOutputStream("svm.model"));

    }

    public Model solve(List<FeatureVector> trainingSet) {
        ModelFactory modelFactory = new LinearModelFactory();
        Learner learner = new SGDLearner(new HingeLoss(), lambda, eta, modelFactory);
        Model model;
        try {
            model = learner.create(trainingSet.size());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        double totalTrainingElapsed = 0.;
        int numberOfEpochs = 100;
        for (int i = 0; i < numberOfEpochs; ++i)
        {
            Collections.shuffle(trainingSet);
            Metrics metrics = new Metrics();
            double t0 = System.currentTimeMillis();
            learner.trainEpoch(model, trainingSet);
            double elapsedThisEpoch = (System.currentTimeMillis() - t0) /1000.;
            totalTrainingElapsed += elapsedThisEpoch;

            learner.eval(model, trainingSet, metrics);

//            if (evalSet != null)
//            {
//                learner.eval(model, evalSet, metrics);
//                showMetrics(metrics, "Test Set Eval Metrics");
//                for (FeatureVector fv : evalSet) {
//                    //Leaner model gives back only one score
//                    double[] scores = model.score(fv);
//                    System.out.println("score : " + scores[0]);
//                    System.out.println("wights of features : " + model.getWeights());
//                }
//            }

        }

        System.out.println("Total training time " + totalTrainingElapsed + "s");
//        model.save(new FileOutputStream("svm.model"));
        return model;
    }
    private static void showMetrics(Metrics metrics, String pre) {
        System.out.println("========================================================");
        System.out.println(pre);
        System.out.println("========================================================");

        System.out.println("\tLoss = " + metrics.getLoss());
        System.out.println("\tCost = " + metrics.getCost());
        System.out.println("\tError = " + 100 * metrics.getError());
        System.out.println("--------------------------------------------------------");
    }
}

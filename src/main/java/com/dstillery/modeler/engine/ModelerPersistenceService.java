package com.dstillery.modeler.engine;/*
  ModelerPersistenceService.h

   Created on: Jul 16, 2015
       Author: mtaabodi
 */


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.modeling.predictive.MySqlModelResultService;
import com.dstillery.common.modeling.predictive.MySqlModelService;
import com.dstillery.modeler.ModelerContext;
import com.dstillery.modeler.ModelerModule;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.dstillery.common.util.Constants.OBJECT_MAPPER;

public class ModelerPersistenceService extends ModelerModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelerPersistenceService.class);
    public static final ObjectMapper MAPPER = new ObjectMapper();

    private MySqlModelService mySqlModelService;
    private MySqlModelResultService mySqlModelResultService;

    public ModelerPersistenceService(MetricReporterService metricReporterService,
                                     MySqlModelService mySqlModelService,
                                     MySqlModelResultService mySqlModelResultService) {
        this.mySqlModelService = mySqlModelService;
        this.mySqlModelResultService = mySqlModelResultService;
    }

    public void process(ModelerContext context) {
        try {
            LOGGER.info("going to persist the nicely built model : {}, model result is : {}",
                    context.getModelRequest(),
                    OBJECT_MAPPER.writeValueAsString(context.getModelRequest().getModelResult()));
            context.getModelRequest().setRebuildModelOnNextRun(false);
            validateFeatureTypesRequested(context.getModelRequest());
            this.mySqlModelService.upsertModel(context.getModelRequest());
            context.getModelRequest().getModelResult().setModelRequestId(context.getModelRequest().getId());
            this.mySqlModelResultService.upsertModel(context.getModelRequest().getModelResult());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void validateFeatureTypesRequested(ModelRequest model) {

        // for (mode.)
    }
}

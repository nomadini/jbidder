package com.dstillery.modeler.engine;

import com.dstillery.common.modeling.devicefeature.DeviceHistorySet;
import com.dstillery.modeler.ModelerContext;
import com.dstillery.modeler.engine.sgdtk.FeatureVector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.dstillery.common.modeling.predictive.RedFlag.NO_NEGATIVE_LABEL;
import static com.dstillery.common.modeling.predictive.RedFlag.NO_POSITIVE_LABEL;
import static com.dstillery.modeler.engine.SgdTkSolver.getFeatureVectorFromDeviceHistories;

public class FeatureVectorCreator {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureVectorCreator.class);

    public List<FeatureVector> createFeatureVectors(ModelerContext context) {
        List<FeatureVector> features = new ArrayList<>();
        int numberOfPositiveLabels = 0;
        int numberOfNegativeLabels = 0;
        for (int i = 0; i < context.getAllDeviceHistories().size(); i++) {

            DeviceHistorySet historiesOfADevice = context.getAllDeviceHistories().get(i);

            FeatureVector featureVector =
                    getFeatureVectorFromDeviceHistories(
                            historiesOfADevice.getHistories(),
                            context.getFeatureIndexMap());
            LOGGER.trace("featureVector : {}", featureVector);
            if (featureVector.getY() == 1) {
                numberOfPositiveLabels++;
            } else {
                numberOfNegativeLabels++;
            }

            features.add(featureVector);
        }

        //create train labels
        if (numberOfNegativeLabels == 0) {
            context.getRedFlags().add(NO_NEGATIVE_LABEL);
            throw new RuntimeException(NO_NEGATIVE_LABEL.name());
        }
        if (numberOfPositiveLabels == 0) {
            context.getRedFlags().add(NO_POSITIVE_LABEL);
            throw new RuntimeException(NO_POSITIVE_LABEL.name());
        }

        return features;
    }
}

package com.dstillery.modeler;

import static com.dstillery.common.modeling.predictive.ModelResult.Result.UNKNOWN;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.modeling.predictive.ModelResult;
import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.common.util.NumberUtil;
import com.dstillery.common.util.RandomUtil;

public class ModelUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelUtil.class);

    public static Map<String, Double> getHighestFeatureScoreMap(Map<String, Double> featureScoreMap,
                                                                    int limit) {
            Map<String, Double> topFeatureScoreMap = new HashMap<>();

            for (Map.Entry<String, Double> kv : featureScoreMap.entrySet()) {
                if (kv.getValue() >= 0) {
                    topFeatureScoreMap.put(kv.getKey(),
                            NumberUtil.roundDouble(kv.getValue(), 2));
                }
            }
            return topFeatureScoreMap;
        }

    static void printFeatureVector(List<Float> featureWeights,
                                   Map<Integer, String> indexFeatureMap) {
        int i = 0;
        for (Float kv : featureWeights) {
            String ptr = indexFeatureMap.get(i);
            i++;
            LOGGER.debug("feature  {} : weight : {} ", ptr, kv);
        }
    }

    public static Map<Integer, String> getIndexFeatureMap(
            Map<String, Integer> featureIndexMap) {
        Map<Integer, String> indexFeatureMap = new HashMap<>();

        for (Map.Entry<String, Integer> it : featureIndexMap.entrySet()) {
            String feature = it.getKey();
            int index = it.getValue();
            indexFeatureMap.put(index, feature);
        }

        return indexFeatureMap;
    }

    static int getHighetIndexInFeatureUniqueMap(
            Map<String, Integer> featureIndexMap) {
        int maxIndex = 1;
        for (Map.Entry<String, Integer> it : featureIndexMap.entrySet()) {
            String feature = it.getKey();
            int index = it.getValue();
            if (index > maxIndex) {
                maxIndex = index;
            }
        }

        return maxIndex;
    }

    /**
     * assigns new indexes for new features in featureIndexMap
     */
    public static void updateFeatureIndexMapWithNewFeatures(
            Set<String> allFeatureSet,
            Map<String, Integer> featureIndexMap) {
        if (allFeatureSet.isEmpty()) {
            return;
        }
        int sizeBefore = featureIndexMap.size();
        for (String feature : allFeatureSet) {
            int index = getIndexFromFeatureIndexMap (featureIndexMap, feature);
            if (index == -1) {
                index = featureIndexMap.size();
            }
            featureIndexMap.put(feature, index);

        }
        LOGGER.trace("added  {} features to  featureIndexMap", featureIndexMap.size() - sizeBefore);
    }

    /**
     * returns the index of a feature in the featureIndexMap
     * returns -1 if the index is not found
     */
    private static int getIndexFromFeatureIndexMap(Map<String, Integer> map, String feature) {

        Integer feaureIndexPair = map.get(feature);
        if (feaureIndexPair != null) {
            LOGGER.trace("index of {} in featureIndexMap is {}", feature , feaureIndexPair);
            return feaureIndexPair;

        } else {
            LOGGER.trace("feature not found  in feaureIndexMap {}, its ok, return -1", feature);
            return -1;
        }
    }

/**
 * builds feature score map from a list of weights
 */
    public static Map<String, Double> buildFeatureScoreMap(
            Map<Integer, String> indexFeatureMap,
            List<Double> featureWeights) {

        Map<String, Double> featureScoreMap = new HashMap<>();

        if (indexFeatureMap.size() != featureWeights.size()) {
            throw new IllegalArgumentException("featureWeights and indexFeatureMap should have equal size");
        }

        for (int index = 0; index < featureWeights.size(); index++) {
            Double weight = featureWeights.get(index);
            double score = weight;

            String featurePair = indexFeatureMap.get(index);
            if (featurePair == null) {
                LOGGER.info("index : {} was not found in indexFeatureMap. ", index);
                continue;
            }
            LOGGER.debug("feature :  {} , weight : {} , score : {} ",featurePair, weight, score);
            featureScoreMap.put(featurePair, score);
        }

        LOGGER.debug("size of featureScoreMap : {} ", featureScoreMap.size());
        return featureScoreMap;
    }

    static String getRandomRecord() {
        StringBuilder record = new StringBuilder();

        if (RandomUtil.sudoRandomNumber (1) % 9 == 0) {
            record.append ("+1 ");
        } else {
            record.append ("-1 ");
        }
        long num;
        do {
            num = RandomUtil.sudoRandomNumber (2);
            for (int i = 0; i < 99; i++) {
                long feature;
                do {
                    feature = RandomUtil.sudoRandomNumber (2);
                } while (feature <= 0);
                record.append(feature);
                record.append(":");
                record.append("1 ");
            }

        } while (num <= 0);

        LOGGER.debug("record : {}", record);
        return record.toString();

    }

    /**
     * returns features set that are in a list of device histories
     */
    public static Set<String> getFeatureSetFromDeviceHistories(
            List<DeviceFeaturePair> deviceFeatureHistory,
            Set<FeatureType> featureTypesRequested) {

        Set<String> featureSet = new HashSet<>();

        int numberOfDevicesWithFeatures = 0;
        int numberOfDevicesWithMatchingTypeFeatures = 0;
        for (DeviceFeaturePair feature : deviceFeatureHistory) {
            numberOfDevicesWithFeatures++;
            if (featureTypesRequested.contains(feature.getFeatureHistory().getFeature().getType())) {
                numberOfDevicesWithMatchingTypeFeatures++;
                featureSet.add(feature.getFeatureHistory().getFeature().getName());
            } else {
                LOGGER.debug("not containing features wanted : featureTypesRequested : {} , feature type : {} ",
                        featureTypesRequested,
                        feature.getFeatureHistory().getFeature().getType());

            }
        }

        if (featureSet.isEmpty()) {
            LOGGER.warn("emptyFeature set for device : {}, numberOfDevicesWithFeatures : {}, " +
                            "numberOfDevicesWithMatchingTypeFeatures : {} ",
                    deviceFeatureHistory.get(0).getDevice().getDeviceId(),
                    numberOfDevicesWithFeatures,
                    numberOfDevicesWithMatchingTypeFeatures);
        } else {
            LOGGER.trace("extracted {} features from device {}, numberOfDevicesWithFeatures : {} " +
                    " numberOfDevicesWithMatchingTypeFeatures : {} ",
                            featureSet.size(),
                            deviceFeatureHistory.get(0).getDevice().getDeviceId(),
                            numberOfDevicesWithFeatures,
                            numberOfDevicesWithMatchingTypeFeatures);

        }

        return featureSet;
    }
    public static Set<Long> getAllFeatureIndexesInDeviceFeatureHistory(
            List<DeviceFeaturePair> deviceFeatureHistory,
            Map<String, Integer> featureIndexMap) {
        Set<Long> allIndexes = new HashSet<>();
        for (DeviceFeaturePair feature : deviceFeatureHistory) {
            Integer featIndexPtr = featureIndexMap.get(
                    feature.getFeatureHistory().getFeature().getName());
            LOGGER.trace("feature {} has index : {}",
                    feature.getFeatureHistory().getFeature().getName(), featIndexPtr);
            if (featIndexPtr != null) {
                allIndexes.add(Long.valueOf(featIndexPtr));
            }
        }
        return allIndexes;
    }


    public static boolean isEligibleToRun(ModelRequest modelRequest, int rebuildBadModelsAfterMinutes) {

        boolean eligibleToRun = false;
        String reason = "none";
        if (modelRequest.isRebuildModelOnNextRun()) {
            //rebuilding model or model variation per user request
            //no matter if the model is created or not
            eligibleToRun = true;
            reason = "by user selection";
        } else if (modelRequest.getModelResult() == null) {
            eligibleToRun = true;
            reason = "no model result was found";
        } else {
            long diffInSeconds = DateTimeUtil.getNowInSecond() -
                    modelRequest.getModelResult().getUpdatedAt().getEpochSecond();
            LOGGER.debug("model diffInSeconds : {}, modelResult.updatedAt.getEpochSecond() : {}",
                    diffInSeconds,
                    modelRequest.getModelResult().getUpdatedAt().getEpochSecond());

            if (diffInSeconds > TimeUnit.MINUTES.toSeconds(rebuildBadModelsAfterMinutes)
                && !modelRequest.getModelResult().getRedFlags().isEmpty()
                && !modelRequest.getModelResult().getProcessResult().equals(ModelResult.Result.CREATED)) {
                LOGGER.info("model was build less than {} minutes ago, so not eligible to run.diffInSeconds : {} modelResult.updatedAt.getEpochSecond() : {}",
                        rebuildBadModelsAfterMinutes,
                        diffInSeconds,
                        modelRequest.getModelResult().getUpdatedAt().getEpochSecond());
                eligibleToRun = true;
                reason = "bad model was made before a while ago";
            }
        }

        if (modelRequest.getModelResult() == null || modelRequest.getModelResult().getProcessResult().equals(UNKNOWN)) {
            reason = "building for the first time";
            eligibleToRun = true;
        }
        if (eligibleToRun) {
            LOGGER.debug("model request id {} is eligible to run  for reason : {}, redFlags : {}, process_result : {}",
                    modelRequest.getId(),
                    reason,
                    modelRequest.getModelResult() == null ? "null" : modelRequest.getModelResult().getRedFlags(),
                    modelRequest.getModelResult() == null ? "not started yet" : modelRequest.getModelResult().getProcessResult());
        } else {
            LOGGER.debug("model request id {} is not eligible to run for reason : {}, redFlags : {}, process_result : {}",
                    modelRequest.getId(), reason,
                    modelRequest.getModelResult().getRedFlags(),
                    modelRequest.getModelResult().getProcessResult());
        }
        return eligibleToRun;
    }

}

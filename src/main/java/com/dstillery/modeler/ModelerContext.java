package com.dstillery.modeler;

import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.devicefeature.DeviceHistorySet;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.modeling.predictive.RedFlag;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ModelerContext {
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelerContext.class);

    private Map<String, String> queryParams = new HashMap<>();
    private Map<String, String> cookieMap = new HashMap<>();
    private Map<String, Integer> featureIndexMap = new HashMap<>();
    private ModelRequest modelRequest;
    private List<DeviceHistorySet> allDeviceHistories = new ArrayList<>();
    private List<DeviceHistorySet> positiveDeviceHistories = new ArrayList<>();
    private List<DeviceHistorySet> negativeDeviceHistories = new ArrayList<>();
    private Set<RedFlag> redFlags = new HashSet<>();

    public ModelerContext(ModelRequest modelRequest) {
        this.modelRequest = modelRequest;
    }

    public Map<String, String> getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(Map<String, String> queryParams) {
        this.queryParams = queryParams;
    }

    public Map<String, String> getCookieMap() {
        return cookieMap;
    }

    public void setCookieMap(Map<String, String> cookieMap) {
        this.cookieMap = cookieMap;
    }

    public ModelRequest getModelRequest() {
        return modelRequest;
    }

    public void setModelRequest(ModelRequest modelRequest) {
        this.modelRequest = modelRequest;
    }

    public List<DeviceHistorySet> getAllDeviceHistories() {
        return allDeviceHistories;
    }

    public void setAllDeviceHistories(List<DeviceHistorySet> allDeviceHistories) {
        this.allDeviceHistories = allDeviceHistories;
    }

    public List<DeviceHistorySet> getPositiveDeviceHistories() {
        return positiveDeviceHistories;
    }

    public void setPositiveDeviceHistories(List<DeviceHistorySet> positiveDeviceHistories) {
        this.positiveDeviceHistories = positiveDeviceHistories;
    }

    public List<DeviceHistorySet> getNegativeDeviceHistories() {
        return negativeDeviceHistories;
    }

    public void setNegativeDeviceHistories(List<DeviceHistorySet> negativeDeviceHistories) {
        this.negativeDeviceHistories = negativeDeviceHistories;
    }

    public Set<RedFlag> getRedFlags() {
        return redFlags;
    }

    public void setRedFlags(Set<RedFlag> redFlags) {
        this.redFlags = redFlags;
    }

    public List<DeviceHistorySet> combineDeviceHistories() {
        List<DeviceHistorySet> allDeviceHistories = new ArrayList<>(getPositiveDeviceHistories());
        allDeviceHistories.addAll(getNegativeDeviceHistories());
        return allDeviceHistories;
    }

    public Map<String, Integer> getFeatureIndexMap() {
        return featureIndexMap;
    }

    public void buildFeatureIndexMap() {
        for (DeviceHistorySet oneDeviceHistory :
                getAllDeviceHistories()) {
            Set<String> allDeviceFeatureSet = ModelUtil.getFeatureSetFromDeviceHistories(
                    oneDeviceHistory.getHistories(), getModelRequest().getFeatureTypesRequested());
            ModelUtil.updateFeatureIndexMapWithNewFeatures(allDeviceFeatureSet, featureIndexMap);
            LOGGER.trace("added {} features to featureIndexMap", allDeviceFeatureSet.size());
        }
    }

    public void addPositiveDeviceHistories(List<DeviceFeaturePair> alldevFeatureHistory) {
        alldevFeatureHistory.forEach(deviceFeatureHistory -> {
            deviceFeatureHistory.setPositiveHistoryValue(true);
        });
        getPositiveDeviceHistories().add(new DeviceHistorySet(alldevFeatureHistory));
    }
}

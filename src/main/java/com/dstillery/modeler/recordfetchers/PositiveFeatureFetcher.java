package com.dstillery.modeler.recordfetchers;/*
  PositiveFeatureFetcher.h

   Created on: Oct 22, 2015
       Author: mtaabodi
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.dstillery.common.modeling.predictive.ModelType;
import com.dstillery.modeler.ModelerContext;
import com.dstillery.modeler.ModelerModule;
import com.dstillery.modeler.recordfetchers.positive.PixelBasedPositiveFeatureFetcher;
import com.dstillery.modeler.recordfetchers.positive.SeedBasedPositiveFeatureFetcher;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PositiveFeatureFetcher extends ModelerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(PositiveFeatureFetcher.class);
    public static final ObjectMapper MAPPER = new ObjectMapper();

    private SeedBasedPositiveFeatureFetcher seedBasedPositiveFeatureFetcher;
    private PixelBasedPositiveFeatureFetcher pixelBasedPositiveFeatureFetcher;

    public PositiveFeatureFetcher(PixelBasedPositiveFeatureFetcher pixelBasedPositiveFeatureFetcher,
                                  SeedBasedPositiveFeatureFetcher seedBasedPositiveFeatureFetcher) {
        this.seedBasedPositiveFeatureFetcher = seedBasedPositiveFeatureFetcher;
        this.pixelBasedPositiveFeatureFetcher = pixelBasedPositiveFeatureFetcher;
    }

    public void process(ModelerContext context) {
        if (context.getModelRequest().getModelType() == ModelType.SEED_MODEL) {
            seedBasedPositiveFeatureFetcher.fetchPositiveFeaturesBasedOnSeedModel(context);
            LOGGER.debug("positiveDeviceHistories size : {} for seedWebsites : {}",
                    context.getPositiveDeviceHistories().size(),
                    context.getModelRequest().getSeedModelFields().getSeedWebsites());

        } else if (context.getModelRequest().getModelType() == ModelType.PIXEL_MODEL) {
            pixelBasedPositiveFeatureFetcher.fetchPositiveFeaturesBasedOnPixelModel(context);

            LOGGER.debug("positiveDeviceHistories size : {} for positiveOfferIds : {} ",
                    context.getPositiveDeviceHistories().size(),
                    context.getModelRequest().getPixelModelFields().getPositivePixelIds()
            );
        } else {
            throw new IllegalArgumentException("model type is wrong. type : " + context.getModelRequest().getModelType() + ", request : " +
                    context.getModelRequest());
        }
    }


}

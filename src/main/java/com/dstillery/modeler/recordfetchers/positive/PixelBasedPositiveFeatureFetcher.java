package com.dstillery.modeler.recordfetchers.positive;/*
  PositiveFeatureFetcher.h

   Created on: Oct 22, 2015
       Author: mtaabodi
 */

import static com.dstillery.common.modeling.predictive.RedFlag.NO_DEVICES_HITTING_THE_PIXEL;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.predictive.RedFlag;
import com.dstillery.common.pixel.Pixel;
import com.dstillery.common.pixel.PixelCacheService;
import com.dstillery.common.pixel.PixelDeviceHistory;
import com.dstillery.modeler.ModelerContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public class PixelBasedPositiveFeatureFetcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(PixelBasedPositiveFeatureFetcher.class);
    public static final ObjectMapper MAPPER = new ObjectMapper();

    private PixelBasedDeviceFeatureService pixelBasedDeviceFeatureService;
    private PixelCacheService pixelCacheService;
    public PixelBasedPositiveFeatureFetcher(
            PixelBasedDeviceFeatureService pixelBasedDeviceFeatureService,
            PixelCacheService pixelCacheService) {
        this.pixelBasedDeviceFeatureService = pixelBasedDeviceFeatureService;
        this.pixelCacheService = pixelCacheService;
    }

    public void fetchPositiveFeaturesBasedOnPixelModel(ModelerContext context) {
        List<Pixel> pixels = pixelCacheService.getAllEntities()
                .stream().filter(
                        pixel -> context.getModelRequest().getPixelModelFields()
                                .getNegativePixelIds().contains(pixel.getId()))
                .collect(Collectors.toList());
        LOGGER.debug("started fetching allDevicesHittingThePixel ");
        List<PixelDeviceHistory> allDevicesHittingThePixel  =
                pixelBasedDeviceFeatureService.fetchAllDevicesHittingPixelsForPixels(
                        pixels,
                        context.getModelRequest().getPixelModelFields().getPixelHitRecencyInSecond());
        LOGGER.debug("fetched {} allDevicesHittingThePixel ", allDevicesHittingThePixel.size());
        if (allDevicesHittingThePixel.isEmpty()) {
            context.getRedFlags().add(NO_DEVICES_HITTING_THE_PIXEL);
        }

        LOGGER.debug("started fetching fetchAllDeviceFeaturesFromPixelDeviceHistories ");
        List<List<DeviceFeaturePair>> allDeviceFeatureHistories  =
                pixelBasedDeviceFeatureService.fetchAllDeviceFeaturesFromPixelDeviceHistories(
                        allDevicesHittingThePixel, context.getModelRequest());
        LOGGER.debug("allDeviceFeatureHistories size : {}",allDeviceFeatureHistories.size());

        allDeviceFeatureHistories.forEach(context::addPositiveDeviceHistories);

        LOGGER.debug("cleaned up all features");

        context.getModelRequest().getModelResult().setNumberOfPositiveDevicesUsed(
                context.getModelRequest().getModelResult().getNumberOfPositiveDevicesUsed() +
                        allDeviceFeatureHistories.size());

        context.getModelRequest().setNumberOfPositiveDevicesToBeUsed(allDeviceFeatureHistories.size());
        if (context.getPositiveDeviceHistories().isEmpty()) {
            context.getRedFlags().add(RedFlag.NO_POSITIVE_DEVICE_HISTORY);
        }
    }
}

package com.dstillery.modeler.recordfetchers.positive;


import static com.dstillery.common.feature.FeatureType.getAllFeatureTypes;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.pixel.Pixel;
import com.dstillery.common.pixel.PixelDeviceHistory;
import com.dstillery.common.pixel.PixelDeviceHistoryCassandraService;
import com.dstillery.common.util.GUtil;

 /* FeatureFetcher.h

   Created on: Oct 22, 2015
       Author: mtaabodi
 */

public class PixelBasedDeviceFeatureService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PixelBasedDeviceFeatureService.class);

    private MetricReporterService metricReporterService;
    private PixelDeviceHistoryCassandraService pixelDeviceHistoryCassandraService;
    private DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService;

    public PixelBasedDeviceFeatureService(MetricReporterService metricReporterService,
                                          PixelDeviceHistoryCassandraService pixelDeviceHistoryCassandraService,
                                          DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService) {
        this.metricReporterService = metricReporterService;;
        this.pixelDeviceHistoryCassandraService = pixelDeviceHistoryCassandraService;
        this.deviceFeatureHistoryCassandraService = deviceFeatureHistoryCassandraService;
    }

    public List<PixelDeviceHistory> fetchAllDevicesHittingPixelsForPixels(List<Pixel> pixels, int pixelHitRecencyInSecond) {

        List<PixelDeviceHistory> allDevicesHittingThePixel= new ArrayList<>();

        for(Pixel pixel :  pixels) {

                List<PixelDeviceHistory> pixelDeviceHistories =
                        pixelDeviceHistoryCassandraService.readDeviceHistoryOfPixel(
                                pixel.getUniqueKey(),
                                pixelHitRecencyInSecond);

                if (pixelDeviceHistories.isEmpty()) {
                    LOGGER.warn("devicesHittingPixel is empty for this pixelUniqueKey : {} , pixelHitRecencyInSecond : {}",
                            pixel.getUniqueKey(), pixelHitRecencyInSecond);
                    metricReporterService.addStateModuleForEntity("EXCEPTION_NO_HISTORY_FOR_PIXEL",
                            "PixelBasedDeviceFeatureService",
                            "pixel" + pixel.getUniqueKey());

                }
                //TODO commented for now. but helpful for testing and debugging
                // TODO : addOnlyDevicesWithHistory(pixelHitRecencyInSecond, allDevicesHittingThePixel, pixelDeviceHistory);
                allDevicesHittingThePixel.addAll(pixelDeviceHistories);
                if (allDevicesHittingThePixel.size() >= 1000 ) {

                    //we don't to load too many devices
                    return allDevicesHittingThePixel;
                }
            }

        return allDevicesHittingThePixel;
    }

    private void addOnlyDevicesWithHistory(int pixelHitRecencyInSecond, List<PixelDeviceHistory> allDevicesHittingThePixel, PixelDeviceHistory pixelDeviceHistory) {
        List<DeviceFeaturePair> deviceFeatureHistoryResponse =
                deviceFeatureHistoryCassandraService.
                        readFeatureHistoryOfDevice(pixelDeviceHistory.getDevice(),
                                pixelHitRecencyInSecond,
                                getAllFeatureTypes(),
                                1);
        if (!deviceFeatureHistoryResponse.isEmpty()) {
            allDevicesHittingThePixel.add(pixelDeviceHistory);
            LOGGER.debug("adding device with history: {}", pixelDeviceHistory.getDevice());
        } else {
            LOGGER.debug("ignoring history less device : {}", pixelDeviceHistory.getDevice());
        }
    }

    public List<List<DeviceFeaturePair>> fetchAllDeviceFeaturesFromPixelDeviceHistories(
            List<PixelDeviceHistory> allDevicesHittingThePixel,
            ModelRequest modelDto) {

        List<List<DeviceFeaturePair>> allDeviceFeatureHistories = new ArrayList<>();

        for(PixelDeviceHistory pixelDeviceHistory : allDevicesHittingThePixel) {
                LOGGER.trace("device hit pixel at : {} , deviceId : {},  deviceType : {} ",
                        pixelDeviceHistory.getTimeOfVisit(),
                        pixelDeviceHistory.getDevice().getDeviceId(),
                        pixelDeviceHistory.getDevice().getDeviceType());

                Long featureRecencyInSecond = modelDto.getFeatureRecencyInSecond();

                List<DeviceFeaturePair> deviceFeatureHistoryResponse =
                        deviceFeatureHistoryCassandraService.
                                readFeatureHistoryOfDevice(pixelDeviceHistory.getDevice(),
                                        featureRecencyInSecond,
                                        modelDto.getFeatureTypesRequested(),
                                        modelDto.getMaxNumberOfHistoryPerDevice());

                if (GUtil.allowedToCall(1000)) {
                    LOGGER.debug("size of feature loaded by this device : {}", deviceFeatureHistoryResponse.size());
                }
                if (deviceFeatureHistoryResponse.isEmpty()) {
                    continue;
                }

                allDeviceFeatureHistories.add(deviceFeatureHistoryResponse);
                LOGGER.trace("size of allDeviceFeatureHistories is {} after adding histories", allDeviceFeatureHistories.size());

                //TODO make this property
                if (allDeviceFeatureHistories.size() >= 1000 ) {

                    //we don't to load too many devices
                    break;
                }
        }

        return allDeviceFeatureHistories;
    }

}

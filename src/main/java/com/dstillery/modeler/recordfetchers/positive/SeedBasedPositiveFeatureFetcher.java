package com.dstillery.modeler.recordfetchers.positive;

import static com.dstillery.common.modeling.predictive.RedFlag.NO_POSITIVE_DEVICE_HISTORY;
import static com.dstillery.common.modeling.predictive.RedFlag.NO_SEEDS;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.devicefeature.DeviceHistorySet;
import com.dstillery.common.modeling.predictive.FeatureDbDto;
import com.dstillery.modeler.ModelerContext;

public class SeedBasedPositiveFeatureFetcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeedBasedPositiveFeatureFetcher.class);

    private DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService;

    public SeedBasedPositiveFeatureFetcher(DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService) {
        this.deviceFeatureHistoryCassandraService = deviceFeatureHistoryCassandraService;
    }

    public void fetchPositiveFeaturesBasedOnSeedModel(
            ModelerContext context) {
        if (context.getModelRequest().getSeedModelFields().getSeedWebsites().isEmpty()) {
            LOGGER.error("seedWebsites filed is empty...aborting");
            context.getRedFlags().add(NO_SEEDS);
            return;
        }

        fetchPositiveDevicesBasedOnRandomDevices(context);

        if (context.getPositiveDeviceHistories().isEmpty()) {
            context.getRedFlags().add(NO_POSITIVE_DEVICE_HISTORY);
        }
    }

    private class PositiveTask implements Consumer<DeviceFeaturePair> {
        private final Set<String> allPositiveSeeds;

        private List<DeviceFeaturePair> allPositiveDevices = new ArrayList<>();

        public PositiveTask(ModelerContext context) {
            allPositiveSeeds = context.getModelRequest().getSeedModelFields().getSeedWebsites()
                    .stream().map(FeatureDbDto::getName).collect(Collectors.toSet());
            LOGGER.debug("allPositiveSeeds is : {}", allPositiveSeeds);
        }

        @Override
        public void accept(DeviceFeaturePair deviceFeaturePair) {
            String featureName = deviceFeaturePair.getFeatureHistory().getFeature().getName();
            if (allPositiveSeeds.contains(featureName)) {
                allPositiveDevices.add(deviceFeaturePair);
            }
        }
    }
    private void fetchPositiveDevicesBasedOnRandomDevices(
            ModelerContext context) {

        PositiveTask task = new PositiveTask(context);
        fetchFeaturesLookingBack(context, task);


        List<DeviceHistorySet> allPositiveHistories = task.allPositiveDevices.stream().map(neg -> {
            List<DeviceFeaturePair> histories = deviceFeatureHistoryCassandraService.readFeatureHistoryOfDevice(neg.getDevice(),
                    context.getModelRequest().getFeatureRecencyInSecond(),
                    context.getModelRequest().getFeatureTypesRequested(),
                    context.getModelRequest().getMaxNumberOfHistoryPerDevice());
            if (histories.isEmpty()) {
                return null;
            }

            context.addPositiveDeviceHistories(histories);
            return new DeviceHistorySet(histories);
        }).filter(Objects::nonNull).collect(Collectors.toList());

        LOGGER.debug("allPositiveHistories size is : {}", allPositiveHistories.size());
    }

    private void fetchFeaturesLookingBack(ModelerContext context, PositiveTask task) {
        int numberOfTries = 1;
        while(task.allPositiveDevices.size() <= 1000 && numberOfTries <= 10) {

            long endTime = Instant.now().minus(
                    context.getModelRequest().getFeatureRecencyInSecond() * (numberOfTries - 1), ChronoUnit.SECONDS).toEpochMilli();

            long startTime = Instant.now().minus(
                    context.getModelRequest().getFeatureRecencyInSecond() * (numberOfTries), ChronoUnit.SECONDS).toEpochMilli();

            String query = "select * from gicapods_mdl.devicehistory where timeofvisit > " + startTime + " and timeofvisit < " + endTime + " LIMIT 10000 ALLOW FILTERING;";

            deviceFeatureHistoryCassandraService.readWithPaging(query, task);
            numberOfTries++;

            LOGGER.debug("query: {}",  query);
            LOGGER.debug("task.allPositiveDevices so far: {}",  task.allPositiveDevices.size());
        }
    }
}

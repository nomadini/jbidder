package com.dstillery.modeler.recordfetchers;
import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.modeling.devicefeature.DeviceHistorySet;
import com.dstillery.common.modeling.featuredevice.FeatureDeviceHistoryCassandraService;
import com.dstillery.common.modeling.predictive.FeatureDbDto;
import com.dstillery.common.modeling.predictive.ModelType;
import com.dstillery.modeler.ModelerContext;
import com.dstillery.modeler.ModelerModule;

/*
FeatureFetcher.h

  Created on: Oct 22, 2015
      Author: mtaabodi
*/

public class NegativeFeatureFetcher extends ModelerModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(NegativeFeatureFetcher.class);

    private MetricReporterService metricReporterService;
    private DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService;

    public NegativeFeatureFetcher(MetricReporterService metricReporterService,
                                  DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService) {
        this.metricReporterService = metricReporterService;
        this.deviceFeatureHistoryCassandraService = deviceFeatureHistoryCassandraService;
    }

    public void process(ModelerContext context) {

        if (context.getModelRequest().getModelType() == ModelType.SEED_MODEL) {
            assertAndThrow(context.getModelRequest().getSeedModelFields().getSeedWebsites().size() != 0);
            fetchNegativeDevicesBasedOnRandomDevices(context, deviceFeatureHistoryCassandraService);
            LOGGER.debug("negativeDeviceHistories size :  {} for seedWebsites {} : {} ",
                    context.getNegativeDeviceHistories().size(),
                    context.getModelRequest().getSeedModelFields().getSeedWebsites());

        } else if (context.getModelRequest().getModelType() == ModelType.PIXEL_MODEL) {

            fetchNegativeDevicesBasedOnRandomDevices(context, deviceFeatureHistoryCassandraService);

            LOGGER.debug("negativeDeviceHistories size : {} for negativeOfferIds : {}",
                    context.getNegativeDeviceHistories().size(),
                    context.getModelRequest().getPixelModelFields().getNegativePixelIds());

        } else {
            throw new IllegalArgumentException("model type is wrong. type : " + context.getModelRequest().getModelType() + ", request : " + context.getModelRequest());
        }
    }

    public void fetchNegativeDevicesBasedOnRandomDevices(
            ModelerContext context,
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService) {

        List<DeviceFeaturePair> randomDevices = deviceFeatureHistoryCassandraService.readWithCustomQuery(
                "select * from gicapods_mdl.devicehistory where timeofvisit > " +
                        context.getModelRequest().getFeatureRecencyInSecond() + " LIMIT " + context.getModelRequest().getNumberOfNegativeDevicesToBeUsed() + " ALLOW FILTERING;",
                100000);

        if (context.getPositiveDeviceHistories().isEmpty()) {
            throw new RuntimeException("positive devices must be found before finding negative devices");
        }
        Set<String> allPositiveDeviceIds =
                context.getPositiveDeviceHistories()
                        .stream()
                        .map(d -> d.getDevice().getDeviceUniqueId())
                        .collect(Collectors.toSet());

        List<DeviceFeaturePair> allNegativeDevices = randomDevices.stream()
                .filter(deviceFeaturePair -> !allPositiveDeviceIds.contains(deviceFeaturePair.getDevice().getDeviceUniqueId()))
                .collect(Collectors.toList());

        List<DeviceHistorySet> allNegativeHistories = allNegativeDevices.stream().map(neg -> {
            List<DeviceFeaturePair> histories = deviceFeatureHistoryCassandraService.readFeatureHistoryOfDevice(neg.getDevice(),
                    context.getModelRequest().getFeatureRecencyInSecond(),
                    context.getModelRequest().getFeatureTypesRequested(),
                    context.getModelRequest().getMaxNumberOfHistoryPerDevice());
            if (histories.isEmpty()) {
                return null;
            }
            return new DeviceHistorySet(histories);
        }).filter(Objects::nonNull).collect(Collectors.toList());
        context.setNegativeDeviceHistories(allNegativeHistories);
    }

}

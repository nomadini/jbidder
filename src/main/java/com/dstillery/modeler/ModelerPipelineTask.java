package com.dstillery.modeler;
/*
  ModelerPipelineTask.h

   Created on: Sep 7, 2015
       Author: mtaabodi
*/


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.predictive.ModelRequestValidator;
import com.dstillery.common.modeling.predictive.ModelResult;
import com.dstillery.common.modeling.predictive.MySqlModelResultService;
import com.dstillery.common.modeling.predictive.MySqlModelService;
import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.modeler.engine.FeatureBasedModeler;
import com.dstillery.modeler.engine.ModelDataCleaner;
import com.dstillery.modeler.engine.ModelerPersistenceService;
import com.dstillery.modeler.recordfetchers.NegativeFeatureFetcher;
import com.dstillery.modeler.recordfetchers.PositiveFeatureFetcher;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.modeling.predictive.RedFlag.UNKNOWN_ERROR;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;

public class ModelerPipelineTask implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelerPipelineTask.class);

    private ModelerContext modelerContext;

    private MetricReporterService metricReporterService;
    private MySqlModelResultService mySqlModelResultService;
    private MySqlModelService mySqlModelService;
    private List<ModelerModule> modules = new ArrayList<>();
    private ModelRequestValidator modelRequestValidator = new ModelRequestValidator();

    public ModelerPipelineTask(ModelerContext modelerContext,
                               MetricReporterService metricReporterService,
                               NegativeFeatureFetcher negativeFeatureFetcher,
                               PositiveFeatureFetcher positiveFeatureFetcher,
                               ModelDataCleaner modelDataCleaner,
                               FeatureBasedModeler featureBasedModeler,
                               ModelerPersistenceService modelerPersistenceService,
                               MySqlModelResultService mySqlModelResultService,
                               MySqlModelService mySqlModelService) {
        this.modelerContext = Objects.requireNonNull(modelerContext);
        this.metricReporterService = metricReporterService;;
        this.mySqlModelService = mySqlModelService;
        this.mySqlModelResultService = mySqlModelResultService;

        modules.add(positiveFeatureFetcher);
        modules.add(negativeFeatureFetcher);
        modules.add(modelDataCleaner);
        modules.add(featureBasedModeler);
        modules.add(modelerPersistenceService);
    }

    public void run() {
        LOGGER.info("processing this request : {} ",  modelerContext.getModelRequest());
        if (!modelRequestValidator.validate(modelerContext.getModelRequest())) {
            LOGGER.error("model is invalid : {}", modelerContext.getModelRequest());
            return;
        }

        initialProcessing();

        for (ModelerModule module : modules) {

            try {
                long startTime = DateTimeUtil.getNowInSecond();
                LOGGER.debug("started processing module {}", module.getClass().getSimpleName());
                module.process(modelerContext);
                long timeDiffInSeconds = DateTimeUtil.getNowInSecond() - startTime;
                LOGGER.debug("finished processing module {} in {} minutes", module.getClass().getSimpleName(), (float) (timeDiffInSeconds / 60));
            } catch (Exception e) {
                LOGGER.warn("exception", e);
                metricReporterService.
                        addStateModuleForEntity(
                                "exceptionInModeler:UnknownException",
                                "ModelerPipelineTask",
                                ALL_ENTITIES,
                                MetricReporterService.MetricPriority.EXCEPTION);
                LOGGER.warn("error while building model", e);
                modelerContext.getRedFlags().add(UNKNOWN_ERROR);
            }

            if (!modelerContext.getRedFlags().isEmpty()) {
                try {
                    LOGGER.error("aborting because of redflag. {}", OBJECT_MAPPER.writeValueAsString(modelerContext.getRedFlags()));
                } catch (JsonProcessingException e) {
                    LOGGER.warn("exception occurred", e);
                }
                modelerContext.getModelRequest().getModelResult().setProcessResult(ModelResult.Result.FAILED);
                modelerContext.getModelRequest().getModelResult().setRedFlags(new ArrayList<>(modelerContext.getRedFlags()));
                updateModelAndResult(modelerContext);
                break;
            }
        }
    }

    private void initialProcessing() {
        if (modelerContext.getModelRequest().getModelResult() == null) {
            modelerContext.getModelRequest().setModelResult(new ModelResult());
        }
        modelerContext.getModelRequest().getModelResult().setProcessResult(ModelResult.Result.STARTED);
        updateModelAndResult(modelerContext);
    }

    private void updateModelAndResult(
            ModelerContext context) {
        try {
            mySqlModelService.upsertModel (context.getModelRequest());
            context.getModelRequest().getModelResult().setModelRequestId(context.getModelRequest().getId());
            mySqlModelResultService.upsertModel (context.getModelRequest().getModelResult());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ModelerContext getModelerContext() {
        return modelerContext;
    }
}

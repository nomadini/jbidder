package com.dstillery.modeler;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class InterestTreeParser {
    public static void main(String[] args) throws IOException {
        String path = "/Users/mtaabodi/Documents/workspace-mt/adserv-mt/HowTo/knowledge/interest_contents.tsv";
        List<String> content = FileUtils.readLines(new File(path), StandardCharsets.UTF_8);
        Map<String, Set<String>> tagsToDomains = new HashMap<>();
        Map<String, Set<String>> domainToTags = new HashMap<>();
        content.forEach(line -> {
                List<String> lineArray = Arrays.asList(line.split("\t"));
//                System.out.println("original line : " + lineArray);
                String domainName = lineArray.get(2);

                List<String> originalTagsList = lineArray.subList(7, lineArray.size());
                if (originalTagsList.isEmpty()) {
                    return;
                }
                Set<String> tags = new HashSet<>(Arrays.asList(originalTagsList.get(0).split(",")));

//                System.out.println("domain : " + domainName);
//                System.out.println("tags : " + tags);

            Set<String> finalDomainNames = ImmutableSet.of(domainName);
            domainToTags.computeIfPresent(domainName, new BiFunction<String, Set<String>, Set<String>>() {
                @Override
                public Set<String> apply(String s, Set<String> strings) {
                    Set<String> newTags = new HashSet<>();
                    newTags.addAll(tags);
                    newTags.addAll(strings);
                    return newTags;
                }
            });
            domainToTags.computeIfAbsent(domainName, new Function<String, Set<String>>() {
                @Override
                public Set<String> apply(String s) {
                    return tags;
                }
            });
            tags.forEach(tag -> {
//                System.out.println("tag : " + tag);
                    tagsToDomains.computeIfPresent(tag, (s, strings) -> {
                        Set<String> newList = new HashSet<>(strings);
                        newList.addAll(finalDomainNames);
                        return newList;
                    });
                    tagsToDomains.computeIfAbsent(tag, new Function<String, Set<String>>() {
                        @Override
                        public Set<String> apply(String s) {
                            return finalDomainNames;
                        }
                    });
//                    System.out.println(tagsToDomains);
                });
        });

        domainToTags.forEach(new BiConsumer<String, Set<String>>() {
            @Override
            public void accept(String s, Set<String> tags) {
                System.out.println("domain : " + s + ", tags : " + tags);
            }
        });
//
//        tagsToDomains.forEach(new BiConsumer<String, List<String>>() {
//            @Override
//            public void accept(String tag, List<String> domains) {
//                System.out.println("tag : " + tag + ", domains : " + domains);
//            }
//        });

    }
}

package com.dstillery.modeler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.feature.FeatureType;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.predictive.ModelCacheService;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.modeling.predictive.MySqlModelResultService;
import com.dstillery.common.modeling.predictive.MySqlModelService;
import com.dstillery.common.util.StringUtil;
import com.dstillery.modeler.engine.FeatureBasedModeler;
import com.dstillery.modeler.engine.ModelDataCleaner;
import com.dstillery.modeler.engine.ModelerPersistenceService;
import com.dstillery.modeler.recordfetchers.NegativeFeatureFetcher;
import com.dstillery.modeler.recordfetchers.PositiveFeatureFetcher;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class ModelVariationBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelVariationBuilder.class);

    private MetricReporterService metricReporterService;
    private ModelCacheService modelCacheService;
    private Map<String, Set<FeatureType>> typePermutationNameMap = new HashMap<>();

    private ModelDataCleaner modelDataCleaner;
    private NegativeFeatureFetcher negativeFeatureFetcher;
    private PositiveFeatureFetcher positiveFeatureFetcher;
    private ModelerPersistenceService modelerPersistenceService;
    private FeatureBasedModeler featureBasedModeler;
    private MySqlModelService mySqlModelService;
    private MySqlModelResultService mySqlModelResultService;

    public ModelVariationBuilder(MetricReporterService metricReporterService,
                                 ModelCacheService modelCacheService,
                                 ModelDataCleaner modelDataCleaner,
                                 NegativeFeatureFetcher negativeFeatureFetcher,
                                 PositiveFeatureFetcher positiveFeatureFetcher,
                                 ModelerPersistenceService modelerPersistenceService,
                                 FeatureBasedModeler featureBasedModeler,
                                 MySqlModelResultService mySqlModelResultService,
                                 MySqlModelService mySqlModelService) {
        this.metricReporterService = metricReporterService;;
        this.modelCacheService = modelCacheService;
        this.modelDataCleaner = modelDataCleaner;
        this.negativeFeatureFetcher = negativeFeatureFetcher;
        this.positiveFeatureFetcher = positiveFeatureFetcher;
        this.modelerPersistenceService = modelerPersistenceService;
        this.featureBasedModeler = featureBasedModeler;
        this.mySqlModelResultService = mySqlModelResultService;
        this.mySqlModelService = mySqlModelService;
        List<Set<FeatureType>> featureTypePermutations = ImmutableList.of(
                ImmutableSet.of(FeatureType.GTLD),
                ImmutableSet.of(FeatureType.MGRS100),
                ImmutableSet.of(FeatureType.PLACE_TAG),
                ImmutableSet.of(FeatureType.GTLD, FeatureType.MGRS100),
                ImmutableSet.of(FeatureType.GTLD, FeatureType.PLACE_TAG),
                ImmutableSet.of(FeatureType.MGRS100, FeatureType.PLACE_TAG),
                ImmutableSet.of(FeatureType.GTLD, FeatureType.MGRS100, FeatureType.PLACE_TAG)
        );

        typePermutationNameMap.put(FeatureType.GTLD.name(),
                featureTypePermutations.get(0));

        typePermutationNameMap.put(FeatureType.MGRS100.name(),
                featureTypePermutations.get(1));

        typePermutationNameMap.put(FeatureType.PLACE_TAG.name(),
                featureTypePermutations.get(2));

        typePermutationNameMap.put(FeatureType.GTLD.name() + "_"+ FeatureType.MGRS100.name(),
                featureTypePermutations.get(3));

        typePermutationNameMap.put(FeatureType.GTLD.name() + "_"+ FeatureType.PLACE_TAG.name(),
                featureTypePermutations.get(4));

        typePermutationNameMap.put(FeatureType.MGRS100.name() + "_"+
                        FeatureType.PLACE_TAG.name(),
                featureTypePermutations.get(5));

        typePermutationNameMap.put(FeatureType.GTLD.name() + "_"
                        + FeatureType.GTLD.name() + "_"
                        + FeatureType.PLACE_TAG.name(),
                featureTypePermutations.get(6));
    }

    private List<ModelerPipelineTask> createModelerTaskWithDifferentFeatureRecency(ModelRequest modelRequest,
                                                                                   String description,
                                                                                   String segmentSeedName,
                                                                                   String modelName,
                                                                                   Set<FeatureType> featureTypesRequested) {
        ModelRequest halfRecencyVariation = ModelRequest.clone(modelRequest);
        halfRecencyVariation.setId(-1L);
        halfRecencyVariation.setName(modelName + "_half_recency");
        halfRecencyVariation.setDescription(description + "\nfeatureRecencyInSecond cut down to half");
        halfRecencyVariation.setFeatureRecencyInSecond(modelRequest.getFeatureRecencyInSecond() / 2);
        halfRecencyVariation.setSegmentSeedName(segmentSeedName + "_half_recency");
        halfRecencyVariation.setParentModelRequestId(modelRequest.getId());
        halfRecencyVariation.setFeatureTypesRequested(featureTypesRequested);
        ModelRequest doubleRecencyVariation = ModelRequest.clone(modelRequest);
        doubleRecencyVariation.setId(-1L);
        doubleRecencyVariation.setName(modelName + "_2x_recency");
        doubleRecencyVariation.setDescription(description + "\nfeatureRecencyInSecond doubled");
        doubleRecencyVariation.setFeatureRecencyInSecond(modelRequest.getFeatureRecencyInSecond() * 2);
        doubleRecencyVariation.setSegmentSeedName(segmentSeedName + "_double_recency");
        doubleRecencyVariation.setParentModelRequestId(modelRequest.getId());
        doubleRecencyVariation.setFeatureTypesRequested(featureTypesRequested);

        ModelRequest tenTimesRecencyVariation = ModelRequest.clone(modelRequest);
        tenTimesRecencyVariation.setId(-1L);
        tenTimesRecencyVariation.setName(modelName + "_10x_recency");
        tenTimesRecencyVariation.setDescription(description + "\nfeatureRecencyInSecond ten times");
        tenTimesRecencyVariation.setFeatureRecencyInSecond(modelRequest.getFeatureRecencyInSecond() * 10);
        tenTimesRecencyVariation.setSegmentSeedName(segmentSeedName + "_10x_recency");
        tenTimesRecencyVariation.setParentModelRequestId(modelRequest.getId());
        tenTimesRecencyVariation.setFeatureTypesRequested(featureTypesRequested);

        ModelerPipelineTask halfRecencyVariationTask = createModelerTask(halfRecencyVariation);
        ModelerPipelineTask doubleRecencyVariationTask = createModelerTask(doubleRecencyVariation);
        ModelerPipelineTask tenTimesRecencyVariationTask = createModelerTask(tenTimesRecencyVariation);

        List<ModelerPipelineTask> allVariations = new ArrayList<>();
        allVariations.add(halfRecencyVariationTask);
        allVariations.add(doubleRecencyVariationTask);
        allVariations.add(tenTimesRecencyVariationTask);
        return allVariations;
    }

    public ModelerPipelineTask createModelerTask(
            ModelRequest modelRequest) {
        //we populate real id, if existing
        populateRealIdIfExisting(modelRequest);

        ModelerContext context = new ModelerContext(modelRequest);

        return new ModelerPipelineTask(
                context,
                metricReporterService,
                negativeFeatureFetcher,
                positiveFeatureFetcher,
                modelDataCleaner,
                featureBasedModeler,
                modelerPersistenceService,
                mySqlModelResultService,
                mySqlModelService
        );
    }

    private void populateRealIdIfExisting(
            ModelRequest modelRequest) {
        //if modeler is a variation , it's id is -1,
        //we look up the "name + advertiser_id" combo from a ModelCacheService (because it's unique key for a modelRequest)
        //and if there is any already. we get the real key
        ModelRequest existingModelRequest = modelCacheService.getUniqueKeyToModelMap().get(modelRequest.getUniqueKey());
        if (existingModelRequest != null) {
            modelRequest.setId(existingModelRequest.getId());
            LOGGER.debug("finding modelRequest for variation real id : {} ", modelRequest.getId());
        }
    }

    List<ModelerPipelineTask> createModelerTaskVariations(
            ModelRequest modelRequest) {
        List<ModelerPipelineTask> allVariations = new ArrayList<>();

        for (Map.Entry<String, Set<FeatureType>> entry : typePermutationNameMap.entrySet()) {

            String description = modelRequest.getDescription();
            String segmentSeedName = modelRequest.getSegmentSeedName();
            String modelName = modelRequest.getName();
            segmentSeedName = StringUtil.replaceString(segmentSeedName, "featureTypesRequest_", "");

            description = description + "\nfeatureTypesRequested changed to " + entry.getKey();
            segmentSeedName = segmentSeedName + "_f_type_req_" + entry.getKey();

            //changing name causes us to insert new model rather than updating parents
            //_f_type_req_ means featureTypesRequested
            modelName = modelName + "_f_type_req_" + entry.getKey();

            List<ModelerPipelineTask> allVariationsWithFeatureTypesWanted =
                    createModelerTaskWithDifferentFeatureRecency(
                            modelRequest,
                            description,
                            segmentSeedName,
                            modelName,
                            entry.getValue());

            //TODO : change these two features too
            // clone.maxNumberOfDeviceHistoryPerFeature = original.maxNumberOfDeviceHistoryPerFeature;                //
            // clone.maxNumberOfNegativeFeaturesToPick = original.maxNumberOfNegativeFeaturesToPick;
            LOGGER.debug("created {} variations for permutation : {}", allVariationsWithFeatureTypesWanted.size(), entry);
            LOGGER.debug("created {} variations for permutation : {}, variations : {}", allVariationsWithFeatureTypesWanted.size(), entry, allVariationsWithFeatureTypesWanted);
            allVariations.addAll(allVariationsWithFeatureTypesWanted);
        }
        return allVariations;
    }

}

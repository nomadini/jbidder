package com.dstillery.modeler;

public abstract class ModelerModule {

public abstract void process(ModelerContext context);
}

package com.dstillery.modeler;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aerospike.client.util.Util;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.predictive.ModelResult;
import com.dstillery.common.util.DateTimeUtil;
import com.google.common.collect.ImmutableMap;

public class ModelTaskRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelTaskRunner.class);

    private MetricReporterService metricReporterService;
    private int delayBetweenBuildingEachModelInSeconds;

    public ModelTaskRunner(MetricReporterService metricReporterService, int delayBetweenBuildingEachModelInSeconds) {
        this.metricReporterService = metricReporterService;
        this.delayBetweenBuildingEachModelInSeconds = delayBetweenBuildingEachModelInSeconds;
    }

    public void runModelerTasks(
            List<ModelerPipelineTask> allTasksForModelRequest) {
        sortModels(allTasksForModelRequest);
        LOGGER.info("running {} tasks", allTasksForModelRequest.size());

        for (ModelerPipelineTask variation :  allTasksForModelRequest) {
            try {
                Util.sleep(TimeUnit.SECONDS.toMillis(delayBetweenBuildingEachModelInSeconds));
                metricReporterService.getHistogram("start_modeling", ImmutableMap.of(
                        "model_id", variation.getModelerContext().getModelRequest().getId())).update(1);

                runTask(variation);

                if (!variation.getModelerContext().getRedFlags().isEmpty()) {
                    metricReporterService.getHistogram("models_with_redflags", ImmutableMap.of(
                            "model_id", variation.getModelerContext().getModelRequest().getId())).update(1);

                    variation.getModelerContext().getRedFlags().forEach(redFlag -> {
                        metricReporterService.getHistogram("models_redflag", ImmutableMap.of(
                                "red_flag", redFlag.name(),
                                "model_id", variation.getModelerContext().getModelRequest().getId())).update(1);
                    });
                } else {
                    metricReporterService.getHistogram("finished_modeling_with_success", ImmutableMap.of(
                            "model_id", variation.getModelerContext().getModelRequest().getId())).update(1);

                }

            } catch (Exception e) {
                metricReporterService.getHistogram("exception_occurred_during_modeling", ImmutableMap.of(
                        "model_id", variation.getModelerContext().getModelRequest().getId())).update(1);

                LOGGER.error("exception occurred when running ModelerPipelineTask", e);
            }
        }
    }

    private void sortModels(List<ModelerPipelineTask> allTasksForModelRequest) {
        allTasksForModelRequest.sort((o1, o2) -> {
            if (o1.getModelerContext().getModelRequest().getModelResult() == null ||
                o2.getModelerContext().getModelRequest().getModelResult() == null) {
                return 1;
            }
            boolean greater = o1.getModelerContext().getModelRequest().getModelResult().getUpdatedAt().getEpochSecond() >
                    o2.getModelerContext().getModelRequest().getModelResult().getUpdatedAt().getEpochSecond();
            if (greater) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    private void runTask(ModelerPipelineTask variation) {
        ModelResult modelResult = variation.getModelerContext().getModelRequest().getModelResult();
        long startTime = DateTimeUtil.getNowInSecond();

        LOGGER.info("building models for model id : {}, name :{} ",
                variation.getModelerContext().getModelRequest().getId(),
                variation.getModelerContext().getModelRequest().getName());
        if (modelResult != null) {
            LOGGER.info("previous model result is {} ",
                    variation.getModelerContext().getModelRequest().getModelResult());
        }

        variation.run();

        LOGGER.debug("finished building modelerTask {} in {} seconds ",
                variation.getModelerContext().getModelRequest().getName(),
                DateTimeUtil.getNowInSecond() - startTime);
    }
}

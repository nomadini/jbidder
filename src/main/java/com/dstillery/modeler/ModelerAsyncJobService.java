package com.dstillery.modeler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.DataWasReloadedEvent;
import com.dstillery.common.feature.FeatureType;
import com.dstillery.common.interest_model.InterestModelCacheService;
import com.dstillery.common.modeling.predictive.ModelCacheService;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.modeling.predictive.ModelResult;
import com.dstillery.common.modeling.predictive.ModelType;
import com.dstillery.common.modeling.predictive.MySqlModelResultService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

class ModelerAsyncJobService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelerAsyncJobService.class);

    private ModelVariationBuilder modelVariationBuilder;
    private ModelTaskRunner modelTaskRunner;
    private InterestModelCacheService interestModelCacheService;
    private int rebuildBadModelsAfterMinutes;
    private ModelCacheService modelCacheService;
    private MySqlModelResultService mySqlModelResultService;

    public ModelerAsyncJobService(ModelCacheService modelCacheService,
                                  MySqlModelResultService mySqlModelResultService,
                                  EventBus eventBus,
                                  ModelVariationBuilder modelVariationBuilder,
                                  InterestModelCacheService interestModelCacheService,
                                  ModelTaskRunner modelTaskRunner,
                                  int rebuildBadModelsAfterMinutes) {

        this.mySqlModelResultService = mySqlModelResultService;
        this.rebuildBadModelsAfterMinutes = rebuildBadModelsAfterMinutes;
        this.modelTaskRunner = modelTaskRunner;
        this.interestModelCacheService = interestModelCacheService;
        this.modelCacheService = modelCacheService;
        this.modelVariationBuilder = modelVariationBuilder;
        List<Set<FeatureType>> featureTypePermutations = ImmutableList.of(
                ImmutableSet.of(FeatureType.GTLD),
                ImmutableSet.of(FeatureType.MGRS100),
            ImmutableSet.of(FeatureType.PLACE_TAG),
        ImmutableSet.of(FeatureType.GTLD, FeatureType.MGRS100),
        ImmutableSet.of(FeatureType.GTLD, FeatureType.PLACE_TAG),
        ImmutableSet.of(FeatureType.MGRS100, FeatureType.PLACE_TAG),
        ImmutableSet.of(FeatureType.GTLD, FeatureType.MGRS100, FeatureType.PLACE_TAG));

        Map<String, Set<FeatureType>> typePermutationNameMap = new HashMap<>();
        typePermutationNameMap.put(FeatureType.GTLD.name(),
                featureTypePermutations.get(0));

        typePermutationNameMap.put(FeatureType.MGRS100.name(),
                featureTypePermutations.get(1));

        typePermutationNameMap.put(FeatureType.PLACE_TAG.name(),
                featureTypePermutations.get(2));

        typePermutationNameMap.put(FeatureType.GTLD.name() + "_"+ FeatureType.MGRS100.name(),
                featureTypePermutations.get(3));

        typePermutationNameMap.put(FeatureType.GTLD.name() + "_"+ FeatureType.PLACE_TAG.name(),
                featureTypePermutations.get(4));

        typePermutationNameMap.put(FeatureType.MGRS100.name() + "_"+ FeatureType.PLACE_TAG.name(),
                featureTypePermutations.get(5));

        typePermutationNameMap.put(FeatureType.GTLD.name() + "_" + FeatureType.GTLD.name() + "_"+ FeatureType.PLACE_TAG.name(),
                featureTypePermutations.get(6));

        eventBus.register(this);
    }

    private synchronized void runModelingOnRequest() {

        List<ModelRequest> allModelsToRun = new ArrayList<>();
        List<ModelRequest> allModels = modelCacheService.getAllEntities();
        //populate modelResult if existing fresh from database. because we update modelresult in modeler. we cannot rely on cached values from dataMaster
        Map<Long, ModelResult> mapOfResults =
                mySqlModelResultService.readAll().stream().collect(Collectors.toMap(ModelResult::getModelRequestId, s -> s));

        for (ModelRequest model : allModels) {

            if (model.getParentModelRequestId() > 0) {
                //we dont want to rebuild a model variation for now
                LOGGER.warn("ignoring model with id : {} since its a variation, will reconstruct variation later",
                        model.getId());
                continue;
            }
            if (model.getModelType() == ModelType.ACTION_TAKER_CATCH_ALL_MODEL) {
                LOGGER.debug("ignoring model with id : {} since its a action taker dummy model", model.getId());
                continue;
            }

            ModelResult modelResult = mapOfResults.get(model.getId());
            if (modelResult != null) {
                model.setModelResult(modelResult);
            }
            allModelsToRun.add(model);
        }

        LOGGER.debug("building models for {} model requests", allModelsToRun.size());

        List<ModelerPipelineTask> allTasksForAllModelVariations = new ArrayList<>();
        List<ModelerPipelineTask> allTasksForModelRequest = new ArrayList<>();

        for (ModelRequest modelRequest : allModelsToRun) {
            if (ModelUtil.isEligibleToRun(modelRequest, rebuildBadModelsAfterMinutes)) {
                ModelerPipelineTask modelerPipelineTask = modelVariationBuilder.createModelerTask(modelRequest);
                allTasksForModelRequest.add(modelerPipelineTask);
            }
        }

        // we disable this variation building for now. it overwhelms the system.
        //        buildVariations(allModelsToRun, allTasksForAllModelVariations);

        modelTaskRunner.runModelerTasks(allTasksForModelRequest);
        modelTaskRunner.runModelerTasks(allTasksForAllModelVariations);
    }

    private void buildVariations(List<ModelRequest> allModelsToRun,
                                 List<ModelerPipelineTask> allTasksForAllModelVariations) {
        for (ModelRequest modelRequest : allModelsToRun) {
            if (ModelUtil.isEligibleToRun(modelRequest, rebuildBadModelsAfterMinutes)) {

                if (interestModelCacheService.getModelIdToInterestModel().containsKey(modelRequest.getId())) {
                    //we won't build variation model for interest based models
                } else {
                    //we add the variations to a list
                    List<ModelerPipelineTask> modelerPipelineTaskVariations = modelVariationBuilder.createModelerTaskVariations(modelRequest);
                    allTasksForAllModelVariations.addAll(modelerPipelineTaskVariations);
                }
            }
        }
    }

    @Subscribe
    public void processEvent(DataWasReloadedEvent event) {
        LOGGER.info("event received : {} by class : {}", event);
        runAsynchronousModeler();
    }

    private void runAsynchronousModeler() {
            try {
                LOGGER.info("going to start reading old modelling requests....");
                runModelingOnRequest();
            } catch (Exception e) {
                LOGGER.error("unknown error happening when building models", e);
            }
    }
}

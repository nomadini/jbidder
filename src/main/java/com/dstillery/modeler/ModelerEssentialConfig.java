package com.dstillery.modeler;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelerEssentialConfig {

    @Bean
    public String appPropertyFileName() {
        return "modeler.properties";
    }
}

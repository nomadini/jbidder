package com.dstillery.modeler;
/*
  ModelRecord.h

   Created on: Jul 16, 2015
       Author: mtaabodi
 */

import com.dstillery.common.util.FileNomadiniUtil;
import com.dstillery.common.util.StringUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
  combines a label and feature string that is in libsvm format
 */
public class ModelRecord {

    private String label;
    private String record;  // index1:feature1 index2:feature2 index3:feature3
    private Map<Integer, Double> indexFeatureCombos = new HashMap<>();

    void add(int index, double feature) {
        indexFeatureCombos.put(index, feature);
    }

    static List<ModelRecord> convertToModelRecords(
            List<String> records, int label) {
        List<ModelRecord> positiveModelRecords = new ArrayList<>();
        for (String record : records) {

            ModelRecord modelRecord = new ModelRecord ();
            modelRecord.label = StringUtil.toStr(label);
            modelRecord.record = StringUtil.toStr(record);

            positiveModelRecords.add(modelRecord);
        }
         //LOGGER.debug("positiveModelRecords.size() : {} ", positiveModelRecords.size();

        return positiveModelRecords;
    }

    void createLibSvmFile(List<ModelRecord> allModelRecords,
                                       String featuresFileName) throws IOException {
        FileNomadiniUtil.deleteFile(featuresFileName);
        boolean first = true;
        FileNomadiniUtil.createFileIfNotExist(featuresFileName);
        for (ModelRecord record : allModelRecords) {
            String recordStr = "";

            if (record.label.equalsIgnoreCase("-1")) {
                recordStr += StringUtil.toStr("0");
            } else {
                recordStr += StringUtil.toStr("1");
            }
            recordStr += StringUtil.toStr(" ");
            recordStr += record.record;

            FileNomadiniUtil.appendALineToFile(featuresFileName, recordStr);
        }
    }

    void createFeaturesFile(List<ModelRecord> allModelRecords,
                                         String featuresFileName) throws IOException {
        FileNomadiniUtil.deleteFile(featuresFileName);
        FileNomadiniUtil.createFileIfNotExist(featuresFileName);
        for (ModelRecord record : allModelRecords) {
            FileNomadiniUtil.appendALineToFile(featuresFileName, record.record);
        }
         //LOGGER.debug("count of lines in feature file :", FileNomadiniUtil.getCountOfLinesInFile(featuresFileName);
    }

    void createLabelsFile(List<ModelRecord> allModelRecords,
                                       String labelsFileName) throws IOException {
        FileNomadiniUtil.deleteFile(labelsFileName);
        FileNomadiniUtil.createFileIfNotExist(labelsFileName);
        for (ModelRecord record : allModelRecords) {
            FileNomadiniUtil.appendALineToFile(labelsFileName, record.label);

        }
         //LOGGER.debug("count of lines in label file : {} ",  FileNomadiniUtil.getCountOfLinesInFile(labelsFileName);
    }

}
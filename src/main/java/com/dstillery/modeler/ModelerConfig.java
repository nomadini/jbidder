package com.dstillery.modeler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.interest.Interest;
import com.dstillery.common.interest_model.InterestModelCacheService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.modeling.predictive.ModelCacheService;
import com.dstillery.common.modeling.predictive.MySqlModelResultService;
import com.dstillery.common.modeling.predictive.MySqlModelService;
import com.dstillery.common.pixel.PixelCacheService;
import com.dstillery.common.pixel.PixelDeviceHistoryCassandraService;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.modeler.engine.FeatureBasedModeler;
import com.dstillery.modeler.engine.FeatureValidator;
import com.dstillery.modeler.engine.FeatureWeightCalculator;
import com.dstillery.modeler.engine.FeatureWeightToModelConverter;
import com.dstillery.modeler.engine.ModelDataCleaner;
import com.dstillery.modeler.engine.ModelerPersistenceService;
import com.dstillery.modeler.recordfetchers.positive.PixelBasedDeviceFeatureService;
import com.dstillery.modeler.recordfetchers.NegativeFeatureFetcher;
import com.dstillery.modeler.recordfetchers.positive.PixelBasedPositiveFeatureFetcher;
import com.dstillery.modeler.recordfetchers.PositiveFeatureFetcher;
import com.dstillery.modeler.recordfetchers.positive.SeedBasedPositiveFeatureFetcher;
import com.google.common.eventbus.EventBus;

@Configuration
public class ModelerConfig {

    @Autowired
    private ConfigService configService;

    @Autowired
    private MetricReporterService metricReporterService;

    @Autowired
    private DiscoveryService discoveryService;

    @Autowired
    private String appName;

    private String dataMasterUrl = ""; // we set this to empty. so discoveryService, will do its job of getting dataMaster url each time

    @Bean
    public PixelCacheService pixelCacheService() {
        return new PixelCacheService(
                discoveryService,
                dataMasterUrl,
                metricReporterService,
                appName);
    }

    @Bean
    public SegmentCacheService segmentCacheService() {
        return new SegmentCacheService(
                discoveryService,
                dataMasterUrl,
                metricReporterService,
                appName
        );
    }
    @Bean
    public ModelCacheService modelCacheService() {
        ModelCacheService modelCacheService =
                new ModelCacheService(discoveryService,
                        dataMasterUrl,
                        metricReporterService,
                        appName);

        modelCacheService.setTimeoutForDataCallInMillis(10000);
        return modelCacheService;
    }
    @Bean
    public CacheService<Interest> interestCacheService() {
        return new CacheService<>(
                discoveryService,
                Interest.class,
                dataMasterUrl,
                metricReporterService,
                appName
        );
    }

    @Bean
    public InterestModelCacheService interestModelCacheService() {
        return new InterestModelCacheService(
                discoveryService,
                dataMasterUrl,
                metricReporterService,
                appName
        );
    }

    @Bean
    public ModelerPersistenceService modelerPersistenceService(
            MySqlModelService mySqlModelService,
            MySqlModelResultService mySqlModelResultService
    ) {
        return new ModelerPersistenceService(
                 metricReporterService,
                 mySqlModelService,
                 mySqlModelResultService
        );
    }
    @Bean
    public PixelBasedDeviceFeatureService featureFetcherService(
            PixelDeviceHistoryCassandraService pixelDeviceHistoryCassandraService,
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService) {
        return new PixelBasedDeviceFeatureService(
                 metricReporterService,
                 pixelDeviceHistoryCassandraService,
                 deviceFeatureHistoryCassandraService);
    }

    @Bean
    public NegativeFeatureFetcher negativeFeatureFetcher(
            MetricReporterService metricReporterService,
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService) {
        return new NegativeFeatureFetcher(
                metricReporterService,
                deviceFeatureHistoryCassandraService);
    }

    @Bean
    public PixelBasedPositiveFeatureFetcher pixelBasedPositiveFeatureFetcher(
            PixelBasedDeviceFeatureService pixelBasedDeviceFeatureService,
            PixelCacheService pixelCacheService) {
        return new PixelBasedPositiveFeatureFetcher(pixelBasedDeviceFeatureService,
                pixelCacheService);
    }

    @Bean
    public SeedBasedPositiveFeatureFetcher seedBasedPositiveFeatureFetcher(
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService) {
        return new SeedBasedPositiveFeatureFetcher(deviceFeatureHistoryCassandraService);
    }

    @Bean
    public PositiveFeatureFetcher positiveFeatureFetcher(
            SeedBasedPositiveFeatureFetcher seedBasedPositiveFeatureFetcher,
            PixelBasedPositiveFeatureFetcher pixelBasedPositiveFeatureFetcher) {
        return new PositiveFeatureFetcher(
                pixelBasedPositiveFeatureFetcher,
                seedBasedPositiveFeatureFetcher
        );
    }

    @Bean
    public ModelDataCleaner modelDataCleaner(
            MetricReporterService metricReporterService,
            ConfigService configService) {
        return new ModelDataCleaner(metricReporterService);
    }

    @Bean
    public FeatureValidator featureValidator() {
        return new FeatureValidator();
    }

    @Bean
    public FeatureWeightToModelConverter featureWeightToModelConverter() {
        return new FeatureWeightToModelConverter();
    }

    @Bean
    public FeatureWeightCalculator featureWeightCalculator(
            MetricReporterService metricReporterService) {
        return new FeatureWeightCalculator(metricReporterService);
    }

    @Bean
    public FeatureBasedModeler featureBasedModeler(
            MetricReporterService metricReporterService,
            FeatureValidator featureValidator,
            FeatureWeightToModelConverter featureWeightToModelConverter,
            FeatureWeightCalculator featureWeightCalculator
    ) {
        return new FeatureBasedModeler(
                 metricReporterService,
                 featureValidator,
                 featureWeightToModelConverter,
                 featureWeightCalculator
        );
    }

    @Bean
    public ModelVariationBuilder modelVariationBuilder(
            ModelCacheService modelCacheService,
            ModelDataCleaner modelDataCleaner,
            NegativeFeatureFetcher negativeFeatureFetcher,
            PositiveFeatureFetcher positiveFeatureFetcher,
            ModelerPersistenceService modelerPersistenceService,
            FeatureBasedModeler featureBasedModeler,
            MySqlModelResultService mySqlModelResultService,
            MySqlModelService mySqlModelService
    ) {
        return new ModelVariationBuilder(
                 metricReporterService,
                 modelCacheService,
                 modelDataCleaner,
                 negativeFeatureFetcher,
                 positiveFeatureFetcher,
                 modelerPersistenceService,
                 featureBasedModeler,
                 mySqlModelResultService,
                 mySqlModelService
        );
    }

    @Bean
    public ModelTaskRunner modelTaskRunner(MetricReporterService metricReporterService) {
        return new ModelTaskRunner(metricReporterService,
                configService.getAsInt("delayBetweenBuildingEachModelInSeconds"));
    }
    @Bean
    public ModelerAsyncJobService modelerAsyncJobService(
            ModelCacheService modelCacheService,
            EventBus eventBus,
            ModelTaskRunner modelTaskRunner,
            ModelVariationBuilder modelVariationBuilder,
            MySqlModelResultService mySqlModelResultService,
            InterestModelCacheService interestModelCacheService) {
        return new ModelerAsyncJobService(
                modelCacheService,
                mySqlModelResultService,
                 eventBus,
                 modelVariationBuilder,
                interestModelCacheService,
                modelTaskRunner,
                configService.getAsInt("rebuildBadModelsAfterMinutes")
        );
    }
}

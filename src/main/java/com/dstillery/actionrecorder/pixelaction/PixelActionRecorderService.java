package com.dstillery.actionrecorder.pixelaction;

import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static com.dstillery.common.util.StringUtil.toLowerCase;

import java.net.MalformedURLException;
import java.time.Instant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.actionrecorder.modules.ActionRecorderContext;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.pixel.Pixel;
import com.dstillery.common.pixel.PixelCacheService;
import com.dstillery.common.util.HttpUtil;

public class PixelActionRecorderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PixelActionRecorderService.class);

    private PixelCacheService pixelCacheService;
    private PixelActionRecorderPipeline pixelActionRecorderPipeline;
    private MetricReporterService metricReporterService;


    public PixelActionRecorderService(PixelCacheService pixelCacheService,
                                      PixelActionRecorderPipeline pixelActionRecorderPipeline,
                                      MetricReporterService metricReporterService) {
        this.pixelCacheService = pixelCacheService;
        this.pixelActionRecorderPipeline = pixelActionRecorderPipeline;
        this.metricReporterService = metricReporterService;;
    }

    public void handleRequest(HttpServletRequest request,
                              HttpServletResponse response,
                              ActionRecorderContext context) {

        try {
            metricReporterService.addStateModuleForEntity(
                    "PIXEL_MATCHING_REQUEST_RECIEVED",
                    "PixelActionRecorderService",
                    "ALL");
            Instant now = Instant.now();

            configureContext(request, context);

            context.setHttpRequestPtr(request);
            context.setHttpResponsePtr(response);

            pixelActionRecorderPipeline.process(context);

            metricReporterService.addStateModuleForEntity(
                    "SENDING_GOOD_RESPONSE",
                    "PixelActionRecorderService",
                    "ALL");
            HttpUtil.setEmptyResponse(response);
        } catch (Exception e) {
            LOGGER.warn("exception happened", e);
            metricReporterService.addStateModuleForEntity(
                    "EXCEPTION_IN_HANDLER",
                    "PixelActionRecorderService",
                    "ALL");
            HttpUtil.setBadRequestResponse (response, e.getMessage());
        }

    }

    private void configureContext(HttpServletRequest request, ActionRecorderContext context) throws MalformedURLException {
        getPixelIdParam(request, context);
        getDeviceGeoParams(context);
        try {
            context.setDomain(HttpUtil.getDomainFromUrl(request.getRequestURL().toString()));
        } catch (IllegalStateException e) {
            //ignore this : java.lang.IllegalStateException: Not under a public suffix: localhost
        }
    }

    private void getDeviceGeoParams(ActionRecorderContext context) {
        boolean latIsSet = false;
        boolean lonIsSet = false;
        String lat = context.getQueryParams().get("lat");
        if (lat != null) {
            context.setDeviceLat(Double.parseDouble(lat));
            latIsSet = true;
        }

        String lon = context.getQueryParams().get("lon");
        if (lon != null) {
            context.setDeviceLon(Double.parseDouble(lon));
            lonIsSet = true;
        }

        if (latIsSet || lonIsSet) {
            assertAndThrow(latIsSet && lonIsSet);
            context.setDeviceGeoIsAvailable(true);
        }
    }

    private void getPixelIdParam(HttpServletRequest request, ActionRecorderContext context) {

        String pixelId = context.getQueryParams().get("pixelId");
        if (pixelId != null) {
            context.setPixelUniqueKey(toLowerCase(pixelId));
        } else {
            HttpUtil.printQueryParamsOfRequest(request);
            throw new RuntimeException("pixelId is required for action");
        }

        Pixel pixel = this.pixelCacheService.getKeyToPixelMap().get(context.getPixelUniqueKey());
        if (pixel != null) {
            context.setPixel(pixel);
        } else {
            metricReporterService.
                    addStateModuleForEntity("ABORTING_PIXEL_ID_PARAM_NOT_FOUND",
                            "PixelActionRecorderService",
                            "pix" + context.getPixelUniqueKey());
            LOGGER.warn("pixel not found for key: {} : map : {}", context.getPixelUniqueKey(),
                        pixelCacheService.getKeyToPixelMap());
            throw new IllegalStateException("ABORTING_PIXEL_ID_PARAM_NOT_FOUND");
        }

    }

    public PixelActionRecorderPipeline getPixelActionRecorderPipeline() {
        return pixelActionRecorderPipeline;
    }

    public void setPixelActionRecorderPipeline(PixelActionRecorderPipeline pixelActionRecorderPipeline) {
        this.pixelActionRecorderPipeline = pixelActionRecorderPipeline;
    }
}


package com.dstillery.actionrecorder.pixelaction;

import com.dstillery.actionrecorder.modules.ActionEventLogCreatorModule;
import com.dstillery.actionrecorder.modules.ActionRecorderContext;
import com.dstillery.actionrecorder.modules.ActionRecorderModule;
import com.dstillery.actionrecorder.modules.NomadiniDeviceIdCookieReaderModule;
import com.dstillery.actionrecorder.modules.PixelActionTakerSegmentWriterModule;
import com.dstillery.actionrecorder.modules.PixelDeviceHistoryUpdaterModule;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

import java.util.ArrayList;
import java.util.List;

public class PixelActionRecorderPipeline  {
    private List<ActionRecorderModule> modules;
    private MetricReporterService metricReporterService;

    public PixelActionRecorderPipeline(NomadiniDeviceIdCookieReaderModule nomadiniDeviceIdCookieReaderModule,
                                       MetricReporterService metricReporterService,
                                       PixelDeviceHistoryUpdaterModule pixelDeviceHistoryUpdaterModule,
                                       PixelActionTakerSegmentWriterModule pixelActionTakerSegmentWriterModule,
                                       ActionEventLogCreatorModule actionEventLogCreatorModule) {
        modules = new ArrayList<>();
        modules.add(nomadiniDeviceIdCookieReaderModule);
        modules.add(pixelDeviceHistoryUpdaterModule);
        modules.add(pixelActionTakerSegmentWriterModule);
        modules.add(actionEventLogCreatorModule);
        this.metricReporterService = metricReporterService;;
    }

    void process(ActionRecorderContext context) {

        for (ActionRecorderModule it : modules) {
            it.process(context);
            metricReporterService.addStateModuleForEntity("LastModule-" + it.getClass().getSimpleName(),
                    "PixelActionRecorderPipeline", "ALL");
        }

    }

}

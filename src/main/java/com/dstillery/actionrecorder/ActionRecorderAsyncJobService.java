package com.dstillery.actionrecorder;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.DataWasReloadedEvent;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.GUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

public class ActionRecorderAsyncJobService implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActionRecorderAsyncJobService.class);

    private MetricReporterService metricReporterService;

    private AtomicBoolean readyToProcessRequests = new AtomicBoolean();

    public ActionRecorderAsyncJobService(MetricReporterService metricReporterService,
                                         EventBus eventBus) {
        this.metricReporterService = metricReporterService;;

        eventBus.register(this);
    }

    @Override
    public void run() {
            try {
                readyToProcessRequests.set(false);
                //wait for current requests to be finished
                GUtil.sleepInSeconds(3);
                //this inserts the action taker segments that are in queue

                readyToProcessRequests.set(true);
            } catch(Exception e) {
                metricReporterService.addStateModuleForEntity(
                        "EXCEPTION_IN_REFRESHING_DATA",
                        "ActionRecorderAsyncJobService",
                        ALL_ENTITIES,
                        MetricReporterService.MetricPriority.EXCEPTION
                );
            }
    }

    @PostConstruct
    public void startAsyncThreads() {

        LOGGER.debug("going to run daemon threads for actionRecorder");
        readyToProcessRequests.set(true);
        //Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0, 1, TimeUnit.MINUTES);
    }

    @Subscribe
    public void processEvent(DataWasReloadedEvent event) {
        LOGGER.info("event received : {} by class : {}", event);
        run();
    }

}

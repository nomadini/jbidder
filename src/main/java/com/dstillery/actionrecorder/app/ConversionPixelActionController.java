package com.dstillery.actionrecorder.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dstillery.actionrecorder.conversionrecording.ConversionRecorderPipeline;
import com.dstillery.actionrecorder.conversionrecording.ConversionRequestService;
import com.dstillery.actionrecorder.modules.ActionRecorderContext;
import com.dstillery.actionrecorder.pixelaction.PixelActionRecorderService;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

@RestController
public class ConversionPixelActionController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConversionPixelActionController.class);
    public static final String NOMADINI_DEVICE_ID = "nomadiniDeviceId";

    private MetricReporterService metricReporterService;
    private PixelActionRecorderService pixelActionRecorderService;
    private ConversionRequestService conversionRequestService;
    public ConversionPixelActionController(MetricReporterService metricReporterService,
                                           ConversionRequestService conversionRequestService,
                                           PixelActionRecorderService pixelActionRecorderService) {
        this.metricReporterService = metricReporterService;;
        this.conversionRequestService = conversionRequestService;
        this.pixelActionRecorderService = pixelActionRecorderService;
    }

    @RequestMapping(value = "conv-pix-action",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void handleConversionRequest(HttpServletRequest request,
                       HttpServletResponse response) {


        String exchangeName = "unknown";
        ActionRecorderContext context = new ActionRecorderContext(request, response, exchangeName, "conv-pix-action");
        conversionRequestService.handleRequest(request, response, context);
    }
}


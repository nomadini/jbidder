package com.dstillery.actionrecorder.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dstillery.actionrecorder.modules.ActionRecorderContext;
import com.dstillery.actionrecorder.pixelaction.PixelActionRecorderService;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

@RestController
public class PixelActionController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PixelActionController.class);
    public static final String NOMADINI_DEVICE_ID = "nomadiniDeviceId";
    private PixelActionRecorderService pixelActionRecorderService;
    private MetricReporterService metricReporterService;
    public PixelActionController(MetricReporterService metricReporterService,
                                 PixelActionRecorderService pixelActionRecorderService) {
        this.pixelActionRecorderService = pixelActionRecorderService;
        this.metricReporterService = metricReporterService;;
    }

    @RequestMapping(value = "pixelaction",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void handleActionRequest(HttpServletRequest request,
                                    HttpServletResponse response) {

        String exchangeName = "unknown";
        ActionRecorderContext context = new ActionRecorderContext(request, response, exchangeName, "pixelaction");
        pixelActionRecorderService.handleRequest(request, response, context);
    }
}


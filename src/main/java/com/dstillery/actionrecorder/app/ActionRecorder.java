package com.dstillery.actionrecorder.app;//
// Created by Mahmoud Taabodi on 7/11/16.
//

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.dstillery.common.config.EssentialConfig;
import com.dstillery.common.config.CacheServiceInitializer;
import com.dstillery.common.config.CommonBeansConfig;
@SpringBootApplication(exclude ={DataSourceAutoConfiguration.class})
@PropertySources({

        @PropertySource(value = "classpath:actionrecorder.properties"),
        @PropertySource(value = "file://${PROPERTY_FILE:/etc/actionrecorder/actionrecorder.properties}",
                ignoreResourceNotFound = true)
})
@Import({
        ActionRecorderEssentialConfig.class,
        EssentialConfig.class,
        CommonBeansConfig.class,
        ActionRecorderConfig.class})
@ComponentScan(basePackages = "com.dstillery.actionrecorder.app")
public class ActionRecorder {
    public static void main( String[] args ) {
        SpringApplication sa = new SpringApplication();
        sa.addListeners(new CacheServiceInitializer());
        sa.setSources(new HashSet<>(Arrays.asList(ActionRecorder.class.getName())));
        ConfigurableApplicationContext context = sa.run(args);
    }
}

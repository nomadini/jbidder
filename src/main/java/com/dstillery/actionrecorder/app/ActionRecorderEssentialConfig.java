package com.dstillery.actionrecorder.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ActionRecorderEssentialConfig {
    @Bean
    public String appPropertyFileName() {
        return "actionrecorder.properties";
    }
}

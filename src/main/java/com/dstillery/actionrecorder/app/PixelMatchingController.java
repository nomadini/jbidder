package com.dstillery.actionrecorder.app;

import com.dstillery.actionrecorder.modules.ActionRecorderContext;
import com.dstillery.actionrecorder.pixelmatching.PixelMatchingPipeline;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

@RestController
public class PixelMatchingController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PixelMatchingController.class);
    public static final String NOMADINI_DEVICE_ID = "nomadiniDeviceId";

    private PixelMatchingPipeline pixelMatchingPipeline;
    private MetricReporterService metricReporterService;

    public PixelMatchingController(PixelMatchingPipeline pixelMatchingPipeline,
                                   MetricReporterService metricReporterService) {
        this.pixelMatchingPipeline = pixelMatchingPipeline;
        this.metricReporterService = metricReporterService;;
    }

    @RequestMapping(value = "rubiconPixel",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void handleRequest(HttpServletRequest request,
                       HttpServletResponse response) {

        String exchangeName = "rubicon";
        ActionRecorderContext context = new ActionRecorderContext(request, response, exchangeName, "rubiconPixel");
        process(request, response, context);
    }

    @RequestMapping(value = "googlepixelmatching",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void handleGoogleRequest(HttpServletRequest request,
                       HttpServletResponse response) {

        String exchangeName = "google";
        ActionRecorderContext context = new ActionRecorderContext(request, response, exchangeName, "googlepixelmatching");
        process(request, response, context);
    }

    private void process(HttpServletRequest request,
                         HttpServletResponse response,
                         ActionRecorderContext context) {

        try {
            Instant now = Instant.now();
            context.setHttpResponsePtr(response);

            pixelMatchingPipeline.process(context);

            response.addCookie(new Cookie(NOMADINI_DEVICE_ID, context.getDevice().getDeviceId()));

            //this is to assert cookie is written
            String nomadiniDeviceId = HttpUtil.getCookieFromResponse(response, NOMADINI_DEVICE_ID);
            LOGGER.debug("nomadiniDeviceId cookie is set to : {}", nomadiniDeviceId);
            assertAndThrow(nomadiniDeviceId != null && !nomadiniDeviceId.isEmpty());
            HttpUtil.setEmptyResponse(response);
        } catch (Exception e) {
            LOGGER.warn("sending no bad request as a result of exception", e);
            HttpUtil.setBadRequestResponse (response, e.getMessage());
        }
    }
}


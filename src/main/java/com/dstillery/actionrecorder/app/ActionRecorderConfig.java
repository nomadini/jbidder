package com.dstillery.actionrecorder.app;

import java.net.UnknownHostException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.dstillery.actionrecorder.ActionRecorderAsyncJobService;
import com.dstillery.actionrecorder.ActionRecorderHealthService;
import com.dstillery.actionrecorder.conversionrecording.ConversionRecorderPipeline;
import com.dstillery.actionrecorder.modules.ActionEventLogCreatorModule;
import com.dstillery.actionrecorder.modules.ConversionDecider;
import com.dstillery.actionrecorder.modules.DeviceHistoryOfVisitFeatureUpdaterModuleWrapper;
import com.dstillery.actionrecorder.modules.NomadiniDeviceIdCookieReaderModule;
import com.dstillery.actionrecorder.modules.PixelActionTakerSegmentWriterModule;
import com.dstillery.actionrecorder.modules.PixelDeviceHistoryUpdaterModule;
import com.dstillery.actionrecorder.modules.VisitFeatureHistoryOfDeviceUpdaterModuleWrapper;
import com.dstillery.actionrecorder.pixelaction.PixelActionRecorderPipeline;
import com.dstillery.actionrecorder.pixelaction.PixelActionRecorderService;
import com.dstillery.actionrecorder.pixelmatching.NomadiniIdToPartnerIdMapperModule;
import com.dstillery.actionrecorder.pixelmatching.PixelMatchingPipeline;
import com.dstillery.common.cassandra.CassandraDriver;
import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.partnermap.NomadiniIdToExchangeIdsMapCassandraService;
import com.dstillery.common.LoadPercentageBasedSampler;
import com.dstillery.common.adhistory.AdEntryDevicePair;
import com.dstillery.common.cache_realtime.RealTimeEntityCacheService;
import com.dstillery.common.common_modules.DeviceHistoryUpdaterModule;
import com.dstillery.common.common_modules.FeatureDeviceHistoryUpdaterModule;
import com.dstillery.common.common_modules.FeatureGuardService;
import com.dstillery.common.config.CommonModulesConfig;
import com.dstillery.common.device.DeviceIdService;
import com.dstillery.common.eventlog.AdInteractionRecordingModule;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistoryCassandraService;
import com.dstillery.common.modeling.featuredevice.FeatureDeviceHistoryCassandraService;
import com.dstillery.common.pixel.PixelCacheService;
import com.dstillery.common.pixel.PixelDeviceHistoryCassandraService;
import com.dstillery.common.pixel.PixelSegmentCacheService;
import com.dstillery.common.segment.MySqlSegmentService;
import com.dstillery.common.targetgroup.targetgroup_maps.tgpixel.TargetGroupPixelCacheService;
import com.google.common.eventbus.EventBus;

@Configuration
@Import({
        CommonModulesConfig.class
})
public class ActionRecorderConfig {

    @Autowired
    private CassandraDriver cassandraDriver;

    @Autowired
    private  MetricReporterService metricReporterService;

    @Bean
    public PixelActionRecorderService pixelActionRecorderService(
            PixelCacheService pixelCacheService,
            PixelActionRecorderPipeline pixelActionRecorderPipeline,
            MetricReporterService metricReporterService
    ) {
        return new PixelActionRecorderService(
                 pixelCacheService,
                 pixelActionRecorderPipeline,
                metricReporterService
        );
    }

    @Bean
    public ActionRecorderHealthService actionRecorderHealthService(MetricReporterService metricReporterService) {
        return new ActionRecorderHealthService(metricReporterService);
    }

    @Bean
    public ActionRecorderAsyncJobService actionRecorderAsyncJobService(
            MetricReporterService metricReporterService,
            EventBus eventBus
    ) {

        return new ActionRecorderAsyncJobService(
                metricReporterService,
                eventBus
        );
    }

    @Bean
    public NomadiniIdToPartnerIdMapperModule gicapodsIdToPartnerIdMapperModule(
            NomadiniIdToExchangeIdsMapCassandraService
                    gicapodsIdToExchangeIdsMapCassandraService) {
        return new NomadiniIdToPartnerIdMapperModule(
                gicapodsIdToExchangeIdsMapCassandraService
        );

    }

    @Bean
    public NomadiniDeviceIdCookieReaderModule nomadiniDeviceIdCookieReaderModule() {
        return new NomadiniDeviceIdCookieReaderModule(metricReporterService);

    }

    @Bean
    public ConversionDecider conversionDecider(
            MetricReporterService metricReporterService,
            RealTimeEntityCacheService<AdEntryDevicePair> adHistoryCacheService,
            AdInteractionRecordingModule adInteractionRecordingModule,
            CassandraService<EventLog> eventLogCassandraService,
            TargetGroupPixelCacheService targetGroupPixelCacheService
    ) {
        return new ConversionDecider(
                TimeUnit.DAYS.toSeconds(1),
                metricReporterService,
                adHistoryCacheService,
                targetGroupPixelCacheService,
                adInteractionRecordingModule,
                eventLogCassandraService
        );
    }

    @Bean
    public PixelActionTakerSegmentWriterModule pixelActionTakerSegmentWriterModule(
            DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService,
            PixelSegmentCacheService pixelSegmentCacheService,
            MetricReporterService metricReporterService
    ) {
        return new PixelActionTakerSegmentWriterModule(
                deviceSegmentHistoryCassandraService,
                pixelSegmentCacheService,
                metricReporterService
        );
    }

    @Bean
    public ActionEventLogCreatorModule actionEventLogCreatorModule(MetricReporterService metricReporterService,
                                                                   CassandraService eventLogCassandraService
    ) throws UnknownHostException {
        String appHostname = java.net.InetAddress.getLocalHost().getHostName();
        return new ActionEventLogCreatorModule(
                appHostname,
                "version",
                metricReporterService,
                eventLogCassandraService
        );
    }

    @Bean
    public DeviceHistoryUpdaterModule visitFeatureDeviceHistoryUpdaterModule(
            DeviceIdService deviceIdService,
            FeatureGuardService featureGuardService,
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService,
            MetricReporterService metricReporterService
    ) {
        return new DeviceHistoryUpdaterModule(
                deviceIdService,
                featureGuardService,
                new LoadPercentageBasedSampler(1, "deviceHistorySizeSampler"),
                deviceFeatureHistoryCassandraService,
                metricReporterService
        );
    }

    @Bean
    public FeatureDeviceHistoryUpdaterModule featureDeviceHistoryUpdaterModule(
            FeatureGuardService featureGuardService,
            FeatureDeviceHistoryCassandraService featureDeviceHistoryCassandraService,
            MetricReporterService metricReporterService
    ) {
        return new FeatureDeviceHistoryUpdaterModule(
                Collections.emptySet(),
                new LoadPercentageBasedSampler(1, "randomFeatureSampler"),
                featureGuardService,
                featureDeviceHistoryCassandraService,
                metricReporterService
        );
    }

    @Bean
    public DeviceHistoryOfVisitFeatureUpdaterModuleWrapper featureDeviceHistoryUpdaterModuleWrapper(
            FeatureDeviceHistoryUpdaterModule featureDeviceHistoryUpdaterModule) {
        return new DeviceHistoryOfVisitFeatureUpdaterModuleWrapper(featureDeviceHistoryUpdaterModule);
    }

    @Bean
    public VisitFeatureHistoryOfDeviceUpdaterModuleWrapper visitFeatureHistoryOfDeviceUpdaterModuleWrapper(
            DeviceHistoryUpdaterModule visitFeatureDeviceHistoryUpdaterModule) {
        return new VisitFeatureHistoryOfDeviceUpdaterModuleWrapper(visitFeatureDeviceHistoryUpdaterModule);
    }

    @Bean
    public PixelDeviceHistoryUpdaterModule pixelDeviceHistoryUpdaterModule(
            MetricReporterService metricReporterService,
            PixelDeviceHistoryCassandraService pixelDeviceHistoryCassandraService) {
        return new PixelDeviceHistoryUpdaterModule(
                metricReporterService,
                pixelDeviceHistoryCassandraService
        );
    }

    @Bean
    public DeviceHistoryOfVisitFeatureUpdaterModuleWrapper deviceHistoryOfVisitFeatureUpdaterModuleWrapper(
            FeatureDeviceHistoryUpdaterModule featureDeviceHistoryUpdaterModule) {
        return new DeviceHistoryOfVisitFeatureUpdaterModuleWrapper(featureDeviceHistoryUpdaterModule);

    }

    @Bean
    public ConversionRecorderPipeline conversionRecorderPipeline(
            NomadiniDeviceIdCookieReaderModule nomadiniDeviceIdCookieReaderModule,
            MetricReporterService metricReporterService,
            ConversionDecider conversionDecider) {
        return new ConversionRecorderPipeline(
                metricReporterService,
                nomadiniDeviceIdCookieReaderModule,
                conversionDecider);

    }
    @Bean
    public PixelMatchingPipeline pixelMatchingPipeline(
            NomadiniDeviceIdCookieReaderModule nomadiniDeviceIdCookieReaderModule,
            MetricReporterService metricReporterService,
            NomadiniIdToPartnerIdMapperModule nomadiniIdToPartnerIdMapperModule) {
        return new PixelMatchingPipeline(
                metricReporterService,
                nomadiniDeviceIdCookieReaderModule,
                nomadiniIdToPartnerIdMapperModule);

    }

    @Bean
    public PixelActionRecorderPipeline pixelActionRecorderPipeline(
            NomadiniDeviceIdCookieReaderModule nomadiniDeviceIdCookieReaderModule,
            MetricReporterService metricReporterService,
            PixelDeviceHistoryUpdaterModule pixelDeviceHistoryUpdaterModule,
            PixelActionTakerSegmentWriterModule pixelActionTakerSegmentWriterModule,
            ActionEventLogCreatorModule actionEventLogCreatorModule
    ) {
        return new PixelActionRecorderPipeline(nomadiniDeviceIdCookieReaderModule,
                metricReporterService,
                pixelDeviceHistoryUpdaterModule,
                pixelActionTakerSegmentWriterModule,
                actionEventLogCreatorModule);
    }
}

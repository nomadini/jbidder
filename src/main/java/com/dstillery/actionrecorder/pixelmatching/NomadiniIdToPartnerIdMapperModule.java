package com.dstillery.actionrecorder.pixelmatching;

import com.dstillery.actionrecorder.modules.ActionRecorderContext;
import com.dstillery.actionrecorder.modules.ActionRecorderModule;
import com.dstillery.common.partnermap.NomadiniIdToExchangeIdsMapCassandraService;
import com.dstillery.common.partnermap.NomadiniIdToPartnerId;

import static com.dstillery.common.util.StringUtil.assertAndThrow;
import static com.dstillery.common.util.StringUtil.equalsIgnoreCase;

public class NomadiniIdToPartnerIdMapperModule implements ActionRecorderModule {

    private NomadiniIdToExchangeIdsMapCassandraService gicapodsIdToExchangeIdsMapCassandraService;

    public NomadiniIdToPartnerIdMapperModule(NomadiniIdToExchangeIdsMapCassandraService gicapodsIdToExchangeIdsMapCassandraService) {
        this.gicapodsIdToExchangeIdsMapCassandraService = gicapodsIdToExchangeIdsMapCassandraService;
    }

//How does cookie syncing work ????

//      us      google   rubicon  cookie
//      -        g1        -        -      Event : google sends us g1 id
//      m1       g1        -        m1     Event : we write the id m1 as our id and write it in cassandra as m1-g1
//      m1        -        r1       m1     Event : rubicon sends r1 id for the same browser, we read the cookie m1
//      m1       g1        r1       m1     Event : we read the m1 from cassandra and find g1 too, and update the m1-g1 to m1-g1-r1

// g1 is sent to us, we read the nomadiniDeviceId cookie, if there is no cookie
//we don't know the browser, thats a new browser, we create a new random id for
//nomadiniDeviceId and write as a cookie and write the mapping between our cookie and google

    //next time, an exchange redirects this browser to us, we read the cookie first,
//if cookie exists, we have a mapping for browser, and we write a mapping for the exchangeId
//and our nomadiniDeviceId and we write the
    public void process(ActionRecorderContext context) {
        String partnerId = null;
        if (equalsIgnoreCase(context.getExchangeName(), "google")) {
            String googleId = context.getQueryParams().get("google_gid");
            assertAndThrow(!googleId.isEmpty());
            partnerId = googleId;
        }

        assertAndThrow(partnerId != null);
        assertAndThrow(!context.getExchangeName().isEmpty());

        upsertMapping(context.getDevice().getDeviceId(),
                partnerId,
                context.getExchangeName());

    }

    private void upsertMapping(
            String nomadiniDeviceId,
            String partnerId,
            String partnerName) {
        assertAndThrow(!partnerId.isEmpty());
        assertAndThrow(!partnerName.isEmpty());

        NomadiniIdToPartnerId nomadiniIdToPartnerId = new NomadiniIdToPartnerId(
                nomadiniDeviceId,
                partnerName, partnerId);
        nomadiniIdToPartnerId.setNomadiniDeviceId(nomadiniDeviceId);

        gicapodsIdToExchangeIdsMapCassandraService.write(nomadiniIdToPartnerId);
    }
}


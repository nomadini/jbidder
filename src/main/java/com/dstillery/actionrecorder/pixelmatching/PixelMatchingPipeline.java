package com.dstillery.actionrecorder.pixelmatching;

import com.dstillery.actionrecorder.modules.ActionRecorderContext;
import com.dstillery.actionrecorder.modules.ActionRecorderModule;
import com.dstillery.actionrecorder.modules.NomadiniDeviceIdCookieReaderModule;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class PixelMatchingPipeline  {
    private static final Logger LOGGER = LoggerFactory.getLogger(PixelMatchingPipeline.class);

    private final MetricReporterService metricReporterService;
    private List<ActionRecorderModule> modules = new ArrayList<>();

    public PixelMatchingPipeline(MetricReporterService metricReporterService,
                                 NomadiniDeviceIdCookieReaderModule nomadiniDeviceIdCookieReaderModule,
                                 NomadiniIdToPartnerIdMapperModule nomadiniIdToPartnerIdMapperModule) {
        this.metricReporterService = metricReporterService;;
        modules.add(nomadiniDeviceIdCookieReaderModule);
        modules.add(nomadiniIdToPartnerIdMapperModule);
    }

    public void process(ActionRecorderContext context) {

        for (ActionRecorderModule it : modules) {
            LOGGER.trace("running module : {}", it.getName());
            it.process(context);
        }
    }

}

package com.dstillery.actionrecorder.conversionrecording;

import java.util.ArrayList;
import java.util.List;

import com.dstillery.actionrecorder.modules.ActionRecorderContext;
import com.dstillery.actionrecorder.modules.ActionRecorderModule;
import com.dstillery.actionrecorder.modules.ConversionDecider;
import com.dstillery.actionrecorder.modules.NomadiniDeviceIdCookieReaderModule;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class ConversionRecorderPipeline {

        private List<ActionRecorderModule> modules = new ArrayList<>();
        private MetricReporterService metricReporterService;

        public ConversionRecorderPipeline(MetricReporterService metricReporterService,
                                          NomadiniDeviceIdCookieReaderModule nomadiniDeviceIdCookieReaderModule,
                                          ConversionDecider conversionDecider) {
                this.metricReporterService = metricReporterService;;
                modules.add(nomadiniDeviceIdCookieReaderModule);
                modules.add(conversionDecider);

        }

        void process(ActionRecorderContext context) {

                for ( ActionRecorderModule it : modules) {
                        it.process(context);
                        metricReporterService.addStateModuleForEntity("LastModule-" + it.getClass().getSimpleName(),
                                "ConversionRecorderPipeline",
                                "ALL");
                }

        }

}

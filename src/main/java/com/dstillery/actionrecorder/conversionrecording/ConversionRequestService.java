package com.dstillery.actionrecorder.conversionrecording;

import static com.dstillery.actionrecorder.app.PixelMatchingController.NOMADINI_DEVICE_ID;
import static com.dstillery.common.util.HttpUtil.setBadRequestResponse;

import java.time.Instant;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.actionrecorder.modules.ActionRecorderContext;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.pixel.Pixel;
import com.dstillery.common.pixel.PixelCacheService;
import com.dstillery.common.util.HttpUtil;

public class ConversionRequestService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConversionRequestService.class);

    private ConversionRecorderPipeline conversionRecorderPipeline;
    private MetricReporterService metricReporterService;
    private PixelCacheService pixelCacheService;

    public ConversionRequestService(ConversionRecorderPipeline conversionRecorderPipeline,
                                    MetricReporterService metricReporterService,
                                    PixelCacheService pixelCacheService) {
        this.conversionRecorderPipeline = conversionRecorderPipeline;
        this.metricReporterService = metricReporterService;

        this.pixelCacheService = pixelCacheService;
    }

    public void handleRequest(HttpServletRequest request,
                              HttpServletResponse response,
                              ActionRecorderContext context) {

        try {
            context.setHttpResponsePtr(response);
            metricReporterService.addStateModuleForEntity(
                    "REQUEST_RECIEVED",
                    "ConversionRequestService",
                    "ALL");
            Instant now = Instant.now();

            configureContext(request, context);
            context.setHttpRequestPtr(request);
            context.setHttpResponsePtr(response);

            conversionRecorderPipeline.process(context);
            long diff = Instant.now().toEpochMilli() - now.toEpochMilli(); // how long did it take?
            //LOGGER.debug("actionrecorder Latency ",diff / 1000," milliseconds";

            //setEmptyResponse already sets the status to 204 but
            //i have include the line below to make sure, to setEmptyResponse
            //will not affect this code here

            //this is just to assert cookie has been written
            Cookie myCookie =
                    new Cookie(NOMADINI_DEVICE_ID, context.getDevice().getDeviceId());
            response.addCookie(myCookie);

            metricReporterService.addStateModuleForEntity(
                    "SENDING_GOOD_RESPONSE",
                    "ConversionRequestService",
                    "ALL");

            HttpUtil.setEmptyResponse(response);
        } catch (Exception e) {
            //LOGGER.error("error happening when handling request ", e);
            //LOGGER.debug("sending no bad request as a result of exception";
            metricReporterService.addStateModuleForEntity(
                    "EXCEPTION_IN_HANDLER",
                    "ConversionRequestService",
                    "ALL");
            LOGGER.warn("exception occurred", e);
            setBadRequestResponse(response, e.getMessage());
        }
    }

    private void configureContext(HttpServletRequest request, ActionRecorderContext context) {
        String pixelKey = context.getQueryParams().get("pixelId");
        if (pixelKey != null) {
            context.setPixelUniqueKey(pixelKey);
        } else {
            HttpUtil.printQueryParamsOfRequest(request);
            throw new RuntimeException("pixelId is required for action");
        }

        Pixel pixel = this.pixelCacheService.getKeyToPixelMap().get(context.getPixelUniqueKey());
        if (pixel != null) {
            context.setPixel(pixel);
        } else {
            metricReporterService.
                    addStateModuleForEntity("ABORTING_PIXEL_ID_PARAM_NOT_FOUND",
                            "ConversionRequestService",
                            "pix" + context.getPixelUniqueKey());
            throw new IllegalStateException("ABORTING_PIXEL_ID_PARAM_NOT_FOUND");
        }

        context.setDomain(request.getRequestURI());
    }

    public ConversionRecorderPipeline getConversionRecorderPipeline() {
        return conversionRecorderPipeline;
    }

    public void setConversionRecorderPipeline(ConversionRecorderPipeline conversionRecorderPipeline) {
        this.conversionRecorderPipeline = conversionRecorderPipeline;
    }
}


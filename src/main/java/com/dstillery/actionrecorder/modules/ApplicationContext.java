package com.dstillery.actionrecorder.modules;

import java.util.concurrent.atomic.AtomicBoolean;

public class ApplicationContext  {

    private AtomicBoolean ThreadsInterruptedFlag = new AtomicBoolean();

    public static AtomicBoolean getThreadsInterruptedFlag() {
        return new AtomicBoolean();
    }

    String getTransactionIdConstant(){
        return "__TRN_ID__";
    }

    ApplicationContext() {

    }

    void info(String str) {

    }

}

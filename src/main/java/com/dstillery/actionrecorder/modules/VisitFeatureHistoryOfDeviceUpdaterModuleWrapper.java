package com.dstillery.actionrecorder.modules;

import com.dstillery.common.common_modules.DeviceHistoryUpdaterModule;

import java.io.IOException;

import static com.dstillery.common.feature.FeatureType.GTLD;

public class VisitFeatureHistoryOfDeviceUpdaterModuleWrapper {

    private DeviceHistoryUpdaterModule visitFeatureDeviceHistoryUpdaterModule;

    public VisitFeatureHistoryOfDeviceUpdaterModuleWrapper(DeviceHistoryUpdaterModule visitFeatureDeviceHistoryUpdaterModule) {
        this.visitFeatureDeviceHistoryUpdaterModule = visitFeatureDeviceHistoryUpdaterModule;
    }

    void process(ActionRecorderContext context) throws IOException, InterruptedException {
        if (context.getDevice() == null || context.getDomain().isEmpty()) {
            throw new RuntimeException("unknown siteDomain or deviceType");
        }
        visitFeatureDeviceHistoryUpdaterModule.process(GTLD,
                context.getDomain(),
                context.getDevice());
    }

}

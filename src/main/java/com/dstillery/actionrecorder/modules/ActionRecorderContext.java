package com.dstillery.actionrecorder.modules;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.geo.Country;
import com.dstillery.common.geo.State;
import com.dstillery.common.pixel.Pixel;
import com.dstillery.common.util.HttpUtil;

public class ActionRecorderContext  {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActionRecorderContext.class);

    private HttpServletRequest httpRequestPtr;
    private HttpServletResponse httpResponsePtr;
    private Map<String, String> queryParams = new HashMap<>();
    private Map<String, String> cookieMap = new HashMap<>();

    private EventLog actionEventLog;
    private NomadiniDevice device;
    private double deviceLat;
    private double deviceLon;
    private boolean deviceGeoIsAvailable;
    private int pixelId;
    private String pixelUniqueKey;
    private Pixel pixel;
    private String domain;
    private String exchangeName;
    private String siteCategory;
    private boolean strictEventLogChecking;

    private String transactionId;
    private int partnerId;
    private String userTimeZone;
    private String deviceUserAgent;
    private String deviceIp;
    private Country deviceCountry;
    private State deviceState;
    private String deviceCity;
    private String deviceZipcode;
    private double mgrs1km;
    private double mgrs100m;
    private double mgrs10m;

    private String appHostname;
    private String appVersion;
    private String servletName;

    public ActionRecorderContext(HttpServletRequest request,
                                 HttpServletResponse response,
                                 String exchangeName,
                                 String servletName) {
        this.httpRequestPtr = request;
        this.httpResponsePtr = response;
        this.exchangeName = exchangeName;
        queryParams = HttpUtil.getMapOfQueryParams(request);

        cookieMap = HttpUtil.getCookiesFromRequest(request);
        LOGGER.info("cookieMap read : {}, queryParams read : {}, servletName :{}", cookieMap, queryParams, servletName);
        pixelId = -1;
        deviceLat = 0.0;
        deviceLon = 0.0;
        deviceGeoIsAvailable = false;
        pixel = null;
        mgrs1km = 0.0;
        mgrs100m = 0.0;
        mgrs10m = 0.0;
        partnerId = 0;
        strictEventLogChecking = false;//for now it's set to false

    }

    public HttpServletRequest getHttpRequestPtr() {
        return httpRequestPtr;
    }

    public void setHttpRequestPtr(HttpServletRequest httpRequestPtr) {
        this.httpRequestPtr = httpRequestPtr;
    }

    public HttpServletResponse getHttpResponsePtr() {
        return httpResponsePtr;
    }

    public void setHttpResponsePtr(HttpServletResponse httpResponsePtr) {
        this.httpResponsePtr = httpResponsePtr;
    }

    public Map<String, String> getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(Map<String, String> queryParams) {
        this.queryParams = queryParams;
    }

    public Map<String, String> getCookieMap() {
        return cookieMap;
    }

    public void setCookieMap(Map<String, String> cookieMap) {
        this.cookieMap = cookieMap;
    }

    public EventLog getActionEventLog() {
        return actionEventLog;
    }

    public void setActionEventLog(EventLog actionEventLog) {
        this.actionEventLog = actionEventLog;
    }

    public NomadiniDevice getDevice() {
        return device;
    }

    public void setDevice(NomadiniDevice device) {
        this.device = device;
    }

    public double getDeviceLat() {
        return deviceLat;
    }

    public void setDeviceLat(double deviceLat) {
        this.deviceLat = deviceLat;
    }

    public double getDeviceLon() {
        return deviceLon;
    }

    public void setDeviceLon(double deviceLon) {
        this.deviceLon = deviceLon;
    }

    public boolean isDeviceGeoIsAvailable() {
        return deviceGeoIsAvailable;
    }

    public void setDeviceGeoIsAvailable(boolean deviceGeoIsAvailable) {
        this.deviceGeoIsAvailable = deviceGeoIsAvailable;
    }

    public int getPixelId() {
        return pixelId;
    }

    public void setPixelId(int pixelId) {
        this.pixelId = pixelId;
    }

    public String getPixelUniqueKey() {
        return pixelUniqueKey;
    }

    public void setPixelUniqueKey(String pixelUniqueKey) {
        this.pixelUniqueKey = pixelUniqueKey;
    }

    public Pixel getPixel() {
        return pixel;
    }

    public void setPixel(Pixel pixel) {
        this.pixel = pixel;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getSiteCategory() {
        return siteCategory;
    }

    public void setSiteCategory(String siteCategory) {
        this.siteCategory = siteCategory;
    }

    public boolean isStrictEventLogChecking() {
        return strictEventLogChecking;
    }

    public void setStrictEventLogChecking(boolean strictEventLogChecking) {
        this.strictEventLogChecking = strictEventLogChecking;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public int getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(int partnerId) {
        this.partnerId = partnerId;
    }

    public String getUserTimeZone() {
        return userTimeZone;
    }

    public void setUserTimeZone(String userTimeZone) {
        this.userTimeZone = userTimeZone;
    }

    public String getDeviceUserAgent() {
        return deviceUserAgent;
    }

    public void setDeviceUserAgent(String deviceUserAgent) {
        this.deviceUserAgent = deviceUserAgent;
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp;
    }

    public Country getDeviceCountry() {
        return deviceCountry;
    }

    public void setDeviceCountry(Country deviceCountry) {
        this.deviceCountry = deviceCountry;
    }

    public State getDeviceState() {
        return deviceState;
    }

    public void setDeviceState(State deviceState) {
        this.deviceState = deviceState;
    }

    public String getDeviceCity() {
        return deviceCity;
    }

    public void setDeviceCity(String deviceCity) {
        this.deviceCity = deviceCity;
    }

    public String getDeviceZipcode() {
        return deviceZipcode;
    }

    public void setDeviceZipcode(String deviceZipcode) {
        this.deviceZipcode = deviceZipcode;
    }

    public double getMgrs1km() {
        return mgrs1km;
    }

    public void setMgrs1km(double mgrs1km) {
        this.mgrs1km = mgrs1km;
    }

    public double getMgrs100m() {
        return mgrs100m;
    }

    public void setMgrs100m(double mgrs100m) {
        this.mgrs100m = mgrs100m;
    }

    public double getMgrs10m() {
        return mgrs10m;
    }

    public void setMgrs10m(double mgrs10m) {
        this.mgrs10m = mgrs10m;
    }

    public String getAppHostname() {
        return appHostname;
    }

    public void setAppHostname(String appHostname) {
        this.appHostname = appHostname;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}


package com.dstillery.actionrecorder.modules;

import com.dstillery.common.common_modules.FeatureDeviceHistoryUpdaterModule;

import static com.dstillery.common.feature.FeatureType.GTLD;

//NOT USED IN actionrecorder but we keep it for future use
public class DeviceHistoryOfVisitFeatureUpdaterModuleWrapper implements ActionRecorderModule {

    private FeatureDeviceHistoryUpdaterModule featureDeviceHistoryUpdaterModule;

    public DeviceHistoryOfVisitFeatureUpdaterModuleWrapper(FeatureDeviceHistoryUpdaterModule featureDeviceHistoryUpdaterModule) {
        this.featureDeviceHistoryUpdaterModule = featureDeviceHistoryUpdaterModule;
    }

    @Override
    public void process(ActionRecorderContext context) {
        if (context.getDevice() == null || context.getDomain().isEmpty()) {
            throw new RuntimeException("unknown siteDomain or deviceType");
        }
        try {
            featureDeviceHistoryUpdaterModule.process(GTLD,
                    context.getDomain(),
                    context.getDevice());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}

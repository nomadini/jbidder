package com.dstillery.actionrecorder.modules;

import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.pixel.PixelDeviceHistory;
import com.dstillery.common.pixel.PixelDeviceHistoryCassandraService;

public class PixelDeviceHistoryUpdaterModule implements ActionRecorderModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(PixelDeviceHistoryUpdaterModule.class);

    private PixelDeviceHistoryCassandraService pixelDeviceHistoryCassandraService;
    private MetricReporterService metricReporterService;

    public PixelDeviceHistoryUpdaterModule(
            MetricReporterService metricReporterService,
            PixelDeviceHistoryCassandraService pixelDeviceHistoryCassandraService) {
        this.metricReporterService = metricReporterService;;
        this.pixelDeviceHistoryCassandraService = pixelDeviceHistoryCassandraService;

    }

    public void process(ActionRecorderContext context) {

        metricReporterService.addStateModuleForEntity(
                "PERSISTING_PIXEL_DEVICE_MAP",
                "PixelDeviceHistoryUpdaterModule",
                context.getPixelUniqueKey());

        PixelDeviceHistory pixelDeviceHistory = new PixelDeviceHistory();
        pixelDeviceHistory.setPixelUniqueKey(context.getPixelUniqueKey());
        pixelDeviceHistory.setTimeOfVisit(Instant.now());
        pixelDeviceHistory.setDevice(context.getDevice());
        LOGGER.info("writing pixelDeviceHistory : {}", pixelDeviceHistory);
        pixelDeviceHistoryCassandraService.write(pixelDeviceHistory);
    }

}

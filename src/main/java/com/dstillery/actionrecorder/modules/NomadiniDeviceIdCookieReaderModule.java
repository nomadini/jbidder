package com.dstillery.actionrecorder.modules;

import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

import javax.servlet.http.Cookie;

import static com.dstillery.actionrecorder.app.PixelMatchingController.NOMADINI_DEVICE_ID;
import static com.dstillery.common.device.NomadiniDevice.createStandardDeviceId;

public class NomadiniDeviceIdCookieReaderModule implements ActionRecorderModule {

    private MetricReporterService metricReporterService;

    public NomadiniDeviceIdCookieReaderModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;;
    }

    public void process(ActionRecorderContext context) {

        String nomadiniDeviceIdCookieValue =
                readOrCreatedNomadiniDeviceIdCookie(
                        metricReporterService,
                        context);
        //TODO : add the time of insertion here, so we can delete it later
        context.setDevice(new NomadiniDevice(nomadiniDeviceIdCookieValue,
                                             DeviceTypeValue.DISPLAY,
                                             DeviceClassValue.DESKTOP));

        setCookie(context);

    }

    private void setCookie(ActionRecorderContext context) {
        context.getHttpResponsePtr().addCookie(new Cookie(
                NOMADINI_DEVICE_ID, context.getDevice().getDeviceId()
        ));
    }

    private String readOrCreatedNomadiniDeviceIdCookie(
            MetricReporterService metricReporterService,
            ActionRecorderContext context) {
        String nomadiniDeviceIdCookieValue = context.getCookieMap().get(NOMADINI_DEVICE_ID);

        if (nomadiniDeviceIdCookieValue != null) {
            //browser is known to us....
            metricReporterService.addStateModuleForEntity(
                    "known_browser_seen",
                    "NomadiniDeviceIdCookieReaderModule",
                    "ALL");

        } else {
            //browser is unknown to us
            //we write the mapping to cassandra and to the nomadiniDeviceId cookie
            metricReporterService.addStateModuleForEntity(
                    "new_browser_seen",
                    "NomadiniDeviceIdCookieReaderModule",
                    "ALL");
            nomadiniDeviceIdCookieValue = createStandardDeviceId();

        }
        return nomadiniDeviceIdCookieValue;
    }
}

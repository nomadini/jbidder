package com.dstillery.actionrecorder.modules;

public interface ActionRecorderModule  {
    void process(ActionRecorderContext context);
    default String getName() {
        return this.getClass().getSimpleName();
    }
}

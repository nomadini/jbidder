package com.dstillery.actionrecorder.modules;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;

import java.util.List;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistoryCassandraService;
import com.dstillery.common.pixel.PixelSegmentCacheService;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentType;

public class PixelActionTakerSegmentWriterModule implements ActionRecorderModule {

    private DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService;
    private PixelSegmentCacheService pixelSegmentCacheService;
    private MetricReporterService metricReporterService;

    public PixelActionTakerSegmentWriterModule(
            DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService,
            PixelSegmentCacheService pixelSegmentCacheService,
            MetricReporterService metricReporterService) {
        this.deviceSegmentHistoryCassandraService = deviceSegmentHistoryCassandraService;
        this.pixelSegmentCacheService = pixelSegmentCacheService;
        this.metricReporterService = metricReporterService;;
    }

    public void process(ActionRecorderContext context) {


        metricReporterService.addStateModuleForEntity("PERSISTING_PIXEL_ACTION_TAKER_DEVICE",
                "PixelActionTakerSegmentWriterModule",
                "pixel"+ context.getPixelUniqueKey());

        List<Segment> segmentList = pixelSegmentCacheService.getPixelIdToSegments()
                .get(context.getPixel().getId());
        if (segmentList == null) {
            return;
        }

        for (Segment segment : segmentList) {
            if (segment.getType().equals(SegmentType.ACTION_TAKER)) {
                recordSegmentForDevice(segment, context);
            }
        }
    }

    public void recordSegmentForDevice(
            Segment segment,
            ActionRecorderContext context) {
        DeviceSegmentHistory deviceSegmentHistory = new DeviceSegmentHistory(context.getDevice());
        deviceSegmentHistory.setDevice(
                        context.getDevice());
        deviceSegmentHistory.setSegment(segment);

        deviceSegmentHistoryCassandraService.write(deviceSegmentHistory);

        metricReporterService.addStateModuleForEntity(
                "ADDING_ACTION_TAKER_DEVICE_SEGMENT",
                "PixelActionTakerSegmentWriterModule",
                ALL_ENTITIES,
                MetricReporterService.MetricPriority.IMPORTANT);

        metricReporterService.addStateModuleForEntity(
                "ADDING_ACTION_TAKER_DEVICE_SEGMENT",
                "PixelActionTakerSegmentWriterModule",
                segment.getName());
    }
}

package com.dstillery.actionrecorder.modules;

import static com.dstillery.common.eventlog.EventLog.createRandomTransactionId;
import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.io.IOException;
import java.time.Instant;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class ActionEventLogCreatorModule implements ActionRecorderModule {

    private CassandraService eventLogCassandraService;
    private MetricReporterService metricReporterService;
    private String appHostname;
    private String appVersion;

    public ActionEventLogCreatorModule(String appHostname,
                                       String appVersion,
                                       MetricReporterService metricReporterService,
                                       CassandraService eventLogCassandraService) {
        this.appHostname = appHostname;
        this.appVersion = appVersion;
        this.metricReporterService = metricReporterService;;
        this.eventLogCassandraService = eventLogCassandraService;
    }

    public void process(ActionRecorderContext context) {
        //this module runs after we have selected a bid

        context.setActionEventLog(createEventLogFrom(context));
        ////LOG_EVERY_N(INFO, 1000) , google.COUNTER, "th sample eventLog : " , context.actionEventLog.toJson();

        metricReporterService.addStateModuleForEntity("action_event_writing_to_queue",
                "ActionEventLogCreatorModule",
                "ALL");

        eventLogCassandraService.write(context.getActionEventLog());

    }

    public EventLog createEventLogFrom(
            ActionRecorderContext context) {

        context.setTransactionId(createRandomTransactionId());

        EventLog eventLog = new EventLog(
                context.getTransactionId(),
                EventLog.BidEventType.ACTION,
                "",
                DeviceClassValue.UNKNOWN,
                DeviceTypeValue.UNKNOWN
        );
        eventLog.setEventTime(Instant.now().toEpochMilli());
        eventLog.setPartnerId(context.getPartnerId());
        eventLog.setPixelId(context.getPixel().getId());
        assertAndThrow(eventLog.getPixelId() > 0);
        eventLog.setUserTimeZone(context.getUserTimeZone());
        eventLog.setDeviceUserAgent(context.getDeviceUserAgent());
        eventLog.setDeviceIp(context.getDeviceIp());
        eventLog.setDevice(context.getDevice());
        eventLog.setDeviceLat(context.getDeviceLat());
        eventLog.setDeviceLon(context.getDeviceLon());
        eventLog.setDeviceCountry(context.getDeviceCountry());
        eventLog.setDeviceState(context.getDeviceState());
        eventLog.setDeviceCity(context.getDeviceCity());
        eventLog.setDeviceZipcode(context.getDeviceZipcode());
        eventLog.setDeviceIpAddress(context.getDeviceIp());
        eventLog.setMgrs1km(String.valueOf(context.getMgrs1km()));
        eventLog.setMgrs100m(String.valueOf(context.getMgrs100m()));
        eventLog.setMgrs10m(String.valueOf(context.getMgrs10m()));

        eventLog.setAppHostname(appHostname);
        eventLog.setAppVersion(appVersion);
        if (context.isStrictEventLogChecking()) {
            //sometimes we want to record no_bid events in cassandra,
            //in BidEventRecorderModule so we set this boolean flag as false
            //and we don't check these

            assertAndThrow(eventLog.getEventTime() > 0);
            assertAndThrow(!eventLog.getEventId().isEmpty());
            assertAndThrow(!eventLog.getDeviceUserAgent().isEmpty());
            assertAndThrow(!eventLog.getDeviceIp().isEmpty());
            assertAndThrow(eventLog.getDeviceLat() != 0);
            assertAndThrow(eventLog.getDeviceLon() != 0);
            assertAndThrow(eventLog.getDeviceCountry() != null);
            assertAndThrow(!eventLog.getDeviceCity().isEmpty());
            assertAndThrow(eventLog.getDeviceState() != null);
            assertAndThrow(!eventLog.getDeviceZipcode().isEmpty());
            assertAndThrow(!eventLog.getDeviceIpAddress().isEmpty());
            assertAndThrow(!eventLog.getAppHostname().isEmpty());
            assertAndThrow(!eventLog.getAppVersion().isEmpty());
        }
        return eventLog;
    }

    public static void main(String[] args) throws IOException {
        Document doc = Jsoup.connect("https://edition.cnn.com/world/live-news/notre-dame-fire/index.html").get();
        String title = doc.title();

        System.out.println("title : " + title);
//        System.out.println(doc);
        System.out.println("h1 : " + doc.body().getElementsByTag("h1").text());
//        System.out.println("h2 : " + doc.body().getElementsByTag("h2"));
//        System.out.println("h3 : " + doc.body().getElementsByTag("h3"));
//        System.out.println("h4 : " + doc.body().getElementsByTag("h4"));
    }

}

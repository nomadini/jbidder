package com.dstillery.actionrecorder.modules;

import static com.dstillery.common.eventlog.EventLog.TARGETING_TYPE_IGNORE_FOR_NOW;
import static com.dstillery.common.eventlog.EventLog.createRandomTransactionId;


import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.util.DateTimeUtil.getNowInMilliSecond;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.adhistory.AdEntry;
import com.dstillery.common.adhistory.AdEntryDevicePair;
import com.dstillery.common.cache_realtime.RealTimeEntityCacheService;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.eventlog.AdInteractionRecordingModule;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.targetgroup_maps.tgpixel.TargetGroupPixelCacheService;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ConversionDecider implements ActionRecorderModule {

    private long dayInSeconds;
    private MetricReporterService metricReporterService;
    private RealTimeEntityCacheService<AdEntryDevicePair> adHistoryCacheService;
    private AdInteractionRecordingModule adInteractionRecordingModule;
    private CassandraService eventLogCassandraService;
    private JavaType adEntryDevicePairListType = new ObjectMapper().getTypeFactory().
            constructCollectionType(List.class, AdEntryDevicePair.class);
    private TargetGroupPixelCacheService targetGroupPixelCacheService;

    public ConversionDecider(long dayInSeconds,
                             MetricReporterService metricReporterService,
                             RealTimeEntityCacheService<AdEntryDevicePair> adHistoryCacheService,
                             TargetGroupPixelCacheService targetGroupPixelCacheService,
                             AdInteractionRecordingModule adInteractionRecordingModule,
                             CassandraService eventLogCassandraService) {
        this.dayInSeconds = dayInSeconds;
        this.metricReporterService = metricReporterService;;
        this.adHistoryCacheService = adHistoryCacheService;
        this.adInteractionRecordingModule = adInteractionRecordingModule;
        this.eventLogCassandraService = eventLogCassandraService;
        this.targetGroupPixelCacheService = targetGroupPixelCacheService;
    }

    private List<Long> findTargetGroupIdsForPixelIds(List<Long> pixelIds) {
        List<Long> targetGroupIds = new ArrayList<>();
        for (Long pixelId : pixelIds) {
            List<Long> targetGroupIdsToAdd = targetGroupPixelCacheService.getMapOfPixelIdToTargetGroupIds()
                    .get(pixelId);
            targetGroupIds.addAll(targetGroupIdsToAdd);

        }
        return targetGroupIds;
    }

    public void process(ActionRecorderContext context) {

        metricReporterService.addStateModuleForEntity("PERSISTING_PIXEL_DEVICE_MAP",
                "ConversionDecider",
                context.getPixelUniqueKey());
        //read adHistory of device...
        AdEntryDevicePair keyOfAdHistory = new AdEntryDevicePair(context.getDevice());
        try {
            List<AdEntryDevicePair> adHistories =
                    adHistoryCacheService.readDataOptional(keyOfAdHistory);

            // find all the targetgroups and tgs that is assigned to this pixelUniqueKey

            List<Long> targetGroupIds = targetGroupPixelCacheService.getMapOfPixelIdToTargetGroupIds()
                    .get(context.getPixel().getId());
            if (targetGroupIds == null || targetGroupIds.isEmpty()) {
                metricReporterService.addStateModuleForEntity("CONVERSION_PIXEL_HIT_FOR_NO_TARGETGROUPS",
                        "ConversionDecider",
                        ALL_ENTITIES,
                        MetricReporterService.MetricPriority.EXCEPTION);
            }
            for (Long targetGroupId : targetGroupIds) {
                List<AdEntry> adEntries = adHistories.stream().filter(
                        adhistory -> adhistory.getAdEntry().getTargetGroupId() == targetGroupId)
                        .map(AdEntryDevicePair::getAdEntry)
                        .collect(Collectors.toList());

                for (AdEntry adEntry : adEntries) {
                    long recencyInSeconds = (getNowInMilliSecond() - adEntry.getTimeAdShownInMillis()) * 1000;
                    if (recencyInSeconds <= dayInSeconds * 7) {
                        persistAConversion(adEntry, context);
                        break;//break out of adEntries lookup for this targetGroup
                    } else {
                        metricReporterService.addStateModuleForEntity("CONVERSION_FOUND_BUT_WAS_TOO_OLD",
                                "ConversionDecider",
                                 adEntry.getTargetGroupId());
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void persistAConversion(AdEntry adEntry, ActionRecorderContext context) {
        AdEntryDevicePair conversion = new AdEntryDevicePair(context.getDevice());

        metricReporterService.addStateModuleForEntity("ADDING_CONVERSION",
                "ConversionDecider",
                 adEntry.getTargetGroupId(),
                MetricReporterService.MetricPriority.IMPORTANT);
        adHistoryCacheService.getRealTimeEntityCassandraService().
                write(conversion);

        EventLog impressionEventLogKey = new EventLog(
                adEntry.getTransactionId(),
                EventLog.BidEventType.IMPRESSION,
                TARGETING_TYPE_IGNORE_FOR_NOW,
                DeviceClassValue.UNKNOWN,
                DeviceTypeValue.UNKNOWN
        );

        EventLog impressionEventLog = (EventLog)
                eventLogCassandraService.readDataOptional(impressionEventLogKey);
        if (impressionEventLog == null) {
            throw new RuntimeException("cannot find impression for the conversion");
        }
        impressionEventLog.setEventType(EventLog.BidEventType.CONVERSION);
        impressionEventLog.setSourceEventId(adEntry.getTransactionId());
        impressionEventLog.setEventId(createRandomTransactionId());
        adInteractionRecordingModule.enqueueEventForPersistence(impressionEventLog);
    }

}

package com.dstillery.adserver.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdServerEssentialConfig {

    @Bean
    public String appPropertyFileName() {
        return "adserver.properties";
    }
}

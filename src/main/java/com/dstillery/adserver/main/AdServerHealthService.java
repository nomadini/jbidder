package com.dstillery.adserver.main;import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.service.dicovery.DiscoveryService;

public class
AdServerHealthService  {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdServerHealthService.class);

    private MetricReporterService metricReporterService;
    private AtomicBoolean isImpressionServerHandlerHealthy;
    private AtomicLong numberOfExceptionsInLast10Min;
    private AtomicLong numberOfRealAdServingInLastMinute;
    private AtomicLong numberOfPSAServingInLastHour;
    private AtomicLong acceptableNumberOfExceptionsInLastTenMintues;
    private AtomicBoolean isReloadProcessHealthy;
    private DiscoveryService discoveryService;

    public AdServerHealthService(
            MetricReporterService metricReporterService,
            DiscoveryService discoveryService,
            AtomicBoolean isImpressionServerHandlerHealthy,
            AtomicLong numberOfExceptionsInLast10Min,
            AtomicLong numberOfRealAdServingInLastMinute,
            AtomicLong numberOfPSAServingInLastHour,
            AtomicLong acceptableNumberOfExceptionsInLastTenMintues,
            AtomicBoolean isReloadProcessHealthy) {
        this.metricReporterService = metricReporterService;;
        this.isImpressionServerHandlerHealthy = isImpressionServerHandlerHealthy;
        this.numberOfExceptionsInLast10Min = numberOfExceptionsInLast10Min;
        this.numberOfRealAdServingInLastMinute = numberOfRealAdServingInLastMinute;
        this.numberOfPSAServingInLastHour = numberOfPSAServingInLastHour;
        this.acceptableNumberOfExceptionsInLastTenMintues = acceptableNumberOfExceptionsInLastTenMintues;
        this.isReloadProcessHealthy = isReloadProcessHealthy;
        this.discoveryService = discoveryService;
    }

    public boolean isAdServerHealthy() {
        try {
            if (discoveryService.discoverAllServiceUrls("adserver").isEmpty()) {
                LOGGER.error("There are no adserver instances up and running in eureka's world");
                return false;
            }

            if (!isReloadProcessHealthy.get()) {
                LOGGER.error("adserver is still reloading data. can't server anything yet");
                return false;
            }

            //TODO : if number of impression served less than 10 per minute or some configurable threshold
            // send the bidder to slow bid mode, because something might be wrong

            if (numberOfRealAdServingInLastMinute.get() >= 500) {
                LOGGER.error("showing too many impressions : {}", numberOfRealAdServingInLastMinute.get());
                metricReporterService.addStateModuleForEntity("SHOWING_TOO_MANY_IMPS",
                        "AdServerHealthService",
                        "ALL",
                        MetricReporterService.MetricPriority.EXCEPTION);
                return false;
            }

            if (numberOfPSAServingInLastHour.get() >= 10) {

                metricReporterService.addStateModuleForEntity("numberOfPSAServingInLastHourIsTooHigh",
                        "AdServerHealthService",
                        "ALL",
                        MetricReporterService.MetricPriority.EXCEPTION);
                return false;
            }

            if (!isImpressionServerHandlerHealthy.get()) {
                metricReporterService.addStateModuleForEntity("isImpressionServerHandlerHealthyNotHealthy",
                        "AdServerHealthService",
                        "ALL",
                        MetricReporterService.MetricPriority.EXCEPTION);
                //LOGGER.error("ImpressionServerHanler IS NOT Healthy";
                return false;
            }

            if (numberOfExceptionsInLast10Min.get() >= acceptableNumberOfExceptionsInLastTenMintues.get()) {
                //LOGGER.error("adserver is facing too many exceptions";
                metricReporterService.addStateModuleForEntity("tooManyExceptionsInLastMinute",
                        "AdServerHealthService",
                        "ALL",
                        MetricReporterService.MetricPriority.EXCEPTION);
                return false;
            }

            metricReporterService.addStateModuleForEntity("IsHealthy",
                    "AdServerHealthService",
                    "ALL");
            return true;
        } catch (Exception e) {
            LOGGER.warn("exception when determining the health of adserver", e);
            return false;
        }
    }
}

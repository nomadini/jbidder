package com.dstillery.adserver.main;

import com.dstillery.common.DataWasReloadedEvent;
import com.dstillery.common.config.ConfigService;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.GUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class AdServerAsyncJobService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdServerAsyncJobService.class);
    private ConfigService configService;

    private MetricReporterService metricReporterService;
    private AtomicBoolean interruptedFlag;

    public AdServerAsyncJobService(AtomicLong numberOfExceptionsInLast10Min,
                                   ConfigService configService,
                                   EventBus eventBus,
                                   MetricReporterService metricReporterService,
                                   AtomicBoolean interruptedFlag) {
        this.configService = configService;
        this.metricReporterService = metricReporterService;;
        this.interruptedFlag = interruptedFlag;

        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                numberOfExceptionsInLast10Min.set(0);
            }
        }, 0, 10, TimeUnit.MINUTES);
        eventBus.register(this);

    }

    @Subscribe
    public void processEvent(DataWasReloadedEvent event) {
        LOGGER.info("event received : {} by class : {}", event);
        reloadCaches();
    }

    private void reloadCaches() {
        try {

            metricReporterService.addStateModuleForEntity("reloadCaches",
                    "AdServerAsyncJobService",
                    "ALL");

        } catch (RuntimeException e) {
            //LOGGER.error("error happening when reloading caches ", e);
            //TODO go red and show in dashboard that this is red
        }

        if (!interruptedFlag.get()) {
            int reloadingCacheInterval = configService.getAsInt("reloadCachesIntervalInSecond");
            GUtil.sleepInSeconds( reloadingCacheInterval);
        }
    }
}


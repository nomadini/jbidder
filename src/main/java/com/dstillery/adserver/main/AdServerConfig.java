package com.dstillery.adserver.main;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dstillery.adserver.clicktracker.ClickPersistentModule;
import com.dstillery.adserver.clicktracker.ClickTrackerHandler;
import com.dstillery.adserver.clicktracker.ClickTrackerMainProcessor;
import com.dstillery.adserver.impressionserver.ImpressionServingProcessor;
import com.dstillery.adserver.impressionserver.ImpressionServingService;
import com.dstillery.adserver.impressiontracker.ImpressionTrackerHandler;
import com.dstillery.adserver.impressiontracker.ImpressionTrackerMainProcessor;
import com.dstillery.adserver.main.asynctasks.HourlyTask;
import com.dstillery.adserver.main.asynctasks.MinutelyTask;
import com.dstillery.adserver.modules.AdHistoryModule;
import com.dstillery.adserver.modules.AdServerEventLockModule;
import com.dstillery.adserver.modules.AdServerWiretapModule;
import com.dstillery.adserver.modules.AdservResponseModule;
import com.dstillery.adserver.modules.AdvertiserCostCalculatorModule;
import com.dstillery.adserver.modules.ClickUrlAppenderModule;
import com.dstillery.adserver.modules.ClientIdModule;
import com.dstillery.adserver.modules.ConfirmWinCounterModule;
import com.dstillery.adserver.modules.CookiesReaderModule;
import com.dstillery.adserver.modules.CookiesWriterModule;
import com.dstillery.adserver.modules.CreativePalletteCleanerModule;
import com.dstillery.adserver.modules.DruidFeederModule;
import com.dstillery.adserver.modules.EventLogRetriever;
import com.dstillery.adserver.modules.ImpressionRecordingModule;
import com.dstillery.adserver.modules.ImpressionTrackerAppenderModule;
import com.dstillery.adserver.modules.PixelMatchingUrlAppenderModule;
import com.dstillery.adserver.modules.PlatformCostCalculatorModule;
import com.dstillery.adserver.requesthandling.AdServerStatusService;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.kafka.KafkaProducerService;
import com.dstillery.common.kafka.NoOpPartitioner;
import com.dstillery.common.EventLock;
import com.dstillery.common.LoadPercentageBasedSampler;
import com.dstillery.common.adhistory.AdHistoryCassandraService;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.creative.CreativeCacheService;
import com.dstillery.common.eventlog.AdInteractionRecordingModule;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.wiretap.WiretapCassandraService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.targetgroup.bidding_performance_metric.models.ConfirmedWinRequest;
import com.google.common.eventbus.EventBus;

@Configuration
public class AdServerConfig {

    private AtomicLong numberOfPSAServingInLastHour = new AtomicLong();
    private AtomicLong numberOfRealAdServingInLastMinute = new AtomicLong();

    private AtomicLong numberOfExceptionsInLast10Min = new AtomicLong();
    private AtomicBoolean interruptedFlag = new AtomicBoolean(false);
    private AtomicBoolean isImpressionServerHandlerHealthy = new AtomicBoolean(true);

    @Autowired
    private AtomicBoolean isReloadProcessHealthy;

    @Bean
    public ConcurrentHashMap<String, ConfirmedWinRequest> bidderToConfirmedWins() {
        return new ConcurrentHashMap<>();
    }

    @Autowired
    private ConfigService configService;

    @Autowired
    private MetricReporterService metricReporterService;

    @Bean
    public HourlyTask hourlyTask(
            MetricReporterService metricReporterService
    ) {
        return new HourlyTask(metricReporterService,
                numberOfPSAServingInLastHour);
    }

    @Bean
    public KafkaProducerService druidKafkaProducerService() {
        return new KafkaProducerService(
                configService.get("druidTransAppKafkaCluster"),
                configService.get("druidTransAppTopicName"),
                new NoOpPartitioner(),
                "druid-producer"
        );
    }

    @Bean
    public DruidFeederModule druidFeederModule(
            MetricReporterService metricReporterService,
            KafkaProducerService druidKafkaProducerService) {
        return new DruidFeederModule(
                metricReporterService,
                druidKafkaProducerService);
    }

    @Bean
    public MinutelyTask minutelyTask(
            MetricReporterService metricReporterService,
            ConcurrentHashMap<String, ConfirmedWinRequest> bidderToConfirmedWins
    ) {
        return new MinutelyTask(
                numberOfRealAdServingInLastMinute,
                metricReporterService,
                bidderToConfirmedWins);
    }

    @Bean
    public ConfirmWinCounterModule confirmWinCounterModule(
            ConcurrentHashMap<String, ConfirmedWinRequest> bidderToConfirmedWins
    ) {
        return new ConfirmWinCounterModule(
                metricReporterService,
                bidderToConfirmedWins);
    }

    @Bean
    public CookiesReaderModule cookiesReaderModule() {
        return new CookiesReaderModule(
                metricReporterService);
    }

    @Bean
    public AdServerEventLockModule impressionLockModule(
            AeroCacheService<EventLock> eventLockPersistenceService
    ) {
        return new AdServerEventLockModule(
                metricReporterService,
                EventLog.BidEventType.IMPRESSION,
                eventLockPersistenceService);
    }

    public AdServerEventLockModule clickLockModule(
            AeroCacheService<EventLock> eventLockPersistenceService
    ) {
        return new AdServerEventLockModule(
                metricReporterService,
                EventLog.BidEventType.CLICK,
                eventLockPersistenceService);
    }

    @Bean
    public ClientIdModule clientIdModule() {
        return new ClientIdModule(
                metricReporterService);
    }

    @Bean
    public AdHistoryModule adHistoryModule(
            AdHistoryCassandraService adHistoryCassandraService) {
        return new AdHistoryModule(
                metricReporterService,
                adHistoryCassandraService);
    }

    @Bean
    public ClickPersistentModule clickPersistentModule(
            AdInteractionRecordingModule adInteractionRecordingModule,
            AdHistoryModule adHistoryModule
    ) {
        return new ClickPersistentModule(
                metricReporterService,
                adInteractionRecordingModule,
                adHistoryModule);
    }

    @Bean
    public PlatformCostCalculatorModule platformCostCalculatorModule(
            MetricReporterService metricReporterService,
            CampaignCacheService campaignCacheService
    ) {
        return new PlatformCostCalculatorModule(
                metricReporterService,
                campaignCacheService
        );
    }

    @Bean
    public AdvertiserCostCalculatorModule advertiserCostCalculatorModule(
            MetricReporterService metricReporterService,
            CampaignCacheService campaignCacheService
    ) {
        return new AdvertiserCostCalculatorModule(
                metricReporterService,
                campaignCacheService
        );
    }

    @Bean
    public ImpressionRecordingModule impressionRecordingModule(
            MetricReporterService metricReporterService,
            AdInteractionRecordingModule adInteractionRecordingModule,
            PlatformCostCalculatorModule platformCostCalculatorModule,
            AdvertiserCostCalculatorModule advertiserCostCalculatorModule,
            DruidFeederModule druidFeederModule
    ) {
        return new ImpressionRecordingModule(
                metricReporterService,
                druidFeederModule,
                adInteractionRecordingModule,
                platformCostCalculatorModule,
                advertiserCostCalculatorModule);
    }

    @Bean
    public CookiesWriterModule cookiesWriterModule() {
        return new CookiesWriterModule(
                metricReporterService);
    }

    @Bean
    public AdservResponseModule adservResponseModule(
            MetricReporterService metricReporterService,
            CreativeCacheService creativeCacheService
    ) {
        return new AdservResponseModule(metricReporterService,
                creativeCacheService);
    }

    @Bean
    public AdServerWiretapModule adServerWiretapModule(
            WiretapCassandraService wiretapCassandraService,
            ConfigService configService
    ) {
        return new AdServerWiretapModule(metricReporterService,
                wiretapCassandraService,
                new LoadPercentageBasedSampler(
                        configService.getAsInt("percentOfImpressionsToBeWiretapped"),
                        "percentOfImpressionsToBeWiretapped"
                ));
    }

    @Bean
    public ClickUrlAppenderModule clickUrlAppenderModule(DiscoveryService discoveryService) {
        return new ClickUrlAppenderModule(metricReporterService, discoveryService);
    }

    //this should be the last module in pipeline
    @Bean
    public PixelMatchingUrlAppenderModule pixelMatchingUrlAppenderModule() {
        AtomicLong numberOfImpressionsDoneForPixelMatchingPerHour = new AtomicLong();
        PixelMatchingUrlAppenderModule pixelMatchingUrlAppenderModule =
                new PixelMatchingUrlAppenderModule (metricReporterService,
                        numberOfImpressionsDoneForPixelMatchingPerHour);
        return pixelMatchingUrlAppenderModule;
    }

    @Bean
    public CreativePalletteCleanerModule creativePalletteCleanerModule() {
        return new CreativePalletteCleanerModule(metricReporterService);
    }

    @Bean
    public ImpressionTrackerAppenderModule impressionTrackerAppenderModule(DiscoveryService discoveryService) {
        return new ImpressionTrackerAppenderModule(metricReporterService, discoveryService);
    }

    @Bean
    public AdServerHealthService adServerHealthService(ConfigService configService,
                                                       DiscoveryService discoveryService) {
        //its set to true, so that adserver doesn't show impressions right away
        //while it has not reloaded data
        AtomicLong numberOfExceptionsInLast10Min = new AtomicLong();
        AtomicLong acceptableNumberOfExceptionsInLastTenMintues = new AtomicLong(
                configService.getAsInt("acceptableNumberOfExceptionsInLastTenMintues"));
        AtomicLong numberOfRealAdServingInLastMinute = new AtomicLong();
        AtomicLong numberOfPSAServingInLastHour = new AtomicLong();
        AdServerHealthService adServerHealthService = new AdServerHealthService(
                metricReporterService,
                discoveryService,
                isImpressionServerHandlerHealthy,
                numberOfExceptionsInLast10Min,
                numberOfRealAdServingInLastMinute,
                numberOfPSAServingInLastHour,
                acceptableNumberOfExceptionsInLastTenMintues,
                isReloadProcessHealthy
        );
        return adServerHealthService;
    }

    @Bean
    public EventLogRetriever eventLogRetriever(
            AeroCacheService<EventLog> realTimeEventLogCacheService
    ) {
        return new EventLogRetriever(metricReporterService,
                realTimeEventLogCacheService);
    }

    @Bean
    public ClickTrackerMainProcessor clickTrackerMainProcessor(
            AeroCacheService<EventLock> eventLockPersistenceService,
            AdServerEventLockModule clickPersistentModule
    ) {
        ClickTrackerMainProcessor clickTrackerMainProcessor = new ClickTrackerMainProcessor ();
        clickTrackerMainProcessor.getModules().add(clickLockModule(eventLockPersistenceService));
        clickTrackerMainProcessor.getModules().add(clickPersistentModule);
        return clickTrackerMainProcessor;
    }

    @Bean
    public ImpressionTrackerMainProcessor impressionTrackerMainProcessor(
            CreativeCacheService creativeCacheService) {

        return new ImpressionTrackerMainProcessor(
                creativeCacheService
        );
    }

    @Bean
    public ImpressionServingProcessor impServMainProcessor(
            CreativeCacheService creativeCacheService,
            AdServerEventLockModule impressionLockModule,
            CookiesReaderModule cookiesReaderModule,
            ClientIdModule clientIdModule,
            AdHistoryModule adHistoryModule,
            ImpressionRecordingModule impressionRecordingModule,
            CookiesWriterModule cookiesWriterModule,
            PixelMatchingUrlAppenderModule pixelMatchingUrlAppenderModule,
            ImpressionTrackerAppenderModule impressionTrackerAppenderModule,
            CreativePalletteCleanerModule creativePalletteCleanerModule,
            ClickUrlAppenderModule clickUrlAppenderModule,
            ConfirmWinCounterModule confirmWinCounterModule,
            AdservResponseModule adservResponseModule,
            AdServerWiretapModule adServerWiretapModule
    ) {

        ImpressionServingProcessor impressionServingProcessor = new ImpressionServingProcessor(creativeCacheService);

        impressionServingProcessor.getModules().add(impressionLockModule);
        impressionServingProcessor.getModules().add(cookiesReaderModule);
        impressionServingProcessor.getModules().add(clientIdModule);
        impressionServingProcessor.getModules().add(adHistoryModule);

        impressionServingProcessor.getModules().add(impressionRecordingModule);
        impressionServingProcessor.getModules().add(cookiesWriterModule);
        impressionServingProcessor.getModules().add(pixelMatchingUrlAppenderModule);
        impressionServingProcessor.getModules().add(impressionTrackerAppenderModule);
        impressionServingProcessor.getModules().add(creativePalletteCleanerModule);

        impressionServingProcessor.getModules().add(clickUrlAppenderModule);
        impressionServingProcessor.getModules().add(confirmWinCounterModule);
        impressionServingProcessor.getModules().add(adservResponseModule);
        impressionServingProcessor.getModules().add(adServerWiretapModule);

        return impressionServingProcessor;
    }

    @Bean
    public ImpressionTrackerHandler impressionTrackerHandler(
            MetricReporterService metricReporterService,
            ImpressionTrackerMainProcessor impressionTrackerMainProcessor,
            EventLogRetriever eventLogRetriever
    ) {
        return new ImpressionTrackerHandler(
                metricReporterService,
                impressionTrackerMainProcessor,
                eventLogRetriever
        );
    }

    @Bean
    public ClickTrackerHandler clickTrackerHandler(
            MetricReporterService metricReporterService,
            ClickTrackerMainProcessor clickTrackerMainProcessor,
            EventLogRetriever eventLogRetriever
    ) {
        return new ClickTrackerHandler(
                metricReporterService,
                clickTrackerMainProcessor,
                eventLogRetriever
        );
    }

    @Bean
    public AdServerStatusService adServStatusHandler(
            ImpressionServingProcessor impressionServingProcessor,
            AdServerHealthService adServerHealthService,
            MetricReporterService metricReporterService
    ) {
        return new AdServerStatusService(
                impressionServingProcessor,
                adServerHealthService,
                metricReporterService);
    }

    @Bean
    public ImpressionServingService impressionServHandler(
            EventLogRetriever eventLogRetriever,
            ImpressionServingProcessor impressionServingProcessor,
            MetricReporterService metricReporterService
    ) {
        return new ImpressionServingService(
                eventLogRetriever,
                impressionServingProcessor,
                numberOfExceptionsInLast10Min,
                numberOfRealAdServingInLastMinute,
                numberOfPSAServingInLastHour,
                metricReporterService
        );
    }

    @Bean
    public AdServerAsyncJobService adServerAsyncJobService(
            ConfigService configService,
            EventBus eventBus,
            MetricReporterService metricReporterService
    ) {
        return new AdServerAsyncJobService(
                numberOfExceptionsInLast10Min,
                configService,
                eventBus,
                metricReporterService,
                interruptedFlag
        );
    }
}

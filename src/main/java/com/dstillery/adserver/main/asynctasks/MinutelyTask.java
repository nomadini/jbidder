package com.dstillery.adserver.main.asynctasks;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.bidding_performance_metric.models.ConfirmedWinRequest;
import com.dstillery.common.util.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.HttpUtil.APPLICATION_JSON;

public class MinutelyTask implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MinutelyTask.class);
    private AtomicLong numberOfRealAdServingInLastMinute;
    private MetricReporterService metricReporterService;
    private ConcurrentHashMap<String, ConfirmedWinRequest> bidderToConfirmedWins;

    public MinutelyTask(
             AtomicLong numberOfRealAdServingInLastMinute,
             MetricReporterService metricReporterService,
             ConcurrentHashMap<String, ConfirmedWinRequest> bidderToConfirmedWins) {

        this.numberOfRealAdServingInLastMinute = numberOfRealAdServingInLastMinute;
        this.metricReporterService = metricReporterService;;
        this.bidderToConfirmedWins = bidderToConfirmedWins;
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0, 1, TimeUnit.HOURS);
    }

    @Override
    public void run() {
        try {

            numberOfRealAdServingInLastMinute.set(0);
            sendConfirmedWinsToBidders();
        } catch(Exception e) {
            //LOGGER.error("exception in run every minute thread";
            metricReporterService.addStateModuleForEntity(
                    "EXCEPTION_IN_RUN_EVERY_MINUTE",
                    "AdServerAsyncJobService",
                    "ALL",
                    MetricReporterService.MetricPriority.EXCEPTION);
        }

    }

    private void sendConfirmedWinsToBidders() {

        for (Entry<String, ConfirmedWinRequest> iter : bidderToConfirmedWins.entrySet()) {
            try {
                String bidderCallbackServletUrl = iter.getKey();
                //LOGGER.error("request to send to bidder for confirmWins : " , iter.getValue().toJson();
                //LOGGER.error("bidderCallbackServletUrl : " , bidderCallbackServletUrl;
                String confirmResponse = HttpUtil.sendPostRequest (
                        bidderCallbackServletUrl,
                        OBJECT_MAPPER.writeValueAsString(iter.getValue()),
                        APPLICATION_JSON,
                        1000,
                        10);
            } catch (Exception e) {
                LOGGER.warn("exception occurred", e);
            }
        }
        bidderToConfirmedWins.clear();
    }
}

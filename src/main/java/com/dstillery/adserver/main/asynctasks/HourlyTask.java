package com.dstillery.adserver.main.asynctasks;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


import com.dstillery.common.metric.dropwizard.MetricReporterService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HourlyTask implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(HourlyTask.class);
    private MetricReporterService metricReporterService;
    private AtomicLong numberOfPSAServingInLastHour;

    public HourlyTask(MetricReporterService metricReporterService,
                      AtomicLong numberOfPSAServingInLastHour) {
        this.numberOfPSAServingInLastHour = numberOfPSAServingInLastHour;
        this.metricReporterService = metricReporterService;;
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0, 1, TimeUnit.HOURS);
    }

    @Override
    public void run() {
        try {
            numberOfPSAServingInLastHour.set(0);
        } catch(Exception e) {
            //LOGGER.error("exception in run every hour thread";
        }
    }
}

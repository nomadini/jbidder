package com.dstillery.adserver.main;

import java.util.Collections;
import java.util.HashSet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.dstillery.common.config.BidderAdServerCacheConfig;
import com.dstillery.common.config.EssentialConfig;
import com.dstillery.common.config.CacheServiceInitializer;
import com.dstillery.common.config.CommonBeansConfig;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
@PropertySources({

        @PropertySource(value = "classpath:adserver.properties"),
        @PropertySource(value = "file://${PROPERTY_FILE:/etc/adserver/adserver.properties}",
                ignoreResourceNotFound = true) })
@Import({
        AdServerEssentialConfig.class,
        EssentialConfig.class,
        CommonBeansConfig.class,
        BidderAdServerCacheConfig.class,
        AdServerConfig.class})
@ComponentScan(basePackages = {
        "com.dstillery.adserver.main",
        "com.dstillery.adserver.requesthandling",
})
public class AdServer {
    public static void main(String[] args) {
        SpringApplication sa = new SpringApplication();
        sa.addListeners(new CacheServiceInitializer());
        sa.setSources(new HashSet<>(Collections.singletonList(AdServer.class.getName())));
        ConfigurableApplicationContext context = sa.run(args);
    }
}

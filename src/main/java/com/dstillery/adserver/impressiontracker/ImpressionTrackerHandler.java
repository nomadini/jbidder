package com.dstillery.adserver.impressiontracker;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dstillery.adserver.modules.AdServContext;
import com.dstillery.adserver.modules.EventLogRetriever;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.HttpUtil;
import com.google.common.collect.ImmutableMap;

public class ImpressionTrackerHandler {

	private ImpressionTrackerMainProcessor impressionTrackerMainProcessor;
	private EventLogRetriever eventLogRetriever;
	private MetricReporterService metricReporterService;

	public ImpressionTrackerHandler(
            MetricReporterService metricReporterService,
	        ImpressionTrackerMainProcessor impressionTrackerMainProcessor,
            EventLogRetriever eventLogRetriever) {
		this.metricReporterService = metricReporterService;
		this.impressionTrackerMainProcessor = impressionTrackerMainProcessor;
		this.eventLogRetriever = eventLogRetriever;
	}

	public void handleRequest(HttpServletRequest request,
                              HttpServletResponse response,
							  AdServContext context) throws IOException {

		//cout("T: Request from " + toStr(request.clientAddress().toString()));
		Map<String, String> mapOfQueryParams = HttpUtil.getMapOfQueryParams(request);
		String transactionId = mapOfQueryParams.get("transactionId");

		eventLogRetriever.retrieveEventLogFromTransactionId(transactionId, context);

		context.setAdInteractionType(EventLog.BidEventType.IMPRESSION_TRACKING);

		//LOGGER.debug("event :  " , context.eventLog.toJson ();

		process(context);
	    response.getWriter().write("");

		metricReporterService.getCounter("impressions_tracked",
				ImmutableMap.of(
						"creative_id", context.getChosenCreative().getId(),
						"targetgroup_id", context.getEventLog().getTargetGroupId(),
						"campaign_id", context.getEventLog().getCampaignId()
				)
		).inc();
	}

	private String process(AdServContext adservRequest) {
		return impressionTrackerMainProcessor.process(adservRequest);
	}
}


package com.dstillery.adserver.impressiontracker;

import java.util.List;

import com.dstillery.adserver.modules.AdServContext;
import com.dstillery.adserver.modules.AdServerModule;
import com.dstillery.common.creative.CreativeCacheService;

public class ImpressionTrackerMainProcessor  {

    List<AdServerModule> modules;

    CreativeCacheService creativeCacheService;

    public ImpressionTrackerMainProcessor(CreativeCacheService creativeCacheService) {
        this.creativeCacheService = creativeCacheService;
    }

    String process(AdServContext context) {
        //LOGGER.debug("chosen Creative is : {} ", context.eventLog.creativeId;
        context.setChosenCreative(creativeCacheService.findByEntityId(
                context.getEventLog().getCreativeId()));

        modules.forEach(module ->{
            //LOGGER.debug("running" , it.getClass().getSimpleName() , " module";
            module.process(context);
        });
        return context.getFinalCreativeContent();
    }
}

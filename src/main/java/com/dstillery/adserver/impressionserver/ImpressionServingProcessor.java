package com.dstillery.adserver.impressionserver;

import com.dstillery.adserver.modules.AdServContext;
import com.dstillery.adserver.modules.AdServerModule;
import com.dstillery.common.creative.CreativeCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ImpressionServingProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(ImpressionServingProcessor.class);

    private List<AdServerModule> modules = new ArrayList<>();

    private CreativeCacheService creativeCacheService;

    public ImpressionServingProcessor(CreativeCacheService creativeCacheService) {
        this.creativeCacheService = creativeCacheService;
    }

    public String process(AdServContext context) {
        LOGGER.debug("chosen Creative is : {} ", context.getEventLog().getCreativeId());
        context.setChosenCreative(creativeCacheService.findByEntityId(
                context.getEventLog().getCreativeId()));

        modules.forEach(module -> {
            LOGGER.trace("running module : {} ", module.getClass().getSimpleName());
            module.process(context);
        });
        return context.getFinalCreativeContent();
    }

    public List<AdServerModule> getModules() {
        return modules;
    }
}

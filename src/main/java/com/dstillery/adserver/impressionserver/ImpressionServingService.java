package com.dstillery.adserver.impressionserver;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.adserver.modules.AdServContext;
import com.dstillery.adserver.modules.EventLogRetriever;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.HttpUtil;
import com.google.common.collect.ImmutableMap;

public class ImpressionServingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ImpressionServingService.class);

    private EventLogRetriever eventLogRetriever;
    private ImpressionServingProcessor impressionServingProcessor;
    private AtomicLong numberOfExceptionsInLast10Min;
    private AtomicLong numberOfRealAdServingInLastMinute;
    private AtomicLong numberOfPSAServingInLastHour;
    private MetricReporterService metricReporterService;

    public ImpressionServingService(EventLogRetriever eventLogRetriever,
                                    ImpressionServingProcessor impressionServingProcessor,
                                    AtomicLong numberOfExceptionsInLast10Min,
                                    AtomicLong numberOfRealAdServingInLastMinute,
                                    AtomicLong numberOfPSAServingInLastHour,
                                    MetricReporterService metricReporterService) {
        this.eventLogRetriever = eventLogRetriever;
        this.impressionServingProcessor = impressionServingProcessor;
        this.numberOfExceptionsInLast10Min = numberOfExceptionsInLast10Min;
        this.numberOfRealAdServingInLastMinute = numberOfRealAdServingInLastMinute;
        this.numberOfPSAServingInLastHour = numberOfPSAServingInLastHour;
        this.metricReporterService = metricReporterService;
    }

    public void handleRequest(HttpServletRequest request,
                              HttpServletResponse response,
                              AdServContext context) throws IOException {

        try {
            //TODO : add a counter to check the difference of numberOfImpressionRequestsProcessed and PROCESS_IMPRESSION_DONE

            process(request, response, context);

            metricReporterService.addStateModuleForEntity("#ImpressionRequestsProcessed",
                    "ImpressionServingService",
                    "ALL",
                    MetricReporterService.MetricPriority.IMPORTANT);

            metricReporterService.getCounter("impressions_served",
                    ImmutableMap.of(
                            "creative_id", context.getChosenCreative().getId(),
                            "targetgroup_id", context.getEventLog().getTargetGroupId(),
                            "campaign_id", context.getEventLog().getCampaignId()
                    )
            ).inc();

        } catch (Exception e) {
            LOGGER.warn("exception", e);
            metricReporterService.addStateModuleForEntity("Exception",
                    "ImpressionServingService",
                    "ALL",
                    MetricReporterService.MetricPriority.EXCEPTION);

            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("");
            //TODO show PSA ad
            numberOfExceptionsInLast10Min.getAndIncrement();
        }
    }

    private void process(HttpServletRequest request,
                         HttpServletResponse response,
                         AdServContext context) throws IOException {

        Map<String, String> mapOfQueryParams = HttpUtil.getMapOfQueryParams(request);
        String transactionId = mapOfQueryParams.get("transactionId");
        String winBidPriceFromReq = mapOfQueryParams.get("won");
        context.setResponse(response);
        eventLogRetriever.retrieveEventLogFromTransactionId(transactionId, context);

        //these values are very important
        context.setRequestURI(request.getRequestURI());
        //these values are very important
        context.setRequestHandlerName("ImpressionServingService");
        //these values are very important
        context.setAdInteractionType(EventLog.BidEventType.IMPRESSION);

        ////LOG_EVERY_N(INFO, 100) , google.COUNTER, "th event :  " , context.eventLog.toJson ();

        context.getEventLog().setWinBidPrice(Double.parseDouble(winBidPriceFromReq));

        LOGGER.debug("context.eventLog.winBidPrice : {}", context.getEventLog().getWinBidPrice());

        boolean valid = requestValidation (context);
        if (!valid) {
            LOGGER.error("bad request coming in,  requestValidation failed.");
            return;
        }
        String completeAdtag;

        metricReporterService.addStateModuleForEntity("SERVING_REAL_AD",
                "ImpressionServingService",
                "ALL");

        numberOfRealAdServingInLastMinute.getAndIncrement();
        context.setFinalAdTag(impressionServingProcessor.process(context));
        incrementPerformanceCounters(context);
    }

    private void incrementPerformanceCounters(AdServContext context) {
        long targetGroupId = context.getEventLog().getTargetGroupId();

        //very important statistic
        metricReporterService.addStateModuleForEntity("PROCESS_IMPRESSION_DONE",
                "ImpressionServingService");

        //there is metric in bidder that is called #TG_XX_Bids that is related to this
        metricReporterService.addStateModuleForEntity("#TG_"+targetGroupId+"_Wins",
                "BidWinTracker");

    }

    private boolean requestValidation(AdServContext context) {
        return true;
    }

    String fetchPSA(AdServContext context) {
        numberOfPSAServingInLastHour.getAndIncrement();
        return "PSA"; //TODO be completed
    }

}


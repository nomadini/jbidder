package com.dstillery.adserver.modules;

import static com.dstillery.common.util.StringUtil.equalsIgnoreCase;

import java.util.concurrent.atomic.AtomicLong;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.StringUtil;

public class PixelMatchingUrlAppenderModule extends AdServerModule {

    MetricReporterService metricReporterService;
    AtomicLong numberOfImpressionsDoneForPixelMatchingPerHour;
    private static String  googlePixelMatchTag =
            "<img src=\"http://cm.g.doubleclick.net/pixel?google_nid=1234&google_cm\" />";
    public PixelMatchingUrlAppenderModule(MetricReporterService metricReporterService,
                                          AtomicLong numberOfImpressionsDoneForPixelMatchingPerHour) {
        this.metricReporterService = metricReporterService;;
        this.numberOfImpressionsDoneForPixelMatchingPerHour = numberOfImpressionsDoneForPixelMatchingPerHour;
    }

    public void process(AdServContext context) {

        if (context.eventLog.isBiddingOnlyForMatchingPixel()) {
            metricReporterService.addStateModuleForEntity("APPEND_PIXEL_MATCHING_URL",
                    "PixelMatchingUrlAppenderModule",
                    context.eventLog.getExchangeName());
            numberOfImpressionsDoneForPixelMatchingPerHour.getAndIncrement();
            buildPixelMatchingUrl(context);

        } else {
            metricReporterService.addStateModuleForEntity("SKIPPING_PIXEL_MATCHING_URL", "PixelMatchingUrlAppenderModule",
                    context.eventLog.getExchangeName());
        }
    }

    private void buildPixelMatchingUrl(AdServContext context) {
        //this is really dependent on the exchanges , so for every exchange we should do it differently
        if (equalsIgnoreCase("google", context.eventLog.getExchangeName())) {
            context.creativePalette = StringUtil.replaceString(context.creativePalette,
                    "____NOMADINI_PIXEL_MATCHING_URL____",
                    googlePixelMatchTag);
        }

    }
}


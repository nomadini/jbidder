package com.dstillery.adserver.modules;

import static com.dstillery.common.util.Constants.OBJECT_MAPPER;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.modules.OpportunityContext;
import com.dstillery.common.kafka.KafkaProducerService;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.TargetGroup;
import com.dstillery.common.util.GUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

public class DruidFeederModule extends AdServerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(DruidFeederModule.class);

    private final KafkaProducerService kafkaProducerService;
    private final MetricReporterService metricReporterService;

    public DruidFeederModule(MetricReporterService metricReporterService,
                             KafkaProducerService kafkaProducerService) {
        this.metricReporterService = metricReporterService;
        this.kafkaProducerService = kafkaProducerService;
    }

    public void process(AdServContext context) {
        EventLog eventLog = context.getEventLog();
        try {
            kafkaProducerService.send(eventLog.getDevice().getDeviceId(),
                    OBJECT_MAPPER.writeValueAsString(eventLog),
                    false);
            metricReporterService.getCounter("druid_messages_written").inc();
            if (GUtil.allowedToCall(1000)) {
                LOGGER.info("msg sent to druid : {}", eventLog);
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean filterTargetGroup(TargetGroup tg, OpportunityContext context) {
        throw new RuntimeException("should not be used");
    }
}


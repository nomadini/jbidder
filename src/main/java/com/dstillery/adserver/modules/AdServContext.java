package com.dstillery.adserver.modules;

import com.dstillery.common.creative.Creative;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.eventlog.EventLog;
import com.dstillery.common.metric.ProcessingStatus;
import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.common.util.StringUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static com.dstillery.common.util.StringUtil.toStr;

/**
 bidder context has all the eventLog info that comes from a bid,
 it also has impression specific fields.
 */
public class AdServContext  {
    /**
     this has all the information regarding the bid that we are showing impression for.
     */
    EventLog eventLog;
    Creative chosenCreative;
    String finalCreativeContent;
    String wrappedCreativeWithClickUrl;
    EventLog.BidEventType adInteractionType;

    NomadiniDevice device;
    String creativePalette;

    String requestHandlerName;
    String requestURI;
    String lastModuleRunInPipeline;

    private Map<String, String> cookieMapIncoming = new HashMap<>();   // this is what will be written in response. it includes all the incoming cookies
    private Map<String, String> cookieMapOutgoing = new HashMap<>();

    ProcessingStatus status;
    private HttpServletResponse response;
    private HttpServletRequest request;
    private String finalAdTag;

    public AdServContext(HttpServletRequest request,
                         HttpServletResponse response) {
        this.request = request;
        this.response = response;
        creativePalette =
                "   ____NOMADINI_AD_TAG____     " +
                        "   ____NOMADINI_IMPRESSION_TRACKER____     " +
                        "   ____NOMADINI_PIXEL_MATCHING_URL____     ";
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    static String createRandomTransactionId() {
        return StringUtil.randomString(12) + toStr("_")
                + toStr(DateTimeUtil.getNowInMilliSecond());
    }

    public EventLog getEventLog() {
        return eventLog;
    }

    public void setEventLog(EventLog eventLog) {
        this.eventLog = eventLog;
    }

    public Creative getChosenCreative() {
        return chosenCreative;
    }

    public void setChosenCreative(Creative chosenCreative) {
        this.chosenCreative = chosenCreative;
    }

    public String getFinalCreativeContent() {
        return finalCreativeContent;
    }

    public void setFinalCreativeContent(String finalCreativeContent) {
        this.finalCreativeContent = finalCreativeContent;
    }

    public String getWrappedCreativeWithClickUrl() {
        return wrappedCreativeWithClickUrl;
    }

    public void setWrappedCreativeWithClickUrl(String wrappedCreativeWithClickUrl) {
        this.wrappedCreativeWithClickUrl = wrappedCreativeWithClickUrl;
    }

    public Map<String, String> getCookieMapIncoming() {
        return cookieMapIncoming;
    }

    public void setCookieMapIncoming(Map<String, String> cookieMapIncoming) {
        this.cookieMapIncoming = cookieMapIncoming;
    }

    public Map<String, String> getCookieMapOutgoing() {
        return cookieMapOutgoing;
    }

    public void setCookieMapOutgoing(Map<String, String> cookieMapOutgoing) {
        this.cookieMapOutgoing = cookieMapOutgoing;
    }

    public EventLog.BidEventType getAdInteractionType() {
        return adInteractionType;
    }

    public void setAdInteractionType(EventLog.BidEventType adInteractionType) {
        this.adInteractionType = adInteractionType;
    }

    public NomadiniDevice getDevice() {
        return device;
    }

    public void setDevice(NomadiniDevice device) {
        this.device = device;
    }

    public String getCreativePalette() {
        return creativePalette;
    }

    public void setCreativePalette(String creativePalette) {
        this.creativePalette = creativePalette;
    }

    public String getRequestHandlerName() {
        return requestHandlerName;
    }

    public void setRequestHandlerName(String requestHandlerName) {
        this.requestHandlerName = requestHandlerName;
    }

    public String getRequestURI() {
        return requestURI;
    }

    public void setRequestURI(String requestURI) {
        this.requestURI = requestURI;
    }

    public String getLastModuleRunInPipeline() {
        return lastModuleRunInPipeline;
    }

    public void setLastModuleRunInPipeline(String lastModuleRunInPipeline) {
        this.lastModuleRunInPipeline = lastModuleRunInPipeline;
    }

    public ProcessingStatus getStatus() {
        return status;
    }

    public void setStatus(ProcessingStatus status) {
        this.status = status;
    }

    public void setFinalAdTag(String finalAdTag) {
        this.finalAdTag = finalAdTag;
    }

    public String getFinalAdTag() {
        return finalAdTag;
    }
}


package com.dstillery.adserver.modules;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.HttpUtil;


public class CookiesWriterModule extends AdServerModule {

	MetricReporterService metricReporterService;

	public CookiesWriterModule(MetricReporterService metricReporterService) {
		this.metricReporterService = metricReporterService;;
	}

	public void process(AdServContext context) {
		writeOutGoingCookies(context);
	}

	private void writeOutGoingCookies(AdServContext context) {
		context.getCookieMapOutgoing().forEach((key, value) ->
                HttpUtil.addCookieToResponse(key, value, context.getResponse()));
	}
}


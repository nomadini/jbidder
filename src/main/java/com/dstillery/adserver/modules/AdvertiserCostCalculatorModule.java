package com.dstillery.adserver.modules;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

import com.dstillery.common.campaign.AdvertiserImpBasedFixedCostEntry;
import com.dstillery.common.campaign.AdvertiserImpBasedPercentageCostEntry;
import com.dstillery.common.campaign.Campaign;
import com.dstillery.common.campaign.CampaignCacheService;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class AdvertiserCostCalculatorModule  {

    private MetricReporterService metricReporterService;
    private CampaignCacheService campaignCacheService;

    public AdvertiserCostCalculatorModule(MetricReporterService metricReporterService,
                                          CampaignCacheService campaignCacheService) {
        this.metricReporterService = metricReporterService;;
        this.campaignCacheService = campaignCacheService;
    }

    void calculateCost(EventLog eventLog) {
        Campaign campaign = campaignCacheService.findByEntityId(eventLog.getCampaignId());
        long allExtraPercCosts = 0;
        for (AdvertiserImpBasedPercentageCostEntry advertiserImpBasedPercentageCostEntry :
                campaign.getAdvCostModel().getAdvertiserImpBasedPercentageCost().getCosts()) {
            assertAndThrow(advertiserImpBasedPercentageCostEntry.getPercentageValue() >=0);
            assertAndThrow(advertiserImpBasedPercentageCostEntry.getPercentageValue() <=100);
            double extraCost = advertiserImpBasedPercentageCostEntry.getPercentageValue()
                    * (eventLog.getPlatformCost() / 100);
            allExtraPercCosts += extraCost;
        }

        long allFixedPercCosts = 0;
        for (AdvertiserImpBasedFixedCostEntry advertiserImpBasedFixedCostEntry :
                campaign.getAdvCostModel().getAdvertiserImpBasedFixedCost().getCosts()) {
            allFixedPercCosts += advertiserImpBasedFixedCostEntry.getFixedValue();
        }

        eventLog.setAdvertiserCost(allFixedPercCosts + allExtraPercCosts + eventLog.getPlatformCost());

    }
}


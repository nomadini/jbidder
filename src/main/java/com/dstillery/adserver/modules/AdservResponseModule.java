package com.dstillery.adserver.modules;

import com.dstillery.common.creative.CreativeCacheService;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.StringUtil;

public class AdservResponseModule extends AdServerModule {

    public AdservResponseModule(
            MetricReporterService metricReporterService,
            CreativeCacheService creativeCacheService) {
    }

    public void process(AdServContext context) {
        context.finalCreativeContent = prepareCreativeAdTag(context);
    }

    private String prepareCreativeAdTag(AdServContext context) {

        String creativePalleteWithCreative = StringUtil.replaceString(context.creativePalette,
                "____NOMADINI_AD_TAG____",
                context.wrappedCreativeWithClickUrl);

        if (StringUtil.contains(creativePalleteWithCreative, "____NOMADINI")) {
            throw new RuntimeException("some macros are still left in creative pallete");
        }

        return creativePalleteWithCreative;
    }
}

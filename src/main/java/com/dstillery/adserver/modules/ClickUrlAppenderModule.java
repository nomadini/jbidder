package com.dstillery.adserver.modules;

import static com.dstillery.common.util.StringUtil.replaceString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.adserver.main.ApplicationContext;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.util.RandomUtil;
import com.dstillery.common.util.StringUtil;

public class ClickUrlAppenderModule extends AdServerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClickUrlAppenderModule.class);

    private static RandomUtil randomizer = new RandomUtil (100000);
    private DiscoveryService discoveryService;
    private MetricReporterService metricReporterService;

    public ClickUrlAppenderModule(
            MetricReporterService metricReporterService,
            DiscoveryService discoveryService) {
        this.metricReporterService = metricReporterService;;
        this.discoveryService = discoveryService;

    }

    private RandomUtil getRandomizer() {
        return randomizer;
    }

    public void process(AdServContext context) {

        String clickUrl = buildClickUrl(context);

        context.wrappedCreativeWithClickUrl =
                replaceString(context.chosenCreative.getAdTag(),
                        "${CLICKURL}",
                        clickUrl,
                        false);
        LOGGER.debug("context.wrappedCreativeWithClickUrl : {} ",  context.wrappedCreativeWithClickUrl);
    }

    private String buildClickUrl(AdServContext context) {
        String clickUrl =
                discoveryService.discoverService("adserver") +
                        "/queue0-clk?transactionId=__TRN_ID__&CACHE_BUSTER=__RND__&ctrack=";
        clickUrl = StringUtil.replaceString(clickUrl,
                ApplicationContext.getTransactionIdConstant(),
                context.eventLog.getEventId());
        clickUrl = StringUtil.replaceString(clickUrl, "__RND__", getRandomizer().randomPositiveNumberV2().toString());
        return clickUrl;
    }

}

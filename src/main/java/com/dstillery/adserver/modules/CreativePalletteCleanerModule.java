package com.dstillery.adserver.modules;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.StringUtil;

public class CreativePalletteCleanerModule extends AdServerModule {

	MetricReporterService metricReporterService;

	public CreativePalletteCleanerModule(MetricReporterService metricReporterService) {
		this.metricReporterService = metricReporterService;;
	}

	public void process(AdServContext context) {

		if (!context.eventLog.isBiddingOnlyForMatchingPixel()) {
			metricReporterService.addStateModuleForEntity("ERASE_PIXEL_MATCHING_MACRO",
					"CreativePalletteCleanerModule", context.eventLog.getExchangeName());
			context.creativePalette =
					StringUtil.replaceString(context.creativePalette, "____NOMADINI_PIXEL_MATCHING_URL____", "");
		}
	}
}


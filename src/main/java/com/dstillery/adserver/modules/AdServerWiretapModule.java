package com.dstillery.adserver.modules;/*
  TargetGroupDailyCapsFilter.h
 *
   Created on: Sep 4, 2015
       Author: mtaabodi
 */

import com.dstillery.common.LoadPercentageBasedSampler;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.wiretap.Wiretap;
import com.dstillery.common.wiretap.WiretapCassandraService;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

public class AdServerWiretapModule extends AdServerModule {
    private WiretapCassandraService wiretapCassandraService;
    private LoadPercentageBasedSampler wiretapImpressionServeResponseSampler;
    private MetricReporterService metricReporterService;

    public AdServerWiretapModule(
            MetricReporterService metricReporterService,
            WiretapCassandraService wiretapCassandraService,
            LoadPercentageBasedSampler wiretapImpressionServeResponseSampler)  {
        this.metricReporterService = metricReporterService;;
        this.wiretapCassandraService = wiretapCassandraService;
        this.wiretapImpressionServeResponseSampler = wiretapImpressionServeResponseSampler;

    }

    public void process(AdServContext context) {

        if (!wiretapImpressionServeResponseSampler.isPartOfSample()) {
            return;
        }

        Wiretap wiretap = new Wiretap();
        wiretap.setEventId(context.eventLog.getEventId());
        wiretap.setAppName("AdServer");
        // String appVersion;
        // String appHost;
        assertAndThrow(!context.requestHandlerName.isEmpty());
        assertAndThrow(!context.requestURI.isEmpty());
        wiretap.setModuleName(context.requestHandlerName);
        wiretap.setRequest(context.requestURI);
        wiretap.setResponse(context.finalCreativeContent);
        wiretapCassandraService.record(wiretap);

    }
}


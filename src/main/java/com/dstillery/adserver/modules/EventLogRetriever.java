package com.dstillery.adserver.modules;

import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

import java.io.IOException;
import java.util.Optional;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventLogRetriever  {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventLogRetriever.class);
    private MetricReporterService metricReporterService;
    private AeroCacheService<EventLog> realTimeEventLogCacheService;

    public EventLogRetriever(MetricReporterService metricReporterService,
                             AeroCacheService<EventLog> realTimeEventLogCacheService) {
        this.metricReporterService = metricReporterService;;
        this.realTimeEventLogCacheService = realTimeEventLogCacheService;
    }

    public void retrieveEventLogFromTransactionId(
            String transactionId,
            AdServContext context) {
        if (transactionId.isEmpty()) {
            metricReporterService.addStateModuleForEntity(
                    "EMPTY_TRANSACTION_ID",
                    "ImpressionServingService",
                    "ALL",
                    MetricReporterService.MetricPriority.EXCEPTION);
            throw new RuntimeException("bad request, eventLog is not passed");
        }

        EventLog inputEventLog = new EventLog(
                transactionId,
                EventLog.BidEventType.IMPRESSION,
                EventLog.TARGETING_TYPE_IGNORE_FOR_NOW,
                DeviceClassValue.UNKNOWN,
                DeviceTypeValue.UNKNOWN);
        Optional<EventLog> event = realTimeEventLogCacheService.readDataOptionalAsObject(inputEventLog);
        if (!event.isPresent()) {
            metricReporterService.addStateModuleForEntity(
                    "EVENT_LOG_NOT_FOUND",
                    "EventLogRetriever",
                    "ALL",
                    MetricReporterService.MetricPriority.EXCEPTION);
            throw new RuntimeException("eventLog is not found : eventLog Id : " + transactionId);
        }
        metricReporterService.addStateModuleForEntity(
                "EVENT_LOG_FOUND",
                "EventLogRetriever",
                "ALL");
        context.eventLog = event.get();
        LOGGER.debug("eventLog retrieved : {}", context.eventLog);
    }

}


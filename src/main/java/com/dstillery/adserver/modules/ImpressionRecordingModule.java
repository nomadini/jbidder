package com.dstillery.adserver.modules;

import com.dstillery.common.eventlog.AdInteractionRecordingModule;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class ImpressionRecordingModule extends AdServerModule {

    private PlatformCostCalculatorModule platformCostCalculatorModule;
    private AdvertiserCostCalculatorModule advertiserCostCalculatorModule;
    private DruidFeederModule druidFeederModule;

    private MetricReporterService metricReporterService;
    private AdInteractionRecordingModule adInteractionRecordingModule;

    public ImpressionRecordingModule(
            MetricReporterService metricReporterService,
            DruidFeederModule druidFeederModule,
            AdInteractionRecordingModule adInteractionRecordingModule,
            PlatformCostCalculatorModule platformCostCalculatorModule,
            AdvertiserCostCalculatorModule advertiserCostCalculatorModule) {
        this.metricReporterService = metricReporterService;;
        this.druidFeederModule = druidFeederModule;
        this.adInteractionRecordingModule = adInteractionRecordingModule;
        this.platformCostCalculatorModule = platformCostCalculatorModule;
        this.advertiserCostCalculatorModule = advertiserCostCalculatorModule;
    }

    public void process(AdServContext context) {

        EventLog eventLog = context.eventLog;
        eventLog.setEventType(EventLog.BidEventType.IMPRESSION);
        eventLog.setSourceEventId(eventLog.getEventId());
        eventLog.setEventId(EventLog.createRandomTransactionId());

        platformCostCalculatorModule.calculateCost (eventLog);
        advertiserCostCalculatorModule.calculateCost (eventLog);
        adInteractionRecordingModule.enqueueEventForPersistence(eventLog);
        druidFeederModule.process(context);
    }
}


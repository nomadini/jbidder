package com.dstillery.adserver.modules;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.targetgroup.bidding_performance_metric.models.ConfirmedWinRequest;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class ConfirmWinCounterModule extends AdServerModule {
    private MetricReporterService metricReporterService;
    private ConcurrentHashMap<String, ConfirmedWinRequest> bidderToConfirmedWins;

    public ConfirmWinCounterModule(
            MetricReporterService metricReporterService,
            ConcurrentHashMap<String, ConfirmedWinRequest> bidderToConfirmedWins) {
        this.metricReporterService = metricReporterService;;
        this.bidderToConfirmedWins = bidderToConfirmedWins;

    }

    public void process(AdServContext context) {

        ConfirmedWinRequest requestPayload = bidderToConfirmedWins.
                computeIfAbsent(context.eventLog.bidderCallbackServletUrl,
                        key -> new ConfirmedWinRequest());
        AtomicLong numberOfWins = requestPayload.getTargetGroupIdWins().computeIfAbsent(
                context.eventLog.getTargetGroupId(), key -> new AtomicLong());
        //TODO : fix this later
        numberOfWins.getAndIncrement();
        ////LOG_EVERY_N(ERROR, 100) , "confirmation : tgId " ,context.eventLog.targetGroupId ,"numberOfWins : {} ", numberOfWins.getValue();
    }
}

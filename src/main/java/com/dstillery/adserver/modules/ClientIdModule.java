package com.dstillery.adserver.modules;

import static com.dstillery.actionrecorder.app.PixelMatchingController.NOMADINI_DEVICE_ID;

import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class ClientIdModule extends AdServerModule {

    private MetricReporterService metricReporterService;

    public ClientIdModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;;
    }

    public void process(AdServContext context) {

        getClientId(context);
    }

    void getClientId(AdServContext context) {

        if (context.getCookieMapIncoming().containsKey(NOMADINI_DEVICE_ID)) {
            context.setDevice(new NomadiniDevice(
                    context.getCookieMapIncoming().get(NOMADINI_DEVICE_ID),
                    DeviceTypeValue.DISPLAY,
                    DeviceClassValue.DESKTOP
            ));
            metricReporterService.addStateModuleForEntity(
                    "KNOWN_VISITOR_SEEN",
                    "ClientIdModule",
                    "ALL");

        } else  {
            String nomadiniDeviceId = NomadiniDevice.createStandardDeviceId();
            context.getCookieMapOutgoing().put(NOMADINI_DEVICE_ID, nomadiniDeviceId);
            context.setDevice(new NomadiniDevice(nomadiniDeviceId,  DeviceTypeValue.DISPLAY, DeviceClassValue.DESKTOP));
            metricReporterService.addStateModuleForEntity(
                    "NEW_VISITOR_SEEN",
                    "ClientIdModule",
                    "ALL");
        }
    }
}


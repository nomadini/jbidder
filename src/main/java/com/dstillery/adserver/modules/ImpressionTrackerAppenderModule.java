package com.dstillery.adserver.modules;

import com.dstillery.adserver.main.ApplicationContext;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.common.util.RandomUtil;
import com.dstillery.common.util.StringUtil;

public class ImpressionTrackerAppenderModule extends AdServerModule {

    private DiscoveryService discoveryService;
    private MetricReporterService metricReporterService;

    public ImpressionTrackerAppenderModule(MetricReporterService metricReporterService,
                                           DiscoveryService discoveryService) {
        this.metricReporterService = metricReporterService;;
        this.discoveryService = discoveryService;
    }


    public void process(AdServContext context) {
        String imptrackerUrl =
                "<img src=\"IMP_TRAKER_SERVER\" border=\"0\" width=\"1\" height=\"1\">";

        imptrackerUrl = StringUtil.replaceString(imptrackerUrl,
                "IMP_TRAKER_SERVER",
                discoveryService.discoverService("adserver") +"/queue0-imptrk?transactionId=__TRN_ID__&CACHE_BUSTER=__RND__");

        imptrackerUrl =
                StringUtil.replaceString(imptrackerUrl,
                        ApplicationContext.getTransactionIdConstant(), context.eventLog.getEventId());

        imptrackerUrl = StringUtil.replaceString(imptrackerUrl, "__RND__", RandomUtil.sudoRandomNumber(1000000).intValue() + "");

        context.creativePalette = StringUtil.replaceString(context.creativePalette,
                "____NOMADINI_IMPRESSION_TRACKER____",
                imptrackerUrl);
    }
}


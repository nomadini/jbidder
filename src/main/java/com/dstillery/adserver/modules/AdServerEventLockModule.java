package com.dstillery.adserver.modules;

import com.dstillery.common.EventLock;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.google.common.collect.ImmutableList;

import java.util.Optional;

public class AdServerEventLockModule extends AdServerModule {

    EventLog.BidEventType eventType;
    AeroCacheService<EventLock> eventLockPersistenceService;

    MetricReporterService metricReporterService;

    public AdServerEventLockModule(
            MetricReporterService metricReporterService,
            EventLog.BidEventType eventType,
            AeroCacheService<EventLock> eventLockPersistenceService) {
        this.metricReporterService = metricReporterService;;
        this.eventType = eventType;
        this.eventLockPersistenceService = eventLockPersistenceService;

    }

    public void process(AdServContext context) {

        EventLock eventLockOriginal = new EventLock(context.getEventLog().getEventId(), eventType);

        Optional<EventLock> eventLock = eventLockPersistenceService.readDataOptionalAsObject(eventLockOriginal);
        if (!eventLock.isPresent()) {
            //event is locked
            eventLockPersistenceService.putDataInCache(eventLockOriginal);
            metricReporterService.addStateModuleForEntity(
                    "Locking_Event_" + eventType,
                    "AdServerEventLockModule",
                    "ALL");

        } else {
            //event is already locked
            metricReporterService.addStateModuleForEntity(
                    "Repeated_Event_" + eventType,
                    "AdServerEventLockModule",
                    "ALL");
            if (eventType == EventLog.BidEventType.IMPRESSION) {
                context.getEventLog().setScrubType(EventLog.SCRUB_TYPE_REPEATED_IMPRESSION);
            } else if (eventType == EventLog.BidEventType.CLICK) {
                context.getEventLog().setScrubType(EventLog.SCRUB_TYPE_REPEATED_CLICK);
            } else {
                throw new RuntimeException("unknown eventType "+ eventType);
            }

        }

    }

}


package com.dstillery.adserver.modules;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

public class CookiesReaderModule extends AdServerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CookiesReaderModule.class);

    MetricReporterService metricReporterService;

    public CookiesReaderModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;;
    }

    public void process(AdServContext context) {
        readIncomingCookies(context);
    }
    /**
     this module reads all the cookies and put it in adservContext , thats all it does
     */
    private void readIncomingCookies(AdServContext context) {
        assertAndThrow(context.getRequest() != null);
        context.setCookieMapIncoming(HttpUtil.getCookiesFromRequest(context.getRequest()));
        LOGGER.debug("context.cookieMapIncoming.size() : {}", context.getCookieMapIncoming().size());
    }
}


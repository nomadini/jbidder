package com.dstillery.adserver.modules;

import com.dstillery.common.adhistory.AdEntry;
import com.dstillery.common.adhistory.AdHistoryCassandraService;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

public class AdHistoryModule extends AdServerModule {
    private MetricReporterService metricReporterService;
    private AdHistoryCassandraService adHistoryCassandraService;

    public AdHistoryModule(MetricReporterService metricReporterService,
                           AdHistoryCassandraService adHistoryCassandraService) {
        this.metricReporterService = metricReporterService;;
        this.adHistoryCassandraService = adHistoryCassandraService;
    }

    public void process(AdServContext context) {
        writeImpressionToDb(context);
    }

    private void writeImpressionToDb(AdServContext context) {

        assertAndThrow(context.eventLog.getCreativeId() > 0);
        assertAndThrow(context.eventLog.getTargetGroupId() > 0 );

        // assertAndThrow(context.eventLog.publisherId> 0); // TODO : send this later

        //TODO : do we need to save the eventLog as an impression event at the end of ImpressionHandler pipeline
        AdEntry adEntry = new AdEntry(
                context.eventLog.getEventId(),
                context.adInteractionType);

        adEntry.setCreativeId(context.eventLog.getCreativeId());
        adEntry.setTargetGroupId(context.eventLog.getTargetGroupId());
        adEntry.setPublisherId(context.eventLog.getPublisherId());
        assertAndThrow(!context.eventLog.getDevice().getDeviceId().isEmpty());
        assertAndThrow(context.eventLog.getDevice().getDeviceType() != null);
        adHistoryCassandraService.saveAdInteraction(context.eventLog.getDevice(), adEntry);
    }
}


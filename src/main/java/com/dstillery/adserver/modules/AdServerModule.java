package com.dstillery.adserver.modules;

public abstract class AdServerModule  {
  public abstract void process(AdServContext context);
}


package com.dstillery.adserver.clicktracker;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dstillery.adserver.modules.AdServContext;
import com.dstillery.adserver.modules.EventLogRetriever;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.HttpUtil;
import com.google.common.collect.ImmutableMap;

public class ClickTrackerHandler {
    private ClickTrackerMainProcessor clickTrackerMainProcessor;
    private EventLogRetriever eventLogRetriever;
    private MetricReporterService metricReporterService;

    public ClickTrackerHandler(MetricReporterService metricReporterService,
                               ClickTrackerMainProcessor clickTrackerMainProcessor,
                               EventLogRetriever eventLogRetriever) {
        this.metricReporterService = metricReporterService;;
        this.eventLogRetriever = eventLogRetriever;
        this.clickTrackerMainProcessor = clickTrackerMainProcessor;
    }

    public void handleRequest(HttpServletRequest request,
                              HttpServletResponse response, AdServContext context) throws IOException {

        context.setResponse(response);

        Map<String, String> mapOfQueryParams = HttpUtil.getMapOfQueryParams(request);
        String transactionId = mapOfQueryParams.get("transactionId");
        String ctrackUrl = mapOfQueryParams.get("ctrack");

        eventLogRetriever.retrieveEventLogFromTransactionId(transactionId, context);

        //LOGGER.debug("event :  " , context.eventLog.toJson ();
        process(context);

        response.sendRedirect(ctrackUrl);
        metricReporterService.getCounter("clicks_tracked",
                ImmutableMap.of(
                        "creative_id", context.getChosenCreative().getId(),
                        "targetgroup_id", context.getEventLog().getTargetGroupId(),
                        "campaign_id", context.getEventLog().getCampaignId()
                )
        ).inc();
    }

    private String process(AdServContext context) {
        return clickTrackerMainProcessor.process(context);
    }
}


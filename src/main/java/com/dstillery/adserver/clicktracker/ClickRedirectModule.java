package com.dstillery.adserver.clicktracker;

import com.dstillery.adserver.modules.AdServContext;
import com.dstillery.adserver.modules.AdServerModule;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class ClickRedirectModule extends AdServerModule {

        private MetricReporterService metricReporterService;

        public ClickRedirectModule(MetricReporterService metricReporterService){
                this.metricReporterService = metricReporterService;
        }

        String getName(){
                return"ClickRedirectModule";
        }

        public void process(AdServContext context) {
                //doesnt do anything now, but keep it for now
        }
}


package com.dstillery.adserver.clicktracker;

import java.util.ArrayList;
import java.util.List;

import com.dstillery.adserver.modules.AdServContext;
import com.dstillery.adserver.modules.AdServerModule;

public class ClickTrackerMainProcessor {

    private List<AdServerModule> modules = new ArrayList<>();

    public String process(AdServContext context) {

        modules.forEach(module -> {
            //LOGGER.debug("running" , it.getClass().getSimpleName() , " module";
            module.process(context);
        });
        return context.getFinalCreativeContent();
    }

    public List<AdServerModule> getModules() {
        return modules;
    }
}

package com.dstillery.adserver.clicktracker;

import com.dstillery.adserver.modules.AdHistoryModule;
import com.dstillery.adserver.modules.AdServContext;
import com.dstillery.adserver.modules.AdServerModule;
import com.dstillery.common.eventlog.AdInteractionRecordingModule;
import com.dstillery.common.eventlog.EventLog;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class ClickPersistentModule extends AdServerModule {
    private MetricReporterService metricReporterService;
    private AdInteractionRecordingModule adInteractionRecordingModule;
    private AdHistoryModule adHistoryModule;

    public ClickPersistentModule(MetricReporterService metricReporterService,
                                 AdInteractionRecordingModule adInteractionRecordingModule,
                                 AdHistoryModule adHistoryModule) {
        this.metricReporterService = metricReporterService;;
        this.adInteractionRecordingModule = adInteractionRecordingModule;
        this.adHistoryModule = adHistoryModule;

    }

    public void process(AdServContext context) {
        context.setAdInteractionType(EventLog.BidEventType.CLICK);
        adHistoryModule.process(context);

        EventLog eventLog = context.getEventLog();
        eventLog.setEventType(EventLog.BidEventType.CLICK);
        eventLog.setSourceEventId(eventLog.getEventId());
        adInteractionRecordingModule.enqueueEventForPersistence(eventLog);
    }
}


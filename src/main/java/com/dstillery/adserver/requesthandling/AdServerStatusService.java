package com.dstillery.adserver.requesthandling;

import static com.dstillery.common.adserver.AppStatusRequest.AdServerStatus.GREEN;
import static com.dstillery.common.adserver.AppStatusRequest.AdServerStatus.RED;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dstillery.adserver.impressionserver.ImpressionServingProcessor;
import com.dstillery.adserver.main.AdServerHealthService;
import com.dstillery.adserver.modules.AdServContext;
import com.dstillery.common.adserver.AppStatusRequest;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class AdServerStatusService {

    private ImpressionServingProcessor impressionServingProcessor;
    private AdServerHealthService adServerHealthService;
    private MetricReporterService metricReporterService;

    public AdServerStatusService(ImpressionServingProcessor impressionServingProcessor,
                                 AdServerHealthService adServerHealthService,
                                 MetricReporterService metricReporterService) {
        this.impressionServingProcessor = impressionServingProcessor;
        this.adServerHealthService = adServerHealthService;
        this.metricReporterService = metricReporterService;;
    }

    public void handleRequest(HttpServletRequest request,
                              HttpServletResponse response) throws IOException {

        //LOGGER.debug("processing status request from bidder ";

        AppStatusRequest adserverStatusResponse = determineStatus();
        //TODO : if the exception happens for a campaingId or targetGroupId , send it to bidder
        //so bidder stop using that campaignId or targetGroupId

        response.getWriter().write(
                OBJECT_MAPPER.writeValueAsString(adserverStatusResponse));
        ////LOG_EVERY_N(INFO, 10) , "sending status of adserver back to bidder : " , statsAsStr;
        return;
    }

    String process(AdServContext adservRequest) {
        return impressionServingProcessor.process(adservRequest);
    }

    private AppStatusRequest determineStatus() {
        AppStatusRequest adserverStatusResponse = new AppStatusRequest ();

        adserverStatusResponse.setStatus(GREEN);
        adserverStatusResponse.setReason("ALL_GOOD");

        if (!adServerHealthService.isAdServerHealthy()) {
            adserverStatusResponse.setStatus(RED);
            adserverStatusResponse.setReason("UNHEALTHY_IMPRESSION_HANLDER");
            metricReporterService.addStateModuleForEntity("adserverIsNotHealthy",
                    "ImpressionServingService",
                    "ALL");
        }

        return adserverStatusResponse;
    }

}


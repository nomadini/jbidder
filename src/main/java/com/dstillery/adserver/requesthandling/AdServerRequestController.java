package com.dstillery.adserver.requesthandling;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dstillery.adserver.clicktracker.ClickTrackerHandler;
import com.dstillery.adserver.impressionserver.ImpressionServingService;
import com.dstillery.adserver.impressiontracker.ImpressionTrackerHandler;
import com.dstillery.adserver.modules.AdServContext;

import com.dstillery.common.metric.dropwizard.MetricReporterService;

@RestController
public class AdServerRequestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdServerRequestController.class);

    private ImpressionTrackerHandler impressionTrackerHandler;
    private MetricReporterService metricReporterService;
    private ImpressionServingService impressionServingService;
    private AdServerStatusService adServerStatusService;
    private ClickTrackerHandler clickTrackerHandler;

    public AdServerRequestController(MetricReporterService metricReporterService,
                                     ImpressionServingService impressionServingService,
                                     AdServerStatusService adServerStatusService,
                                     ClickTrackerHandler clickTrackerHandler,
                                     ImpressionTrackerHandler impressionTrackerHandler) {
        this.metricReporterService = metricReporterService;;
        this.impressionServingService = impressionServingService;
        this.adServerStatusService = adServerStatusService;
        this.clickTrackerHandler = clickTrackerHandler;
        this.impressionTrackerHandler = impressionTrackerHandler;
    }

    @RequestMapping(value = "status", method = RequestMethod.GET)
    public void status(HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        adServerStatusService.handleRequest(request, response);
    }

    @RequestMapping(value = "queue0-clk", method = RequestMethod.GET)
    public void clickRequest(HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        AdServContext context = new AdServContext(request, response);
        clickTrackerHandler.handleRequest(request, response, context);
    }

    @RequestMapping(value = "queue0-imptrk", method = RequestMethod.GET)
    public void impTrackRequest(HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        AdServContext context = new AdServContext(request, response);
        impressionTrackerHandler.handleRequest(request, response, context);
    }

    @RequestMapping(value = "win", method = RequestMethod.GET)
    public String winRequest(HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        AdServContext context = new AdServContext(request, response);
        impressionServingService.handleRequest(request, response, context);
        LOGGER.info("response with adtag : {}" , context.getFinalAdTag());
        return context.getFinalAdTag();
    }

}


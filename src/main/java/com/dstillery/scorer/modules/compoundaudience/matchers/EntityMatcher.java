package com.dstillery.scorer.modules.compoundaudience.matchers;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;

import com.dstillery.common.audience.models.AudienceEntity;
import com.dstillery.common.audience.models.EntityType;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.scorer.main.ScorerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityMatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityMatcher.class);

    private DeviceClassMatcher deviceClassMatcher;
    private DeviceTypeMatcher deviceTypeMatcher;
    private EnvironmentTypeMatcher environmentTypeMatcher;
    private InterestMatcher interestMatcher;
    private FirstPartySegmentMatcher firstPartySegmentMatcher;
    private GpsCoordinatesMatcher gpsCoordinatesMatcher;
    private MetricReporterService metricReporterService;

    public EntityMatcher(DeviceClassMatcher deviceClassMatcher,
                         DeviceTypeMatcher deviceTypeMatcher,
                         EnvironmentTypeMatcher environmentTypeMatcher,
                         InterestMatcher interestMatcher,
                         GpsCoordinatesMatcher gpsCoordinatesMatcher,
                         FirstPartySegmentMatcher firstPartySegmentMatcher,
                         MetricReporterService metricReporterService) {
        this.deviceClassMatcher = deviceClassMatcher;
        this.deviceTypeMatcher = deviceTypeMatcher;
        this.environmentTypeMatcher = environmentTypeMatcher;
        this.interestMatcher = interestMatcher;
        this.gpsCoordinatesMatcher = gpsCoordinatesMatcher;
        this.firstPartySegmentMatcher = firstPartySegmentMatcher;
        this.metricReporterService = metricReporterService;;
    }

    public boolean isEntityMatched(AudienceEntity audienceEntity,
                                   ScorerContext context) {

        EntityType type = audienceEntity.getId().getType();
        boolean result = false;
        try {
            switch (type) {
                case INTEREST:
                    result = interestMatcher.matches(audienceEntity, context);
                    break;
                case DEVICE_TYPE:
                    result = deviceTypeMatcher.matches(audienceEntity, context);
                    break;
                case DEVICE_CLASS:
                    result = deviceClassMatcher.matches(audienceEntity, context);
                    break;
                case ENVIRONMENT_TYPE:
                    result = environmentTypeMatcher.matches(audienceEntity, context);
                    break;
                case GPS_COORDINATE:
                    result = gpsCoordinatesMatcher.matches(audienceEntity, context);
                    LOGGER.error("GPS COORDINATE iS NOT IMPLEMENTED YET, : {}", audienceEntity);
                    break;
                case DOMAIN:
                    LOGGER.error("domain is deprecated for : {}", audienceEntity);
                    break;
                case FIRST_PARTY_SEGMENT:
                    result = firstPartySegmentMatcher.matches(audienceEntity, context);
                    break;
                case BAD_TYPE:
                    break;
                default:
                    throw new IllegalArgumentException("bad type seen " + type);
            }
        } catch (RuntimeException e) {
            LOGGER.warn("exception in score audiences", e);
            metricReporterService.addStateModuleForEntity(
                    "EXCEPTION_IN_AUDIENCE_SCORING",
                    getClass().getSimpleName(),
                    ALL_ENTITIES);
        }
        return result;
    }
}

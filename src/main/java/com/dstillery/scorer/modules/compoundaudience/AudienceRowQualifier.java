package com.dstillery.scorer.modules.compoundaudience;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.dstillery.common.audience.AudienceRow;
import com.dstillery.common.audience.models.AudienceGroup;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.scorer.main.ScorerContext;
/**
 * This service
 */
public class AudienceRowQualifier {

    private MetricReporterService metricReporterService;
    private AudienceGroupQualifier audienceGroupQualifier;

    public AudienceRowQualifier(
            MetricReporterService metricReporterService,
            AudienceGroupQualifier audienceGroupQualifier) {
        this.metricReporterService = metricReporterService;;
        this.audienceGroupQualifier = audienceGroupQualifier;
    }

    public Boolean isQualified(AudienceRow audience, ScorerContext context) {
        if (audience.getCompoundAudience() == null ||
            audience.getCompoundAudience().getOuterOperand() == null ||
            audience.getCompoundAudience().getAudiences().isEmpty()) {
            return false;
        }
        List<Boolean> allGroupsResults =
                audience.getCompoundAudience()
                        .getAudiences()
                        .stream()
                        .map(audienceGroup ->
                                audienceGroupQualifier.isAudienceGroupQualified(audienceGroup, context))
                        .distinct()
                        .collect(Collectors.toCollection(ArrayList::new));
        if (audience.getCompoundAudience().getOuterOperand().equals(AudienceGroup.Operand.AND)) {
            return allGroupsResults.size() == 1 && allGroupsResults.get(0) == Boolean.TRUE;
        } else {
            return allGroupsResults.stream().anyMatch(res -> res.booleanValue() == Boolean.TRUE);
        }
    }
}

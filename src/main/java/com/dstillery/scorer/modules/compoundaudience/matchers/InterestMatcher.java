package com.dstillery.scorer.modules.compoundaudience.matchers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.audience.models.AudienceEntity;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.interest.Interest;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.scorer.main.ScorerContext;

public class InterestMatcher implements AudienceMatcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(InterestMatcher.class);
    private CacheService<Interest> interestCacheService;
    private SegmentCacheService segmentCacheService;

    public InterestMatcher(CacheService<Interest> interestCacheService,
                           SegmentCacheService segmentCacheService) {
        this.interestCacheService = interestCacheService;
        this.segmentCacheService = segmentCacheService;
    }

    @Override
    public boolean matches(AudienceEntity audienceEntity, ScorerContext context) {
        LOGGER.debug("audienceEntity : {}", audienceEntity);

        Interest interest = interestCacheService.getAllEntitiesMap().get(
                Long.valueOf(audienceEntity.getId().getId())
        );

        Segment segment = segmentCacheService.getInterestBasedModelToSegment().get(interest.getId());
        return segment != null;
    }
}

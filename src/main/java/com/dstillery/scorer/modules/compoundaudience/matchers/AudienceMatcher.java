package com.dstillery.scorer.modules.compoundaudience.matchers;

import com.dstillery.common.audience.models.AudienceEntity;
import com.dstillery.scorer.main.ScorerContext;

public interface AudienceMatcher {
    boolean matches(AudienceEntity audienceEntity, ScorerContext context);
}

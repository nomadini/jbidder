package com.dstillery.scorer.modules.compoundaudience.matchers;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.audience.models.AudienceEntity;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.scorer.main.ScorerContext;

public class DomainMatcher implements AudienceMatcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(DomainMatcher.class);
    private int considerFeatureSeenInDays;
    public DomainMatcher(int considerFeatureSeenInDays) {
        this.considerFeatureSeenInDays = considerFeatureSeenInDays;
    }

    @Override
    public boolean matches(AudienceEntity audienceEntity, ScorerContext context) {
        boolean result = false;
        LOGGER.debug("audienceEntity : {}", audienceEntity);
        String domainName = audienceEntity.getId().getId();
        LOGGER.debug("domainName: {}", domainName);

        for (DeviceFeaturePair dfh : context.getDeviceFeatureHistory()) {
            //LOGGER.debug("dfh : {}, audienceEntity", dfh, audienceEntity);
            if (dfh.getFeatureHistory().getFeatureSeenInMillis() >=
                    Instant.now().minus(considerFeatureSeenInDays, ChronoUnit.DAYS).toEpochMilli()) {
                //if feature is seen in the past day
                LOGGER.debug("feature is seen in the past 3 days");

                return dfh.getFeatureHistory().getFeature().getName().equalsIgnoreCase(domainName);
            }
        }
        return result;
    }
}

package com.dstillery.scorer.modules.compoundaudience;

import com.dstillery.common.audience.models.AudienceGroup;
import com.dstillery.scorer.main.ScorerContext;
import com.dstillery.scorer.modules.compoundaudience.matchers.EntityMatcher;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class AudienceGroupQualifier {
    private EntityMatcher entityMatcher;

    public AudienceGroupQualifier(EntityMatcher entityMatcher) {
        this.entityMatcher = entityMatcher;
    }

    public Boolean isAudienceGroupQualified(AudienceGroup audienceGroup,
                                            ScorerContext context) {
        AudienceGroup.Operand audienceGroupOperand = audienceGroup.getGroupOperand();
        ArrayList<Boolean> list = audienceGroup.getData().stream()
                .map(audienceEntity -> entityMatcher.isEntityMatched(audienceEntity, context))
                .distinct()
                .collect(Collectors.toCollection(ArrayList::new));
        if (audienceGroupOperand.equals(AudienceGroup.Operand.AND)) {
            return list.size() == 1 && list.get(0) == Boolean.TRUE;
        } else {
            return list.stream().anyMatch(res -> res.booleanValue() == Boolean.TRUE);
        }
    }

}

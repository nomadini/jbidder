package com.dstillery.scorer.modules.compoundaudience.matchers;

import com.dstillery.common.audience.models.AudienceEntity;
import com.dstillery.common.audience.models.AudienceEntityTypeValue;
import com.dstillery.common.device.DeviceEnvironmentType;
import com.dstillery.scorer.main.ScorerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnvironmentTypeMatcher implements AudienceMatcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnvironmentTypeMatcher.class);

    @Override
    public boolean matches(AudienceEntity audienceEntity, ScorerContext context) {
        boolean result = false;

        LOGGER.debug("EnvironmentType : {}", context.getTransAppMessage().getDevice().getDeviceEnvironmentType());
        LOGGER.debug("audienceEntity.getText() : {}", audienceEntity.getLabel());
        AudienceEntityTypeValue type =
                AudienceEntityTypeValue.fromValue(
                        Long.valueOf(audienceEntity.getId().getId()));

        if (context
                .getTransAppMessage()
                .getDevice()
                .getDeviceEnvironmentType()
                .equals(DeviceEnvironmentType.valueOf(type.getTypeValueAsString()))) {

            result = true;
        }
        return  result;
    }
}

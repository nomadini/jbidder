package com.dstillery.scorer.modules.compoundaudience.matchers;

import com.dstillery.common.audience.models.AudienceEntity;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.scorer.main.ScorerContext;

public class FirstPartySegmentMatcher implements AudienceMatcher {

    private final SegmentCacheService segmentCacheService;

    public FirstPartySegmentMatcher(SegmentCacheService segmentCacheService) {
        this.segmentCacheService = segmentCacheService;
    }

    @Override
    public boolean matches(AudienceEntity audienceEntity, ScorerContext context) {
        Segment segment = segmentCacheService.getAllEntitiesMap().get(Long.valueOf(audienceEntity.getId().getId()));
        return  segment != null;
    }
}

package com.dstillery.scorer.modules.compoundaudience.matchers;

import com.dstillery.common.audience.models.AudienceEntity;
import com.dstillery.common.audience.models.AudienceEntityTypeValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.scorer.main.ScorerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeviceTypeMatcher implements AudienceMatcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceTypeMatcher.class);

    @Override
    public boolean matches(AudienceEntity audienceEntity, ScorerContext context) {
        boolean result = false;
        LOGGER.debug("DeviceType : {}", context.getTransAppMessage().getDevice().getDeviceType());
        LOGGER.debug("audienceEntity.getText() : {}", audienceEntity.getLabel());

        AudienceEntityTypeValue type =
                AudienceEntityTypeValue.fromValue(
                        Long.valueOf(audienceEntity.getId().getId()));
        if (context
                .getTransAppMessage()
                .getDevice()
                .getDeviceType()
                .equals(DeviceTypeValue.valueOf(type.getTypeValueAsString()))) {

            result = true;
        }
        return  result;
    }
}

package com.dstillery.scorer.modules;

import static com.dstillery.scorer.modules.ScoreLocationPlaceTagHomeModule.getPlaces;

import java.time.Clock;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.PlacesCachedResult;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.geo.places.Place;
import com.dstillery.common.geo.places.PlaceFinderModule;
import com.dstillery.common.geo.places.Tag;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentBuilder;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.segment.SegmentNameCreator;
import com.dstillery.common.segment.SegmentSourceType;
import com.dstillery.common.segment.SegmentSubType;
import com.dstillery.common.segment.SegmentType;
import com.dstillery.scorer.main.ScorerContext;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

public class ScoreLocationPlaceTagHereNowModule extends ScorerModule  {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScoreLocationPlaceTagHereNowModule.class);

    private MetricReporterService metricReporterService;
    private SegmentInsertionService segmentInsertionService;

    private AeroCacheService<PlacesCachedResult> aerospikePlaceCache;
    private PlaceFinderModule placeFinderModule;
    private Clock clock;

    public ScoreLocationPlaceTagHereNowModule(
            MetricReporterService metricReporterService,
            SegmentInsertionService segmentInsertionService,
            AeroCacheService<PlacesCachedResult> aerospikePlaceCache,
            PlaceFinderModule placeFinderModule,
            Clock clock) {

        this.placeFinderModule = placeFinderModule;
        this.aerospikePlaceCache = aerospikePlaceCache;
        this.metricReporterService = metricReporterService;
        this.segmentInsertionService = segmentInsertionService;
        this.clock = clock;
    }

    public void process(ScorerContext context) {
        int maxNumberOfWantedFeatures = 5;
        if (context.getTransAppMessage().getMgrs10m() == null ||
                context.getTransAppMessage().getMgrs10m().isEmpty()) {
            metricReporterService.getCounter("have_mgrs10m", ImmutableMap.of("found", "no")).inc();
            return;
        } else {
            metricReporterService.getCounter("have_mgrs10m", ImmutableMap.of("found", "yes")).inc();
        }

        List<Place> places = getPlacesFromCache(
                context.getTransAppMessage().getMgrs10m(),
                context.getTransAppMessage().getDeviceLat(),
                context.getTransAppMessage().getDeviceLon(),
                maxNumberOfWantedFeatures);

        Set<Tag> allTags = getTagIdsOfFeatures(places);
        allTags.forEach(tag -> {
            metricReporterService.getCounter("place_tags_found", ImmutableMap.of("tag_id", tag.toString())).inc();
            Segment segment = convertToSegment(tag);
            context.addSegmentsForDevice(segment);
            metricReporterService.getCounter(
                    "qualified_location_point_of_interest_audience",
                    ImmutableMap.of(
                            "tag_id", tag.toString()
                    )).inc();

            LOGGER.debug("segment created for device : {}, segment : {}", context.getDevice(), segment);
        });
    }

    private Segment convertToSegment(Tag tag) {
        Segment segment = new SegmentBuilder()
                .withDateCreatedInMillis(clock.millis())
                .withName(
                SegmentNameCreator.createName(
                        tag.getName(),
                        "",
                        SegmentType.LOCATION_POINT_OF_INTEREST))
                .withType(SegmentType.LOCATION_POINT_OF_INTEREST)
                .withSubType(SegmentSubType.ONCE) // we create a HERE_NOW or once subtype
                .withSourceEntityId(tag.getId())
                .withSourceType(SegmentSourceType.PLACE_TAG).build();
        return segmentInsertionService.writeAndGet(segment);
    }

    private List<Place> getPlacesFromCache(
            String mgrs10m,
            double deviceLat,
            double deviceLon,
            int maxNumberOfWantedFeatures) {

        return getPlaces(mgrs10m, deviceLat, deviceLon, maxNumberOfWantedFeatures,
                aerospikePlaceCache,
                metricReporterService,
                placeFinderModule);
    }

    private Set<Tag> getTagIdsOfFeatures(
            List<Place> allPlaces) {
        Set<Tag> tagIdsSet = new HashSet<>();
        for (Place place : allPlaces) {
            tagIdsSet.addAll(place.getTags());
        }

        return tagIdsSet;
    }

}

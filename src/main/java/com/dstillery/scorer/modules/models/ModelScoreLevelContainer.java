package com.dstillery.scorer.modules.models;

import java.util.ArrayList;
import java.util.List;

public class ModelScoreLevelContainer  {

    private List<ModelScoreLevel> modelScoreLevels;

    public ModelScoreLevelContainer() {
        modelScoreLevels = new ArrayList<>();

    }

    public List<ModelScoreLevel> getModelScoreLevels() {
        return modelScoreLevels;
    }

    public void setModelScoreLevels(List<ModelScoreLevel> modelScoreLevels) {
        this.modelScoreLevels = modelScoreLevels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ModelScoreLevelContainer)) return false;

        ModelScoreLevelContainer that = (ModelScoreLevelContainer) o;

        return getModelScoreLevels() != null ? getModelScoreLevels().equals(that.getModelScoreLevels()) : that.getModelScoreLevels() == null;
    }

    @Override
    public int hashCode() {
        return getModelScoreLevels() != null ? getModelScoreLevels().hashCode() : 0;
    }
}


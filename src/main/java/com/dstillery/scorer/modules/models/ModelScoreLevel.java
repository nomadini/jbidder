package com.dstillery.scorer.modules.models;

public class ModelScoreLevel  {

    double minScoreOfLevel;
    double maxScoreOfLevel;
    String levelName;

    public ModelScoreLevel() {
        minScoreOfLevel = 0;
        maxScoreOfLevel = 0;

    }

    public double getMinScoreOfLevel() {
        return minScoreOfLevel;
    }

    public void setMinScoreOfLevel(double minScoreOfLevel) {
        this.minScoreOfLevel = minScoreOfLevel;
    }

    public double getMaxScoreOfLevel() {
        return maxScoreOfLevel;
    }

    public void setMaxScoreOfLevel(double maxScoreOfLevel) {
        this.maxScoreOfLevel = maxScoreOfLevel;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ModelScoreLevel)) return false;

        ModelScoreLevel that = (ModelScoreLevel) o;

        if (Double.compare(that.getMinScoreOfLevel(), getMinScoreOfLevel()) != 0) return false;
        if (Double.compare(that.getMaxScoreOfLevel(), getMaxScoreOfLevel()) != 0) return false;
        return getLevelName() != null ? getLevelName().equals(that.getLevelName()) : that.getLevelName() == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getMinScoreOfLevel());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMaxScoreOfLevel());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getLevelName() != null ? getLevelName().hashCode() : 0);
        return result;
    }
}


package com.dstillery.scorer.modules.persistence;/*
  SegmentToDevicePersistenceModule.h
 *
   Created on: Oct 29, 2015
       Author: mtaabodi
 */


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.SegmentDevice;
import com.dstillery.common.modeling.SegmentToDeviceCassandraService;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.util.GUtil;
import com.dstillery.scorer.main.ScorerContext;
import com.dstillery.scorer.modules.ScorerModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.dstillery.common.util.StringUtil.assertAndThrow;

public class SegmentToDevicePersistenceModule extends ScorerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(SegmentToDevicePersistenceModule.class);

    private SegmentToDeviceCassandraService segmentToDeviceCassandraService;
    private MetricReporterService metricReporterService;

    public SegmentToDevicePersistenceModule(
            MetricReporterService metricReporterService,
            SegmentToDeviceCassandraService segmentToDeviceCassandraService) {
        this.segmentToDeviceCassandraService = segmentToDeviceCassandraService;
        this.metricReporterService = metricReporterService;;
    }

    public void process(ScorerContext context) {

        for (Segment segment :
             context.getAllSegmentsCreatedForDevice()) {
            //TODO delte this class after confirming it's never used
//            SegmentDevice segmentDevices = new SegmentDevice();
//            assertAndThrow(segment.getId() != null);
//            segmentDevices.setSegment(segment);
//            segmentDevices.setDevice(context.getDeviceFeatureHistory().get(0).getDevice());
//
////            segmentToDeviceCassandraService.write(segmentDevices);
//            metricReporterService.addStateModuleForEntity(
//                    "ADDING_SEGMENT_DEVICE",
//                    "SegmentToDevicePersistenceModule",
//                    "ALL");
//
//            GUtil.callNThTime(100, (s) -> LOGGER.debug("100th persisting segmentDevice : {}", segmentDevices));
        }
    }

}

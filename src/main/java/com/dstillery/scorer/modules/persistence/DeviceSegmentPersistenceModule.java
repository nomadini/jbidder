package com.dstillery.scorer.modules.persistence;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistoryCassandraService;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.util.GUtil;
import com.dstillery.scorer.main.ScorerContext;
import com.dstillery.scorer.modules.ScorerModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.metric.dropwizard.MetricReporterService.MetricPriority.IMPORTANT;

/*
  h
 *
   Created on: Oct 29, 2015
       Author: mtaabodi
 */
public class DeviceSegmentPersistenceModule extends ScorerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceSegmentPersistenceModule.class);

    private DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService;
    private MetricReporterService metricReporterService;
    public DeviceSegmentPersistenceModule(
            MetricReporterService metricReporterService,
            DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService) {
        this.deviceSegmentHistoryCassandraService = deviceSegmentHistoryCassandraService;
        this.metricReporterService = metricReporterService;
    }

    public void process(ScorerContext context) {
        for (Segment segment : context.getAllSegmentsCreatedForDevice()) {
            DeviceSegmentHistory deviceSegmentHistory = new DeviceSegmentHistory(
                    context.getDeviceFeatureHistory().get(0).getDevice()
            );
            deviceSegmentHistory.setSegment(segment);

            deviceSegmentHistoryCassandraService.write(deviceSegmentHistory);

            GUtil.callNThTime(100, (s) -> LOGGER.debug("persisting deviceSegmentHistory : {}", deviceSegmentHistory));

            metricReporterService.addStateModuleForEntity(
                    "ADDING_DEVICE_SEGMENT",
                    "DeviceSegmentPersistenceModule",
                    ALL_ENTITIES,
                    IMPORTANT);

            //we want to know what segments are being created for these devices
            //this a very important metric for scorer
            metricReporterService.addStateModuleForEntity(
                    "device_scored",
                    "DeviceSegmentPersistenceModule",
                    segment.getId());

        }

    }
}

package com.dstillery.scorer.modules.persistence;

/*
 *
   Created on: Oct 29, 2015
       Author: mtaabodi
*/import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.util.DateTimeUtil.getNowInMilliSecond;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.common_modules.CrossWalkReaderModule;
import com.dstillery.common.device.NomadiniDevice;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistoryCassandraService;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentBuilder;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.segment.SegmentSubType;
import com.dstillery.common.util.GUtil;
import com.dstillery.scorer.main.ScorerContext;
import com.dstillery.scorer.modules.ScorerModule;

public class CrossWalkDeviceSegmentPersistenceModule extends ScorerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CrossWalkDeviceSegmentPersistenceModule.class);

    private DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService;
    private CrossWalkReaderModule crossWalkReaderModule;
    private MetricReporterService metricReporterService;
    private SegmentInsertionService segmentInsertionService;
    public CrossWalkDeviceSegmentPersistenceModule(
            MetricReporterService metricReporterService,
            CrossWalkReaderModule crossWalkReaderModule,
            DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService,
            SegmentInsertionService segmentInsertionService) {
        this.deviceSegmentHistoryCassandraService = deviceSegmentHistoryCassandraService;
        this.metricReporterService = metricReporterService;;
        this.crossWalkReaderModule = crossWalkReaderModule;
        this.segmentInsertionService = segmentInsertionService;
    }

    public void process(ScorerContext context) {
        //read all devices cross walked to this device
        List<NomadiniDevice> allCrosswalkedDevices =
                crossWalkReaderModule.readNearByDevices(context.getDeviceFeatureHistory().get(0).getDevice());
        persistTheCrosswalkedDeviceSegments(context);
    }

    private void persistTheCrosswalkedDeviceSegments(
            ScorerContext context) {
        for (Segment segment : context.getAllSegmentsCreatedForDevice()) {
            Segment crossWalk = createCrosswalkSegment(segment);

            DeviceSegmentHistory deviceSegmentHistory = new DeviceSegmentHistory(
                    context.getDeviceFeatureHistory().get(0).getDevice()
            );
            deviceSegmentHistory.setSegment(crossWalk);

            deviceSegmentHistoryCassandraService.write(deviceSegmentHistory);
            GUtil.callNThTime(100, s -> LOGGER.debug("persisting deviceSegmentHistory : {}", deviceSegmentHistory));

            metricReporterService.addStateModuleForEntity(
                    "ADDING_CROSSWALKED_DEVICE_SEGMENT",
                    "CrossWalkDeviceSegmentPersistenceModule",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.IMPORTANT);

        }

    }

    private Segment createCrosswalkSegment(Segment segment) {
        Segment crossWalkSegment = new SegmentBuilder()
                .withId(null)
                .withDateCreatedInMillis(getNowInMilliSecond())
                .withName(segment.getName())
                .withType(segment.getType())
                .withSubType(SegmentSubType.CROSSWALK)
                .withSourceEntityId(segment.getSourceEntityId())
                .withSourceType(segment.getSourceType())
                .withMetaData(segment.getMetaData())
                .build();

        return segmentInsertionService.writeAndGet(crossWalkSegment);
    }

}

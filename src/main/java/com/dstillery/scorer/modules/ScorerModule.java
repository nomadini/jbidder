package com.dstillery.scorer.modules;


import com.dstillery.scorer.main.ScorerContext;

public abstract class ScorerModule  {

    public void beforeProcessing(ScorerContext context) {

    }

    public abstract void process(ScorerContext context);

    public void afterProcessing(ScorerContext context) {

    }

}


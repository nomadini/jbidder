package com.dstillery.scorer.modules;

import static com.dstillery.common.util.DateTimeUtil.getNowInMilliSecond;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.audience.AudienceRow;
import com.dstillery.common.cache.datamaster.CacheService;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentBuilder;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.segment.SegmentNameCreator;
import com.dstillery.common.segment.SegmentSourceType;
import com.dstillery.common.segment.SegmentSubType;
import com.dstillery.common.segment.SegmentType;
import com.dstillery.scorer.main.ScorerContext;
import com.dstillery.scorer.modules.compoundaudience.AudienceGroupQualifier;
import com.dstillery.scorer.modules.compoundaudience.AudienceRowQualifier;
import com.google.common.collect.ImmutableMap;

public class ScoreCompoundAudiencesModule extends ScorerModule  {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScoreCompoundAudiencesModule.class);

    private CacheService<AudienceRow> audienceCacheService;

    private AudienceRowQualifier audienceRowQualifier;
    private AudienceGroupQualifier audienceGroupQualifier;
    private SegmentInsertionService segmentInsertionService;
    private MetricReporterService metricReporterService;

    public ScoreCompoundAudiencesModule(
            MetricReporterService metricReporterService,
            AudienceRowQualifier audienceRowQualifier,
            CacheService<AudienceRow> audienceCacheService,
            SegmentInsertionService segmentInsertionService) {
        this.audienceCacheService = audienceCacheService;
        this.audienceRowQualifier = audienceRowQualifier;
        this.segmentInsertionService = segmentInsertionService;
        this.metricReporterService = metricReporterService;
    }

    public void process(ScorerContext context) {
        for (AudienceRow audience : audienceCacheService.getAllEntities()) {
            if (audienceRowQualifier.isQualified(audience, context)) {
                Segment segment = convertToSegment(audience);
                LOGGER.debug("segment created for device : {}, segment : {}", context.getDevice(), segment);
                context.addSegmentsForDevice(segment);
                metricReporterService.getHistogram(
                        "qualified_compound_audience", ImmutableMap.of("segment", segment.getName()))
                        .update(1);
            }
        }
    }

    private Segment convertToSegment(AudienceRow audience) {
        Segment segment = new SegmentBuilder()
                .withDateCreatedInMillis(getNowInMilliSecond())
                .withName(
                        SegmentNameCreator.createName(
                                audience.getId() + "_" + audience.getName(),
                                "",
                                SegmentType.COMPOUND_AUDIENCE))
                .withType(SegmentType.COMPOUND_AUDIENCE)
                .withSubType(SegmentSubType.ONCE)
                .withSourceEntityId(audience.getId())
                .withSourceType(SegmentSourceType.COMPOUND_AUDIENCE).build();
        return segmentInsertionService.writeAndGet(segment);
    }
}

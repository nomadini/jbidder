package com.dstillery.scorer.modules;

import static com.dstillery.common.util.StringUtil.toStr;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.PlacesCachedResult;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.eventlog.UniqueDeviceBidEventLog;
import com.dstillery.common.geo.places.Place;
import com.dstillery.common.geo.places.PlaceFinderModule;
import com.dstillery.common.geo.places.Tag;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentBuilder;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.segment.SegmentNameCreator;
import com.dstillery.common.segment.SegmentSourceType;
import com.dstillery.common.segment.SegmentSubType;
import com.dstillery.common.segment.SegmentType;
import com.dstillery.common.util.StringUtil;
import com.dstillery.scorer.main.ScorerContext;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

public class ScoreLocationPlaceTagHomeModule extends ScorerModule  {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScoreLocationPlaceTagHomeModule.class);

    private MetricReporterService metricReporterService;
    private SegmentInsertionService segmentInsertionService;

    private AeroCacheService<PlacesCachedResult> aerospikePlaceCache;
    private PlaceFinderModule placeFinderModule;
    private CassandraService<UniqueDeviceBidEventLog> deviceBidEventLogCassandraService;
    private Integer minNumberOfFrequencyToBePickedAsTop = 10;
    private Integer numberOfTopPlacesToPick;
    private Clock clock;

    public ScoreLocationPlaceTagHomeModule(
            MetricReporterService metricReporterService,
            SegmentInsertionService segmentInsertionService,
            AeroCacheService<PlacesCachedResult> aerospikePlaceCache,
            PlaceFinderModule placeFinderModule,
            CassandraService<UniqueDeviceBidEventLog> deviceBidEventLogCassandraService,
            Clock clock,
            Integer numberOfTopPlacesToPick) {
        this.deviceBidEventLogCassandraService = deviceBidEventLogCassandraService;
        this.placeFinderModule = placeFinderModule;
        this.aerospikePlaceCache = aerospikePlaceCache;
        this.metricReporterService = metricReporterService;
        this.segmentInsertionService = segmentInsertionService;
        this.clock = clock;
        this.numberOfTopPlacesToPick = numberOfTopPlacesToPick;
    }

    public void process(ScorerContext context) {
        int maxNumberOfWantedFeatures = 5;
        if (context.getTransAppMessage().getMgrs10m() == null ||
                context.getTransAppMessage().getMgrs10m().isEmpty()) {
            metricReporterService.getCounter("have_mgrs10m", ImmutableMap.of("found", "no")).inc();
            return;
        } else {
            metricReporterService.getCounter("have_mgrs10m", ImmutableMap.of("found", "yes")).inc();
        }

        List<UniqueDeviceBidEventLog> allEventLogsForDevice =
                deviceBidEventLogCassandraService.readWithCustomQuery(getQuery(context, 30), 100);

        LOGGER.debug("read {} eventLogs", allEventLogsForDevice.size());

        Map<Instant, List<UniqueDeviceBidEventLog>> daysToEventLogs = groupEventsBasedOnDays(allEventLogsForDevice);

        daysToEventLogs.forEach((day, events) -> {
            List<Place> placesVisitedInDays = events.stream().map(event ->
                    getPlacesFromCache(event.getEventLog().getMgrs10m(),
                            event.getEventLog().getDeviceLat(),
                            event.getEventLog().getDeviceLon(), 10))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());

            LOGGER.debug("fetched {} placesVisitedInDays", placesVisitedInDays.size());
            LOGGER.debug("placesVisitedInDays : {}", placesVisitedInDays);

            Map<Long, Integer> placesToNumberOfTimesVisited = new HashMap<>();
            Map<Long, Place> placeMap = new HashMap<>();
            placesVisitedInDays.forEach(place -> {
                placesToNumberOfTimesVisited.putIfAbsent(place.getId(), 0);
                placeMap.putIfAbsent(place.getId(), place);

                placesToNumberOfTimesVisited.put(place.getId(),
                    placesToNumberOfTimesVisited.get(place.getId()) + 1);
            });

            List<Integer> numberOfTimesAsList = new ArrayList<>(placesToNumberOfTimesVisited.values());
            numberOfTimesAsList.sort(Comparator.reverseOrder());
            LOGGER.debug("numberOfTimesAsList {}", numberOfTimesAsList);

            Integer lowestNumberOfTimesToPickCandidate = minNumberOfFrequencyToBePickedAsTop; //10 by default
            for (Integer i = numberOfTopPlacesToPick; i > 0; i--) {
                if (numberOfTimesAsList.size() > i) {
                    lowestNumberOfTimesToPickCandidate = numberOfTimesAsList.get(i - 1);
                }
            }

            Integer lowestNumberOfTimesToPick = lowestNumberOfTimesToPickCandidate;
            LOGGER.debug("lowestNumberOfTimesToPick  = {}", lowestNumberOfTimesToPick);

            List<Pair<Place, Integer>> allPlacesPickedAsTopOnes = placesToNumberOfTimesVisited.entrySet().stream()
                    .filter(entry -> entry.getValue() >= lowestNumberOfTimesToPick)
                    .map(ent -> ImmutablePair.of(placeMap.get(ent.getKey()), ent.getValue()))
                    .collect(Collectors.toList());

            if (allPlacesPickedAsTopOnes.size() > numberOfTopPlacesToPick) {
                allPlacesPickedAsTopOnes = allPlacesPickedAsTopOnes.subList(0, numberOfTopPlacesToPick);
            }

            LOGGER.debug("allPlacesPickedAsTopOnes  = {}", allPlacesPickedAsTopOnes);

            allPlacesPickedAsTopOnes.forEach(placeLongEntry -> {
                getTagsAndAddAsSegment(placeLongEntry, context);
            });
        });
    }

    private Map<Instant, List<UniqueDeviceBidEventLog>> groupEventsBasedOnDays(List<UniqueDeviceBidEventLog> allEventLogsForDevice) {
        Map<Instant, List<UniqueDeviceBidEventLog>> daysToEventLogs = new HashMap<>();
        allEventLogsForDevice.forEach(newEvent -> {
            Instant day = Instant.ofEpochMilli(newEvent.getEventLog().getEventTime()).truncatedTo(ChronoUnit.DAYS);
            daysToEventLogs.computeIfAbsent(day, (e) -> new ArrayList<>());

            daysToEventLogs.computeIfPresent(day, (instant, currentDayEvents) -> {
                currentDayEvents.add(newEvent);
                return currentDayEvents;
            });

        });
        return daysToEventLogs;
    }

    private void getTagsAndAddAsSegment(Map.Entry<Place, Integer> placeEntry, ScorerContext context) {
        Set<Tag> allTags = getTagIdsOfFeatures(ImmutableList.of(placeEntry.getKey()));
        allTags.forEach(tag -> {
            metricReporterService.getCounter("place_tags_found", ImmutableMap.of("tag_id", tag.toString())).inc();
            Segment segment = convertToSegment(tag);
            context.addSegmentsForDevice(segment);
            metricReporterService.getCounter(
                    "qualified_location_point_of_interest_audience",
                    ImmutableMap.of(
                            "tag_id", tag.toString()
                    )).inc();

            LOGGER.debug("segment created for device : {}, segment : {}", context.getDevice(), segment);
        });
    }

    private String getQuery(ScorerContext context, int fetchLocationsPeriodInDays ) {

        String queryStr =
                "select * from  gicapods.daily_unique_device_bid_eventlog where " +
                        " device_id='__DEVICE_ID__' and device_type='__DEVICE_TYPE__' and eventtime >= " +
        Instant.now().minus(fetchLocationsPeriodInDays, ChronoUnit.DAYS).toEpochMilli();

        queryStr = StringUtil.replaceStringForMySql(queryStr, toStr("__DEVICE_ID__"),
                context.getTransAppMessage().getDevice().getCompositeDeviceId());
        queryStr = StringUtil.replaceStringForMySql(queryStr, toStr("__DEVICE_TYPE__"),
                context.getTransAppMessage().getDevice().getDeviceClass().getTypeValue());
        return queryStr;
    }

    private Segment convertToSegment(Tag tag) {
        Segment segment = new SegmentBuilder()
                .withDateCreatedInMillis(clock.millis())
                .withName(
                SegmentNameCreator.createName(
                        tag.getName(),
                        "",
                        SegmentType.LOCATION_POINT_OF_INTEREST))
                .withType(SegmentType.LOCATION_POINT_OF_INTEREST)
                .withSubType(SegmentSubType.HOMED) // we create a HOMED or once subtype
                .withSourceEntityId(tag.getId())
                .withSourceType(SegmentSourceType.PLACE_TAG).build();
        return segmentInsertionService.writeAndGet(segment);
    }

    private List<Place> getPlacesFromCache(
            String mgrs10m,
            double deviceLat,
            double deviceLon,
            int maxNumberOfWantedFeatures) {

        return getPlaces(mgrs10m, deviceLat, deviceLon, maxNumberOfWantedFeatures, aerospikePlaceCache, metricReporterService, placeFinderModule);
    }

    static List<Place> getPlaces(String mgrs10m, double deviceLat, double deviceLon, int maxNumberOfWantedFeatures,
                                 AeroCacheService<PlacesCachedResult> aerospikePlaceCache,
                                 MetricReporterService metricReporterService,
                                 PlaceFinderModule placeFinderModule) {
        PlacesCachedResult cacheKey = new PlacesCachedResult(mgrs10m, maxNumberOfWantedFeatures);

        Optional<PlacesCachedResult> inMemoryValue = aerospikePlaceCache.readDataOptionalAsObject(cacheKey);
        if (inMemoryValue.isPresent() && !inMemoryValue.get().getPlaces().isEmpty()) {
            metricReporterService.getCounter("place_found", ImmutableMap.of("cache_hit", "yes")).inc();
            List<Place> placesFound = inMemoryValue.get().getPlaces();
            LOGGER.debug("places found : {}", placesFound);
            metricReporterService.getHistogram("num_places_found").update(inMemoryValue.get().getPlaces().size());
            return placesFound;
        } else {
            metricReporterService.getCounter("place_found", ImmutableMap.of("cache_hit", "no")).inc();
        }

        List<Place> allPlaces = new ArrayList<>();
        double rectangleRadiusInMileage = 0.4;
        try {

            while(allPlaces.isEmpty() && rectangleRadiusInMileage <= 2.00) {
                allPlaces = placeFinderModule.findNearestFeatures(
                        deviceLat,
                        deviceLon,
                        maxNumberOfWantedFeatures,
                        rectangleRadiusInMileage
                );
                rectangleRadiusInMileage += 0.2;
            }

            metricReporterService.getCounter("place_call", ImmutableMap.of("success", "true")).inc();
            if (!allPlaces.isEmpty()) {
                metricReporterService.getCounter("place_put_in_cache").inc();

                //we only put non empty results in the caches
                cacheKey.setPlaces(allPlaces);
                aerospikePlaceCache.putDataInCache(cacheKey);
            }
        } catch(Exception e) {
            metricReporterService.getCounter("place_call", ImmutableMap.of("success", "false")).inc();
        }

        return allPlaces;
    }

    private Set<Tag> getTagIdsOfFeatures(
            List<Place> allPlaces) {
        Set<Tag> tagIdsSet = new HashSet<>();
        for (Place place : allPlaces) {
            tagIdsSet.addAll(place.getTags());
        }

        return tagIdsSet;
    }

    public Integer getMinNumberOfFrequencyToBePickedAsTop() {
        return minNumberOfFrequencyToBePickedAsTop;
    }
}

package com.dstillery.scorer.modules;

import static com.dstillery.common.util.StringUtil.toStr;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistoryCassandraService;
import com.dstillery.common.util.StringUtil;
import com.dstillery.scorer.main.ScorerContext;
import com.google.common.collect.ImmutableMap;

public class DeviceSegmentHistoryFetcherModule extends ScorerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceSegmentHistoryFetcherModule.class);

    private DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService;
    private Integer fetchDeviceSegmentHistoryInPastNDays;
    private MetricReporterService metricReporterService;

    public DeviceSegmentHistoryFetcherModule(
            MetricReporterService metricReporterService,
            DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService,
            Integer fetchDeviceSegmentHistoryInPastNDays) {

        this.fetchDeviceSegmentHistoryInPastNDays = fetchDeviceSegmentHistoryInPastNDays;
        this.deviceSegmentHistoryCassandraService = deviceSegmentHistoryCassandraService;
        this.metricReporterService = metricReporterService;
    }

    public void process(ScorerContext context) {

        List<DeviceSegmentHistory> allSegmentsInThePastNDays =
                deviceSegmentHistoryCassandraService.readWithCustomQuery(getQuery(context, fetchDeviceSegmentHistoryInPastNDays), 100);
        metricReporterService.getHistogram("segment_size_per_device", ImmutableMap.of()).update(allSegmentsInThePastNDays.size());
        context.setDeviceSegmentHistories(allSegmentsInThePastNDays);
    }

    private String getQuery(ScorerContext context, int fetchPeriodInDays) {

        String queryStr =
                "select * from gicapods.deviceSegments where " +
                        " deviceid='__DEVICE_ID__' and devicetype='__DEVICE_TYPE__' and dateCreated >= " +
                        Instant.now().minus(fetchPeriodInDays, ChronoUnit.DAYS).toEpochMilli();

        queryStr = StringUtil.replaceStringForMySql(queryStr, toStr("__DEVICE_ID__"),
                context.getTransAppMessage().getDevice().getCompositeDeviceId());
        queryStr = StringUtil.replaceStringForMySql(queryStr, toStr("__DEVICE_TYPE__"),
                context.getTransAppMessage().getDevice().getDeviceClass().getTypeValue());
        return queryStr;
    }

}

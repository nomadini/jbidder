package com.dstillery.scorer.modules;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentBuilder;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.segment.SegmentSubType;
import com.dstillery.common.segment.SegmentType;
import com.dstillery.common.util.GUtil;
import com.dstillery.scorer.main.ScorerContext;
import com.google.common.collect.ImmutableMap;

public class ScoreATRankedAudiencesModule extends ScorerModule  {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScoreATRankedAudiencesModule.class);

    private MetricReporterService metricReporterService;
    private SegmentInsertionService segmentInsertionService;

    private SegmentCacheService segmentCacheService;
    private Integer numberOfTimesSeenInADayToBeQualified;
    private Integer fetchDeviceSegmentHistoryInPastNDays;
    private Clock clock;

    public ScoreATRankedAudiencesModule(
            MetricReporterService metricReporterService,
            SegmentInsertionService segmentInsertionService,
            SegmentCacheService segmentCacheService,
            Clock clock,
            Integer numberOfTimesSeenInADayToBeQualified) {
        this.clock = clock;
        this.segmentCacheService = segmentCacheService;
        this.metricReporterService = metricReporterService;
        this.segmentInsertionService = segmentInsertionService;
        this.numberOfTimesSeenInADayToBeQualified = numberOfTimesSeenInADayToBeQualified;
    }

    public void process(ScorerContext context) {
        int maxNumberOfWantedFeatures = 5;
        if (context.getTransAppMessage().getMgrs10m() == null ||
                context.getTransAppMessage().getMgrs10m().isEmpty()) {
            metricReporterService.getCounter("have_mgrs10m", ImmutableMap.of("found", "no")).inc();
            return;
        } else {
            metricReporterService.getCounter("have_mgrs10m", ImmutableMap.of("found", "yes")).inc();
        }

        List<DeviceSegmentHistory> allSegmentsInThePastNDays = context.getDeviceSegmentHistories();
        Map<Long, Segment> deviceSegments = new HashMap<>();
        allSegmentsInThePastNDays.forEach(seg -> {
            deviceSegments.computeIfAbsent(seg.getSegment().getId(), s -> seg.getSegment());
        });

        LOGGER.debug("read {} allSegmentsInThePastNDays", allSegmentsInThePastNDays.size());

        List<DeviceSegmentHistory> allActionsInThePastNDays = allSegmentsInThePastNDays.stream().filter(deviceSegmentHistory -> {
            Segment segment = segmentCacheService.getAllEntitiesMap().get(deviceSegmentHistory.getSegment().getId());
            if (segment == null && GUtil.allowedToCall(100)) {
                LOGGER.warn("segment id {} has no segment in db ", deviceSegmentHistory.getSegment().getId());
            }
            return segment != null && segment.getType().equals(SegmentType.ACTION_TAKER);
        }).collect(Collectors.toList());

        LOGGER.debug("read {} actionsInThePastNDays", allActionsInThePastNDays.size());

        Map<Instant, Map<Segment, List<DeviceSegmentHistory>>> daysToActionTakers = groupEventsBasedOnDays(allActionsInThePastNDays);

        List<Long> alreadyRankedATSegmentSourceIds = allActionsInThePastNDays.stream()
                .filter(segment -> segment.getSegment().getSubType().equals(SegmentSubType.RANKED))
                .map(segment -> segment.getSegment().getSourceEntityId()).collect(Collectors.toList());

        List<Long> alreadyRankedATSegmentIds = allActionsInThePastNDays.stream()
                .filter(segment -> segment.getSegment().getSubType().equals(SegmentSubType.RANKED))
                .map(segment -> segment.getSegment().getId()).collect(Collectors.toList());
        LOGGER.debug("alreadyRankedATSegmentIds = {}", alreadyRankedATSegmentIds);
        LOGGER.debug("alreadyRankedATSegmentSourceIds = {}", alreadyRankedATSegmentSourceIds);

        daysToActionTakers.forEach((day, segmentToHistories) -> {

            segmentToHistories.forEach((segmentSource, histories)-> {

                if (alreadyRankedATSegmentIds.contains(segmentSource.getId()) ||
                    alreadyRankedATSegmentSourceIds.contains(segmentSource.getId())) {
                    return;
                }

                if (histories.size() >= numberOfTimesSeenInADayToBeQualified)  {
                    //device has visited the AT pixel more than 5 times in a day
                    //so we create an AT ranked segment for it

                    if (histories.isEmpty()) {
                        return;
                    }
                    LOGGER.debug("segmentSource : {}", segmentSource);
                    scoreDeviceIntoATRankedSegment(segmentSource, context);
                } else {
                    LOGGER.debug("ignoring the AT events because it only occurred {} times in a day", histories.size());
                }
            });

        });
    }

    private Map<Instant, Map<Segment, List<DeviceSegmentHistory>>> groupEventsBasedOnDays(
            List<DeviceSegmentHistory> allActionTakersForDevice) {


        List<Segment> allActionTakersSegmentForDevice = allActionTakersForDevice.stream()
                .map(DeviceSegmentHistory::getSegment).collect(Collectors.toList());

        Map<Instant, Map<Segment, List<DeviceSegmentHistory>>> daysToActionTakers = new HashMap<>();
        allActionTakersForDevice.forEach(newEvent -> {

            Instant day = Instant.ofEpochMilli(newEvent.getSegment().getDateCreatedInMillis())
                    .truncatedTo(ChronoUnit.DAYS);
            daysToActionTakers.computeIfAbsent(day, e -> new HashMap<>());


            daysToActionTakers.computeIfPresent(day, (instant, currentDayEvents) -> {
                Objects.requireNonNull(newEvent.getSegment().getId());
                currentDayEvents.computeIfAbsent(newEvent.getSegment(), s -> new ArrayList<>());
                currentDayEvents.get(newEvent.getSegment()).add(newEvent);
                return currentDayEvents;
            });

        });

        LOGGER.debug("daysToActionTakers : {}", daysToActionTakers);
        return daysToActionTakers;
    }

    private void scoreDeviceIntoATRankedSegment(Segment segmentSource,
                                                ScorerContext context) {

        Segment segment = convertToSegment(segmentSource);

        context.addSegmentsForDevice(segment);
        metricReporterService.getCounter(
                "qualified_at_ranked_audience",
                ImmutableMap.of(
                        "tag_id", segment.getId()
                )).inc();

        LOGGER.debug("segment created for device : {}, segment : {}", context.getDevice(), segment);
    }

    private Segment convertToSegment(Segment segmentSource) {
        Segment segment = new SegmentBuilder()
                .withDateCreatedInMillis(clock.millis())
                .withName(segmentSource.getName() + SegmentSubType.RANKED)
                .withType(segmentSource.getType())
                .withSubType(SegmentSubType.RANKED) // we create a HERE_NOW or once subtype
                .withSourceEntityId(segmentSource.getSourceEntityId())
                .withMetaData(segmentSource.getMetaData())
                .withSourceType(segmentSource.getSourceType()).build();
        return segmentInsertionService.writeAndGet(segment);
    }

}

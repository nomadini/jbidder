package com.dstillery.scorer.modules;


import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.util.StringUtil.toStr;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.feature.Feature;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.featuredevice.FeatureHistory;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.scorer.main.ScorerContext;

public class CalculateScoreModule extends ScorerModule  {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalculateScoreModule.class);

    private MetricReporterService metricReporterService;

    public CalculateScoreModule(MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }

    public void process(ScorerContext context) {

        for (Map.Entry<Long, ModelRequest> modelPair : context.getAllLoadedModels().entrySet()) {
            metricReporterService.addStateModuleForEntity(
                    "SCORING_MODEL_FOR_MODEL",
                    "CalculateScoreModule");
            double scoreOfModel = calculateScoreForModel(context, modelPair.getValue());
             if (scoreOfModel != 0) {
                 context.getModelIdToScoreMap().compute(modelPair.getValue().getId(),
                         (integer, aDouble) -> scoreOfModel);
             } else {
                 metricReporterService.addStateModuleForEntity(
                         "SCORE_FOR_MODEL_IS_ZERO",
                         "CalculateScoreModule");
             }
        }
    }

    private Map<String, Feature> getFeaturesAsMap(List<FeatureHistory> features) {
        Map<String, Feature> featureMap = new HashMap<>();
        for (FeatureHistory feature : features) {
            featureMap.put(feature.getFeature().getName(), feature.getFeature());
        }
        return featureMap;
    }

    private double calculateScoreForModel(ScorerContext context, ModelRequest model) {

        double sumOfScore = 0;

        Map<String, Feature> featureHistoryMap = getFeaturesAsMap(
                context.getDeviceFeatureHistory().stream().map(
                        DeviceFeaturePair::getFeatureHistory)
                .collect(Collectors.toList()));

        Map<String, Double> featureScoreMap = model.getModelResult().getTopFeatureScoreMap();

        if (featureScoreMap.isEmpty()) {
            featureScoreMap = model.getModelResult().getFeatureScoreMap();
        }
        boolean atLeastOneFeatureWasFound = false;

        LOGGER.trace("featureHistoryMap : {}", featureHistoryMap);

        for (Map.Entry<String, Double> entry : featureScoreMap.entrySet()) {
            LOGGER.trace("entry : {} :", entry);
            if (featureHistoryMap.containsKey(entry.getKey())) {
                Double scoreOfTopFeature = entry.getValue();
                LOGGER.trace("feature found and score is gt than zero, adding to sum, " +
                        " feature : {} , score : {} ", entry.getKey(), scoreOfTopFeature);

                sumOfScore += scoreOfTopFeature;
                atLeastOneFeatureWasFound = true;
            }
        }

        if (!atLeastOneFeatureWasFound) {
            metricReporterService.addStateModuleForEntity(
                    "SCORING_DEVICE_ID_FAILED",
                    "CalculateScoreModule",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.IMPORTANT);
            LOGGER.trace("featureHistoryMap : {}", featureHistoryMap);
            LOGGER.trace("featureScoreMap : {}", featureScoreMap);
            return 0;
        }

        LOGGER.trace("deviceId : {} , deviceType : {} , sumOfScore : {}",
                context.getDeviceFeatureHistory().get(0).getDevice().getDeviceId(),
                context.getDeviceFeatureHistory().get(0).getDevice().getDeviceType(),
                sumOfScore);

        if (atLeastOneFeatureWasFound) {
            metricReporterService.addStateModuleForEntity(
                    "SCORING_DEVICE_ID_SUCCESS",
                    "CalculateScoreModule",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.IMPORTANT);

        }
        return sumOfScore;

    }
}

package com.dstillery.scorer.modules;

import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.recency.LastTimeSeenSource;
import com.dstillery.scorer.main.ScorerContext;

public class ScoredInLastNMinutesModule extends ScorerModule  {

    private int intervalOfScoringADeviceInSeconds;

    private MetricReporterService metricReporterService;
    private LastTimeSeenSource lastTimeSeenSource;

    public ScoredInLastNMinutesModule(int intervalOfScoringADeviceInSeconds,
                                      MetricReporterService metricReporterService,
                                      LastTimeSeenSource lastTimeSeenSource) {
        this.intervalOfScoringADeviceInSeconds = intervalOfScoringADeviceInSeconds;
        this.metricReporterService = metricReporterService;;
        this.lastTimeSeenSource = lastTimeSeenSource;
    }

    public void process(ScorerContext context) {

        if (lastTimeSeenSource.hasSeenInLastXSeconds(
                context.getTransAppMessage().getDevice())) {
            context.stopProcessing(this.getClass().getSimpleName());
            metricReporterService.addStateModuleForEntity(
                    "SCORED_RECENTLY_SKIPPING",
                    "ScoredInLastNMinutesModule",
                    ALL_ENTITIES,
                    MetricReporterService.MetricPriority.IMPORTANT);
        } else {
            lastTimeSeenSource.markAsSeenInLastXSeconds(
                    context.getTransAppMessage().getDevice(),
                    intervalOfScoringADeviceInSeconds);
        }
    }
}

package com.dstillery.scorer.modules.segment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modelScore.ModelScore;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.util.GUtil;
import com.dstillery.scorer.main.ScorerContext;
import com.dstillery.scorer.modules.ScorerModule;

/*
  SegmentCreatorModule.h

   Created on: Oct 29, 2015
       Author: mtaabodi
*/
public class SegmentCreatorModule extends ScorerModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(SegmentCreatorModule.class);
    public static final int SCORE_MULTIPLIER = 1000;

    private ScoreToSegmentConverter scoreToSegmentConverter;
    private CassandraService<ModelScore> modelScoreCassandraService;
    private MetricReporterService metricReporterService;
    private Map<Long, List<Double>> modelTemporaryScoresMap = new ConcurrentHashMap<>();
    // we use our own internal privateMetricRegistry, because we don't histogram to be reset every n seconds when reporting metric is done
    private MetricRegistry privateMetricRegistry = new MetricRegistry();
    private SegmentInsertionService segmentInsertionService;
    private int ignoreFirstNScoresBeforeScoringAModel;

    public SegmentCreatorModule(ScoreToSegmentConverter scoreToSegmentConverter,
                                CassandraService<ModelScore> modelScoreCassandraService,
                                SegmentInsertionService segmentInsertionService,
                                MetricReporterService metricReporterService,
                                int ignoreFirstNScoresBeforeScoringAModel) {
        this.ignoreFirstNScoresBeforeScoringAModel = ignoreFirstNScoresBeforeScoringAModel;
        this.scoreToSegmentConverter = scoreToSegmentConverter;
        this.modelScoreCassandraService = modelScoreCassandraService;
        this.metricReporterService = metricReporterService;
        this.segmentInsertionService = segmentInsertionService;
    }

    public void process(ScorerContext context) {
        LOGGER.trace("context.getAllLoadedModels().entrySet() {} ", context.getAllLoadedModels().entrySet());
        for (Map.Entry<Long, ModelRequest> modelPair : context.getAllLoadedModels().entrySet()) {
            Double score = context.getModelIdToScoreMap().get(modelPair.getKey());
            LOGGER.trace("model {} has score {} ", modelPair.getKey(), score);
            if (score != null) {
                ModelRequest model = modelPair.getValue();
                createSegmentAndPersist(context, score, model);
            } else {
                metricReporterService.addStateModuleForEntity(
                        "SCORE_NOT_EXISTS_FOR_MODEL",
                        "SegmentCreatorModule");
            }
        }
    }

    private void createSegmentAndPersist(ScorerContext context, Double score, ModelRequest model) {
        Histogram modelScoreLevel = privateMetricRegistry.histogram("score_model_" + model.getId());
        LOGGER.trace("scoring model : {}", model.getId());
        modelScoreLevel.update((int) (score * SCORE_MULTIPLIER));
        if (modelScoreLevel.getCount() < ignoreFirstNScoresBeforeScoringAModel) {
            //too few samples to create segment from scores
            GUtil.callNThTime(1000, (s) -> LOGGER.warn("too few samples to create segment from scores {} for model : {}",
                    modelScoreLevel.getCount(), model.getId()));
            saveTheScoreForLaterSegmenting(score, model);
            return;
        }
        if (ignoreFirstNScoresBeforeScoringAModel > 0) {
            createSegmentBasedOnTempScores(context, model, modelScoreLevel);
        }
        createSegment(context, score, model, modelScoreLevel);
    }

    private void saveTheScoreForLaterSegmenting(Double score, ModelRequest model) {
        modelTemporaryScoresMap.computeIfAbsent(model.getId(), s -> new ArrayList<>());
        modelTemporaryScoresMap.computeIfPresent(model.getId(), (id, scores) -> {
            scores.add(score);
            return scores;
        });
    }

    private void createSegmentBasedOnTempScores(ScorerContext context, ModelRequest model, Histogram modelScoreLevel) {
        //now that model has enough scores. we create segment based on temp scores and clear them
        List<Double> scores = modelTemporaryScoresMap.get(model.getId());
        if (scores != null) {
            scores.forEach(tempScore -> {
                createSegment(context, tempScore, model, modelScoreLevel);
            });
            modelTemporaryScoresMap.get(model.getId()).clear();
        }
    }

    private void createSegment(ScorerContext context, Double score, ModelRequest model, Histogram modelScoreLevel) {
        Optional<Segment> segment = scoreToSegmentConverter.toSegment(score, model, modelScoreLevel);
        segment.ifPresent(seg -> {
            seg = segmentInsertionService.writeAndGet(seg);
            context.addSegmentsForDevice(seg);
        });
    }

    public void setIgnoreFirstNScoresBeforeScoringAModel(int ignoreFirstNScoresBeforeScoringAModel) {
        this.ignoreFirstNScoresBeforeScoringAModel = ignoreFirstNScoresBeforeScoringAModel;
    }
}

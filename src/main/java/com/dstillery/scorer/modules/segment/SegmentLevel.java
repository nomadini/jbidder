package com.dstillery.scorer.modules.segment;

public enum SegmentLevel {
    AAAA,
    AAA,
    AA,
    A,
    B,
    C,
    D
}

package com.dstillery.scorer.modules.segment;

import static com.dstillery.scorer.modules.segment.SegmentCreatorModule.SCORE_MULTIPLIER;
import static com.dstillery.scorer.modules.segment.SegmentLevel.A;
import static com.dstillery.scorer.modules.segment.SegmentLevel.AA;
import static com.dstillery.scorer.modules.segment.SegmentLevel.AAA;
import static com.dstillery.scorer.modules.segment.SegmentLevel.AAAA;
import static com.dstillery.scorer.modules.segment.SegmentLevel.B;
import static com.dstillery.scorer.modules.segment.SegmentLevel.C;
import static com.dstillery.scorer.modules.segment.SegmentLevel.D;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Histogram;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.google.common.collect.ImmutableMap;

public class SegmentClassDecider {

    private static final Logger LOGGER = LoggerFactory.getLogger(SegmentClassDecider.class);
    private MetricReporterService metricReporterService;

    public SegmentClassDecider(
            MetricReporterService metricReporterService) {
        this.metricReporterService = metricReporterService;
    }

    public SegmentClass calculateSegmentClass(double score, Histogram modelScoreLevel, ModelRequest model) {
        double percentageClass = 0;
        SegmentLevel segmentLevel = null;
        score = score * SCORE_MULTIPLIER;
        for (int i = 0; i <= 100; i = i + 5) {
            double iPercentile = (double) i / 100;
            double iPercentileValue = modelScoreLevel.getSnapshot().getValue(iPercentile);
            LOGGER.trace("score : {}, iPercentile : {} for iPercentileValue : {}",
                    score, iPercentile, iPercentileValue);
            if (score > iPercentileValue) {
                //score belongs to higher percentile
                if (i == 100) {
                    percentageClass = 99;
                    segmentLevel = AAAA;
                }
            } else {
                percentageClass = (100 - i - 5);
                if (percentageClass <= 0) {
                    percentageClass = 1;
                }
                if (percentageClass > 95 && percentageClass <=98) {
                    segmentLevel = AAA;
                } else if (percentageClass > 90 && percentageClass <=95) {
                    segmentLevel = AA;
                } else if (percentageClass > 80 && percentageClass <=90) {
                    segmentLevel = A;
                } else if (percentageClass > 70 && percentageClass <=80) {
                    segmentLevel = B;
                } else if (percentageClass > 60 && percentageClass <=70) {
                    segmentLevel = C;
                } else if (percentageClass > 50 && percentageClass <=60) {
                    segmentLevel = D;
                }
                break;
            }
        }
        metricReporterService.getHistogram("segment_level_for_model", ImmutableMap.of("model_id", model.getId(),
                "level", segmentLevel == null ? "NONE" : segmentLevel));
        return new SegmentClass(segmentLevel, percentageClass);
    }
}

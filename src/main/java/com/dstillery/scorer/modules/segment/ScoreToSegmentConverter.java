package com.dstillery.scorer.modules.segment;/*
  ScoreToSegmentConverter.h
 *
   Created on: Aug 1, 2015
       Author: mtaabodi
 */

import static com.dstillery.common.util.DateTimeUtil.getNowInMilliSecond;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Histogram;
import com.dstillery.common.cache.AuditedEntity;
import com.dstillery.common.cache.SimpleEntityCacheService;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.interest.Interest;
import com.dstillery.common.interest_model.InterestModel;
import com.dstillery.common.interest_model.InterestModelCacheService;
import com.dstillery.common.interest_model.MySqlInterestModelService;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.segment.ProspectOnceMetaData;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.segment.SegmentBuilder;
import com.dstillery.common.segment.SegmentMetaData;
import com.dstillery.common.segment.SegmentNameCreator;
import com.dstillery.common.segment.SegmentSourceType;
import com.dstillery.common.segment.SegmentSubType;
import com.dstillery.common.segment.SegmentType;
import com.google.common.collect.ImmutableMap;

public class ScoreToSegmentConverter  {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScoreToSegmentConverter.class);
    private MetricReporterService metricReporterService;
    private SegmentClassDecider segmentClassDecider;
    private InterestModelCacheService interestModelCacheService;
    private CacheService<Interest> interestCacheService;

    public ScoreToSegmentConverter(MetricReporterService metricReporterService,
                                   SegmentClassDecider segmentClassDecider,
                                   InterestModelCacheService interestModelCacheService,
                                   CacheService<Interest> interestCacheService) {
        this.metricReporterService = metricReporterService;
        this.segmentClassDecider = segmentClassDecider;
        this.interestModelCacheService = interestModelCacheService;
        this.interestCacheService = interestCacheService;

    }

    public Optional<Segment> toSegment(
            double score,
            ModelRequest model,
            Histogram modelScoreLevel) {

        SegmentClass segmentClass = segmentClassDecider.calculateSegmentClass(score, modelScoreLevel, model);
        if (segmentClass.getSegmentLevel() == null) {
            metricReporterService.getCounter("empty_segment_level").inc();
            return Optional.empty();
        }
        SegmentMetaData segmentMetaData = new SegmentMetaData();
        segmentMetaData.setProspectOnceMetaData(new ProspectOnceMetaData(
                segmentClass.getSegmentLevel().name(), segmentClass.getPercentageClass()));

        InterestModel interestModel = interestModelCacheService.getModelIdToInterestModel().get(model.getId());
        if (interestModel != null) {
            LOGGER.debug("found interestModel : {}", interestModel);
            Interest interest = interestCacheService.getAllEntitiesMap().get(interestModel.getInterestId());
            metricReporterService.getCounter("interest_scored", ImmutableMap.of("id", interest.getId())).inc();
            Segment segment = convertToSegment(interest);
            LOGGER.debug("interest based segment got created : {}", segment);
            return Optional.of(segment);
        }

        return Optional.of(new SegmentBuilder()
                .withDateCreatedInMillis(getNowInMilliSecond())
                .withName(
                        SegmentNameCreator.createName(
                                model.getSegmentSeedName(),
                                segmentClass.getSegmentLevel().name(),
                                SegmentType.PROSPECT))
                .withType(SegmentType.PROSPECT)

                .withMetaData(segmentMetaData)
                .withSubType(SegmentSubType.ONCE)
                .withSourceEntityId(model.getId())
                .withSourceType(SegmentSourceType.MODEL).build());
    }

    private Segment convertToSegment(Interest interest) {
        return new SegmentBuilder()
                .withDateCreatedInMillis(getNowInMilliSecond())
                .withName(
                        SegmentNameCreator.createName(
                                interest.getName(),
                                "",
                                SegmentType.NOMADINI_AUDIENCE))
                .withType(SegmentType.NOMADINI_AUDIENCE)
                .withSubType(SegmentSubType.ONCE)
                .withSourceEntityId(interest.getId())
                .withSourceType(SegmentSourceType.INTEREST).build();
    }

    private Supplier<Map<Long, InterestModel>> getInterestIdToModelIdMapperSupplier(
            MySqlInterestModelService mySqlInterestModelService) {
        return () -> mySqlInterestModelService.readAll()
                .stream()
                .collect(Collectors.toMap(
                        InterestModel::getInterestId, entity1 -> entity1));
    }
}

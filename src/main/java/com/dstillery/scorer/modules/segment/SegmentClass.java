package com.dstillery.scorer.modules.segment;

public class SegmentClass {
    private SegmentLevel segmentLevel;
    private double percentageClass;

    public SegmentClass(SegmentLevel segmentLevel, double percentageClass) {
        this.segmentLevel = segmentLevel;
        this.percentageClass = percentageClass;
    }

    public SegmentLevel getSegmentLevel() {
        return segmentLevel;
    }

    public void setSegmentLevel(SegmentLevel segmentLevel) {
        this.segmentLevel = segmentLevel;
    }

    public double getPercentageClass() {
        return percentageClass;
    }

    public void setPercentageClass(double percentageClass) {
        this.percentageClass = percentageClass;
    }
}

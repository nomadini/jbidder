package com.dstillery.scorer.modules;

import static com.dstillery.common.util.StringUtil.toStr;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.feature.FeatureType;

import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.util.NumberUtil;
import com.dstillery.scorer.main.ScorerContext;
import com.google.common.collect.ImmutableSet;

public class DeviceFeatureFetcherModule extends ScorerModule  {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceFeatureFetcherModule.class);

    private MetricReporterService metricReporterService;

    private DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService;
    private int fetchDeviceHistoryForLastNDay;
    private AtomicLong totalFeatures = new AtomicLong();
    private AtomicLong totalDevices = new AtomicLong();

    public DeviceFeatureFetcherModule(MetricReporterService metricReporterService,
                                      DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService,
                                      int fetchDeviceHistoryForLastNDay) {
        this.metricReporterService = metricReporterService;;
        this.deviceFeatureHistoryCassandraService = deviceFeatureHistoryCassandraService;
        this.fetchDeviceHistoryForLastNDay = fetchDeviceHistoryForLastNDay;
    }

    public void process(ScorerContext context) {

        context.setDeviceFeatureHistory(deviceFeatureHistoryCassandraService.
                readFeatureHistoryOfDevice(context.getTransAppMessage().getDevice(),
                        TimeUnit.DAYS.toSeconds(fetchDeviceHistoryForLastNDay),
                        ImmutableSet.of(FeatureType.GTLD),
                        10000));
        LOGGER.trace("featureHistory size : {}: {}",
                context.getDeviceFeatureHistory().size(),
                context.getDeviceFeatureHistory());
        if (context.getDeviceFeatureHistory().isEmpty()) {
            metricReporterService.addStateModuleForEntity("NO_FEATURE_HISTORY_FOR_DEVICE",
                    "DeviceFeatureFetcherModule",
                    "ALL");
            context.stopProcessing(this.getClass().getSimpleName());
            return;
        }

        totalFeatures.addAndGet(context.getDeviceFeatureHistory().size());
        totalDevices.getAndIncrement();

        if (totalDevices.get() % 10000 == 1) {
            long avgFeatureSizeForDevice =
                    totalFeatures.get() / totalDevices.get();

            metricReporterService.addStateModuleForEntity(
                    "size=" + toStr(
                            NumberUtil.getNearestMultipleToNumber(avgFeatureSizeForDevice, 20)),
                    "DeviceFeatureFetcherModule",
                    "AVG_FEATURE_HISTORY_SIZE_FOR_DEVICE");
            totalDevices.set(0);
            totalFeatures.set(0);
        }

    }
}


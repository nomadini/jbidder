package com.dstillery.scorer.main;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.dstillery.common.metric.dropwizard.MetricReporterService;

public class ScorerHealthService  {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScorerHealthService.class);

    private MetricReporterService metricReporterService;

    private AtomicLong consumerTimeout;
    private AtomicLong consumingMsgFailed;
    private AtomicLong consumerReadMessage;
    private AtomicLong endOfPartitionReached;
    private AtomicLong numberOfExceptionsInScorerMessageProcessor;
    private AtomicBoolean scorerHealthFlag;
    private AtomicLong lastOffsetRead;
    private AtomicLong numberOfExceptionsInScorerMessageProcessorLimit;

    public ScorerHealthService(MetricReporterService metricReporterService,
                               AtomicLong consumerTimeout,
                               AtomicLong consumingMsgFailed,
                               AtomicLong consumerReadMessage,
                               AtomicLong endOfPartitionReached,
                               AtomicLong numberOfExceptionsInScorerMessageProcessor,
                               AtomicBoolean scorerHealthFlag,
                               AtomicLong lastOffsetRead,
                               AtomicLong numberOfExceptionsInScorerMessageProcessorLimit) {
        this.metricReporterService = metricReporterService;;
        this.consumerTimeout = consumerTimeout;
        this.consumingMsgFailed = consumingMsgFailed;
        this.consumerReadMessage = consumerReadMessage;
        this.endOfPartitionReached = endOfPartitionReached;
        this.numberOfExceptionsInScorerMessageProcessor = numberOfExceptionsInScorerMessageProcessor;
        this.scorerHealthFlag = scorerHealthFlag;
        this.lastOffsetRead = lastOffsetRead;
        this.numberOfExceptionsInScorerMessageProcessorLimit = numberOfExceptionsInScorerMessageProcessorLimit;
    }

    public void determineHealth() {

        LOGGER.trace("consumerTimeout : {}, consumingMsgFailed : {} consumerReadMessage : {}" +
                        " numberOfExceptionsInScorerMessageProcessor : {}, lastOffsetRead : {}," +
                        " endOfPartitionReached : {}", consumerTimeout.get()
                , consumingMsgFailed.get(),
                consumerReadMessage.get(),
                lastOffsetRead.get(),
                numberOfExceptionsInScorerMessageProcessor.get(),
                endOfPartitionReached.get());

        if (numberOfExceptionsInScorerMessageProcessor.get() > numberOfExceptionsInScorerMessageProcessorLimit.get()) {
            LOGGER.error("too many exceptions : {} stopping consumer now", numberOfExceptionsInScorerMessageProcessor.get());
            scorerHealthFlag.set(false);
        } else {
            scorerHealthFlag.set(true);
        }
        // if (queueOfContexts.size() > 1000 ) {
        //         ////LOGGER.debug("size of async job queue is too Large: {} ",  queueOfContexts.size();
        //         String reason = "AsyncPipelineIsSlow queueSize : :" + toStr(queueOfContexts.size());
        //         reasons.add(reason);
        //         bidMode.setStatus("NO_BID", "ScorerHealthService",reason);
        // }
        //
        // ////LOG_EVERY_N(ERROR, 1000) ," kafka queueSizeWaitingForAck : " , queueSizeWaitingForAck.get() , std.endl;
        //
        //
        // if (queueSizeWaitingForAck.get() > 1000) {
        //         //         String reason = "EnqueueForScoringModule_queueSizeWaitingForAck:" + toStr(queueSizeWaitingForAck.get());
        //         //         reasons.add(reason);
        //         //         bidMode.setStatus("NO_BID", "ScorerHealthService",reason);
        //         ////LOGGER.debug("kafka queueSizeWaitingForAck : " , queueSizeWaitingForAck.get();
        // }
        //
        // //set to DO_BID, if none of NO_BID conditions are true
        // if (!equalsIgnoreCase(bidMode.getStatus(), "NO_BID")) {
        //         bidMode.setStatus("DO_BID", "ScorerHealthService", "ALL_IN_GOOD_CONDITIONS_BEFORE_EVALUATION");
        // } else {
        //         String allReasons = JsonArrayUtil.convertListToJson(reasons);
        //         bidMode.setStatus("NO_BID", "ScorerHealthService", allReasons);
        //         ////LOGGER.debug("bidder is NO_BID_MODE, reasons : " , allReasons;
        // }
        //
        //
        // return bidMode;
    }

}

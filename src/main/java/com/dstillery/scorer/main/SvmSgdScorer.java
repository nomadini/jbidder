package com.dstillery.scorer.main;/*
  h
 *
   Created on: Jul 16, 2015
       Author: mtaabodi
 */

import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.modeling.predictive.ModelRequest;

import java.util.Set;

public class SvmSgdScorer {

    private DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService;

    public SvmSgdScorer(
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService) {
        this.deviceFeatureHistoryCassandraService = deviceFeatureHistoryCassandraService;
    }

    double calculateTheScoreOfBrowserBasedOnModel(
            Set<String> features, ModelRequest model) {

        double sumOfFeaturesScores = 0;

        for (String iterator : features) {

            Double feaureScorePair = model.getModelResult().getFeatureScoreMap().get(iterator);
            if (feaureScorePair != null) {

                //LOGGER.debug("feature ",feaureScorepair.getKey()," was found with score ",feaureScorepair.getValue();

                sumOfFeaturesScores += feaureScorePair;
            } else {
                //LOGGER.warn("feature ",*featureIter," was not found to score ";
            }

        } //end of summing up the feature scores for an offerId
        if (sumOfFeaturesScores != 0) {
            //LOGGER.debug("sumOfFeaturesScores : {} ", sumOfFeaturesScores;
        }
        return sumOfFeaturesScores;

    }

}

package com.dstillery.scorer.main.asynctasks;

import static com.dstillery.common.util.Constants.OBJECT_MAPPER;
import static com.dstillery.common.util.StringUtil.assertAndThrow;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.util.DateTimeUtil;
import com.dstillery.common.util.FileNomadiniUtil;
import com.dstillery.scorer.modules.models.ModelScoreLevel;
import com.dstillery.scorer.modules.models.ModelScoreLevelContainer;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HourlyTask implements Runnable {
    public static final ObjectMapper MAPPER = new ObjectMapper();
    private static final Logger LOGGER = LoggerFactory.getLogger(HourlyTask.class);
    private Map<Long, ModelScoreLevelContainer> modelIdToModelScoreLevelMap;

    private MetricReporterService metricReporterService;

    public HourlyTask(MetricReporterService metricReporterService,
                      Map<Long, ModelScoreLevelContainer> modelIdToModelScoreLevelMap) {
        this.modelIdToModelScoreLevelMap = modelIdToModelScoreLevelMap;
        this.metricReporterService = metricReporterService;;
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0, 1, TimeUnit.HOURS);
    }

    @Override
    public void run() {

        try {
            refreshFiles("model_score_distribution_last7days");

            metricReporterService.addStateModuleForEntity("runEveryHour",
                    "ScorerAsyncJobsService",
                    "ALL");
        } catch (RuntimeException e) {
            //GUtil.showStackTrace();
            //TODO : go red and show in dashboard that this is red
            //LOGGER.error("error happening when runEveryHour ", e);
        }
    }

    private void refreshFiles(String fileName) {
        // String fileName = "/tmp/top_common_devices";
        String fileFullName = "/tmp/" + fileName + ".txt";
        if (!FileNomadiniUtil.checkIfFileExists(fileFullName)
                || DateTimeUtil.getNowInSecond() - FileNomadiniUtil.getLastModifiedInSeconds(fileFullName) > 600) {
            //LOGGER.debug("downling file from server : {} ",  fileFullName;
            String url = "scp://67.205.170.169/tmp/" + fileName + "/part-00000";
            if (FileNomadiniUtil.checkIfFileExists(fileFullName)) {
                FileNomadiniUtil.deleteFile(fileFullName);
            }
            LOGGER.info("reloading banned devices feature is not working as expected");
//            HttpUtil.downloadFileUsingScpWithCurl(url, fileFullName);
        }

        String modelDistributionsInJson = FileNomadiniUtil.readFileInString(fileFullName);

        /*
           {"5":[85.81,143.43750000000003,201.06500000000005,258.69250000000005,316.32000000000005,373.9475000000001,431.5750000000001,489.20250000000016,546.8300000000002],"7":[0.0,6.7375,13.475,20.2125,26.95,33.6875,40.425,47.1625,53.9],"8":[0.0,8.05125,16.1025,24.15375,32.205,40.256249999999994,48.3075,56.35875,64.41],"9":[6.64,23.24,39.839999999999996,56.44,73.03999999999999,89.63999999999999,106.24,122.83999999999999,139.43999999999997]}
         */
        readMapFromJsonString(modelDistributionsInJson);
    }

    private void readMapFromJsonString(String jsonString) {
        Map<String, List<Double>> rawMap;
        try {
            rawMap = OBJECT_MAPPER.readValue(jsonString, Map.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        for (Map.Entry<String, List<Double>> entry : rawMap.entrySet())
        {

            Long modelId = Long.parseLong(entry.getKey());
            ModelScoreLevelContainer container = new ModelScoreLevelContainer();

            List<Double> values = entry.getValue();
            double levelPercentage = 100 / values.size();
            double beginningOfWindow = values.get(0);
            for (int i = 0; i < values.size(); i++) {

                ModelScoreLevel modelScoreLevel = new ModelScoreLevel();
                modelScoreLevel.setMinScoreOfLevel(beginningOfWindow);
                double endOfWindow = beginningOfWindow * 100;
                if ( i + 1 < values.size()) {
                    //next value exists
                    Double nextValue = values.get(i+1);
                    endOfWindow = nextValue;
                    //this is to make sure array is sorted as expected
                    assertAndThrow(endOfWindow >= beginningOfWindow);
                }
                modelScoreLevel.setMaxScoreOfLevel(endOfWindow);
                modelScoreLevel.setLevelName(levelPercentage * (i + 1) + "%");
                container.getModelScoreLevels().add(modelScoreLevel);
            }

            modelIdToModelScoreLevelMap.put(modelId, container);
        }

        //LOGGER.error("modelIdToModelScoreLevelMap : {} ",
        //JsonMapUtil.convertMapToJsonArray (*modelIdToModelScoreLevelMap);

    }

}

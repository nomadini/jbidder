package com.dstillery.scorer.main.asynctasks;


import com.dstillery.common.metric.dropwizard.MetricReporterService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MinutelyTask implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(MinutelyTask.class);
    private MetricReporterService metricReporterService;

    public MinutelyTask(
            MetricReporterService metricReporterService) {
       this.metricReporterService = metricReporterService;;
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this, 0, 1, TimeUnit.MINUTES);
    }

    @Override
    public void run() {

        try {

            metricReporterService.addStateModuleForEntity("runEveryMinute",
                    "ScorerAsyncJobsService",
                    "ALL");
        } catch (RuntimeException e) {
            //GUtil.showStackTrace();
            //TODO : go red and show in dashboard that this is red
            //LOGGER.error("error happening when runEveryMinute ", e);
        }
    }
}

package com.dstillery.scorer.main;

import java.time.Clock;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dstillery.bidder.modules.TransAppBuilderService;
import com.dstillery.common.ClientAppDataReloadService;
import com.dstillery.common.adhistory.AdEntryDevicePair;
import com.dstillery.common.aerospike.AeroSpikeDriver;
import com.dstillery.common.cache_realtime.RealTimeEntityCacheService;
import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.interest_model.InterestModelCacheService;
import com.dstillery.common.kafka.KafkaConsumerService;
import com.dstillery.common.PlacesCachedResult;
import com.dstillery.common.audience.AudienceRow;
import com.dstillery.common.cache.datamaster.CacheService;
import com.dstillery.common.cache_realtime.AeroCacheService;
import com.dstillery.common.common_modules.CrossWalkReaderModule;
import com.dstillery.common.eventlog.UniqueDeviceBidEventLog;
import com.dstillery.common.geo.PlaceTreeContainer;
import com.dstillery.common.geo.places.PlaceFinderModule;
import com.dstillery.common.interest.Interest;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modelScore.ModelScore;
import com.dstillery.common.modeling.SegmentToDeviceCassandraService;
import com.dstillery.common.modeling.devicefeature.DeviceFeatureHistoryCassandraService;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistoryCassandraService;
import com.dstillery.common.modeling.predictive.ModelCacheService;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.modeling.predictive.ModelResultCacheService;
import com.dstillery.common.segment.SegmentCacheService;
import com.dstillery.common.segment.SegmentInsertionService;
import com.dstillery.common.recency.LastTimeSeenSource;
import com.dstillery.common.service.dicovery.DiscoveryService;
import com.dstillery.scorer.main.asynctasks.HourlyTask;
import com.dstillery.scorer.main.asynctasks.MinutelyTask;
import com.dstillery.scorer.message_processing.ScoringMessageProcessor;
import com.dstillery.scorer.modules.CalculateScoreModule;
import com.dstillery.scorer.modules.DeviceFeatureFetcherModule;
import com.dstillery.scorer.modules.DeviceSegmentHistoryFetcherModule;
import com.dstillery.scorer.modules.ScoreATRankedAudiencesModule;
import com.dstillery.scorer.modules.ScoreCompoundAudiencesModule;
import com.dstillery.scorer.modules.ScoreLocationPlaceTagHereNowModule;
import com.dstillery.scorer.modules.ScoreLocationPlaceTagHomeModule;
import com.dstillery.scorer.modules.ScoredInLastNMinutesModule;
import com.dstillery.scorer.modules.compoundaudience.AudienceGroupQualifier;
import com.dstillery.scorer.modules.compoundaudience.AudienceRowQualifier;
import com.dstillery.scorer.modules.compoundaudience.matchers.DeviceClassMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.DeviceTypeMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.EntityMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.EnvironmentTypeMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.FirstPartySegmentMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.GpsCoordinatesMatcher;
import com.dstillery.scorer.modules.compoundaudience.matchers.InterestMatcher;
import com.dstillery.scorer.modules.models.ModelScoreLevelContainer;
import com.dstillery.scorer.modules.persistence.CrossWalkDeviceSegmentPersistenceModule;
import com.dstillery.scorer.modules.persistence.DeviceSegmentPersistenceModule;
import com.dstillery.scorer.modules.persistence.SegmentToDevicePersistenceModule;
import com.dstillery.scorer.modules.segment.ScoreToSegmentConverter;
import com.dstillery.scorer.modules.segment.SegmentClassDecider;
import com.dstillery.scorer.modules.segment.SegmentCreatorModule;
import com.google.common.eventbus.EventBus;

@Configuration
public class ScorerConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScorerConfig.class);

    private AtomicBoolean scorerHealthFlag = new AtomicBoolean(false);
    private AtomicBoolean isMessageProcessorReadyToProcess = new AtomicBoolean(false);
    private AtomicLong numberOfExceptionsInScorerMessageProcessor = new AtomicLong();

    private AtomicLong numberOfExceptionsInWritingEvents = new AtomicLong();
    private AtomicLong numberOfBidRequestRecievedPerMinute = new AtomicLong();
    private AtomicLong numberOfBidsForPixelMatchingPurposePerHour = new AtomicLong();
    private AtomicLong consumerTimeout = new AtomicLong();
    private AtomicLong consumingMsgFailed = new AtomicLong();
    private AtomicLong consumerReadMessage = new AtomicLong();
    private AtomicLong endOfPartitionReached = new AtomicLong();
    private AtomicLong lastOffsetRead = new AtomicLong();
    private AtomicLong numberOfExceptionsInScorerMessageProcessorLimit = new AtomicLong();
    private Map<Long, ModelRequest> allLoadedModels = new ConcurrentHashMap<>();
    private Map<Long, ModelScoreLevelContainer> modelIdToModelScoreLevelMap = new ConcurrentHashMap<>();

    @Autowired
    private ConfigService configService;

    @Autowired
    private MetricReporterService metricReporterService;

    @Autowired
    private DiscoveryService discoveryService;

    @Autowired
    private String appName;

    private String dataMasterUrl = ""; // we set this to empty. so discoveryService, will do its job of getting dataMaster url each time

    @Bean
    public SegmentCacheService segmentCacheService() {
        return new SegmentCacheService(
                discoveryService,
                dataMasterUrl,
                metricReporterService,
                appName
        );
    }

    @Bean
    public CacheService<AudienceRow> audienceCacheService() {
        return new CacheService<>(
                discoveryService,
                AudienceRow.class,
                dataMasterUrl,
                metricReporterService,
                appName
        );
    }
    @Bean
    public ScorerHealthService scorerHealthService(MetricReporterService metricReporterService) {
        numberOfExceptionsInScorerMessageProcessorLimit.set(
                configService.getAsInt("numberOfExceptionsInScorerMessageProcessorLimit"));
        return new ScorerHealthService(
                metricReporterService,
                consumerTimeout,
                consumingMsgFailed,
                consumerReadMessage,
                endOfPartitionReached,
                numberOfExceptionsInScorerMessageProcessor,
                scorerHealthFlag,
                lastOffsetRead,
                numberOfExceptionsInScorerMessageProcessorLimit
        );
    }

    @Bean
    public DeviceFeatureFetcherModule deviceFeatureFetcherModule(
            ConfigService configService,
            DeviceFeatureHistoryCassandraService deviceFeatureHistoryCassandraService) {

        //this is to make sure cassandra is up and running
        try {
            deviceFeatureHistoryCassandraService.readWithPaging(null, deviceFeatureHistory -> {});
        } catch (Exception e) {
            LOGGER.error("exiting because cassandra is not running., exception", e);
            System.exit(1);
        }

        return new DeviceFeatureFetcherModule(
                metricReporterService,
                deviceFeatureHistoryCassandraService,
                configService.getAsInt("scoring.fetchDeviceHistoryForLastNDay")
        );
    }

    @Bean
    public DeviceSegmentHistoryFetcherModule deviceSegmentHistoryFetcherModule(
            MetricReporterService metricReporterService,
            DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService) {

        //this is to make sure cassandra is up and running
        try {
            deviceSegmentHistoryCassandraService.readWithPaging(null, deviceSegmentHistory -> {});
        } catch (Exception e) {
            LOGGER.error("exiting because cassandra is not running., exception", e);
            System.exit(1);
        }

        return new DeviceSegmentHistoryFetcherModule(
                metricReporterService,
                deviceSegmentHistoryCassandraService,
                configService.getAsInt("AtRankedAudience.fetchDeviceSegmentHistoryInPastNDays")
        );
    }

    @Bean
    public SegmentToDevicePersistenceModule segmentToDevicePersistenceModule(
            SegmentToDeviceCassandraService segmentToDeviceCassandraService) {
        return new SegmentToDevicePersistenceModule(
                metricReporterService,
                segmentToDeviceCassandraService
        );
    }

    @Bean
    public CrossWalkDeviceSegmentPersistenceModule crossWalkDeviceSegmentPersistenceModule(
            CrossWalkReaderModule crossWalkReaderModule,
            DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService,
            SegmentInsertionService segmentInsertionService) {
        return new CrossWalkDeviceSegmentPersistenceModule(metricReporterService,
                crossWalkReaderModule,
                deviceSegmentHistoryCassandraService,
                segmentInsertionService);
    }

    @Bean
    public DeviceSegmentPersistenceModule deviceSegmentPersistenceModule(

            DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService) {
        return new DeviceSegmentPersistenceModule(metricReporterService,
                deviceSegmentHistoryCassandraService);

    }

    @Bean
    public SegmentCreatorModule segmentCreatorModule(
            ScoreToSegmentConverter scoreToSegmentConverter,
            CassandraService<ModelScore> modelScoreCassandraService,
            SegmentInsertionService segmentInsertionService,
            MetricReporterService metricReporterService) {
        return new SegmentCreatorModule(
                scoreToSegmentConverter,
                modelScoreCassandraService,
                segmentInsertionService,
                metricReporterService,
                configService.getAsInt("ignoreFirstNScoresBeforeScoringAModel")
        );
    }

    @Bean
    public ScoreToSegmentConverter scoreToSegmentConverter(
            MetricReporterService metricReporterService,
            InterestModelCacheService interestModelCacheService,
            CacheService<Interest> interestCacheService) {

        return new ScoreToSegmentConverter(
                metricReporterService,
                new SegmentClassDecider(metricReporterService),
                interestModelCacheService,
                interestCacheService);
    }

    @Bean
    public CalculateScoreModule calculateScoreModule(MetricReporterService metricReporterService) {
        return new CalculateScoreModule(metricReporterService);
    }

    @Bean
    public DeviceClassMatcher deviceClassMatcher() {
        return new DeviceClassMatcher();
    }

    @Bean
    public DeviceTypeMatcher deviceTypeMatcher() {
        return new DeviceTypeMatcher();
    }

    @Bean
    public EnvironmentTypeMatcher environmentTypeMatcher() {
        return new EnvironmentTypeMatcher();
    }

    @Bean
    public InterestMatcher interestMatcher(CacheService<Interest> interestCacheService,
                                           SegmentCacheService segmentCacheService) {
        return new InterestMatcher(interestCacheService, segmentCacheService);
    }

    @Bean
    public FirstPartySegmentMatcher firstPartySegmentMatcher(
            SegmentCacheService segmentCacheService) {
        return new FirstPartySegmentMatcher(segmentCacheService);
    }
    @Bean
    public GpsCoordinatesMatcher gpsCoordinatesMatcher() {
        return new GpsCoordinatesMatcher();
    }

    @Bean
    public EntityMatcher entityMatcher(
            DeviceClassMatcher deviceClassMatcher,
            DeviceTypeMatcher deviceTypeMatcher,
            EnvironmentTypeMatcher environmentTypeMatcher,
            InterestMatcher interestMatcher,
            GpsCoordinatesMatcher gpsCoordinatesMatcher,
            FirstPartySegmentMatcher firstPartySegmentMatcher,
            MetricReporterService metricReporterService
    ) {
        return new EntityMatcher(
                deviceClassMatcher,
                deviceTypeMatcher,
                environmentTypeMatcher,
                interestMatcher,
                gpsCoordinatesMatcher,
                firstPartySegmentMatcher,
                metricReporterService
        );
    }

    @Bean
    public AudienceGroupQualifier audienceGroupQualifier(
            EntityMatcher entityMatcher) {
        return new AudienceGroupQualifier(entityMatcher);
    }

    @Bean
    public AudienceRowQualifier audienceRowQualifier(
            MetricReporterService metricReporterService,
            AudienceGroupQualifier audienceGroupQualifier) {
        return new AudienceRowQualifier(metricReporterService,
                                        audienceGroupQualifier);
    }

    @Bean
    public ScoreCompoundAudiencesModule scoreAudiencesModule(
            MetricReporterService metricReporterService,
            AudienceRowQualifier audienceRowQualifier,
            CacheService<AudienceRow> audienceCacheService,
            SegmentInsertionService segmentInsertionService) {
        return new ScoreCompoundAudiencesModule(
                metricReporterService,
                audienceRowQualifier,
                audienceCacheService,
                segmentInsertionService);
    }

    @Bean
    public LastTimeSeenSource lastTimeSeenSource(AeroSpikeDriver aeroSpikeDriver) {
        return new LastTimeSeenSource(
                aeroSpikeDriver,
                "rtb2",
                "usersScoredSet",
                "bidInLastN"
        );
    }
    @Bean
    public ScoredInLastNMinutesModule scoredInLastNMinutesModule(
            LastTimeSeenSource lastTimeSeenSource) {
        return new ScoredInLastNMinutesModule(
                configService.getAsInt("intervalOfScoringADeviceInSeconds"),
                metricReporterService,
                lastTimeSeenSource
        );
    }

    @Bean
    public ScorerAsyncJobsService scorerAsyncJobsService(ConfigService configService,
                                                         ModelCacheService modelCacheService,
                                                         ModelResultCacheService modelResultCacheService,
                                                         MetricReporterService metricReporterService,
                                                         ScorerHealthService scorerHealthService,
                                                         EventBus eventBus) {
        return new ScorerAsyncJobsService(
                isMessageProcessorReadyToProcess,
                allLoadedModels,
                modelCacheService,
                modelResultCacheService,
                metricReporterService,
                scorerHealthService,
                eventBus,
                scorerHealthFlag);
    }

    @Bean
    public TransAppMessageFetcherService transAppMessageFetcherService(CassandraService<UniqueDeviceBidEventLog> deviceBidEventLogCassandraService,
                                                                       ClientAppDataReloadService clientAppDataReloadService,
                                                                       ScoringMessageProcessor scoringMessageProcessor,
                                                                       RealTimeEntityCacheService<AdEntryDevicePair> adHistoryCacheService,
                                                                       MetricReporterService metricReporterService,
                                                                       KafkaConsumerService kafkaConsumerService,
                                                                       ConfigService configService,
                                                                       SegmentCreatorModule segmentCreatorModule) {
        boolean isTransAppMessageFetcherServiceEnabled = configService.getAsBoolean("testing.transAppMessageFetcherService.isEnabled");
        if (isTransAppMessageFetcherServiceEnabled) {
            scorerHealthFlag.set(true);//we set the health to true. because we read the event logs manually
            kafkaConsumerService.getDisabled().set(true);
            // we want to score everything, because we are in debugging mode
            segmentCreatorModule.setIgnoreFirstNScoresBeforeScoringAModel(0);
        }
        return new TransAppMessageFetcherService(
                new TransAppBuilderService(),
                clientAppDataReloadService,
                deviceBidEventLogCassandraService,
                scoringMessageProcessor,
                adHistoryCacheService,
                metricReporterService,
                isTransAppMessageFetcherServiceEnabled,
                configService.getAsInt("testing.transAppMessageFetcherService.fetchRecentEventsInMinutes"),
                configService.getAsInt("testing.transAppMessageFetcherService.millisToSleepBetweenInEachMessage"),
                configService.getAsInt("testing.transAppMessageFetcherService.pumpEventLogIntervalInSeconds"));
    }

    @Bean
    public KafkaConsumerService kafkaConsumerService(ScoringMessageProcessor scoringMessageProcessor) {
        AtomicBoolean disabled = new AtomicBoolean(false);
        return new KafkaConsumerService(
                configService.get("scoringKafkaCluster"),
                configService.get("scoringLaneGroupId"),
                configService.get("scoringTopicName"),
                scorerHealthFlag,
                1,
                disabled,
                scoringMessageProcessor
        );
    }

    @Bean
    public HourlyTask hourlyTask() {
        return new HourlyTask(metricReporterService,
                              modelIdToModelScoreLevelMap);
    }

    @Bean
    public MinutelyTask minutelyTask() {
        return new MinutelyTask(metricReporterService);
    }

    @Bean
    public ScoreLocationPlaceTagHereNowModule scoreLocationPlaceTagModule(
            MetricReporterService metricReporterService,
            SegmentInsertionService segmentInsertionService,
            AeroCacheService<PlacesCachedResult> aerospikePlaceCache,
            PlaceFinderModule placeFinderModule) {
        return new ScoreLocationPlaceTagHereNowModule(
                metricReporterService,
                segmentInsertionService,
                aerospikePlaceCache,
                placeFinderModule,
                Clock.systemUTC());
    }

    @Bean
    public ScoreLocationPlaceTagHomeModule scoreLocationPlaceTagHomeModule(
            MetricReporterService metricReporterService,
            SegmentInsertionService segmentInsertionService,
            AeroCacheService<PlacesCachedResult> aerospikePlaceCache,
            PlaceFinderModule placeFinderModule,
            CassandraService<UniqueDeviceBidEventLog> deviceBidEventLogCassandraService) {
        Integer numberOfTopPlacesToPick = 5;
        return new ScoreLocationPlaceTagHomeModule(
                 metricReporterService,
                segmentInsertionService,
                aerospikePlaceCache,
                placeFinderModule,
                deviceBidEventLogCassandraService,
                Clock.systemUTC(),
                numberOfTopPlacesToPick);
    }

    @Bean
    public ScoreATRankedAudiencesModule scoreATRankedAudiencesModule(
            MetricReporterService metricReporterService,
            SegmentInsertionService segmentInsertionService,
            SegmentCacheService segmentCacheService,
            DeviceSegmentHistoryCassandraService deviceSegmentHistoryCassandraService,
            ConfigService configService) {
        return new ScoreATRankedAudiencesModule(
                metricReporterService,
                segmentInsertionService,
                segmentCacheService,
                Clock.systemUTC(),
                configService.getAsInt("AtRankedAudience.numberOfTimesSeenInADayToBeQualified"));
    }

    @Bean
    public PlaceFinderModule placeFinderModule(PlaceTreeContainer placeTreeContainer) {
        return new PlaceFinderModule(placeTreeContainer);
    }

    @Bean
    public ScoringMessageProcessor scoringMessageProcessor(
            DeviceFeatureFetcherModule deviceFeatureFetcherModule,
            DeviceSegmentHistoryFetcherModule deviceSegmentHistoryFetcherModule,
            CalculateScoreModule calculateScoreModule,
            SegmentCreatorModule segmentCreatorModule,
            DeviceSegmentPersistenceModule deviceSegmentPersistenceModule,
            CrossWalkDeviceSegmentPersistenceModule crossWalkDeviceSegmentPersistenceModule,
            SegmentToDevicePersistenceModule segmentToDevicePersistenceModule,
            ScoredInLastNMinutesModule scoredInLastNMinutesModule,
            ScoreLocationPlaceTagHereNowModule scoreLocationPlaceTagHereNowModule,
            ScoreLocationPlaceTagHomeModule scoreLocationPlaceTagHomeModule,
            ScoreATRankedAudiencesModule scoreATRankedAudiencesModule,
            ScoreCompoundAudiencesModule scoreCompoundAudiencesModule,
            ScorerHealthService scorerHealthService,
            PlaceTreeContainer placeTreeContainer) {

        return new ScoringMessageProcessor(
                numberOfExceptionsInScorerMessageProcessor,
                allLoadedModels,
                metricReporterService,
                deviceFeatureFetcherModule,
                deviceSegmentHistoryFetcherModule,
                scoreCompoundAudiencesModule,
                scoreLocationPlaceTagHereNowModule,
                calculateScoreModule,
                segmentCreatorModule,
                deviceSegmentPersistenceModule,
                crossWalkDeviceSegmentPersistenceModule,
                segmentToDevicePersistenceModule,
                scoredInLastNMinutesModule,
                scoreLocationPlaceTagHomeModule,
                scoreATRankedAudiencesModule,
                scorerHealthService,
                placeTreeContainer,
                scorerHealthFlag,
                configService.getAsBoolean("testing.setPlacesLatLonForTestingPurposes"));
    }
}


package com.dstillery.scorer.main;

import java.util.Collections;
import java.util.HashSet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.dstillery.common.config.EssentialConfig;
import com.dstillery.common.config.CacheServiceInitializer;
import com.dstillery.common.config.CommonBeansConfig;
import com.dstillery.common.config.CommonModulesConfig;
import com.dstillery.common.config.ModelInterestCacheConfig;
import com.dstillery.common.config.PixelCacheConfig;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@PropertySources({
        @PropertySource(value = "classpath:scorer.properties"),
        @PropertySource(value = "file://${PROPERTY_FILE:/etc/scorer/scorer.properties}",
                ignoreResourceNotFound = true) })
@Import({
        ScorerEssentialConfig.class,
        EssentialConfig.class,
        CommonBeansConfig.class,
        CommonModulesConfig.class,
        PixelCacheConfig.class,
        ModelInterestCacheConfig.class,
        ScorerConfig.class})
@ComponentScan(basePackages = {
        "com.dstillery.scorer.main",
})
public class Scorer {
    public static void main(String[] args) {
        SpringApplication sa = new SpringApplication();
        sa.addListeners(new CacheServiceInitializer());
        sa.setSources(new HashSet<>(Collections.singletonList(Scorer.class.getName())));
        ConfigurableApplicationContext context = sa.run(args);
    }
}

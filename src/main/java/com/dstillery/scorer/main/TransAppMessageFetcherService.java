package com.dstillery.scorer.main;

import static com.dstillery.common.util.Constants.OBJECT_MAPPER;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.bidder.modules.TransAppBuilderService;
import com.dstillery.common.ClientAppDataReloadService;
import com.dstillery.common.TransAppMessage;
import com.dstillery.common.adhistory.AdEntryDevicePair;
import com.dstillery.common.cache_realtime.RealTimeEntityCacheService;
import com.dstillery.common.cassandra.CassandraService;
import com.dstillery.common.eventlog.UniqueDeviceBidEventLog;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.scorer.message_processing.ScoringMessageProcessor;
import com.fasterxml.jackson.core.JsonProcessingException;

/*
    runs periodically and fetches events from cassandra in order to make it easy to run scoring for testing purposes
 */
public class TransAppMessageFetcherService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransAppMessageFetcherService.class);
    private final CassandraService<UniqueDeviceBidEventLog> deviceBidEventLogCassandraService;
    private final MetricReporterService metricReporterService;
    private final int millisToSleepBetweenInEachMessage;
    private final int fetchRecentEventsInMinutes;
    private final ScoringMessageProcessor scoringMessageProcessor;
    private final TransAppBuilderService transAppBuilderService;
    private final ClientAppDataReloadService clientAppDataReloadService;
    ;
    private final RealTimeEntityCacheService<AdEntryDevicePair> adHistoryCacheService;

    public TransAppMessageFetcherService(
            TransAppBuilderService transAppBuilderService,
            ClientAppDataReloadService clientAppDataReloadService,
            CassandraService<UniqueDeviceBidEventLog> deviceBidEventLogCassandraService,
            ScoringMessageProcessor scoringMessageProcessor,
            RealTimeEntityCacheService<AdEntryDevicePair> adHistoryCacheService,
            MetricReporterService metricReporterService,
            boolean isTransAppMessageFetcherServiceEnabled,
            int fetchRecentEventsInMinutes,
            int millisToSleepBetweenInEachMessage,
            int pumpEventLogIntervalInSeconds) {
        this.clientAppDataReloadService = clientAppDataReloadService;
        this.deviceBidEventLogCassandraService = deviceBidEventLogCassandraService;
        this.metricReporterService = metricReporterService;
        this.fetchRecentEventsInMinutes = fetchRecentEventsInMinutes;
        this.scoringMessageProcessor = scoringMessageProcessor;
        this.transAppBuilderService = transAppBuilderService;
        this.adHistoryCacheService = adHistoryCacheService;
        this.millisToSleepBetweenInEachMessage = millisToSleepBetweenInEachMessage;

        if (isTransAppMessageFetcherServiceEnabled) {
            Executors.newScheduledThreadPool(1).scheduleAtFixedRate(this::pumpEventLog, 10,
                    pumpEventLogIntervalInSeconds, TimeUnit.SECONDS);
        }
    }

    private void pumpEventLog() {
        try {

            if(!clientAppDataReloadService.isReloadProcessHealthy().get()) {
                LOGGER.debug("waiting for data to be reloaded");
                sleep(1000);
                return;
            }
            List<UniqueDeviceBidEventLog> allEventLogsForDevice =
                    deviceBidEventLogCassandraService.readWithCustomQuery(getQuery(fetchRecentEventsInMinutes), 10000);

            LOGGER.debug("read {} eventLogs", allEventLogsForDevice.size());
            allEventLogsForDevice.forEach(deviceBidEventLog -> {
                AdEntryDevicePair keyOfAdHistory = new AdEntryDevicePair(deviceBidEventLog.getEventLog().getDevice());
                TransAppMessage transAppMessage =
                        transAppBuilderService.constructTransAppMessage(deviceBidEventLog,
                                adHistoryCacheService.readDataOptional(keyOfAdHistory));
                try {
                    scoringMessageProcessor.process(OBJECT_MAPPER.writeValueAsString(transAppMessage));
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
                LOGGER.debug("done processing a message");
                sleep(millisToSleepBetweenInEachMessage);
            });
        } catch (Exception e) {
            LOGGER.warn("suppressing exception occurred", e);
        }
    }

    private void sleep(int millis) {
        try {
            LOGGER.debug("sleeping for {} millis", millis);

            Thread.sleep(millis);
        } catch (InterruptedException e) {
            //ignore
        }
    }

    private String getQuery(int fetchRecentEventsInMinutes ) {
        return "select * from  gicapods.daily_unique_device_bid_eventlog where eventtime >= " +
                Instant.now().minus(fetchRecentEventsInMinutes, ChronoUnit.MINUTES).toEpochMilli() + " LIMIT 10000 ALLOW FILTERING ";
    }

}

package com.dstillery.scorer.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.TransAppMessage;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.metric.ProcessingStatus;
import com.dstillery.common.modeling.devicefeature.DeviceFeaturePair;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.segment.Segment;

public class ScorerContext {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScorerContext.class);

    private ProcessingStatus status = ProcessingStatus.CONTINUE_PROCESSING;
    private TransAppMessage transAppMessage;
    private Map<Long, ModelRequest> allLoadedModels;
    private Map<Long, Double> modelIdToScoreMap;
    private List<Segment> allSegmentsCreatedForDevice;
    private List<DeviceFeaturePair> deviceFeatureHistory = new ArrayList<>();
    private List<String> modulesStoppedProcessing = new ArrayList<>();
    private String lastModuleRunInPipeline;
    private List<DeviceSegmentHistory> deviceSegmentHistories;

    public ScorerContext(TransAppMessage transAppMessage,
                         Map<Long, ModelRequest> allLoadedModels) {
        modelIdToScoreMap = new ConcurrentHashMap<>();
        allSegmentsCreatedForDevice = new ArrayList<>();
        this.transAppMessage = transAppMessage;
        this.allLoadedModels = allLoadedModels;
        if (allLoadedModels.isEmpty()) {
            throw new IllegalStateException("no loaded models");
        }
    }

    public ProcessingStatus getStatus() {
        return status;
    }

    public NomadiniDevice getDevice() {
        return transAppMessage.getDevice();
    }

    public TransAppMessage getTransAppMessage() {
        return transAppMessage;
    }

    public void setTransAppMessage(TransAppMessage transAppMessage) {
        this.transAppMessage = transAppMessage;
    }

    public Map<Long, ModelRequest> getAllLoadedModels() {
        return allLoadedModels;
    }

    public void setAllLoadedModels(Map<Long, ModelRequest> allLoadedModels) {
        this.allLoadedModels = allLoadedModels;
    }

    public Map<Long, Double> getModelIdToScoreMap() {
        return modelIdToScoreMap;
    }

    public void setModelIdToScoreMap(Map<Long, Double> modelIdToScoreMap) {
        this.modelIdToScoreMap = modelIdToScoreMap;
    }

    public List<DeviceFeaturePair> getDeviceFeatureHistory() {
        return deviceFeatureHistory;
    }

    public void setDeviceFeatureHistory(List<DeviceFeaturePair> deviceFeatureHistory) {
        this.deviceFeatureHistory = deviceFeatureHistory;
    }

    public List<String> getModulesStoppedProcessing() {
        return modulesStoppedProcessing;
    }

    public void setModulesStoppedProcessing(List<String> modulesStoppedProcessing) {
        this.modulesStoppedProcessing = modulesStoppedProcessing;
    }

    public String getLastModuleRunInPipeline() {
        return lastModuleRunInPipeline;
    }

    public void setLastModuleRunInPipeline(String lastModuleRunInPipeline) {
        this.lastModuleRunInPipeline = lastModuleRunInPipeline;
    }

    public void stopProcessing(String moduleName) {
        modulesStoppedProcessing.add(moduleName);
        status = ProcessingStatus.STOP_PROCESSING;
    }

    public void addSegmentsForDevice(Segment segment) {
        if (segment.getId() == null) {
            throw new IllegalArgumentException("bad segment " + segment);
        }
        allSegmentsCreatedForDevice.add(segment);
    }

    public List<Segment> getAllSegmentsCreatedForDevice() {
        return allSegmentsCreatedForDevice;
    }

    public void setDeviceSegmentHistories(List<DeviceSegmentHistory> allSegmentsInThePastNDays) {
        deviceSegmentHistories = allSegmentsInThePastNDays;
    }

    public List<DeviceSegmentHistory> getDeviceSegmentHistories() {
        return deviceSegmentHistories;
    }
}


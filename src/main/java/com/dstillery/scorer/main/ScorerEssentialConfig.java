package com.dstillery.scorer.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ScorerEssentialConfig {

    @Bean
    public String appPropertyFileName() {
        return "scorer.properties";
    }
}

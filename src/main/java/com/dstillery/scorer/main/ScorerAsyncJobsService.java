package com.dstillery.scorer.main;

import static com.dstillery.common.util.StringUtil.toStr;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.DataWasReloadedEvent;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.predictive.ModelCacheService;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.modeling.predictive.ModelResult;
import com.dstillery.common.modeling.predictive.ModelResultCacheService;
import com.dstillery.common.util.GUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

public class ScorerAsyncJobsService implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScorerAsyncJobsService.class);

    private AtomicBoolean isMessageProcessorReadyToProcess;
    private ModelCacheService modelCacheService;
    private ModelResultCacheService modelResultCacheService;
    private MetricReporterService metricReporterService;
    private AtomicBoolean scorerHealthFlag;
    private ScorerHealthService scorerHealthService;
    private Map<Long, ModelRequest> allLoadedModels;
    public ScorerAsyncJobsService(AtomicBoolean isMessageProcessorReadyToProcess,
                                  Map<Long, ModelRequest> allLoadedModels,
                                  ModelCacheService modelCacheService,
                                  ModelResultCacheService modelResultCacheService,
                                  MetricReporterService metricReporterService,
                                  ScorerHealthService scorerHealthService,
                                  EventBus eventBus,
                                  AtomicBoolean scorerHealthFlag) {
        this.isMessageProcessorReadyToProcess = isMessageProcessorReadyToProcess;
        this.modelCacheService = modelCacheService;
        this.modelResultCacheService = modelResultCacheService;
        this.metricReporterService = metricReporterService;;
        this.scorerHealthService = scorerHealthService;
        this.allLoadedModels = allLoadedModels;
        this.scorerHealthFlag = scorerHealthFlag;

        eventBus.register(this);
    }

    public void run() {
        try {
            metricReporterService.addStateModuleForEntity("refreshData",
                    "ScorerAsyncJobsService",
                    "ALL");

            List<ModelRequest> allModels = modelCacheService.getAllEntities();

            if (allModels.isEmpty()) {
                metricReporterService.addStateModuleForEntity(
                        "EXCEPTION_NO_MODEL_RECEIVED_FROM_DATAMASTER",
                        "ScorerAsyncJobsService",
                        "ALL");
            }

            LOGGER.debug("loaded {} models for initial consideration", allModels.size());
            isMessageProcessorReadyToProcess.set(false);
            //wait 3 seconds before clearing the models
            GUtil.sleepInSeconds (3);
            allLoadedModels.clear();

            for (ModelRequest model : allModels) {

                ModelResult modelResult = modelResultCacheService.
                        getModelRequestIdToResultMap().get(model.getId());
                if (modelResult != null) {
                    model.setModelResult(modelResult);
                }

                //if a model doesn't have topFeatureScoreMap or featureScoreMap
                // means its not built yet, or something is wrong with it
                // so we can't use it for scoring
                if (model.getModelResult() == null ||
                        model.getModelResult().getTopFeatureScoreMap().isEmpty() &&
                        model.getModelResult().getFeatureScoreMap().isEmpty()) {
                    metricReporterService.addStateModuleForEntity("MODEL_NOT_ELIGIBLE_FOR_SCORING",
                            "ScorerAsyncJobsService",
                            "mdl" + toStr(model.getId()));
                } else {
                    metricReporterService.addStateModuleForEntity("MODEL_ELIGIBLE_FOR_SCORING",
                            "ScorerAsyncJobsService",
                            "mdl" + toStr(model.getId()));
                    allLoadedModels.put(model.getId(), model);
                }

            }
            if (!allLoadedModels.isEmpty()) {
                LOGGER.debug("all loaded models for final scoring: {}", allLoadedModels);
                isMessageProcessorReadyToProcess.set(true);
                //we set the health to true, so kafka can give us some messages
                scorerHealthFlag.set(true);
            } else {
                LOGGER.error("NO MODEL WAS LOADED, Message Processor is not Ready");
            }
            LOGGER.info("read {} active models for all advertisers", allModels.size());
        } catch (RuntimeException e) {
            //TODO : go red and show in dashboard that this is red
            LOGGER.error("error happening when runEveryMinute ", e);
        }
    }

    @Subscribe
    public void processEvent(DataWasReloadedEvent event) {
        LOGGER.info("event received : {} by class : {}", event);
        run();
    }

}


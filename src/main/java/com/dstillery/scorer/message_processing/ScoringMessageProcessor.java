package com.dstillery.scorer.message_processing;/*
  h
 *
   Created on: Aug 1, 2015
       Author: mtaabodi
 */

import static com.dstillery.common.metric.ProcessingStatus.CONTINUE_PROCESSING;
import static com.dstillery.common.metric.ProcessingStatus.STOP_PROCESSING;
import static com.dstillery.common.metric.dropwizard.MetricReporterService.ALL_ENTITIES;
import static com.dstillery.common.metric.dropwizard.MetricReporterService.MetricPriority.EXCEPTION;
import static com.dstillery.common.util.Constants.OBJECT_MAPPER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.common.config.ConfigService;
import com.dstillery.common.geo.PlaceTreeContainer;
import com.dstillery.common.geo.places.Place;
import com.dstillery.common.kafka.KafkaMessageProcessor;
import com.dstillery.common.TransAppMessage;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.metric.dropwizard.MetricReporterService;
import com.dstillery.common.modeling.predictive.ModelRequest;
import com.dstillery.common.util.GUtil;
import com.dstillery.common.util.RandomUtil;
import com.dstillery.scorer.main.ScorerContext;
import com.dstillery.scorer.main.ScorerHealthService;
import com.dstillery.scorer.modules.CalculateScoreModule;
import com.dstillery.scorer.modules.DeviceFeatureFetcherModule;
import com.dstillery.scorer.modules.DeviceSegmentHistoryFetcherModule;
import com.dstillery.scorer.modules.ScoreATRankedAudiencesModule;
import com.dstillery.scorer.modules.ScoreCompoundAudiencesModule;
import com.dstillery.scorer.modules.ScoreLocationPlaceTagHereNowModule;
import com.dstillery.scorer.modules.ScoreLocationPlaceTagHomeModule;
import com.dstillery.scorer.modules.ScoredInLastNMinutesModule;
import com.dstillery.scorer.modules.ScorerModule;
import com.dstillery.scorer.modules.persistence.CrossWalkDeviceSegmentPersistenceModule;
import com.dstillery.scorer.modules.persistence.DeviceSegmentPersistenceModule;
import com.dstillery.scorer.modules.persistence.SegmentToDevicePersistenceModule;
import com.dstillery.scorer.modules.segment.SegmentCreatorModule;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This processor is injected into {@code KafkaConsumerService} and reads messages via kafka queue
 * it also gets called by a {@code ScoringMessageController}. we have the rest way of processing messages
 * in order to make scoring no dependent on kafka for ease of testing
 */
public class ScoringMessageProcessor implements KafkaMessageProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScoringMessageProcessor.class);

    public static final ObjectMapper MAPPER = new ObjectMapper();
    private AtomicLong numberOfExceptionsInScorerMessageProcessor;

    private AtomicBoolean scorerHealthFlag;
    private MetricReporterService metricReporterService;
    private ScorerHealthService scorerHealthService;

    private Map<Long, ModelRequest> allLoadedModels;
    private List<ScorerModule> modules = new ArrayList<>();
    private PlaceTreeContainer placeTreeContainer;
    private ArrayList<Place> placesList = new ArrayList<>();
    private boolean setPlacesLatLonForTestingPurposes;

    public ScoringMessageProcessor(AtomicLong numberOfExceptionsInScorerMessageProcessor,
                                   Map<Long, ModelRequest> allLoadedModels,
                                   MetricReporterService metricReporterService,
                                   DeviceFeatureFetcherModule deviceFeatureFetcherModule,
                                   DeviceSegmentHistoryFetcherModule deviceSegmentHistoryFetcherModule,
                                   ScoreCompoundAudiencesModule scoreCompoundAudiencesModule,
                                   ScoreLocationPlaceTagHereNowModule scoreLocationPlaceTagHereNowModule,
                                   CalculateScoreModule calculateScoreModule,
                                   SegmentCreatorModule segmentCreatorModule,
                                   DeviceSegmentPersistenceModule deviceSegmentPersistenceModule,
                                   CrossWalkDeviceSegmentPersistenceModule crossWalkDeviceSegmentPersistenceModule,
                                   SegmentToDevicePersistenceModule segmentToDevicePersistenceModule,
                                   ScoredInLastNMinutesModule scoredInLastNMinutesModule,
                                   ScoreLocationPlaceTagHomeModule scoreLocationPlaceTagHomeModule,
                                   ScoreATRankedAudiencesModule scoreATRankedAudiencesModule,
                                   ScorerHealthService scorerHealthService,
                                   PlaceTreeContainer placeTreeContainer,
                                   AtomicBoolean scorerHealthFlag,
                                   boolean setPlacesLatLonForTestingPurposes) {
        this.allLoadedModels = allLoadedModels;
        this.numberOfExceptionsInScorerMessageProcessor = numberOfExceptionsInScorerMessageProcessor;
        this.metricReporterService = metricReporterService;;
        this.scorerHealthService = scorerHealthService;
        this.scorerHealthFlag = scorerHealthFlag;
        this.placeTreeContainer = placeTreeContainer;
        this.setPlacesLatLonForTestingPurposes = setPlacesLatLonForTestingPurposes;

        this.modules.add(scoredInLastNMinutesModule);
        this.modules.add(deviceFeatureFetcherModule);
        this.modules.add(deviceSegmentHistoryFetcherModule);
        this.modules.add(scoreLocationPlaceTagHereNowModule);
        this.modules.add(scoreLocationPlaceTagHomeModule);
        this.modules.add(scoreATRankedAudiencesModule);
        this.modules.add(scoreCompoundAudiencesModule);
        this.modules.add(calculateScoreModule);
        this.modules.add(segmentCreatorModule);
        this.modules.add(deviceSegmentPersistenceModule);
        this.modules.add(crossWalkDeviceSegmentPersistenceModule);
        this.modules.add(segmentToDevicePersistenceModule);
    }

    public void process(String msg)  {
        TransAppMessage transAppMessage;

        try {
            transAppMessage = OBJECT_MAPPER.readValue(msg, TransAppMessage.class);

            setArtificialPlace(transAppMessage);

            if (GUtil.allowedToCall(10000)) {
                LOGGER.debug("read allLoadedModels size : {}, message : {}",
                        allLoadedModels.size(),
                        transAppMessage);
            }
            if (GUtil.allowedToCall(100)) {
                LOGGER.info("receiving 100th msg and processing it");
            }
            if (!transAppMessage.getDevice().getDeviceId().equalsIgnoreCase(NomadiniDevice.UNMAPPED_USER)) {
                ScorerContext context = new ScorerContext(transAppMessage, allLoadedModels);
                processContext(context);
            } else if (GUtil.allowedToCall(1000)) {
                LOGGER.info("receiving 1000th unmapped device msg and ignoring it");
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void setArtificialPlace(TransAppMessage transAppMessage) {
        if (!setPlacesLatLonForTestingPurposes) {
            return;
        }
        GUtil.callNThTime(100, (s) -> {
            //create a new list based on latest tree which gets updated by load task
            placesList = new ArrayList<>(placeTreeContainer.getPlaceMap().values());
        });
        if (!placesList.isEmpty()) {
            Place place = placesList.get(RandomUtil.sudoRandomNumber(placesList.size()).intValue());
            if (place != null) {
                transAppMessage.setDeviceLat(RandomUtil.getRandomBetweenRange(place.getLat() - 0.000001, place.getLat() + 0.000001));
                transAppMessage.setDeviceLon(RandomUtil.getRandomBetweenRange(place.getLon() - 0.000001, place.getLon() + 0.000001));
            }
        } else {
            //some random point within USAA
            transAppMessage.setDeviceLat(RandomUtil.getRandomBetweenRange(24.7433195, 49.3457868));
            transAppMessage.setDeviceLon(RandomUtil.getRandomBetweenRange(-124.7844079, -66.9513812));
        }
    }

    private void processContext(ScorerContext context) {

        scorerHealthService.determineHealth();
        metricReporterService.addStateModuleForEntity(
                "PROCESSING_MESSAGE",
                "ScoringMessageProcessor",
                "ALL");
        if (context.getAllLoadedModels().isEmpty()) {
            throw new RuntimeException("no models to build score");
        }
        for (ScorerModule module : modules) {

            if (context.getStatus() == CONTINUE_PROCESSING) {
                try {
                    LOGGER.trace("processing module : {}", module.getClass().getSimpleName());
                    module.process(context);
                } catch(Exception e) {
                    numberOfExceptionsInScorerMessageProcessor.getAndIncrement();
                    metricReporterService.addStateModuleForEntity(
                            "PROCESSOR_EXCEPTION",
                            "ScoringMessageProcessor",
                            ALL_ENTITIES,
                            EXCEPTION);
                    context.stopProcessing(this.getClass().getSimpleName());
                    LOGGER.warn("exception in processing", e);
                }
            }
            if (context.getStatus() == STOP_PROCESSING) {
                metricReporterService.
                        addStateModuleForEntity(
                                "LastModule-" + module.getClass().getSimpleName(),
                                "ScoringMessageProcessor",
                                "ALL");
                break;
            }
        }

    }

    public boolean isReadyToProcess() {
        return scorerHealthFlag.get();
    }
}

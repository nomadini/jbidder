package com.dstillery.scorer.message_processing;

import static com.dstillery.common.util.Constants.OBJECT_MAPPER;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dstillery.common.TransAppMessage;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
public class ScoringMessageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScoringMessageController.class);
    private ScoringMessageProcessor scoringMessageProcessor;

    @PostMapping("/scoring/message")
    public void processMessage(@RequestBody TransAppMessage payload,
                               HttpServletResponse response) throws JsonProcessingException {
        if (!scoringMessageProcessor.isReadyToProcess()) {
            response.setStatus(HttpStatus.SC_NOT_ACCEPTABLE);
            LOGGER.info("not accepting the message");
            return;
        }
        scoringMessageProcessor.process(OBJECT_MAPPER.writeValueAsString(payload));
    }
}

#!/usr/bin/env bash

APP_NAME=modeler
HEAT_SIZE_MB=256
JMX_PORT=1025
MAIN_CLASS=com.dstillery.modeler.Modeler
APP_OPTS=""
JAR_PATH_NAME=~/.m2/repository/com/adserv-mt/1.0-SNAPSHOT/adserv-mt-1.0-SNAPSHOT.jar
FINAL_RELEASE_NAME=~/modeler-1.0-SNAPSHOT.jar
PROJECT_DIR=/Users/mtaabodi/Documents/workspace-mt/adserv-mt

BASEDIR=$(dirname "$0")
source /Users/mtaabodi/Documents/workspace-mt/config/run.sh

#!/usr/bin/env bash
APP_NAME=adserver
HEAT_SIZE_MB=256
JMX_PORT=1022
MAIN_CLASS=com.dstillery.adserver.main.AdServer
APP_OPTS=""
JAR_PATH_NAME=~/.m2/repository/com/adserv-mt/1.0-SNAPSHOT/adserv-mt-1.0-SNAPSHOT.jar
FINAL_RELEASE_NAME=~/adserver-1.0-SNAPSHOT.jar
PROJECT_DIR=/Users/mtaabodi/Documents/workspace-mt/adserv-mt

BASEDIR=$(dirname "$0")
source /Users/mtaabodi/Documents/workspace-mt/config/run.sh

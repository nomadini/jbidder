#### How to run prodmon locally?
    run it in docker and git clone it in docker and use VS code to attach to that docker container
    and remove 'FOR UPDATE SKIP LOCKED' from _GET_NEXT_TASK query in sql.py in order to make get next task behavior more predictable
    

    sudo docker run --name prodmon -v /Users/mahmoudtaabodi/.aws:/root/.aws -v /Users/mahmoudtaabodi/workspace-cpp/core-prodmon:/root/core-prodmon -p 80:80 -e FLASK_CONFIG="dev" -d prodmon
 
    docker container start prodmon
    
 #### the psql configs 
    they are in local.prodmon.toml
    
 ### How to access psql 
    docker pull postgres
    docker run -it postgres /bin/bash
    
    psql -h mm-core-prodmon-prd.cgifjcqo1pea.us-east-1.rds.amazonaws.com -d mm_core_prodmon_prd -U core
    password is : core-prodmon-prd
     
    psql -h mm-core-prodmon-dev.cgifjcqo1pea.us-east-1.rds.amazonaws.com -d mm_core_prodmon_dev -U core
    password is : core-prodmon-dev
    
    Strategy Scenario 1
    https://t1.mediamath.com/app/#strategy/edit/6303680/details
    Strategy Scenario 2
    https://t1.mediamath.com/app/#strategy/edit/6288787/details
    
    Strategy Scenario 3
    https://t1.mediamath.com/app/#strategy/edit/6305202/supply
    
    Strategy Scenario 3
    https://t1.mediamath.com/app/#strategy/edit/6312695/details
    
    PORT=8000 python run.py

#### How to run test suite
    first disable auth login required from controller.py
    curl -X POST      -H 'Authorization: Bearer 8383-AKJ0SKKKD-FF&Z=='      http://localhost:8000/api/v1/test-suite/run?id=1
    

#### How to Force start a new task 
    create task for test_config to schedule immediately like test with id = 80
    
    make sure the test config entry is enabled and is set to development type
    update  test_config set enabled = true, test_type='Development' where test_id = '82';
    
    either update an existing task like this
        update task set status = 'Pending', details = '{}', scheduled_time = (NOW() - interval '1 minute'), config='{"class": "TestAgentTask", "module": "app.lib.service.task.test_agent", "test_id": "81", "scheduler": 1, "description": "Runs the test"}' where task_id = '111209';    

    OR create a task for it
    INSERT INTO task ("task_name", "status", "retries", "created", "scheduled_time", "executed_time", "config", "details") VALUES
    ('TestAgent', 'Pending', 0, (NOW() - interval '100 minute'), (NOW() - interval '100 minute'), NULL,
    '{"class": "TestAgentTask", "module": "app.lib.service.task.test_agent", "test_id": "82", "scheduler": 1, "description": "Runs the test"}',
     NULL);
    

#### how to force run an old task
    1. find the latest task
        select * from  task where  scheduled_time >= (NOW() - interval '200 minute') ; 
    2.  update their status and scheduled time
        update task set status = 'Pending' , scheduled_time = (NOW() - interval '1 minute') WHERE task_id='112274' ;
        
    3.  update task set status = 'CompletedWithError', scheduled_time = (NOW() - interval '10 minute'), config='{"class": "TestAgentTask", "module": "app.lib.service.task.test_agent", "test_id": "75", "scheduler": 1, "description": "Runs the test"}' where task_id = '111209';

#### GDPR test strategy
    https://t1.mediamath.com/app/#strategy/edit/6288787/supply    
#### PSQL commands
    https://www.postgresqltutorial.com/psql-commands/
    
    SELECT * FROM test_config WHERE test_id='81';
    UPDATE test_config set enabled='true', test_type='Development' where test_id='81';
    
    SELECT * FROM task WHERE task_id='63389';
    update task set status='CompletedWithError' WHERE task_id='63389';
    
     
        
    INSERT INTO test_config (test_id, enabled, created, last_updated, data)
    VALUES("<your-test-id>", TRUE, NOW(), NOW(), "<test-config-json>"
    
    delete from test_config where test_id='80' or test_id='81' or test_id='82';
 
##### good command to insert my test config
    with Custom Payload : 
    INSERT INTO test_config (test_id, enabled, created, last_updated, test_type, modified_by, data) VALUES(80, TRUE, NOW(), NOW(),'Development', 'mtaabodi' ,'{"fpg": {"total_spend_clear_cpm_ratio": 1.05}, "enable": true, "report": {"error_rate": 0}, "metrics": {"bid": ["=", 0]}, "test_name": "GDPR Test Strategy", "test_plan": "Send bid request without gdpr consent and verify there is no bid", "bid_request": {"target": "canary", "payload": {"${CUSTOM_PAYLOAD}": {"app": {"domain": "mygame.foo.com", "paid": 0, "pagecat": ["IAB2-3"], "privacypolicy": 0, "bundle": "com.foo.mygame", "keywords": "testing,app", "sectioncat": ["IAB2-1"], "id": "234234", "storeurl": "app.store.url", "ver": "1.0.9", "name": "appname", "cat": ["IAB2", "IAB2"]}, "site": {"domain": "http://siteabcd.com", "keywords": "test,bid", "privacypolicy": 1, "pagecat": ["IAB2-7"], "sectioncat": ["IAB2-7"], "id": "234345", "publisher": {"domain": "foopub.com", "cat": ["IAB2-1", "IAB2-2"], "id": "pub12345", "name": "Publisher A"}, "search": "test", "name": "Site ABCD", "cat": ["IAB2-7"], "content": {"embeddable": 0, "episode": 1, "language": "en", "producer": {"domain": "http://producer.com", "cat": ["IAB2-7"], "id": "prod12345", "name": "Producer A"}, "title": "Test bidflux", "url": "http://example.com", "series": "frontend server", "sourcerelationship": 1, "contentrating": "MPAA", "len": 30, "cat": ["IAB2-1", "IAB2-2"], "userrating": "SSSS", "context": "1", "season": "Season 1", "keywords": "fronted,test,bidflux", "videoquality": 3, "livestream": 0, "id": "test-1", "qagmediarating": 1}, "ref": "http://referringsite.com/referringpage.html", "page": "http://siteabcd.com/page.html"}, "regs": {"ext": {"gdpr": 1}}, "user": {"buyeruid": "040b568c-6cc6-11de-af57-9f2eb213af44", "gender": "M", "customdata": "abbccc", "yob": 1976, "geo": {"lat": 12.234459, "lon": 15.847534}, "ext": {"consent": "COx_A4OOx_A4OAXACBAAACC4AIAAAHIAAIAAAGAAQAMAAgAggAIADAwAEABggACAAwcABAAY\n"}, "keywords": "search,test", "data": [{"segment": [{"id": "412587", "value": "male", "name": "gender"}], "id": "21", "name": "AdTech"}], "id": "7777777777"}, "device": {"flashver": "14.0.0.125", "ip": "3.2.4.1", "connectiontype": 1, "osv": "10.1.7", "make": "Apple", "dpidsha1": "76eecfa4d86a1631a4820c9a79b3ed0f9cd302c9", "dnt": 1, "ua": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.14 Safari/537.4", "ipv6": "2607:f0d0:1002:51::4", "macsha1": "9a115a2436808828606eb0c01f315062070513c0", "macmd5": "4134323a534eb11ce5eeeefe666b7064", "js": 1, "didmd5": "9DDDF85AFF0A87974CE4541BD94D5F55", "devicetype": 2, "geo": {"city": "AL", "zip": "10345", "metro": "691", "type": 2, "lat": 87.833423, "country": "US", "region": "US-AL", "lon": 98.348634, "regionfips104": "US"}, "dpidmd5": "9DDDF85AFF0A87974CE4541BD94D5F55", "language": "en", "didsha1": "76eecfa4d86a1631a4820c9a79b3ed0f9cd302c9", "h": 12, "carrier": "544", "ifa": "040b568c-6cc6-11de-af57-9f2eb213af44", "w": 11, "model": "Macbook", "os": "OS X"}, "id": "234987234823", "badv": ["company1.com", "company2.com"], "cur": ["USD", "EUR"], "tmax": 120, "imp": [{"pmp": {"deals": [{"wseat": ["PMP-Seat-1"], "bidfloorcur": "USD", "at": 1, "id": "PMP-Deal-1", "bidfloor": 2.5}, {"wseat": ["PMP-Seat-2"], "bidfloorcur": "USD", "at": 1, "id": "PMP-Deal-2", "bidfloor": 2.5}], "private_auction": 0}, "bidfloor": 1.42, "displaymanagerver": "2.7.0", "displaymanager": "video", "bidfloorcur": "USD", "video": {"playbackmethod": [1, 3], "maxbitrate": 600, "protocol": 2, "boxingallowed": 1, "mimes": ["video/x-flv"], "sequence": 1, "api": [3, 4], "h": 240, "maxextended": 0, "linearity": 1, "pos": 1, "companionad": [{"h": 1, "w": 1}], "delivery": [3, 5], "battr": [13], "startdelay": 0, "skip": 1, "minbitrate": 60, "w": 320, "minduration": 5, "maxduration": 60, "protocols": [1, 2, 3]}, "instl": 0, "banner": {"mimes": ["image/jpg", "image/gif", "application/xshockwave-flash"], "pos": 1, "api": [3], "id": "1", "hmin": 0, "h": 1, "battr": [13], "w": 1, "btype": [1], "topframe": 0, "wmin": 0, "expdir": [4, 5]}, "id": "1", "iframebuster": ["adtech-1", "adtech-2"], "clickbrowser": 0, "tagid": "test-tag"}], "ext": {"exchangeid": "exchid-1", "mm_uid": "040b568c-6cc6-11de-af57-9f2eb213af44"}, "bcat": ["IAB2-1", "IAB2-3"], "at": 1}}}, "campaign_id": 701926, "strategy_id": 6288787, "notification": {"target": "canary", "win_rate": 100, "clear_cpm": 0.5}, "thread_sleep_interval": 10}');
    
    without Custom Payload :    
    INSERT INTO test_config (test_id, enabled, created, last_updated, test_type, modified_by, data) VALUES(80, TRUE, NOW(), NOW(),'Development', 'mtaabodi' ,'{"fpg": {"total_spend_clear_cpm_ratio": 1.05}, "enable": true, "report": {"error_rate": 0}, "metrics": {"bid": ["=", 0]}, "test_name": "GDPR Test Strategy", "test_plan": "Send bid request with a gdpr string without consent and make sure we dont bid", "bid_request": {"target": "canary", "campaign_id": 701926, "strategy_id": 6288787, "notification": {"target": "canary", "win_rate": 100, "clear_cpm": 0.5}, "thread_sleep_interval": 10}}');
    
    INSERT INTO test_config (test_id, enabled, created, last_updated, test_type, modified_by, data) VALUES(82, TRUE, NOW(), NOW(),'Production', 'mtaabodi' ,'{"fpg": {"total_spend_clear_cpm_ratio": 1.05}, "enable": true, "report": {"error_rate": 0}, "metrics": {"bid": ["=", 1]}, "test_name": "GDPR Test Strategy 3", "test_plan": "Send bid request with a v2 gdpr string with consent and make sure we bid", "bid_request": {"target": "canary", "campaign_id": 701926, "strategy_id": 6312695, "notification": {"target": "canary", "win_rate": 100, "clear_cpm": 0.5}, "thread_sleep_interval": 10}}');
 

     {"app": {"domain": "mygame.foo.com", "paid": 0, "pagecat": ["IAB2-3"], "privacypolicy": 0, "bundle": "com.foo.mygame", "keywords": "testing,app", "sectioncat": ["IAB2-1"], "id": "234234", "storeurl": "app.store.url", "ver": "1.0.9", "name": "appname", "cat": ["IAB2", "IAB2"]}, "site": {"domain": "http://siteabcd.com", "keywords": "test,bid", "privacypolicy": 1, "pagecat": ["IAB2-7"], "sectioncat": ["IAB2-7"], "id": "234345", "publisher": {"domain": "foopub.com", "cat": ["IAB2-1", "IAB2-2"], "id": "pub12345", "name": "Publisher A"}, "search": "test", "name": "Site ABCD", "cat": ["IAB2-7"], "content": {"embeddable": 0, "episode": 1, "language": "en", "producer": {"domain": "http://producer.com", "cat": ["IAB2-7"], "id": "prod12345", "name": "Producer A"}, "title": "Test bidflux", "url": "http://example.com", "series": "frontend server", "sourcerelationship": 1, "contentrating": "MPAA", "len": 30, "cat": ["IAB2-1", "IAB2-2"], "userrating": "SSSS", "context": "1", "season": "Season 1", "keywords": "fronted,test,bidflux", "videoquality": 3, "livestream": 0, "id": "test-1", "qagmediarating": 1}, "ref": "http://referringsite.com/referringpage.html", "page": "http://siteabcd.com/page.html"}, "regs": {"ext": {"gdpr": 1}}, "user": {"buyeruid": "040b568c-6cc6-11de-af57-9f2eb213af44", "gender": "M", "customdata": "abbccc", "yob": 1976, "geo": {"lat": 12.234459, "lon": 15.847534}, "ext": {"consent": "COx_A4OOx_A4OAXACBAAACC4AIAAAHIAAIAAAGAAQAMAAgAggAIADAwAEABggACAAwcABAAY\n"}, "keywords": "search,test", "data": [{"segment": [{"id": "412587", "value": "male", "name": "gender"}], "id": "21", "name": "AdTech"}], "id": "7777777777"}, "device": {"flashver": "14.0.0.125", "ip": "3.2.4.1", "connectiontype": 1, "osv": "10.1.7", "make": "Apple", "dpidsha1": "76eecfa4d86a1631a4820c9a79b3ed0f9cd302c9", "dnt": 1, "ua": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.14 Safari/537.4", "ipv6": "2607:f0d0:1002:51::4", "macsha1": "9a115a2436808828606eb0c01f315062070513c0", "macmd5": "4134323a534eb11ce5eeeefe666b7064", "js": 1, "didmd5": "9DDDF85AFF0A87974CE4541BD94D5F55", "devicetype": 2, "geo": {"city": "AL", "zip": "10345", "metro": "691", "type": 2, "lat": 87.833423, "country": "US", "region": "US-AL", "lon": 98.348634, "regionfips104": "US"}, "dpidmd5": "9DDDF85AFF0A87974CE4541BD94D5F55", "language": "en", "didsha1": "76eecfa4d86a1631a4820c9a79b3ed0f9cd302c9", "h": 12, "carrier": "544", "ifa": "040b568c-6cc6-11de-af57-9f2eb213af44", "w": 11, "model": "Macbook", "os": "OS X"}, "id": "234987234823", "badv": ["company1.com", "company2.com"], "cur": ["USD", "EUR"], "tmax": 120, "imp": [{"pmp": {"deals": [{"wseat": ["PMP-Seat-1"], "bidfloorcur": "USD", "at": 1, "id": "PMP-Deal-1", "bidfloor": 2.5}, {"wseat": ["PMP-Seat-2"], "bidfloorcur": "USD", "at": 1, "id": "PMP-Deal-2", "bidfloor": 2.5}], "private_auction": 0}, "bidfloor": 1.42, "displaymanagerver": "2.7.0", "displaymanager": "video", "bidfloorcur": "USD", "video": {"playbackmethod": [1, 3], "maxbitrate": 600, "protocol": 2, "boxingallowed": 1, "mimes": ["video/x-flv"], "sequence": 1, "api": [3, 4], "h": 240, "maxextended": 0, "linearity": 1, "pos": 1, "companionad": [{"h": 1, "w": 1}], "delivery": [3, 5], "battr": [13], "startdelay": 0, "skip": 1, "minbitrate": 60, "w": 320, "minduration": 5, "maxduration": 60, "protocols": [1, 2, 3]}, "instl": 0, "banner": {"mimes": ["image/jpg", "image/gif", "application/xshockwave-flash"], "pos": 1, "api": [3], "id": "1", "hmin": 0, "h": 1, "battr": [13], "w": 1, "btype": [1], "topframe": 0, "wmin": 0, "expdir": [4, 5]}, "id": "1", "iframebuster": ["adtech-1", "adtech-2"], "clickbrowser": 0, "tagid": "test-tag"}], "ext": {"exchangeid": "exchid-1", "mm_uid": "040b568c-6cc6-11de-af57-9f2eb213af44"}, "bcat": ["IAB2-1", "IAB2-3"], "at": 1}
#### good custom payload with consent    
     {"fpg": {"total_spend_clear_cpm_ratio": 1.05}, "enable": true, "report": {"error_rate": 0}, "metrics": {"bid": ["=", 0]}, "test_name": "GDPR Test Strategy", "test_plan": "Send bid request with a gdpr string without consent and make sure we dont bid", "bid_request": {"target": "canary", "payload": {"${CUSTOM_PAYLOAD}": {"app": {"domain": "mygame.foo.com", "paid": 0, "pagecat": ["IAB2-3"], "privacypolicy": 0, "bundle": "com.foo.mygame", "keywords": "testing,app", "sectioncat": ["IAB2-1"], "id": "234234", "storeurl": "app.store.url", "ver": "1.0.9", "name": "appname", "cat": ["IAB2", "IAB2"]}, "site": {"domain": "http://siteabcd.com", "keywords": "test,bid", "privacypolicy": 1, "pagecat": ["IAB2-7"], "sectioncat": ["IAB2-7"], "id": "234345", "publisher": {"domain": "foopub.com", "cat": ["IAB2-1", "IAB2-2"], "id": "pub12345", "name": "Publisher A"}, "search": "test", "name": "Site ABCD", "cat": ["IAB2-7"], "content": {"embeddable": 0, "episode": 1, "language": "en", "producer": {"domain": "http://producer.com", "cat": ["IAB2-7"], "id": "prod12345", "name": "Producer A"}, "title": "Test bidflux", "url": "http://example.com", "series": "frontend server", "sourcerelationship": 1, "contentrating": "MPAA", "len": 30, "cat": ["IAB2-1", "IAB2-2"], "userrating": "SSSS", "context": "1", "season": "Season 1", "keywords": "fronted,test,bidflux", "videoquality": 3, "livestream": 0, "id": "test-1", "qagmediarating": 1}, "ref": "http://referringsite.com/referringpage.html", "page": "http://siteabcd.com/page.html"}, "regs": {"ext": {"gdpr": 1}}, "user": {"buyeruid": "040b568c-6cc6-11de-af57-9f2eb213af44", "gender": "M", "customdata": "abbccc", "yob": 1976, "geo": {"lat": 12.234459, "lon": 15.847534}, "ext": {"consent": "COx_A4OOx_A4OAXACBAAACC4AIAAAHIAAIAAAGAAQAMAAgAggAIADAwAEABggACAAwcABAAY\n"}, "keywords": "search,test", "data": [{"segment": [{"id": "412587", "value": "male", "name": "gender"}], "id": "21", "name": "AdTech"}], "id": "7777777777"}, "device": {"flashver": "14.0.0.125", "ip": "3.2.4.1", "connectiontype": 1, "osv": "10.1.7", "make": "Apple", "dpidsha1": "76eecfa4d86a1631a4820c9a79b3ed0f9cd302c9", "dnt": 1, "ua": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.14 Safari/537.4", "ipv6": "2607:f0d0:1002:51::4", "macsha1": "9a115a2436808828606eb0c01f315062070513c0", "macmd5": "4134323a534eb11ce5eeeefe666b7064", "js": 1, "didmd5": "9DDDF85AFF0A87974CE4541BD94D5F55", "devicetype": 2, "geo": {"city": "AL", "zip": "10345", "metro": "691", "type": 2, "lat": 87.833423, "country": "US", "region": "US-AL", "lon": 98.348634, "regionfips104": "US"}, "dpidmd5": "9DDDF85AFF0A87974CE4541BD94D5F55", "language": "en", "didsha1": "76eecfa4d86a1631a4820c9a79b3ed0f9cd302c9", "h": 12, "carrier": "544", "ifa": "040b568c-6cc6-11de-af57-9f2eb213af44", "w": 11, "model": "Macbook", "os": "OS X"}, "id": "234987234823", "badv": ["company1.com", "company2.com"], "cur": ["USD", "EUR"], "tmax": 120, "imp": [{"pmp": {"deals": [{"wseat": ["PMP-Seat-1"], "bidfloorcur": "USD", "at": 1, "id": "PMP-Deal-1", "bidfloor": 2.5}, {"wseat": ["PMP-Seat-2"], "bidfloorcur": "USD", "at": 1, "id": "PMP-Deal-2", "bidfloor": 2.5}], "private_auction": 0}, "bidfloor": 1.42, "displaymanagerver": "2.7.0", "displaymanager": "video", "bidfloorcur": "USD", "video": {"playbackmethod": [1, 3], "maxbitrate": 600, "protocol": 2, "boxingallowed": 1, "mimes": ["video/x-flv"], "sequence": 1, "api": [3, 4], "h": 240, "maxextended": 0, "linearity": 1, "pos": 1, "companionad": [{"h": 1, "w": 1}], "delivery": [3, 5], "battr": [13], "startdelay": 0, "skip": 1, "minbitrate": 60, "w": 320, "minduration": 5, "maxduration": 60, "protocols": [1, 2, 3]}, "instl": 0, "banner": {"mimes": ["image/jpg", "image/gif", "application/xshockwave-flash"], "pos": 1, "api": [3], "id": "1", "hmin": 0, "h": 1, "battr": [13], "w": 1, "btype": [1], "topframe": 0, "wmin": 0, "expdir": [4, 5]}, "id": "1", "iframebuster": ["adtech-1", "adtech-2"], "clickbrowser": 0, "tagid": "test-tag"}], "ext": {"exchangeid": "exchid-1", "mm_uid": "040b568c-6cc6-11de-af57-9f2eb213af44"}, "bcat": ["IAB2-1", "IAB2-3"], "at": 1}}}, "campaign_id": 701926, "strategy_id": 6288787, "notification": {"target": "canary", "win_rate": 100, "clear_cpm": 0.5}, "thread_sleep_interval": 10}
#### good test_config without payload     
     {"fpg": {"total_spend_clear_cpm_ratio": 1.05}, "enable": true, "report": {"error_rate": 0}, "metrics": {"bid": ["=", 0]}, "test_name": "GDPR Test Strategy", "test_plan": "Send bid request with a gdpr string without consent and make sure we dont bid", "bid_request": {"target": "canary", "campaign_id": 701926, "strategy_id": 6288787, "notification": {"target": "canary", "win_rate": 100, "clear_cpm": 0.5}, "thread_sleep_interval": 10}}
 
 
    
     
#### How to check the transaction log
    select * from transaction_log where test_id='80';

#### How to force start a reporting task
    INSERT INTO task (task_name, status, retries, created, scheduled_time, executed_time, config, details)
    VALUES('TestReporter', 'Pending', 0, (NOW() - interval '100 minute'), (NOW() - interval '100 minute'), NULL, '{"module": "app.lib.service.task.test_reporter", "class": "TestReporterTask", "scheduler": 1}', NULL);


#### how to force stop a task all newly created tasks        
    select * from  task where created > (NOW() - interval '100 minute') and status != 'Completed';
    update task set status='Completed' where created > (NOW() - interval '100 minute') and status != 'Completed';
                       
                       
#### How to check test_reports
    select * from test_report where test_id = '82' ORDER BY last_updated DESC limit 1;
    
#### Get all ready to go tasks
     SELECT task_id, task_name, status, retries, created, scheduled_time, executed_time, config, details FROM task WHERE status='Pending' AND scheduled_time <= NOW() ORDER BY scheduled_time LIMIT 1;


    SELECT * FROM transaction_log WHERE test_id = '75' and request_type = 'bid' and created > (NOW() - interval '1 HOUR') ORDER BY created DESC LIMIT 10;


### jira links 
    http://ewr-monitor-x1.mediamath.com:8080/ team jira in cloud
    https://jenkins.mediamath.com/login?from=%2Fview%2FTops%2Fjob%2Fmedia-deploy%2F
##### Very Nice Link about industry knowledge
    https://mediamathsupport.force.com/s/article/Attribution-Overview
##### How to show all campaign loaded in a bidder
     http://zrh-bidder-x33.mediamath.com:8081/campaign?op=query&exch=&org=&size=&value=1-
     http://zrh-bidder-x33.mediamath.com:8081/campaign?op=query&value=804476
     
##### Fast Release build Job
    https://core-github-webhook.mediamath.com/job/Bidder-build-release/

##### Regression Job
    https://core-github-webhook.mediamath.com/job/Bidder-PR-regression-tests/

### Hosts
   ##### GDPR page
    http://zrh-bidder-x3.mediamath.com:8081/gdpr_consent_str?consentString=    
    http://zrh-bidder-x13.mediamath.com:8081/gdpr_consent_str?consentString=    

### run this after cloning bidder, before configure 
    automake --add-missing

### GDPR qaf tests collection
    /apps/bidder/run_single_qaf_test.sh tests.delphi_tests.test_delphi_request_and_response.TestDelphiRequestAndResponse
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_bid_opportunity_firehose
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_bid_opportunity_firehose_tcf
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_bid_opportunity_firehose.TestBidOpportunityFirehose.test_GDPR_forward_bad_daisy
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_bid_opportunity_firehose.TestBidOpportunityFirehose.test_GDPR_forward_good_daisy
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_bid_opportunity_firehose.TestBidOpportunityFirehose.test_GDPR_not_forwarding_bad_daisy
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_connected_id.TestConnectedId
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_connected_id.TestConnectedId.test_should_log_mm_user_id_and_connected_id_for_optout_user_if_config_is_turned_on
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_log_optout_json.TestLogOptOutJson
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_optout_user.TestOptoutUser.test_optout_user_log_random_uuid_and_do_not_update_frequency_store
    
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_optout_user.TestOptoutUser.test_optout_user_log_random_uuid_and_do_not_update_frequency_store
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_auction_skip_by_bidprice
    
    /apps/bidder/run_single_qaf_test.sh tests.exchanges_tests.pubmatic.test_pubmatic_openrtb_pmp.TestPubMaticOpenRTBPMP.test_basic_bid
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_auction_skip_by_bidprice.TestAuctionSkipByBidPrice.test_case6
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_auction_skip_by_bidprice.TestAuctionSkipByBidPrice.test_case7_1
#### User sync migration

    How to curl mathtag with cookie?
    first hit it witout cookie and get the uuid from set-cookie in the response, then hit it with cookie
        curl -v "https://sync.mathtag.com/sync/img?sync=auto&source=bidder&mt_lim=1&type=1,2" 
    
    curl -v "https://sync.mathtag.com/sync/img?sync=auto&source=bidder&mt_lim=1&type=1,2" --header "Cookie: uuid=10cf5f12-148e-4900-b378-1c2b28c448c4"
      
    /apps/bidder/run_single_qaf_test.sh tests.notification.test_imp_notification_video.TestImpNotificationVideo.test_impression_tracking_url_for_video_mathtag_redirect
    /apps/bidder/run_single_qaf_test.sh tests.exchanges.appnexus.test_appnexus_cookie.TestAppNexusCookie.test_apn_log_old_user_uuid_mathtag_sync
    
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_multiple_user_syncs_gdpr.TestMultipleUserSyncsGDPR
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_multiple_multiple_user_syncs_via_mathtag
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_bidder_macros
     /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_gdpr_field
     /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_multiple_user_syncs_gdpr
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_multiple_user_syncs_tcfv2
    
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_tcfv2_bidding.TestTCFv2.test_tcfv2_bid_unsuccessful_for_banned_cmp
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_tcfv2_bidding.TestTCFv2.test_tcfv2_bid_unsuccessful_for_banned_cmp

    /apps/bidder/run_single_qaf_test.sh tests.exchanges.common_data_validate_tests
    
    /apps/bidder/run_single_qaf_test.sh tests.exchanges.common_data_validate_tests.CommonDataValidateTests
    
     /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_delphi_logging
     /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_delphi_request_and_response
     /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_log_delphi_internal_metadata
     /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_log_delphi_internal_metadata.TestLogDelphiInternalMetaData.test_log_delphi_internal_meta_data_from_pbc
     /apps/bidder/run_single_qaf_test.sh tests.pbs_regression.test_pbs_regression
     /apps/bidder/run_single_qaf_test.sh tests.pbs.test_pbs_endpoint
     /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_log_ccpa_privacy_string.TestLogCcpaPrivacyString
     /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_log_ccpa_privacy_string.TestLogCcpaPrivacyString.test_logging_from_pbc_when_only_ccpa_privacy_is_enabled
     
    
    /apps/bidder/run_single_qaf_test.sh tests.timemachine_tests.test_daypart_targeting.TestDayPartTargeting
    /apps/bidder/run_single_qaf_test.sh tests.timemachine_tests.test_daypart_targeting.TestDayPartTargeting.test_daypart_targeting_based_on_v0_timezone
    /apps/bidder/run_single_qaf_test.sh tests.timemachine_tests.test_daypart_targeting.TestDayPartTargeting.test_daypart_targeting_based_on_v1_geo_timezone_target_zip_with_duplicate_timezone_entry
    /apps/bidder/run_single_qaf_test.sh tests.timemachine_tests.test_daypart_targeting.TestDayPartTargeting.test_daypart_targeting_based_on_v1_geo_timezone
    /apps/bidder/run_single_qaf_test.sh tests.exchanges.appnexus.test_appnexus_cookie_max.TestAppNexusCookieMax
    
    cat /apps/bidder/static_geo_timezone_info.txt
    
#### Sync Tests
    /apps/bidder/run_single_qaf_test.sh  tests.general_tests.below_10s.test_exchange_user_sync
    /apps/bidder/run_single_qaf_test.sh  tests.general_tests.below_10s.test_user_sync_block_list
    /apps/bidder/run_single_qaf_test.sh  tests.general_tests.below_10s.test_multiple_user_syncs_gdpr
    /apps/bidder/run_single_qaf_test.sh  tests.general_tests.below_10s.test_multiple_user_syncs_tcfv2
    
    /apps/bidder/run_single_qaf_test.sh  tests.general_tests.below_10s.test_multiple_user_syncs_tcfv2.TestMultipleUserSyncsTCFv2.testBadGdprConsentStringSync 
    /apps/bidder/run_single_qaf_test.sh  tests.general_tests.below_10s.test_multiple_user_syncs_tcfv2.TestMultipleUserSyncsTCFv2.testSyncViaMT3 
    /apps/bidder/run_single_qaf_test.sh  tests.general_tests.below_10s.test_multiple_user_syncs_tcfv2.TestMultipleUserSyncsTCFv2.testSyncViaMT3_optedout_user 
    /apps/bidder/run_single_qaf_test.sh  tests.general_tests.below_10s.test_creative_adtag_template.TestCreativeAdtagTemplate.test_bidder_campaignID_not_in_campaign_whitelist_with_feature_flag
    /apps/bidder/run_single_qaf_test.sh  tests.general_tests.below_10s.test_creative_adtag_template.TestCreativeAdtagTemplate
    
    pub:
    http://zrh-bidder-x3.mediamath.com:8081/bid_sniffer?reset=0&max_recs=1000&capture=resp_bid_only&type=&exchange=pub&filter=&inc_enrich=&inc_delphi=&inc_auction=&campaign=&strategy=
    http://zrh-bidder-x3.mediamath.com:8081/bid_sniffer?reset=0&max_recs=1000&capture=all_notify_both&type=&exchange=pub&filter=&inc_enrich=&inc_delphi=&inc_auction=&campaign=&strategy=
    opx:
    http://zrh-bidder-x3.mediamath.com:8081/bid_sniffer?reset=0&max_recs=1000&capture=resp_bid_only&type=&exchange=opx&filter=&inc_enrich=&inc_delphi=&inc_auction=&campaign=&strategy=
    http://zrh-bidder-x3.mediamath.com:8081/bid_sniffer?reset=0&max_recs=1000&capture=all_notify_both&type=&exchange=opx&filter=&inc_enrich=&inc_delphi=&inc_auction=&campaign=&strategy=
    adx:
    http://zrh-bidder-x3.mediamath.com:8081/bid_sniffer?reset=0&max_recs=1000&capture=both_bid_only&type=&exchange=adx&filter=&inc_enrich=&inc_delphi=&inc_auction=&campaign=&strategy=
    http://zrh-bidder-x3.mediamath.com:8081/bid_sniffer?reset=0&max_recs=1000&capture=notify_adm&type=&exchange=adx&filter=&inc_enrich=&inc_delphi=&inc_auction=&campaign=&strategy=
    
    zrh-bidder-x3.mediamath.com:8081/bid_sniffer?reset=0&max_recs=1000&capture=notify_adm&type=&exchange=adx&filter=&inc_enrich=&inc_delphi=&inc_auction=&campaign=&strategy=
#### GOTTCHA in QAF test. 
    when writing code and writing qaf test, make sure you copy core-bidder/bidder/mmbid to /apps/bidder/ everytime, or you will run the OLD code
    root@core-bidder-dev:/workspaces/core-bidder# cp bidder/mmbid /apps/bidder/

#### how to run a SINGLE test
    /apps/bidder/run_single_qaf_test.sh tests.pbs_regression.test_pbs_regression.TestPBSRegression.test_ccpa_fields_pbs
    /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_bidder_macros.TestBidderMacros.test_tcfv2_vads
    
#### Unit tests
    /workspaces/core-bidder/unit_tests/runTests
    
    
    http://pao-bidder-x19.mediamath.com:8081/bid_sniffer?reset=0&max_recs=&filter=&capture=&exchange=&type=&inc_enrich=&inc_delphi=&inc_auction=&campaign=&strategy=
    
    
     /apps/bidder/mmbid --uid 0 --gid 0 --config /apps/bidder/bidder_config.json --clear_state
     
     
     
#### Bidder sample hosts
    http://ord-bidder-x202.mediamath.com:8081/config (ORD) (edited) 
    http://cdg-bidder-x23.mediamath.com:8081/config (CDG) (edited) 
    http://ewr-bidder-x10.mediamath.com:8081/config (EWR)
    http://hkg-bidder-x23.mediamath.com:8081/config (HKG)
    http://nrt-bidder-x3.mediamath.com:8081/config (NRT) (edited)
    http://pao-bidder-x202.mediamath.com:8081/config (PAO)
    http://zrh-bidder-x1.mediamath.com:8081/config )(ZRH)
    
    
 #### Config Client
    tail -f /d1/apps/config_client/log/client.log
    
    sudo service config_client restart 
    
    sudo service bidder restart
    sudo service mmtor restart


#### syncing exchanges in mathtag feature

    https://wiki.mediamath.com/wiki/pages/viewpage.action?spaceKey=mmEngineering&title=Migration+of+Bidder+Syncs+to+Mathtag
    https://wiki.mediamath.com/wiki/display/mmEngineering/Migration+of+Bidder+Syncs+to+Mathtag
    
    1. /apps/bidder/run_single_qaf_test.sh tests.exchanges.appnexus.test_appnexus_cookie_max.TestAppNexusCookieMax
     taabodi: GenerateUserSyncResponse : syncUrl : http://test.ib.adnxs.com/getuid?http://sync.mathtag.com/sync/img?mt_exid=13&mt_exuid=$UID
    2. /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.test_user_sync_block_list.TestUserSyncBlockList                      
    
    3. /apps/bidder/run_single_qaf_test.sh tests.general_tests.below_10s.TestExchangeUserSync.test_exchange_user_sync_via_mt2
    
    show iframe 
    http://tags.mathtag.com/notify/iframe?exch=cas&id=5aW95q2jLzIyLyAvTVRnNVpUVmxaamt0WVdVeVlTMDBNekF3TFdGaU56VXRaREkyTURGaFptSm1OMkZoLzYzOTE4OTAxOTUwMjU1OTczMy84MTgwMjY0LzYwMDE3NDAvMTUvejVDOEtsZ3dRMFFyVTVzeTU3WjUxcjN4U3hsQ2ZNdVNueG1QYXFxTjNYRS8yLzM0ODk4Mi8wLzAvMTMxNzMyNi83NzkyMjMyOTYvMTI5NjY4LzI2MjA1OC80LzAvMC9NVGc1WlRWbFpqa3RZV1V5WVMwME16QXdMV0ZpTnpVdFpESTJNREZoWm1KbU4yRmgvMC8wLzM0ODk4Mi8wLzEvNjM5MTg5MDE5NTAyNTU5NzMzL3pyaC8wLzUzOC8zLzUwLzU2LzQ2LjExNC4xLjAvMC4wMDAvMTU5NDE2MjQyMS8xNTk0MTc1MDIxLzE1Lw/h74WpFB4cQ1N43IDGFEAM7bOi6c&nodeid=35&group=eu&auctionid=639189019502559733&sid=6001740&cid=8180264&price=1.29&bp=b_cjaaaa&nfy_act=LD5wfnw&type=adm&client=c2s&act=LiIiJiQocHxrPSwuJCMqcHxrKy5wfGshIioqJCMqcHxrOiwkOQsiPwQgPQMiOSQrcH0&bfip=185.29.133.238&cp=1.29%20HTTP/1.1
    
    
    Rubicon.sso migration
    bidder dashboard : iad-router-x2 routes to iad-bidder-x2 (that's the pattern)
    https://mediamath.grafana.net/d/ehJdom1Zz/bidder-individual-bidder-stats?orgId=1&from=now-1h&to=now&var-POP=iad&var-NODE=iad-bidder-x2&var-Supply=ruc&var-BidEx_SVC=traffic_signal_white_op_ivt&refresh=1m
    
    /opt/mediamath/mm-schema# cat impression.avsc
    
##### Strategy TroubleShooting
    http://iad-bidder-x25.mediamath.com:8081/strategy_target?op=report&test_id=13011451931&stop=1
    
    
##### Geo counters 
    http://iad-bidder-x2.mediamath.com:8081/monitor?type=perfor_counter&fmt=json&names=Has%20IP%20Based%20Tz,Has%20Geo%20Based%20Tz,Has%20No%20Tz
    http://pao-bidder-x33.mediamath.com:8081/monitor?type=perfor_counter&fmt=json&names=Has%20IP%20Based%20Tz,Has%20Geo%20Based%20Tz,Has%20No%20Tz
    http://iad-bidder-x2.mediamath.com:8081/monitor?type=perfor_counter&group_by_exch=1&fmt=json&names=Has%20IP%20Based%20Tz,Has%20Geo%20Based%20Tz,Has%20No%20Tz
    http://pao-bidder-x33.mediamath.com:8081/monitor?type=perfor_counter&group_by_exch=1&fmt=json&names=Has%20IP%20Based%20Tz,Has%20Geo%20Based%20Tz,Has%20No%20Tz
    
    
    https://circleci.mediamath.com/gh/MediaMath/core-bidder
    
    
    http://zrh-bidder-x33.mediamath.com:8081/monitor?type=perfor_counter&group_by_exch=1&fmt=json&names=Audio%20Requests
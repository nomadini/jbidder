
#### Hosts
    http://cdg-router-x28.mediamath.com:8081/config
    http://zrh-router-x27.mediamath.com:8081/config
    
    canary deployed here
    http://zrh-router-x31.mediamath.com:8081/
    http://ewr-router-x23.mediamath.com:8081/menu
    
#### Bidflux regression test
    http://ewr-monitor-x1.mediamath.com:8080/job/Bidflux-PR-regression-tests/989/

#### how to run bidflux tests? 
    cd /workspaces/core-bidflux && /apps/bidflux/run_single_qaf_test.sh tests.exchanges.test_adaptv.TestAdaptv
    /apps/bidflux/run_single_qaf_test.sh tests.exchanges.test_adingo.TestAdingo
    /apps/bidflux/run_single_qaf_test.sh tests.exchanges.test_rubicon
    /apps/bidflux/run_single_qaf_test.sh tests.exchanges.test_rubicon2
    /apps/bidflux/run_single_qaf_test.sh tests.exchanges.test_smartclip.TestSmartClip
    
    
    cd /workspaces/core-bidflux && /apps/bidflux/run_single_qaf_test.sh tests.exchanges.test_adingo.TestAdingo
    cd /workspaces/core-bidflux && /apps/bidflux/run_single_qaf_test.sh tests.exchanges.test_adaptv.TestAdaptv
    cd /workspaces/core-bidflux && /apps/bidflux/run_single_qaf_test.sh tests.exchanges.test_pubmatic.TestPubmatic
    cd /workspaces/core-bidflux && /apps/bidflux/run_all_tests.sh
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_bidex_enrichment_ccpa.TestBidexEnrichmentCCPA.test_bidex_enrichment_key_blocked_by_ccpa_because_of_audience_enrichment
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_tcfv2_decisioning.TestTCFv2Decisioning
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_tcfv2_decisioning.TestTCFv2Decisioning.test_not_biding_for_banned_cmp
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_tcfv2_decisioning.TestTCFv2Decisioning.test_truncating_geo_for_bidding_when_user_has_no_geo_consent
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_bidex_enrichment_key_lat_long
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_bidex_enrichment_key_ua_ip
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_query_parrable
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_bidflux_filter
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_bidflux_filter.TestBidfluxFilter.test_case_01_c5
 
    
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_query_parrable.TestQueryParrable.test_query_parrable_with_ids_from_parrable_only
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_query_parrable.TestQueryParrable.test_query_parrable_with_ids_from_parrable_with_multiple_ids
    
    /apps/bidflux/run_single_qaf_test.sh tests.general_tests.test_bidex_enrichment_key_ua_ip.TestBidexEnrichmentKeyUaIp.test_bidex_enrichment_key_blocked_by_ccpa

#### diagnostic tool
    we have a diagnostic tool in bidflux and bidder
    http://zrh-router-x31.mediamath.com:8081/diagnose?verbose=
    header : x-openrtb-version:2.2 (required)
    
#### diagnostic tool (Bidder)
    make sure you send the request for the exchanges to /bid/mms 
    because they are the common format
    
#### How to send a bidrequest to bidflux : 
    1. get a nice bid request from bidder sniffer
    2. hit bidflux like this :
        this wont work for adx, because adx has moved to ssl you can set the ip of your current bidder in /etc/hosts file as bidder.mathtag.com host and curl it
        canary has the ssl certificate as bidder.mathtag.com, so try to hit that  
        curl -v -H "Content-Type: application/json" -H "x-openrtb-version:2.2" -H "x-mm-debug:3" -H "x-mm-test:3" -d @bid1.txt -X POST http://ord-router-x23.mediamath.com:9160/bid/adx
        
        
        this uses ssl, because for adx, we have moved to ssl 
        curl -v -H "Content-Type: application/json" -H "x-openrtb-version:2.2" -H "x-mm-debug:3" -H "x-mm-test:3" -d @bid.txt -X POST https:/bidder.mathtag.com:9061/bid/adx
        curl -v -H "Content-Type: application/json" -H "x-openrtb-version:2.2" -H "x-mm-debug:3" -H "x-mm-test:3" -d @original_audio_request_from_google.txt -X POST https://bidder.mathtag.com:9061/bid/adx
        
  
        curl -v -H "Content-Type: application/json" -d @config.txt -X POST http://ord-bvs-x1.ord.mmracks.internal:8081/http_config/bvs_config
         
    3. then filter on the sniffer with ip of bid request
    "ip":"98.19.171.0"
    
##### sniff 1000 normal bidder request     
    http://ewr-bidder-x7.mediamath.com:8081/bid_sniffer?reset=0&max_recs=1000&filter=&capture=both&exchange=bsw&type=&inc_enrich=&inc_delphi=&inc_auction=&campaign=&strategy=
    http://ewr-bidder-x2.mediamath.com:8081/bid_sniffer?reset=0&max_recs=1000&filter=&capture=both&exchange=bsw&type=&inc_enrich=&inc_delphi=&inc_auction=&campaign=&strategy=
        
##### sniff bidder request with specific ip    
    http://ewr-bidder-x2.mediamath.com:8081/bid_sniffer?reset=0&max_recs=1000&filter=ipv4[98.19.171.0]&capture=both&exchange=bsw&type=&inc_enrich=1&inc_delphi=&inc_auction=&campaign=&strategy=
    
##### sniff canary bidex canary
    * with enrichment result coming from canary bidflux 
    http://ewr-bidexgen-x30.mediamath.com:8081/enrichment_sniffer?reset=0&stop=0&service=3&type=req&from_host=ewr-router-x23&to_host=&write_to_log=0&max_recs=1000        
    
    http://ewr-bidexgen-x30.mediamath.com:8081/enrichment_sniffer?reset=0&stop=0&service=&type=both&from_host=&to_host=&write_to_log=0&max_recs=1000        
    
    http://ewr-bidexgen-x30.mediamath.com:8081/upstream_sniffer?reset=0&stop=0&service=3&dim=&type=all&ns_val=&write_to_log=0&max_recs=1000
    
#### How to check what we are sending to grafana
    vi /d1/apps/diamond/log/archive.log
    
#### How to check what we are logging as stats    
    http://zrh-router-x31.mediamath.com:8081/monitor?type=perfor_counter&fmt=json&names=request,bidable,bid,filtered,forwarded+bid+req,unparsable,new+conn,conn+error,notification,forwarded+notification,NFY_STAT%20Received,Notify%20Dropped%20-%20Queue%20Full,Notify%20Dropped%20-%20No%20Valid%20Bidder,Notify%20Dropped%20-%20Network%20Issue,Thread+pool+task+queue+is+full,io+error+with+backEnd,bidreq+query+idm,bidreq+ontime+query+idm,bidreq+timeout+query+idm,bidreq+hashed+idfa,unparsable+resp+from+idm,404+resp+from+idm,500+resp+from+idm,unhandled+req,[GDPR]+Can+Bid,[GDPR]+Consent+Check,[GDPR]+Pass(No+Consent),[GDPR]+Geo+Truncated
    
    
    http://tags.mathtag.com/notify/img/notify/img?wo_lookup=ead9ffab-522a-40d7-a864-5e393acf780d&count=1&ip=112.168.99.164&cmp=816294&creative_name=OB_KR_Budweiser_Henry-Campaign_Hero_vertical_(22sec)_0717_(LP_HeroFilm)&optout=0&bfip=103.229.206.106&ua=Mozilla%2F5.0+(Windows+NT+10.0%3B+Win64%3B+x64)+AppleWebKit%2F537.36+(KHTML%2C+like+Gecko)+Chrome%2F84.0.4147.89+Safari%2F537.36&uuid=78bf5e5b-7bea-4e00-99e4-0e2733b2f355&price=0.249&oid=101992&nodeid=559&site=op.gg&nfy_act=LD5wfnw&campaign_name=OB_Bud_Henry_Reach_Wk3_JUL&lmt=0&cvid=1&pid_string=6059&skippable=2&buyerCreativeId=8277414&id=5aW95q2jLzIyLyAvTnpoaVpqVmxOV0l0TjJKbFlTMDBaVEF3TFRrNVpUUXRNR1V5TnpNellqSm1NelUxLzg1Mzk4NjM0MzcwOTg2NTUzOC84Mjc3NDE0LzY0MzE0MjMvNjIvMFNueE1TNGtmSGpPYTk0TmhDTHdCYVNtbDlMdnhLY3dGd3dxV0pvM0tWQS8xLzYyLzAvMS8xMzI1MDE1LzE4OTAwODM3NDgvMjM2Mjg2LzgxNjI5NC8yLzAvMC9OemhpWmpWbE5XSXROMkpsWVMwMFpUQXdMVGs1WlRRdE1HVXlOek16WWpKbU16VTEvMC8wLzAvMC8wLzg1Mzk4NjM0MzcwOTg2NTUzOC9oa2cvMC85OTk1LzgwLzk5OS8yNTgvMTEyLjE2OC45OS4xNjQvMC4wMDAvMTU5NTUxOTI5OC8xNTk1NTM3Mjk4LzYyLw%2FwxfTihgcaRKeWzhPZmv7X5R9URs&cid=8277414&pid=6059&siteid=50995&bp=a_cejgjh&client=c2s&exch=gor&cdsp=false&cacheBuster=853986343709865538&auctionid=853986343709865538&mm_device=43003&type=adm&act=LiIiJiQocHxrPSwuJCMqcHxrKy5wfGshIioqJCMqcHxrOiwkOQsiPwQgPQMiOSQrcH0&protocol_version=1&group=hkg&page_url=https%3A%2F%2Fwww.op.gg%2Fchampion%2Fkarthus%2Fstatistics%2Fjungle&sid=6431423&strategy_name=Hero_all&s_exch=ss6&prot=https%3A%2F%2F&&cp=0.249%20HTTP/1.1
    
    curl -v -H "Content-Type: application/json" http://tags.mathtag.com/notify/js?exch=pub&id=5aW95q2jLzIyLyAvTjJVMVpEVmxabVl0TlRoak9DMDBNVEF3TFRnMk16SXRaakk0T0dKaE9XSmhORFk0LzY0NzY4NTIwNjc2OTcxMTMyNS84MjEwNzI2LzYzMTA5MDcvMy9TenROM0sweGl6Sy1Id0htSjlQUnhUbkZ6Tmk0UzdaU2tJQzQwRXlWUzRNLzIvMzk5NzYwLzAvMC8xMzE2NjA0LzEwNTQzOTYxNjAvMjM0NzIzLzgwODQ4OC8xLzAvMi9OMlUxWkRWbFptWXROVGhqT0MwME1UQXdMVGcyTXpJdFpqSTRPR0poT1dKaE5EWTQvMC8wLzM5OTc2MC8wLzEvNjQ3Njg1MjA2NzY5NzExMzI1L3pyaC8wLzU3OTgvNDUvMy84LzYyLjIxNi4yMDcuMC8wLjAwMC8xNTk0MjI1NjI5LzE1OTQyMzgyMjkvMy8/GZxbO6KuPo5Ixf6Z8aPU7tk5KSI&nodeid=35&group=eu&auctionid=647685206769711325&sid=6310907&cid=8210726&price=0.460000&bp=a_egaaaa&nfy_act=LD5wfnw&type=adm&client=c2s&act=LiIiJiQocHxrPSwuJCMqcHxrKy5wfGshIioqJCMqcHxrOiwkOQsiPwQgPQMiOSQrcH0&bfip=185.29.133.171&3pck=https%3A%2F%2Fclicktrack.pubmatic.com%2FAdServer%2FAdDisplayTrackerServlet%3FclickData%3DJnB1YklkPTE1ODA3NSZzaXRlSWQ9NTU1NTYyJmFkSWQ9MTk5MTE3OSZrYWRzaXplaWQ9OSZ0bGRJZD01NzEzMDU5OCZjYW1wYWlnbklkPTE2NzM1JmNyZWF0aXZlSWQ9MCZ1Y3JpZD01MzY5NTg3MjQ4NTEzNzQyMjMyJmFkU2VydmVySWQ9MjQzJmltcGlkPUVBMEU1RkI3LTkwMjQtNDEyOS1COEI2LUMyQTU0QUEzMzkzRSZwYXNzYmFjaz0w_url%3D&cp=0.460000%20HTTP/1.1
    
### iad canary has different url for bidflux
    http://iad-router-x2.iad.mmracks.internal:8081/menu


    ssh -p 722 mtaabodi@iad-router-x2.iad.mmracks.internal
    
    http://iad-router-x2.iad.mmracks.internal:8081/server?name=9090
    http://iad-router-x2.iad.mmracks.internal:8081/messagelog
#### Only build your so file in makeSoDocker.sh
	if [ $OUTPUT == "rubicon2.so" ]; then
        echo "Generating $OUTPUT file.."
        CMD=$CPP$CPPFLAGS$OUTPUT" "$f
        $CMD 
    fi      	
    if [ $OUTPUT == "rubicon.so" ]; then
        echo "Generating $OUTPUT file.."
        CMD=$CPP$CPPFLAGS$OUTPUT" "$f
        $CMD 
    fi      
    
    
    sudo kill -9 $(ps -ef | grep bidflux | awk '{print $2}') || true
    
    
#### How to check if an exchange is doing GDPR/TCFv2 work
    http://zrh-router-x21.mediamath.com:8081/monitor?type=perfor_counter&group_by_exch=1&fmt=json&names=request,bidable,bid,Gdpr%20Consent%20Check,Gdpr+Can+Bid,Gdpr+Pass,Gdpr+V2,Gdpr+V1,Gdpr+V0,Gdpr+Geo+Truncated
    
    http://zrh-bidder-x33.mediamath.com:8081/monitor?type=perfor_counter&group_by_exch=1&fmt=json&names=request,bidable,bid,GDPR%20Flag%20On,GDPR%20V1%20consent,GDPR%20V2%20consent,GDPR%20Bad%20Daisy%20Bit
#### How to check the identity-kv response in bidflux
    http://iad-router-x1.iad.mmracks.internal:8081/cross_device?userid=5dd55cfb-de86-4e00-a148-152959a6e3d4
    http://iad-router-x1.iad.mmracks.internal:8081/cross_device?userid=7e4f5f59-0646-4b00-9b37-bfc8346a02a6
    http://pao-router-x17.mediamath.com:8081/cross_device?userid=a0385f59-05f9-4a00-a0f2-ff5a7f1eccbb
    
    
#### How to log something in bidflux or bidder
        count++;
        if (count % 10000 = 1) {
            gBidfluxLog.info() << "taabodi :  size of presentOptouts : "  << presentOptouts.size();
            for (auto& optout : presentOptouts) {
                gBidfluxLog.info() << "taabodi : optout : , count : "  << optout << ", "<< count;
            }
            count = 0;
        }
        
#### How to log in jenkins container
    ssh -p 722 mtaabodi@iad-bidtools-x1.iad.mmracks.internal
    sudo docker ps
    sudo docker exec -it "core-jenkins-id" /bin/bash
    cd ~/workspaces
    cd Bidflux-PR-regression-tests    
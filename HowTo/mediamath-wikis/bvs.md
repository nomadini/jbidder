
http://ord-bvs-x1.ord.mmracks.internal:8081/

docker exec -it a5c625ceb4de /bin/bash
tail -f /apps/bvs/log/bvs/bvs.log
/apps/bvs/run_single_test.sh tests.general_tests.test_replication_dep
/apps/bvs/run_single_test.sh tests.general_tests.test_replication_http


curl -v -d @update_payload.json -X POST localhost:8084/update

curl -v -d @query_payload.json -X POST localhost:8084/query



gMsgLog.info()<< "send replication msg to "<< hostPort.string(); 
gMsgLog.info()<< "send replication data.string() "<< data.string(); 
        
        
query payload 

{
      "reply_to": "localhost:10000",
      "op": 1,
      "msgs": [
        {
          "op" :      1,
          "req_id" :  12345,
          "aux_id" :  0,
          "type" :    "did",
          "keys" :    ["02275f3d-5640-4c01-a684-6f873381bd8c", "94dc5f5a-be43-4701-8433-be26ed7e0ff5"]
        },
        {
          "op" :      1,
          "req_id" :  12346,
          "aux_id" :  0,
          "type" :    "did",
          "keys" :    ["3682aecc-acfc-4cd1-915b-64a0057235da"]
        },
        {
          "op" :      1,
          "req_id" :  12346,
          "aux_id" :  1,
          "type" :    "pmp",
          "keys" :    ["deal_1234", "deal_2234"]
        }
      ]
    }


update payload

{
	"op": 2,
	"msgs": [{
			"op": 2,
			"type": "did",
			"keys": ["02275f3d-5640-4c01-a684-6f873381bd8c", "94dc5f5a-be43-4701-8433-be26ed7e0ff5"],
			"bid": 1
		},
		{
			"op": 2,
			"type": "did",
			"keys": ["3682aecc-acfc-4cd1-915b-64a0057235da"],
			"bid": 0
		},
		{
			"op": 2,
			"type": "pmp",
			"keys": ["deal_1234", "deal_4321"],
			"bid": 1
		}
	]
}


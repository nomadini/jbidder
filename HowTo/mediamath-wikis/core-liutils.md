
#Important Note
 Make sure you run all the old images with the same tag, for core-lituls and core-qaf
 because if the same tag like latest exists, the newer version of latest wont get downloaded
 
#### How to change core-liutils and see your change in other apps? (for local development)
   * clone it in docker environment, make your changes, then run ./make_liutils.sh 
   then run these commands in core-liutils directory: cp -r include/* /opt/mediamath/include/ && cp lib/libmm_liutils_* /opt/mediamath/lib/

#### Liutil job (for building the tag so it's available in bidder/bidflux jenkin jobs)
    https://core-github-webhook.mediamath.com/job/Liutils/build

#### How to build and push branch image to circleCI for core-liutil app (
    you don't have to do this for local development though,
    because you will need to recreate the image and that takes a long time)
   * modify .circleci/config.yml like below  
````
    name: Build Image
    command: |
      docker build -f docker/Dockerfile -t core-liutils:local .	      
      if [ "$CIRCLE_BRANCH" != "master" ]; then
          docker tag core-liutils:local docker.mediamath.com/media/core-liutils:1.30.2-MT
          docker push docker.mediamath.com/media/core-liutils:1.30.2-MT
      fi

````
   * there is a Liutils job in jenkins that you need to build you branch tag with,
   so jenkins job passes.


    https://circleci.mediamath.com/gh/MediaMath/workflows/core-liutils
    
### How to use your branch of core-qaf-lib in other project?
    branch is tagged and pushed as a docker env by CircleCI
    
    https://circleci.mediamath.com/gh/MediaMath/core-qaf-lib
    
### How to use your branch of core-qaf-lib in jenkins job for other projects?
    let's say your branch name is userSyncMigration, 
    change requirements.txt to be like this, before eqq=qaf
    -e git+ssh://git@github.com/MediaMath/core-qaf-lib.git@userSyncMigration#egg=qaf
    
    
    
    
### How to use the latest qaf in docker env
    run this `pip show qaf | grep "Location:" | cut -d " " -f2` to see where python packages are installed
    
    
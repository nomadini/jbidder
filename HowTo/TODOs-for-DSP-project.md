# create a docker bidder
# create a docker adserver
# create a docker scorer
# create a docker modeler
# create a docker exchangeSimulator
# deploy them using docker compose
# deploy them using docker kubernetes in aws




#UI tasks
  
    1. show dma on a map and indexes per dma 
    this is the dma geojson
    https://raw.githubusercontent.com/yergi/Nielsen-DMA-Spatial-File/master/dma.geojson

    1. har jaa v-select darim ro test kon ke vuex ro dorost taghir bede mesle pacingMethod to tg
    
        cross walk va retarging ro  haa ro be soorat e tab dar biar ta tree e segment haa dorost bashe
        bara har interest faghat ye model segment dorost kon va score kon bara oon interest ta scale bishtar begire
        va ye retargeting segment dorost kon 
        ``
        
        roo map kar kon(map ziad mohem nist, bezar baara badan)
        
        get the full audience list with website ranks as seed for them
        fix the audit log links
        add visit data pruning logic to improve quality of visit data    
                
    create model for scoring peolpe in male/femal and age categories(demographics analysis) 
    fix the picture uploading in website
     parse websites and stuff top key words into event log and find the top event log 
            for each device segments
            
            5.a add DMA to the hierarchy and the bidder to target dma segment 
            and convert lat lon to dma for scoring using polygons
            
            5.score zip codes and dma and device types for segment overlap counts and etc
            5.4 populate the segment hierarchy based on segment types
            5.7 add isBiddingOn flag to tg page so we know if a tg is being bid on actively or not
            
    
    1. finish the assignments in tg bulk page (bulk edit still has some subtle bugs)
    2. add targetGroup list to dashboard , with an option to see the filter chart
    2. add more criteria to tg page (add zip code, metro code targeting, browser, and other stuff)
        all browser types
            [{"id":1,"name":"Internet Explorer"},{"id":2,"name":"Firefox"},{"id":3,"name":"Chrome"},{"id":4,"name":"Safari"},{"id":5,"name":"Opera"},{"id":6,"name":"Netscape"},{"id":7,"name":"AOL"},{"id":8,"name":"Silk"},{"id":9,"name":"Opera Mini"},{"id":10,"name":"Internet Explorer Mobile"},{"id":12,"name":"Microsoft Edge"}]
            all connection types
            [{"id":2,"name":"CABLE_DSL"},{"id":3,"name":"CORPORATE"},{"id":4,"name":"CELLULAR"}]
            
            all isps 
            [{"id":1,"name":"Verizon Wireless"},{"id":2,"name":"AT&T Wireless"},{"id":3,"name":"T-Mobile USA"},{"id":4,"name":"Sprint PCS"},{"id":5,"name":"Time Warner"},{"id":6,"name":"Comcast"},{"id":9,"name":"AT&T U-verse"},{"id":10,"name":"Charter Communications"},{"id":11,"name":"Verizon Fios"},{"id":12,"name":"CenturyLink"},{"id":13,"name":"Cox Communications"},{"id":14,"name":"Frontier Communications"},{"id":16,"name":"Optimum Online"},{"id":17,"name":"AT&T Internet Services"},{"id":18,"name":"Amazon Technologies"},{"id":19,"name":"Rogers Cable"},{"id":20,"name":"Bell Canada"},{"id":21,"name":"Shaw Communications"},{"id":22,"name":"Telus Communications"},{"id":23,"name":"Suddenlink Communications"},{"id":24,"name":"Zayo Bandwidth"},{"id":25,"name":"Sky Broadband"},{"id":26,"name":"Windstream Communications"},{"id":27,"name":"Mediacom Cable"},{"id":28,"name":"BT"},{"id":29,"name":"Videotron Ltee"},{"id":30,"name":"Virgin Media"},{"id":31,"name":"Google"},{"id":32,"name":"Comcast Business"},{"id":33,"name":"Verizon Internet Services"},{"id":34,"name":"Bright House Networks"},{"id":35,"name":"Wireless Data Service Provider Corporation"},{"id":36,"name":"WideOpenWest"},{"id":43,"name":"Cable One"},{"id":45,"name":"AT&T Services"},{"id":48,"name":"Cogeco Cable"},{"id":49,"name":"RCN"},{"id":50,"name":"Bell Aliant"},{"id":60,"name":"Bell Mobility"},{"id":63,"name":"Verizon Business"},{"id":71,"name":"AT&T Wi-Fi Services"},{"id":76,"name":"Level 3 Communications"},{"id":103,"name":"Verizon Fios Business"},{"id":116,"name":"Other "},{"id":117,"name":"EPB"},{"id":118,"name":"Metronet"},{"id":119,"name":"Spectrum"},{"id":120,"name":"LogicWeb Inc"},{"id":121,"name":"Globalive Wireless Management Corp."},{"id":122,"name":"Scansafe"},{"id":123,"name":"Comporium"},{"id":124,"name":"Vodafone Limited"},{"id":125,"name":"EPB Fiber Optics"},{"id":126,"name":"Distributel Communications"},{"id":127,"name":"Integra Telecom"},{"id":128,"name":"Hawaiian Telcom"},{"id":129,"name":"Hotwire Fision"}]
        
        
    6. cache total counts and impressive results of druid and only do last week last day counts
    
    update reporing to have dma and zip widgets
    
    add GPS coordinates as a form of file upload to trees in edit tg and audience(test this)
    
        add propect segments to the edit tg and audience select and 
        compound audience kitchen
         
        create compare segment section
        add lift inedx of each segment based on a selected segment
        add segment composition 
        think about segment top lift index by category(location, places, websites, domains)
        
        add search capabilities for segments to find the right segmnet 
        an to go from a term to a website to be able 
        to create compound audience 
        this is an example of sentiment analysis use this for smart search
        https://github.com/deeplearning4j/dl4j-examples/blob/master/dl4j-examples/src/main/java/org/deeplearning4j/examples/recurrent/word2vecsentiment/Word2VecSentimentRNN.java
        
        use this to find the category of publisher content and do targeting based on that
        https://github.com/deeplearning4j/dl4j-examples/blob/master/dl4j-examples/src/main/java/org/deeplearning4j/examples/recurrent/processnews/TrainNews.java
        
        6.Add more System-KPI.md items2.use scylladb in digital ocean that you installed and see how good it is
    5.make your cassanda driver to read and write to multiple cassandra clusters
    to be able to replicate data and compare cassandra vs scylladb
   
   
# immediate tasks to do next
 
* use interest_content.tsv to map websites to interest and bid on them  (Look at InterestTreeParser class)
* add lucene targeting capability for a url  (ELASTIC search)
* add modelling based on search terms that come from referer url 
* save impression events in files too, for safety of data
* calculate the boost factor for each segment comparing to the base line 
and show them, like platform in dstillery

fix the tracing in cassandra driver ---> very important
fix the not bidding when adserver is down ---> very important

* DO THE DOOMSDAY TEST! (Process Test) 
  * turn off the external services like cassandra and other dbs and see
   if the apps stop working and start alarming as expected
  * turn off adserver and see if bidder stops
  * keep doing this test every month, never delete this process
  * test and see if modeler doesn't work, do we alarm?
  * test and see if scoring doesnt work, do we alarm?
   
##
 * run and monitor modeler and scoring

* https://github.com/tdebatty/java-LSH (use this lib to calculate segment sizes)

# Future Stuff
* add Deep Learning capability using Deep Learning 4j (in java)
* add scraping and insights for pixels and segments and geo categories
* add segments that were created based on offer in the edit offer page
* add image classificaiton based on tenser flow for each url that comes as bidding
* add StochasticSGD which you already have as another algo, There are many other classifiers
add them as other algos too
* add Support Vector Regression from shogun , you can get coeffiecients too as another alog :
http://www.shogun-toolbox.org/examples/latest/examples/regression/support_vector_regression.html

 vue-tables-2 component is very good with lots of capabilities
    az https://amsik.github.io/liquor-tree/#Drag-amp-Drop bara tree estefade kon
    drag va drop ham dare
    deeplearning4j for neural network classification
    https://github.com/deeplearning4j/dl4j-examples/tree/master/dl4j-spark-examples/dl4j-spark/src/main/java/org/deeplearning4j/patent
    https://deeplearning4j.org/tutorials/03-logistic-regression
  

 this is how we calculate performance index, and attribute index 
    
    "create table costa.demo_calculated stored as orc as
    select attribute_interest_id as subject_interest_id, descr, subject_interest_id as attribute_interest_id,
    record.overlap_count/record.attribute_count composition, 
    (record.overlap_count/record.attribute_with_subject_type_count)/(record.subject_with_attribute_type_count/record.total_devices_with_subject_and_attribute_type_count) attribute_index, 
    (record.overlap_count/record.attribute_count)/(record.subject_count/record.total_devices_count) performance_index
    from demo_data;"
    
    SELECT
        run_stamp,
        indexee_interest_id,
        map(${siteId}, collect(named_struct(
            "content_id", indexer_content_id,
            "index_score", cast(index_score as double),
            "composition", cast(composition as double),
            "expansionFactor", cast(expansionFactor as double))))
    FROM
        (SELECT
            ${runStamp} run_stamp,
            a.indexee_interest_id indexee_interest_id,
            a.indexer_content_id indexer_content_id,
            (a.device_count / subject_counts.subject_count_with_attribute) / xy.x_over_y index_score,
            a.device_count / subject_counts.subject_count composition,
            xy.x_device_count / a.device_count expansionFactor,
            -- any changes to index score calc must also be made to the following sort clause
            ROW_NUMBER() OVER (PARTITION BY a.indexee_interest_id ORDER BY (a.device_count / subject_counts.subject_count_with_attribute) / xy.x_over_y DESC) AS rank
        FROM
            tmp_interest_type_${interestTypeId}_site_${siteId}_a a
        JOIN
            tmp_interest_type_${interestTypeId}_site_${siteId}_subject_counts subject_counts
                on (a.indexee_interest_id=subject_counts.indexee_interest_id)
        JOIN
            tmp_interest_type_${interestTypeId}_site_${siteId}_x_over_y xy
                on (a.indexer_content_id=xy.indexer_content_id)
        WHERE
            subject_counts.subject_count_with_attribute * xy.x_over_y > ${magicScoreThreshold} and a.indexee_interest_id in (${candidateInterestIds})
        ) innerq
    WHERE
        rank <= ${topNContentsPerInterest}
    GROUP BY
        run_stamp,
        indexee_interest_id
    ;

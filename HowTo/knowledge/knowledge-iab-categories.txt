{
	"iabCategories": [{
		"deviceId": "IAB1",
		"name": "Arts & Entertainment",
		"children": [{
			"deviceId": "IAB1-1",
			"name": "Books & Literature"
		}, {
			"deviceId": "IAB1-2",
			"name": "Celebrity Fan/Gossip"
		}, {
			"deviceId": "IAB1-3",
			"name": "Fine Art"
		}, {
			"deviceId": "IAB1-4",
			"name": "Humor"
		}, {
			"deviceId": "IAB1-5",
			"name": "Movies"
		}, {
			"deviceId": "IAB1-6",
			"name": "Music"
		}, {
			"deviceId": "IAB1-7",
			"name": "Television"
		}]
	}, {
		"deviceId": "IAB2",
		"name": "Automotive",
		"children": [{
			"deviceId": "IAB2-1",
			"name": "Auto Parts"
		}, {
			"deviceId": "IAB2-2",
			"name": "Auto Repair"
		}, {
			"deviceId": "IAB2-3",
			"name": "Buying/Selling Cars"
		}, {
			"deviceId": "IAB2-4",
			"name": "Car Culture"
		}, {
			"deviceId": "IAB2-5",
			"name": "Certified Pre-Owned"
		}, {
			"deviceId": "IAB2-6",
			"name": "Convertible"
		}, {
			"deviceId": "IAB2-7",
			"name": "Coupe"
		}, {
			"deviceId": "IAB2-8",
			"name": "Crossover"
		}, {
			"deviceId": "IAB2-9",
			"name": "Diesel"
		}, {
			"deviceId": "IAB2-10",
			"name": "Electric Vehicle"
		}, {
			"deviceId": "IAB2-11",
			"name": "Hatchback"
		}, {
			"deviceId": "IAB2-12",
			"name": "Hybrid"
		}, {
			"deviceId": "IAB2-13",
			"name": "Luxury"
		}, {
			"deviceId": "IAB2-14",
			"name": "MiniVan"
		}, {
			"deviceId": "IAB2-15",
			"name": "Mororcycles"
		}, {
			"deviceId": "IAB2-16",
			"name": "Off-Road Vehicles"
		}, {
			"deviceId": "IAB2-17",
			"name": "Performance Vehicles"
		}, {
			"deviceId": "IAB2-18",
			"name": "Pickup"
		}, {
			"deviceId": "IAB2-19",
			"name": "Road-Side Assistance"
		}, {
			"deviceId": "IAB2-20",
			"name": "Sedan"
		}, {
			"deviceId": "IAB2-21",
			"name": "Trucks & Accessories"
		}, {
			"deviceId": "IAB2-22",
			"name": "Vintage Cars"
		}, {
			"deviceId": "IAB2-23",
			"name": "Wagon"
		}]
	}, {
		"deviceId": "IAB3",
		"name": "Business",
		"children": [{
			"deviceId": "IAB3-1",
			"name": "Advertising"
		}, {
			"deviceId": "IAB3-2",
			"name": "Agriculture"
		}, {
			"deviceId": "IAB3-3",
			"name": "Biotech/Biomedical"
		}, {
			"deviceId": "IAB3-4",
			"name": "Business Software"
		}, {
			"deviceId": "IAB3-5",
			"name": "Construction"
		}, {
			"deviceId": "IAB3-6",
			"name": "Forestry"
		}, {
			"deviceId": "IAB3-7",
			"name": "Government"
		}, {
			"deviceId": "IAB3-8",
			"name": "Green Solutions"
		}, {
			"deviceId": "IAB3-9",
			"name": "Human Resources"
		}, {
			"deviceId": "IAB3-10",
			"name": "Logistics"
		}, {
			"deviceId": "IAB3-11",
			"name": "Marketing"
		}, {
			"deviceId": "IAB3-12",
			"name": "Metals"
		}]
	}, {
		"deviceId": "IAB4",
		"name": "Careers",
		"children": [{
			"deviceId": "IAB4-1",
			"name": "Career Planning"
		}, {
			"deviceId": "IAB4-2",
			"name": "College"
		}, {
			"deviceId": "IAB4-3",
			"name": "Financial Aid"
		}, {
			"deviceId": "IAB4-4",
			"name": "Job Fairs"
		}, {
			"deviceId": "IAB4-5",
			"name": "Job Search"
		}, {
			"deviceId": "IAB4-6",
			"name": "Resume Writing/Advice"
		}, {
			"deviceId": "IAB4-7",
			"name": "Nursing"
		}, {
			"deviceId": "IAB4-8",
			"name": "Scholarships"
		}, {
			"deviceId": "IAB4-9",
			"name": "Telecommuting"
		}, {
			"deviceId": "IAB4-10",
			"name": "U.S. Military"
		}, {
			"deviceId": "IAB4-11",
			"name": "Career Advice"
		}]
	}, {
		"deviceId": "IAB5",
		"name": "Education",
		"children": [{
			"deviceId": "IAB5-1",
			"name": "7-12 Education"
		}, {
			"deviceId": "IAB5-2",
			"name": "Adult Education"
		}, {
			"deviceId": "IAB5-3",
			"name": "Art History"
		}, {
			"deviceId": "IAB5-4",
			"name": "College Administration"
		}, {
			"deviceId": "IAB5-5",
			"name": "College Life"
		}, {
			"deviceId": "IAB5-6",
			"name": "Distance Learning"
		}, {
			"deviceId": "IAB5-7",
			"name": "English as a 2nd Language"
		}, {
			"deviceId": "IAB5-8",
			"name": "Language Learning"
		}, {
			"deviceId": "IAB5-9",
			"name": "Graduate School"
		}, {
			"deviceId": "IAB5-10",
			"name": "Homeschooling"
		}, {
			"deviceId": "IAB5-11",
			"name": "Homework/Study Tips"
		}, {
			"deviceId": "IAB5-12",
			"name": "K-6 Educators"
		}, {
			"deviceId": "IAB5-13",
			"name": "Private School"
		}, {
			"deviceId": "IAB5-14",
			"name": "Special Education"
		}, {
			"deviceId": "IAB5-15",
			"name": "Studying Business"
		}]
	}, {
		"deviceId": "IAB6",
		"name": "Family & Parenting",
		"children": [{
			"deviceId": "IAB6-1",
			"name": "Adoption"
		}, {
			"deviceId": "IAB6-2",
			"name": "Babies & Toddlers"
		}, {
			"deviceId": "IAB6-3",
			"name": "Daycare/Pre School"
		}, {
			"deviceId": "IAB6-4",
			"name": "Family Internet"
		}, {
			"deviceId": "IAB6-5",
			"name": "Parenting - K-6 Kids"
		}, {
			"deviceId": "IAB6-6",
			"name": "Parenting teens"
		}, {
			"deviceId": "IAB6-7",
			"name": "Pregnancy"
		}, {
			"deviceId": "IAB6-8",
			"name": "Special Needs Kids"
		}, {
			"deviceId": "IAB6-9",
			"name": "Eldercare"
		}]
	}, {
		"deviceId": "IAB7",
		"name": "Health & Fitness",
		"children": [{
			"deviceId": "IAB7-1",
			"name": "Exercise"
		}, {
			"deviceId": "IAB7-2",
			"name": "A.D.D."
		}, {
			"deviceId": "IAB7-3",
			"name": "AIDS/HIV"
		}, {
			"deviceId": "IAB7-4",
			"name": "Allergies"
		}, {
			"deviceId": "IAB7-5",
			"name": "Alternative Medicine"
		}, {
			"deviceId": "IAB7-6",
			"name": "Arthritis"
		}, {
			"deviceId": "IAB7-7",
			"name": "Asthma"
		}, {
			"deviceId": "IAB7-8",
			"name": "Autism/PDD"
		}, {
			"deviceId": "IAB7-9",
			"name": "Bipolar Disorder"
		}, {
			"deviceId": "IAB7-10",
			"name": "Brain Tumor"
		}, {
			"deviceId": "IAB7-11",
			"name": "Cancer"
		}, {
			"deviceId": "IAB7-12",
			"name": "Cholesterol"
		}, {
			"deviceId": "IAB7-13",
			"name": "Chronic Fatigue Syndrome"
		}, {
			"deviceId": "IAB7-14",
			"name": "Chronic Pain"
		}, {
			"deviceId": "IAB7-15",
			"name": "Cold & Flu"
		}, {
			"deviceId": "IAB7-16",
			"name": "Deafness"
		}, {
			"deviceId": "IAB7-17",
			"name": "Dental Care"
		}, {
			"deviceId": "IAB7-18",
			"name": "Depression"
		}, {
			"deviceId": "IAB7-19",
			"name": "Dermatology"
		}, {
			"deviceId": "IAB7-20",
			"name": "Diabetes"
		}, {
			"deviceId": "IAB7-21",
			"name": "Epilepsy"
		}, {
			"deviceId": "IAB7-22",
			"name": "GERD/Acid Reflux"
		}, {
			"deviceId": "IAB7-23",
			"name": "Headaches/Migraines"
		}, {
			"deviceId": "IAB7-24",
			"name": "Heart Disease"
		}, {
			"deviceId": "IAB7-25",
			"name": "Herbs for Health"
		}, {
			"deviceId": "IAB7-26",
			"name": "Holistic Healing"
		}, {
			"deviceId": "IAB7-27",
			"name": "IBS/Crohn's Disease"
		}, {
			"deviceId": "IAB7-28",
			"name": "Incest/Abuse Support"
		}, {
			"deviceId": "IAB7-29",
			"name": "Incontinence"
		}, {
			"deviceId": "IAB7-30",
			"name": "Infertility"
		}, {
			"deviceId": "IAB7-31",
			"name": "Men's Health"
		}, {
			"deviceId": "IAB7-32",
			"name": "Nutrition"
		}, {
			"deviceId": "IAB7-33",
			"name": "Orthopedics"
		}, {
			"deviceId": "IAB7-34",
			"name": "Panic/Anxiety Disorders"
		}, {
			"deviceId": "IAB7-35",
			"name": "Pediatrics"
		}, {
			"deviceId": "IAB7-36",
			"name": "Physical Therapy"
		}, {
			"deviceId": "IAB7-37",
			"name": "Psychology/Psychiatry"
		}, {
			"deviceId": "IAB7-38",
			"name": "Senor Health"
		}, {
			"deviceId": "IAB7-39",
			"name": "Sexuality"
		}, {
			"deviceId": "IAB7-40",
			"name": "Sleep Disorders"
		}, {
			"deviceId": "IAB7-41",
			"name": "Smoking Cessation"
		}, {
			"deviceId": "IAB7-42",
			"name": "Substance Abuse"
		}, {
			"deviceId": "IAB7-43",
			"name": "Thyroid Disease"
		}, {
			"deviceId": "IAB7-44",
			"name": "Weight Loss"
		}, {
			"deviceId": "IAB7-45",
			"name": "Women's Health"
		}]
	}, {
		"deviceId": "IAB8",
		"name": "Food & Drink",
		"children": [{
			"deviceId": "IAB8-1",
			"name": "American Cuisine"
		}, {
			"deviceId": "IAB8-2",
			"name": "Barbecues & Grilling"
		}, {
			"deviceId": "IAB8-3",
			"name": "Cajun/Creole"
		}, {
			"deviceId": "IAB8-4",
			"name": "Chinese Cuisine"
		}, {
			"deviceId": "IAB8-5",
			"name": "Cocktails/Beer"
		}, {
			"deviceId": "IAB8-6",
			"name": "Coffee/Tea"
		}, {
			"deviceId": "IAB8-7",
			"name": "Cuisine-Specific"
		}, {
			"deviceId": "IAB8-8",
			"name": "Desserts & Baking"
		}, {
			"deviceId": "IAB8-9",
			"name": "Dining Out"
		}, {
			"deviceId": "IAB8-10",
			"name": "Food Allergies"
		}, {
			"deviceId": "IAB8-11",
			"name": "French Cuisine"
		}, {
			"deviceId": "IAB8-12",
			"name": "Health/Lowfat Cooking"
		}, {
			"deviceId": "IAB8-13",
			"name": "Italian Cuisine"
		}, {
			"deviceId": "IAB8-14",
			"name": "Japanese Cuisine"
		}, {
			"deviceId": "IAB8-15",
			"name": "Mexican Cuisine"
		}, {
			"deviceId": "IAB8-16",
			"name": "Vegan"
		}, {
			"deviceId": "IAB8-17",
			"name": "Vegetarian"
		}, {
			"deviceId": "IAB8-18",
			"name": "Wine"
		}]
	}, {
		"deviceId": "IAB9",
		"name": "Hobbies & Interests",
		"children": [{
			"deviceId": "IAB9-1",
			"name": "Art/Technology"
		}, {
			"deviceId": "IAB9-2",
			"name": "Arts & Crafts"
		}, {
			"deviceId": "IAB9-3",
			"name": "Beadwork"
		}, {
			"deviceId": "IAB9-4",
			"name": "Birdwatching"
		}, {
			"deviceId": "IAB9-5",
			"name": "Board Games/Puzzles"
		}, {
			"deviceId": "IAB9-6",
			"name": "Candle & Soap Making"
		}, {
			"deviceId": "IAB9-7",
			"name": "Card Games"
		}, {
			"deviceId": "IAB9-8",
			"name": "Chess"
		}, {
			"deviceId": "IAB9-9",
			"name": "Cigars"
		}, {
			"deviceId": "IAB9-10",
			"name": "Collecting"
		}, {
			"deviceId": "IAB9-11",
			"name": "Comic Books"
		}, {
			"deviceId": "IAB9-12",
			"name": "Drawing/Sketching"
		}, {
			"deviceId": "IAB9-13",
			"name": "Freelance Writing"
		}, {
			"deviceId": "IAB9-14",
			"name": "Genealogy"
		}, {
			"deviceId": "IAB9-15",
			"name": "Getting Published"
		}, {
			"deviceId": "IAB9-16",
			"name": "Guitar"
		}, {
			"deviceId": "IAB9-17",
			"name": "Home Recording"
		}, {
			"deviceId": "IAB9-18",
			"name": "Investors & Patents"
		}, {
			"deviceId": "IAB9-19",
			"name": "Jewelry Making"
		}, {
			"deviceId": "IAB9-20",
			"name": "Magic & Illusion"
		}, {
			"deviceId": "IAB9-21",
			"name": "Needlework"
		}, {
			"deviceId": "IAB9-22",
			"name": "Painting"
		}, {
			"deviceId": "IAB9-23",
			"name": "Photography"
		}, {
			"deviceId": "IAB9-24",
			"name": "Radio"
		}, {
			"deviceId": "IAB9-25",
			"name": "Roleplaying Games"
		}, {
			"deviceId": "IAB9-26",
			"name": "Sci-Fi & Fantasy"
		}, {
			"deviceId": "IAB9-27",
			"name": "Scrapbooking"
		}, {
			"deviceId": "IAB9-28",
			"name": "Screenwriting"
		}, {
			"deviceId": "IAB9-29",
			"name": "Stamps & Coins"
		}, {
			"deviceId": "IAB9-30",
			"name": "Video & Computer Games"
		}, {
			"deviceId": "IAB9-31",
			"name": "Woodworking"
		}]
	}, {
		"deviceId": "IAB10",
		"name": "Home & Garden",
		"children": [{
			"deviceId": "IAB10-1",
			"name": "Appliances"
		}, {
			"deviceId": "IAB10-2",
			"name": "Entertaining"
		}, {
			"deviceId": "IAB10-3",
			"name": "Environmental Safety"
		}, {
			"deviceId": "IAB10-4",
			"name": "Gardening"
		}, {
			"deviceId": "IAB10-5",
			"name": "Home Repair"
		}, {
			"deviceId": "IAB10-6",
			"name": "Home Theater"
		}, {
			"deviceId": "IAB10-7",
			"name": "Interior Decorating"
		}, {
			"deviceId": "IAB10-8",
			"name": "Landscaping"
		}, {
			"deviceId": "IAB10-9",
			"name": "Remodeling & Construction"
		}]
	}, {
		"deviceId": "IAB11",
		"name": "Law, Gov't & Politics",
		"children": [{
			"deviceId": "IAB11-1",
			"name": "Immigration"
		}, {
			"deviceId": "IAB11-2",
			"name": "Legal Issues"
		}, {
			"deviceId": "IAB11-3",
			"name": "U.S. Government Resources"
		}, {
			"deviceId": "IAB11-4",
			"name": "Politics"
		}, {
			"deviceId": "IAB11-5",
			"name": "Commentary"
		}]
	}, {
		"deviceId": "IAB12",
		"name": "News",
		"children": [{
			"deviceId": "IAB12-1",
			"name": "International News"
		}, {
			"deviceId": "IAB12-2",
			"name": "National News"
		}, {
			"deviceId": "IAB12-3",
			"name": "Local News"
		}]
	}, {
		"deviceId": "IAB13",
		"name": "Personal Finance",
		"children": [{
			"deviceId": "IAB13-1",
			"name": "Beginning Investing"
		}, {
			"deviceId": "IAB13-2",
			"name": "Credit/Debt & Loans"
		}, {
			"deviceId": "IAB13-3",
			"name": "Financial News"
		}, {
			"deviceId": "IAB13-4",
			"name": "Financial Planning"
		}, {
			"deviceId": "IAB13-5",
			"name": "Hedge Fund"
		}, {
			"deviceId": "IAB13-6",
			"name": "Insurance"
		}, {
			"deviceId": "IAB13-7",
			"name": "Investing"
		}, {
			"deviceId": "IAB13-8",
			"name": "Mutual Funds"
		}, {
			"deviceId": "IAB13-9",
			"name": "Options"
		}, {
			"deviceId": "IAB13-10",
			"name": "Retirement Planning"
		}, {
			"deviceId": "IAB13-11",
			"name": "Stocks"
		}, {
			"deviceId": "IAB13-12",
			"name": "Tax Planning"
		}]
	}, {
		"deviceId": "IAB14",
		"name": "Society",
		"children": [{
			"deviceId": "IAB14-1",
			"name": "Dating"
		}, {
			"deviceId": "IAB14-2",
			"name": "Divorce Support"
		}, {
			"deviceId": "IAB14-3",
			"name": "Gay Life"
		}, {
			"deviceId": "IAB14-4",
			"name": "Marriage"
		}, {
			"deviceId": "IAB14-5",
			"name": "Senior Living"
		}, {
			"deviceId": "IAB14-6",
			"name": "Teens"
		}, {
			"deviceId": "IAB14-7",
			"name": "Weddings"
		}, {
			"deviceId": "IAB14-8",
			"name": "Ethnic Specific"
		}]
	}, {
		"deviceId": "IAB15",
		"name": "Science",
		"children": [{
			"deviceId": "IAB15-1",
			"name": "Astrology"
		}, {
			"deviceId": "IAB15-2",
			"name": "Biology"
		}, {
			"deviceId": "IAB15-3",
			"name": "Chemistry"
		}, {
			"deviceId": "IAB15-4",
			"name": "Geology"
		}, {
			"deviceId": "IAB15-5",
			"name": "Paranormal Phenomena"
		}, {
			"deviceId": "IAB15-6",
			"name": "Physics"
		}, {
			"deviceId": "IAB15-7",
			"name": "Space/Astronomy"
		}, {
			"deviceId": "IAB15-8",
			"name": "Geography"
		}, {
			"deviceId": "IAB15-9",
			"name": "Botany"
		}, {
			"deviceId": "IAB15-10",
			"name": "Weather"
		}]
	}, {
		"deviceId": "IAB16",
		"name": "Pets",
		"children": [{
			"deviceId": "IAB16-1",
			"name": "Aquariums"
		}, {
			"deviceId": "IAB16-2",
			"name": "Birds"
		}, {
			"deviceId": "IAB16-3",
			"name": "Cats"
		}, {
			"deviceId": "IAB16-4",
			"name": "Dogs"
		}, {
			"deviceId": "IAB16-5",
			"name": "Large Animals"
		}, {
			"deviceId": "IAB16-6",
			"name": "Reptiles"
		}, {
			"deviceId": "IAB16-7",
			"name": "Veterinary Medicine"
		}]
	}, {
		"deviceId": "IAB17",
		"name": "Sports",
		"children": [{
			"deviceId": "IAB17-1",
			"name": "Auto Racing"
		}, {
			"deviceId": "IAB17-2",
			"name": "Baseball"
		}, {
			"deviceId": "IAB17-3",
			"name": "Bicycling"
		}, {
			"deviceId": "IAB17-4",
			"name": "Bodybuilding"
		}, {
			"deviceId": "IAB17-5",
			"name": "Boxing"
		}, {
			"deviceId": "IAB17-6",
			"name": "Canoeing/Kayaking"
		}, {
			"deviceId": "IAB17-7",
			"name": "Cheerleading"
		}, {
			"deviceId": "IAB17-8",
			"name": "Climbing"
		}, {
			"deviceId": "IAB17-9",
			"name": "Cricket"
		}, {
			"deviceId": "IAB17-10",
			"name": "Figure Skating"
		}, {
			"deviceId": "IAB17-11",
			"name": "Fly Fishing"
		}, {
			"deviceId": "IAB17-12",
			"name": "Football"
		}, {
			"deviceId": "IAB17-13",
			"name": "Freshwater Fishing"
		}, {
			"deviceId": "IAB17-14",
			"name": "Game & Fish"
		}, {
			"deviceId": "IAB17-15",
			"name": "Golf"
		}, {
			"deviceId": "IAB17-16",
			"name": "Horse Racing"
		}, {
			"deviceId": "IAB17-17",
			"name": "Horses"
		}, {
			"deviceId": "IAB17-18",
			"name": "Hunting/Shooting"
		}, {
			"deviceId": "IAB17-19",
			"name": "Inline Skating"
		}, {
			"deviceId": "IAB17-20",
			"name": "Martial Arts"
		}, {
			"deviceId": "IAB17-21",
			"name": "Mountain Biking"
		}, {
			"deviceId": "IAB17-22",
			"name": "NASCAR Racing"
		}, {
			"deviceId": "IAB17-23",
			"name": "Olympics"
		}, {
			"deviceId": "IAB17-24",
			"name": "Paintball"
		}, {
			"deviceId": "IAB17-25",
			"name": "Power & Motorcycles"
		}, {
			"deviceId": "IAB17-26",
			"name": "Pro Basketball"
		}, {
			"deviceId": "IAB17-27",
			"name": "Pro Ice Hockey"
		}, {
			"deviceId": "IAB17-28",
			"name": "Rodeo"
		}, {
			"deviceId": "IAB17-29",
			"name": "Rugby"
		}, {
			"deviceId": "IAB17-30",
			"name": "Running/Jogging"
		}, {
			"deviceId": "IAB17-31",
			"name": "Sailing"
		}, {
			"deviceId": "IAB17-32",
			"name": "Saltwater Fishing"
		}, {
			"deviceId": "IAB17-33",
			"name": "Scuba Diving"
		}, {
			"deviceId": "IAB17-34",
			"name": "Skateboarding"
		}, {
			"deviceId": "IAB17-35",
			"name": "Skiing"
		}, {
			"deviceId": "IAB17-36",
			"name": "Snowboarding"
		}, {
			"deviceId": "IAB17-37",
			"name": "Surfing/Bodyboarding"
		}, {
			"deviceId": "IAB17-38",
			"name": "Swimming"
		}, {
			"deviceId": "IAB17-39",
			"name": "Table Tennis/Ping-Pong"
		}, {
			"deviceId": "IAB17-40",
			"name": "Tennis"
		}, {
			"deviceId": "IAB17-41",
			"name": "Volleyball"
		}, {
			"deviceId": "IAB17-42",
			"name": "Walking"
		}, {
			"deviceId": "IAB17-43",
			"name": "Waterski/Wakeboard"
		}, {
			"deviceId": "IAB17-44",
			"name": "World Soccer"
		}]
	}, {
		"deviceId": "IAB18",
		"name": "Style & Fashion",
		"children": [{
			"deviceId": "IAB18-1",
			"name": "Beauty"
		}, {
			"deviceId": "IAB18-2",
			"name": "Body Art"
		}, {
			"deviceId": "IAB18-3",
			"name": "Fashion"
		}, {
			"deviceId": "IAB18-4",
			"name": "Jewelry"
		}, {
			"deviceId": "IAB18-5",
			"name": "Clothing"
		}, {
			"deviceId": "IAB18-6",
			"name": "Accessories"
		}]
	}, {
		"deviceId": "IAB19",
		"name": "Technology & Computing",
		"children": [{
			"deviceId": "IAB19-1",
			"name": "3-D Graphics"
		}, {
			"deviceId": "IAB19-2",
			"name": "Animation"
		}, {
			"deviceId": "IAB19-3",
			"name": "Antivirus Software"
		}, {
			"deviceId": "IAB19-4",
			"name": "C/C++"
		}, {
			"deviceId": "IAB19-5",
			"name": "Cameras & Camcorders"
		}, {
			"deviceId": "IAB19-6",
			"name": "Cell Phones"
		}, {
			"deviceId": "IAB19-7",
			"name": "Computer Certification"
		}, {
			"deviceId": "IAB19-8",
			"name": "Computer Networking"
		}, {
			"deviceId": "IAB19-9",
			"name": "Computer Peripherals"
		}, {
			"deviceId": "IAB19-10",
			"name": "Computer Reviews"
		}, {
			"deviceId": "IAB19-11",
			"name": "Data Centers"
		}, {
			"deviceId": "IAB19-12",
			"name": "Databases"
		}, {
			"deviceId": "IAB19-13",
			"name": "Desktop Publishing"
		}, {
			"deviceId": "IAB19-14",
			"name": "Desktop Video"
		}, {
			"deviceId": "IAB19-15",
			"name": "Email"
		}, {
			"deviceId": "IAB19-16",
			"name": "Graphics Software"
		}, {
			"deviceId": "IAB19-17",
			"name": "Home Video/DVD"
		}, {
			"deviceId": "IAB19-18",
			"name": "Internet Technology"
		}, {
			"deviceId": "IAB19-19",
			"name": "Java"
		}, {
			"deviceId": "IAB19-20",
			"name": "JavaScript"
		}, {
			"deviceId": "IAB19-21",
			"name": "Mac Support"
		}, {
			"deviceId": "IAB19-22",
			"name": "MP3/MIDI"
		}, {
			"deviceId": "IAB19-23",
			"name": "Net Conferencing"
		}, {
			"deviceId": "IAB19-24",
			"name": "Net for Beginners"
		}, {
			"deviceId": "IAB19-25",
			"name": "Network Security"
		}, {
			"deviceId": "IAB19-26",
			"name": "Palmtops/PDAs"
		}, {
			"deviceId": "IAB19-27",
			"name": "PC Support"
		}, {
			"deviceId": "IAB19-28",
			"name": "Portable"
		}, {
			"deviceId": "IAB19-29",
			"name": "Entertainment"
		}, {
			"deviceId": "IAB19-30",
			"name": "Shareware/Freeware"
		}, {
			"deviceId": "IAB19-31",
			"name": "Unix"
		}, {
			"deviceId": "IAB19-32",
			"name": "Visual Basic"
		}, {
			"deviceId": "IAB19-33",
			"name": "Web Clip Art"
		}, {
			"deviceId": "IAB19-34",
			"name": "Web Design/HTML"
		}, {
			"deviceId": "IAB19-35",
			"name": "Web Search"
		}, {
			"deviceId": "IAB19-36",
			"name": "Windows"
		}]
	}, {
		"deviceId": "IAB20",
		"name": "Travel",
		"children": [{
			"deviceId": "IAB20-1",
			"name": "Adventure Travel"
		}, {
			"deviceId": "IAB20-2",
			"name": "Africa"
		}, {
			"deviceId": "IAB20-3",
			"name": "Air Travel"
		}, {
			"deviceId": "IAB20-4",
			"name": "Australia & New Zealand"
		}, {
			"deviceId": "IAB20-5",
			"name": "Bed & Breakfasts"
		}, {
			"deviceId": "IAB20-6",
			"name": "Budget Travel"
		}, {
			"deviceId": "IAB20-7",
			"name": "Business Travel"
		}, {
			"deviceId": "IAB20-8",
			"name": "By US Locale"
		}, {
			"deviceId": "IAB20-9",
			"name": "Camping"
		}, {
			"deviceId": "IAB20-10",
			"name": "Canada"
		}, {
			"deviceId": "IAB20-11",
			"name": "Caribbean"
		}, {
			"deviceId": "IAB20-12",
			"name": "Cruises"
		}, {
			"deviceId": "IAB20-13",
			"name": "Eastern Europe"
		}, {
			"deviceId": "IAB20-14",
			"name": "Europe"
		}, {
			"deviceId": "IAB20-15",
			"name": "France"
		}, {
			"deviceId": "IAB20-16",
			"name": "Greece"
		}, {
			"deviceId": "IAB20-17",
			"name": "Honeymoons/Getaways"
		}, {
			"deviceId": "IAB20-18",
			"name": "Hotels"
		}, {
			"deviceId": "IAB20-19",
			"name": "Italy"
		}, {
			"deviceId": "IAB20-20",
			"name": "Japan"
		}, {
			"deviceId": "IAB20-21",
			"name": "Mexico & Central America"
		}, {
			"deviceId": "IAB20-22",
			"name": "National Parks"
		}, {
			"deviceId": "IAB20-23",
			"name": "South America"
		}, {
			"deviceId": "IAB20-24",
			"name": "Spas"
		}, {
			"deviceId": "IAB20-25",
			"name": "Theme Parks"
		}, {
			"deviceId": "IAB20-26",
			"name": "Traveling with Kids"
		}, {
			"deviceId": "IAB20-27",
			"name": "United Kingdom"
		}]
	}, {
		"deviceId": "IAB21",
		"name": "Real Estate",
		"children": [{
			"deviceId": "IAB21-1",
			"name": "Apartments"
		}, {
			"deviceId": "IAB21-2",
			"name": "Architects"
		}, {
			"deviceId": "IAB21-3",
			"name": "Buying/Selling Homes"
		}]
	}, {
		"deviceId": "IAB22",
		"name": "Shopping",
		"children": [{
			"deviceId": "IAB22-1",
			"name": "Contests & Freebies"
		}, {
			"deviceId": "IAB22-2",
			"name": "Couponing"
		}, {
			"deviceId": "IAB22-3",
			"name": "Comparison"
		}, {
			"deviceId": "IAB22-4",
			"name": "Engines"
		}]
	}, {
		"deviceId": "IAB23",
		"name": "Religion & Spirituality",
		"children": [{
			"deviceId": "IAB23-1",
			"name": "Alternative Religions"
		}, {
			"deviceId": "IAB23-2",
			"name": "Atheism/Agnosticism"
		}, {
			"deviceId": "IAB23-3",
			"name": "Buddhism"
		}, {
			"deviceId": "IAB23-4",
			"name": "Catholicism"
		}, {
			"deviceId": "IAB23-5",
			"name": "Christianity"
		}, {
			"deviceId": "IAB23-6",
			"name": "Hinduism"
		}, {
			"deviceId": "IAB23-7",
			"name": "Islam"
		}, {
			"deviceId": "IAB23-8",
			"name": "Judaism"
		}, {
			"deviceId": "IAB23-9",
			"name": "Latter-Day Saints"
		}, {
			"deviceId": "IAB23-10",
			"name": "Pagan/Wiccan"
		}]
	}, {
		"deviceId": "IAB26",
		"name": "Illegal Content",
		"children": [{
			"deviceId": "IAB26-1",
			"name": "Illegal Content"
		}, {
			"deviceId": "IAB26-2",
			"name": "Warez"
		}, {
			"deviceId": "IAB26-3",
			"name": "Spyware/Malware"
		}, {
			"deviceId": "IAB26-4",
			"name": "Copyright Infringement"
		}]
	}]
}

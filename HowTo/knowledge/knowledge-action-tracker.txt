Dstillery

what are action trakcers for campaigns and target groups

Event Trackers (previously Action Trackers)
Skip to end of metadata
Created by Joshua Stewart, last modified on May 11, 2016 Go to start of metadata
Event trackers are the pixels (or data seeds) we attribute to site visits and conversions for any given campaign; used to optimize a campaign, as well as track performance internally
Setup
Campaign Type
Auto-Create Campaigns: Event trackers are assigned at campaign level
Manual Launch Type: Event trackers are assigned at target group (spend) level
Strategy
Assign the conversion event as the conversion event tracker no matter the size
It is important for the site visit pixel to be relatively large. Best to have at least 1k loads/day. If not this size, add other relevant pixels from the marketer's page in order to surpass 1k/day. Use the largest amount possible if none are this large but be aware that it might cause issues
The reason the Site Visit must be large is because 1/100 of every site visit are added to the conversion data so this data must be large enough to break ties in the conversion data in the event that conversion data is not large enough to optimize

Optimization

Check Viability of Event Trackers
After running three weeks make sure event trackers are set up optimally
Here’s how to check:
Log in to the Data Science Dashboard: http://datasci.dstillery.com/campaigns
Select campaign in Campaign Index, go to Spend tab
Click “show/hide columns”, add PV SV and PV Buy
Most of the spends delivering a majority of the impressions should have at least 5 site visits or conversions
The data shown here is number of post view site visits and post view buys for the last 28 days
This is the same data used by Optimus Prime
If most spends don’t have 5 SV or Conversions, consider adding more pixels as site visits

Pro Optimization Tip:
For campaigns with small amounts of conversions, change event trackers from "Site Visit" to "Buyer" as long as they are beyond halfway down the site funnel toward conversion. For campaigns with few conversions events, Optimus relies heavily on Site Visit data. It would be better to move the "lower funnel" site visits to the Buyer side so Optimus knows which site visits are the most important rather than treating them all the same.

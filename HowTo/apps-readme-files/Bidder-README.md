
#How to Run Bidder

run this command : 
    bash src/main/resources/runAppBidder.sh build  or
    bash src/main/resources/runAppBidder.sh

check which port is taken :   lsof -i -P -n | grep 9100

then run all the dependent microservices :

1. You need the DataMaster to be running
2. You need the AdServer to be running
#How to find top processes by cpu and memory usage
ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head

#How to rsync over pipeline and copy data  (this is for dstillery job)
rsync -ae 'ssh -A mtaabodi@login.pipelane.net ssh -A' mtaabodi@ala1002.la.pl.pvt:/var/data/geo-data/* .

#make sure you increase the limit of open files on Bidder Server
ulimit -n 90000

# how to get number of files open
lsof | wc -l


### how many file descriptors can we open?
    cat /proc/sys/fs/file-max
## Use below command to find out how many file descriptors are currently being utilized:
    more /proc/sys/fs/file-nr

#Bidder
dependent micro services :
AdServer

###Dashboard
dashboard is located at : http://67.205.132.35:8141/dashboard/

grafana dashboard : http://67.205.161.25:3100/dashboard/db/bidder

#How to easily commit different projects

cd ActionRecorder/ && git commit -am "fixing" && git push && cd ..

# How to run All the bidder tests
/home/vagrant/workspace/Bidder/src/test/cpp/Cmake/BidderUnitTests.out

# How to run All the common tests
/home/vagrant/workspace/Common/src/ObjectModels/Tests/Cmake/CommonTests.out
#How to Run Bidder


how to query for the counts in bidder :
mysql -h 127.0.0.1 -u root -p
mahin


DIFFERENT SPECS
Rubicon Spec: http://kb.rubiconproject.com/index.php/RTB/DSP_Best_Practices_For_DealID
http://kb.rubiconproject.com/index.php/RTB/OpenRTB#PMP_Object
Username: tony@dstillery.com
Password: Cougar99

Google Spec: https://developers.google.com/ad-exchange/rtb/downloads/realtime-bidding-proto

OpenRTB Spec 2.3: https://github.com/google/openrtb

AppNexus Spec (by request) 2.4: https://wiki.appnexus.com/pages/viewpage.action?spaceKey=adnexusdocumentation&title=OpenRTB+2.4+Bidding+Protocol
Username: bidder_prospect_july16
Password: peony
PulsePoint spec: http://docs.pulsepoint.com/display/BUYER/PulsePoint+RTB+API

Regards,
Mahmoud Taabodi
Things to do in future
*)very important, add win rate % metrics , if its too low, for an exchange, we throttle the bids

*) very important,
add specific featuresRequired fields for target groups so that we don't do RON when something is wrong with the system. and data.

*)add creativeType and adType,(as arrays) to make sure that
right creatives are being selected for banner, mobile and video

*)advertiser cost calculation and pacing by adveriserCost

*) conversion recording and optimizing the spend creatives for conversion

*)write a module in bidder to bid higher for the devices that have clicks and conversions in their adInteractions

*) convert lat and lon to mgprs 100 by 100 or 10 by 10 and store it in cassandra and build models based on that

*) put 1 percent of bids in cassandra and add report jobs in spark. to see what your bidding on

*) add bidder face app that calls the bidder, so we can take the bidder down without disrupting exchanges

*) find bad geo data using spark in cassandra and black list those devices (if they have moved more than speed of sound , or if they exist in the same lat lon more than 100 times in a day for a deviceId)

*) write a metricCompressor

*)read the crosswalkedDevices for a device in scoring and write the segments for them too.(with crosswalked description- to differentiate them in reporting)
*)read crosswalkedDevices for a device in bidder and check the crosswalked segments for that device too
*)add IabSubCategory modeling feature

*)implement ip counting based last seen filter in bidder (using aerospike)
*)implement the fraud deviceId Service using spark
*)implement good pattern device deviceId service using spark
*)implement mobile whitelist feature

*)add spark job that
parse deal for openrtb23, continue target filter for deal ,  
add device segment support in cache using aerospike and ad history and ip device deviceIp map
*) add strict UI validations for models
* * )implement the reading of crosswalked devices from cache and scoring them too, when you are scoring a device, and in bidder, read the crosswalked devices and their segments too when reading a segment.

* )have a set of always passing target groups in bidder to be able to test the adserver and pacer functionality

*) add full time and the sample part to the deviceIds so can sample them.

*) add a queue to randomUtil, so that you read the premade random numbers from a queue, it speeds up things faster.

*) add pattern extractor interface in scorer, and find users with different patters, based on the time of the feature history, and geo history and add them to segments...like isNewsJunkie? IsBrowsingNews? isSportJunkie?
isFoodie? isHungryNow? isLookingForClothes? and blah blah, and add these patterns to segments table..

*) add device make and model detection from bid requests based on 51 degrees

*)improve the datarequest handler to return only the modified data, so that we can reload the data every minute...
and send a notifiction on what was reloaded.....using entityStatus

*) make all apps dependent on the metricHealthCheck and if anything is wrong, they have to stop working specially bidding....

*) add logs of event after bid and impression and add them to hive..

send batch to scorer- more models type building - better cut off estimatoradd GelocationCache for code in MySqlGeoLocationService
same for this MySqlGeoSegmentListService and MySqlInventoryService

featureDeviceHistory only for segment seeds and other whitlisted domains, and limit per day

DeviceFeatureHistory should have a limit on size per day and if exceeds should have badDeviceLists

ipDevice and DeviceIps should have a limit and if too many  should have bad Ip and bad DeviceLists
add deal discovery feature to UI and backend :
A deal discovered can have,
name, description, available impressions , bid floor, type(first party , third party), publisher name, and etc...

you get it from exchanges and show it in UI and users can select and add a client deal.
